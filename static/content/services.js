let data = [
    {
        title: 'LIGHTING MASTER PLAN DESIGN',
        slug: 'lighting-master-plan-design',
        text: 'Lighting Master Plan Design is our commitment to creating dynamic and environmentally conscious solutions, turning ordinary spaces into brilliant showcases that blend practicality with visual allure.',
        detail_text: 'LIGHTING MASTER PLAN. IS THE TESTAMENT OF OUR DESIRE TO CREATE RELEVANT IDENTITY OF ENVIRONMENT THAT ENCOURAGE HUMAN INTERACTION AT NIGHT. LIGHTING MASTER PLAN IS THE LIGHTING PLANNING FOR PEOPLE TO COMMUNITY TO URBAN ENVIRONMENT OF WHICH CAN BE FITTED FOR VARIOUS SOCIETIES, CULTURES AND GENERATIONS. BECAUSE HUMAN ALWAYS EVOLVE, SO OUR PERCEPTION DOES SO THE SAME..',
        image: 'https://previews.dropbox.com/p/thumb/ACJWrNxArYn0lmLN2eCFyohdXr2mg62CWNWW95Uf4huWwBChodLD2H_FCdXpGxDhlnsMnPd5TdnCThKjQGbkLSq3rKQ9uTJi1ZvCvZ1TCTzCSUYcQRZSP2ZUU-9nP_N35t1bD5BfjLcjyY5Wf7pNzlXwCGNdIx6SRR3VjR3clO9IEtFSvtNDcLaPtxCBOpzTjbhW0pUeyrEbjnRUV3tjZtLe59w9OB6wqt8u84QupnozIn6P1tBkRjYsCNbXJBrrPF-lUYQ6Kied-0M_HZLUESpcPLrIP521_DxjbjgZ-XccTCcbX-4aeVUCSAdcuUgp4EhZ-TFj9crfKovH07zDDIOb/p.png'
    },
    {
        title: 'LIGHTING STRATEGY DESIGN FOR RETAIL',
        slug: 'lighting-strategy-design-for-retail',
        text: 'INTEGRATING LIGHTING ELEMENTS, WE AIM TO ENHANCE THE VISIBILITY OF MERCHANDISE, ACCENTUATE KEY FOCAL POINTS, AND CREATE AN INVITING ATMOSPHERE. OUR STRATEGY NOT ONLY FOCUSES ON AESTHETICS BUT ALSO PRIORITIZES ENERGY-EFFICIENT SOLUTIONS, AND SMART CONTROLS TO OPTIMIZE OPERATIONAL COSTS.',
        detail_text: 'Retail lighting is the business of visual attraction and experience. By design of retail lighting is the key to differentiate brands. Visual focal points designed to not only accentuate merchandise but also to create attractive atmosphere. Our lighting design to strategically plan priority of space besides strategy of visualization merchandise. Our design approach in energy efficiency solution using smart retail control to minimise cost and optimised profits to each store.',
        image: 'https://previews.dropbox.com/p/thumb/ACLJG6MAs7Ttv6VQJErnd5D-4bCXuImhQJ3p_gHoQkejBKEBvXA1yeiuzX2Ui6ETjJc_hR3vpbkViur_WBhnRXO4DXzGjBl_zcdwH3pK4Cdn5e-cb07SPU1A6f3R-gb1xFRWOgFLvp_Ub-MrnZ4bD807CnBrAof11Beq8_K2g5A0VhBOg_CpRlKP2kqF0sOHTDIV3WifxIZpQ4NEba9O1S1qam43f2Ym4yX8lySfJUuEME-dHQ3W13R-rXIiYcQ0naVsU6aAlqAiN1QKTcciaacrRrHDGZo4UPzMU-5HwJ1J_wk97rK-EXWhbImJvIv7kL_Q-3caTg8oc5lVyudIczLf0dyI65-vVjwLz_1iUXDnFA/p.jpeg'
    },
    {
        title: 'LIGHT FIXTURE PRODUCT DESIGN',
        slug: 'light-fixture-product-design',
        text: 'Light Fixture Product Design represents the symbol of innovation and functionality, combining aesthetics with cutting-edge technology. Whether for residential, commercial, or industrial settings.',
        detail_text: 'Products design is the combination of function, innovation, form and technology in one single object to serve human needs. Our lighting product design service derives from years of collective experience in lighting industry. We are not only design the product but ensure the needs of those products in the market. Good products shall perform well in both technology and economical aspects whether for commercial, residential or industrial realm.',
        image: 'https://previews.dropbox.com/p/thumb/ACKjVBsPaRWuI6c8PKpOFIf7YF76Tr0z72e3aiL4S65N_fVPUTZwXP9JW969KxzxApspcfibfzN8IYWHgU6jmx5XiJ0nuteIEVOBT6UnkKmBpTIiopEj9ufaoCAjp_cW37raDfFeNiQNwi_ICBVRXifVD64jp73_k3JcXwK46jPzHscB8zQVJXFij07XnNRGKBS7j3UgDsNbAX1IJeCOceVoHq_01LcUscaKdpQv_VEf4nPYik4MkgUq1kEbLGXBlFqZJAqM8WOz3CVDUewGcuAmYSulk1uL0hgC3KggC5YYYei1sgGMOW7jAkFB5YUCS810CfKRF5RrutujMm5Kjf6B/p.png'
    },
    {
        title: 'DECORATIVE LIGHT FIXTURE DESIGN',
        slug: 'Decorative-light-fixture-design',
        text: 'utilizing premium materials and finishes to ensure durability and longevity. Beyond visual allure, these fixtures are engineered to provide a warm and inviting ambiance, creating a captivating atmosphere.',
        detail_text: 'Decorative light fixture is the form if expression through execptional craftmanships and technical skill of utilizing materials. Beyond the visual feast that decorative lighting can offer to architectural spaces, it express the engineering methods that could become expression of space it dwell.',
        image: 'https://previews.dropbox.com/p/thumb/ACIZ3IOWiag0zquhihQRb2Cklb-lVDbuVRRYjfsDGQsOU6dZICK_456EcsNJmine95bCM4DFCP7ZWQbYE3EMCvFWskM_FDUwxso811Q7tWYmWCnGwvFSq_ERsyqLEolQO_YbpdCUF79-z0J_ke8PxU-rkmZqhDH_iEwvIugcJ3E13sLRkW2PpA_RsYjtySZTIjZ4tT2J9jAiIPlQXFvHWNHLmQQaGzIVN5bFY2z2VhEPQFgDST0lbNi8MMBztEqVmlAy6mIRvmphDXISpfVZkZU4Y-OfXnzxjjwrH4VXJduV8sh_D0qhmNnN32FvnK3Vzpo-AkHNmin6--DOZ6eFkFRe/p.jpeg'
    },
    {
    title: 'LIGHTING ECOSYSTEM DESIGN',
        slug: 'lighting-ecosystem-design',
        text: 'The holistic intelligent approach to factor of utility, efficiency and sustainability by means of technology to curate lighting ecosystem solutiin that adapts to evolving needs of human.',
        detail_text: 'Lighting Ecosystem Design is a holistic and intelligent approach to factors such as energy efficiency, sustainability, and user experience, utilizing cutting-edge technologies to curate an ecosystem that adapts to the evolving needs of modern space.',
        image: 'https://previews.dropbox.com/p/thumb/ACIppZj4DVenp8AKRswmBLhCSLeIqVz3dlGfhb-QOqcgXy_c6lm3PFs7UoKgtq1-1I2pr7FEYkGBb_mfGDXnnjTubFpMPKX3R7hb8x-UDeCixTpCRllubdQgXOUe89jvKOyFaH5dkRlBTz2Z0vFhgxB_PYuaeyunFbtXAsZ2uU2eoMm7R0ahwCw68G_Ofhpt0y3D_nWO5BZWSPmGQTRi2xyKdh5EjGsl3JHa-XwYqWFIX0TpWCdhFwXEWQDrj4S2_tIbxNGOB7_KvCLidJHvT7X85WrA2ClOkHGHiy-Wh0pV2QcbwqrTXavftvCg_EY2LOxx9TNd88T01FnBjBNlEw-t3DXGBvMzWHYHqIwsGijYng/p.png'
    }


]

module.exports = data