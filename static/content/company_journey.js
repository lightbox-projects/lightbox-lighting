let data = [
    { year: '2004', text: 'LIGHTBOX WAS ESTABLISED IN SINGAPORE' },
    { year: '2006', text: 'ENTERED INTO INDIA MARKET AND OPENED 1ST THAILAND OFFICE' },
    { year: '2008', text: 'WORLD ECONOMIC CRISIS. CLOSED OUR THAILAND OFFICE' },
    { year: '2010', text: 'LIGHTBOX SINGAPORE OFFICE MOVED FROM KANDAHAR TO KEWALRAM STREET' },
    { year: '2013', text: 'LIGHTBOX INDONESIA WAS ESTABLISHED' },
    { year: '2014', text: 'REOPENED LIGHTBOX THAILAND & COMPANY TRIP TO BALI' },
    // { year: '2016', text: 'SPARK AND LBX PROJECTS. COMPANY TRIP TO SRI LANGKA' },
    { year: '2017', text: 'VIETNAM OFFICE WAS ESTABLISHED' },
    { year: '2019', text: 'Lightbox 15th Anniversary <br/> Company Trip to Pyaw Nyet San, Myanmar' },
    { year: '2024', text: 'Lightbox 20th Anniversary <br/> Website Rebranding' },
]

module.exports = data