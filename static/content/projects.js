let data = [
  {
    code: '14',
    slug: 'bmw-showroom',
    title: 'BMW Showroom',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://i.pinimg.com/736x/98/0d/c5/980dc5b96adbe8cde8bb113e0d7f0868.jpg',
    category: 'COMMERCIAL',
    text: `
        <div className="">
          <h2>Project Description</h2>
          <p> Located along Alexandra Road, BMW's new showroom design direction is a sleek and contemporary facade. Clean lines of LED light speeds across the curtain glass facade. White lines run horizontally across the grey curtain glass; cutting the facade at varied intervals and speed. The lighting design offers a new edge of lighting solution to enhance and complement the new architecture of the building.</p>
        </div>

      `,
    image_collections: [
      'https://i.pinimg.com/736x/98/0d/c5/980dc5b96adbe8cde8bb113e0d7f0868.jpg',
      'https://i.pinimg.com/736x/98/0d/c5/980dc5b96adbe8cde8bb113e0d7f0868.jpg',
      'https://i.pinimg.com/736x/98/0d/c5/980dc5b96adbe8cde8bb113e0d7f0868.jpg',
    ],
    featured: false
  },
  {
    code: '20',
    slug: 'cecil-street-tower-building',
    title: 'Cecil Street Tower Building',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2020',
    image: 'https://previews.dropbox.com/p/thumb/ACKFxMlX_o9aFctd17HrTBNlohDCQC8NN-LuUlkYbbHy5rhNimNLxJ1iad1JBOpcmtajAbKOH6tuk6MOkDzCl0TVRiwpen969krNS4BH-WeDkXJh6GlDljfGuryT3qK9SAN2RAbVGO6S3EvnJdZTTz-CvmnSZ6fu2JxwG8tC_7xMaJ69qOaB24gvaZDJ89B4iD34X4CQjInIZBwCPV0b8eAxZgt0gJYoROTI0RTx5wVKelQBAZOUx_X1EOIy2DXh5EQelQ9aRdrw7uylAgwwPFrPiydEX_RODprNONb-CmHuc3HO8o4lrc6QF74XDhZJl11irQYQ-WbOPiKt4o1VstNl/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The unique architectural design office tower in the center of Singapore business district. The form was inspired by the Thai gesture of Respect and light has been designed to enhance the uncovering character at night.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLhX3j_0R1G-yOyiOv1CLDsTHbH9B-URJJl0nz1Cbs9kdiczaVb5vNil9sNRkD-cad-YcMu1F4Vukie3pbUhrMDkee3bts4fbmffdHmd7g8HoX2Y1R5EOCi-EMwCyHFGwjyv_SO5kJksMHTdmeTJBwz9zvyuI_-0cW7WwkBDg-ORFtaPKKHSCCzE6dDDscem6xPefQ9bJcwkMx9n8z2834aRhUsR4b_1wwQxT6aUUuhwQ16Z_QD81O0hvCBQO5UPb_eIe7DOMk6mqCrKKPFY1ICqs5FYdU_NaAtNfvF88uoTVrW4dj8WSvEObT-QZWgXv-ZJPVDSIC7MGn2F1bA4FCyGVBeiX4KyFtAG0eNrI1fnw/p.png',
      'https://previews.dropbox.com/p/thumb/ACIYuPvtodJhWUXWqwfDcqUbStVJe5_vL50oHQYb463zt6SvygN6opB4uJ9OTBBGdi0lF5JBuhFE-0TVskYKPEKgJMGs8aNVNyf1nPdVS4ioC6U4knmgDYmX3QHDBx87tGTgwbHK2Y8O8BkzCvg3lP6UA4zYKoQLGbYYze97ofLm4Mky0l7Uu99jirhY_uDdbdzLdZoqiowBxr8OSjk_h9m4xhoNGM2ILR9xvWJrOEa7p_ttisiKRwfPxBGXexnNg3CeWTEjYFWDsplA52ui3zD1Fondc2G7ob3MdT19lzWlML1ZD2MVmXvpfKWR6G4PhDUucZhUJzO4iubty1pw28ZvYrCR5I-jiI2Wq6zWceFyQQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACLBHEhHyi7uI3GoJDJDMeVHRHk65ouN4rxTCxBJ36usYvnMPiGOugeE_UwspDB449-tEeSgcY1bIKrRNviY7WHUZiq4LJ9OMo9yd8Ep2dJk6NLQHiueyYZ2IfjRQWVY2OMSxno-kATnT1nn_ecfTJLj-0xTZXFJ742NZkVWQ1pbnI2cgFL9xh4Y9FNYomQrM0HN6zRqayMOPtknHdP-t3lhIWz7DNq-8uGEvZdx25sSKxnihnRavcj4qc7SWh27kG4TFiDU80RkyAPtgHUox7dwCkSJ6MElkjndJcco0WevHM_nQp56eUH0VeOl9NyJwfLm8a5gQcwZx1H6mORgy3s_/p.png',
      'https://previews.dropbox.com/p/thumb/ACKFxMlX_o9aFctd17HrTBNlohDCQC8NN-LuUlkYbbHy5rhNimNLxJ1iad1JBOpcmtajAbKOH6tuk6MOkDzCl0TVRiwpen969krNS4BH-WeDkXJh6GlDljfGuryT3qK9SAN2RAbVGO6S3EvnJdZTTz-CvmnSZ6fu2JxwG8tC_7xMaJ69qOaB24gvaZDJ89B4iD34X4CQjInIZBwCPV0b8eAxZgt0gJYoROTI0RTx5wVKelQBAZOUx_X1EOIy2DXh5EQelQ9aRdrw7uylAgwwPFrPiydEX_RODprNONb-CmHuc3HO8o4lrc6QF74XDhZJl11irQYQ-WbOPiKt4o1VstNl/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'central-soho',
    title: 'Central Soho',
    location: 'Singapore',
    scope: 'Facade/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJ5v7oQ26EY0JRcI9d4v4fH38JOyRkoSL23sgIh48RRITimG6z7IoFk9jfbTQUg19zwSYJ4QOD73wKYrt2fdTTTBTHmF0Wzj96kBcNe4T0FAqtxux7In9trW8k_FCMwrVowCuMUtVxbPUWy_2McByqe5nlnjI9hJSYmtUljZMd5kuUAPllqHU2hgZlrQtc51v8xKHji0_RuhPzplubsrndPuIGWRCEc1eykbXdl7bfensz7YRn3BxGsh_8ow92lxLUFAK-Z2iS9FzL2RYmlhGXeEAiCLfqVcE6WNwgomarf6rFBJ0N-oDoVm5OCo8LlpX6ygv8QkI4rmqNUP8CAVsJ8/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p>Central SOHO, a building in the midst of the Singapore River: its promenade faces the vibrant and lively Clarke Quay which bursts with life at night with many pubs and jazz bars dotting its pier. Central SOHO is a multi-usage building which caters to residential, commercial and retail activities. It is a remarkable architecture that expresses the lives of the new generation. The lighting adopted for the architecture needed to be sensitive to the extensive and highly diverse applications compounded within this structure. Located near the river, the lighting is mainly kept at blue tones to highlight the main architectural elements, and whites for the dwelling spaces. Blue also defines the location as the building sits along the water banks, and is significantly different from the varied and busy lighting from the neighbouring outlets. Lighting for Central SOHO allows it to integrate and interplay into this urban context, defining its architecture on the river banks.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJBfBo1FpiPu3q8PtHRrj8hfObZH3wfSpRAsx7GiDc9hApHVwC2_0pg6Eo_5ztOiQjy6FRc-NKxlQLtoD9E-6fcxHA4uvxg4QtZGksZkeg6wWrm3ZQos_54doxEjRt9_8qiUwfMWwhAUd9-URt2XbqCakxpKFDjs2Zubwu7CW7OkW72tFdFCtk6fcVyys_GgvXZoV1E8Fg-dG4-qUsu6ByZvo92qeqzSjHVuYlh4tzwUSZr3NHZ8AnkzI2XfI4vKsfe_mG51sL-AXbJ2DRjgoX9vpODTPZ2TpYijVHABfPVVXOeNeNf6GmC06BzfWEZPGwu3Bxc8D1eetMVzhaxxO1Z/p.png',
      'https://previews.dropbox.com/p/thumb/ACLZLxaik7J0DrTiovwgheemDHGtiM3xiuH6GpzKh-SGmzX3MvXp39lBu64moN14vAEUcEmg-1-FXvA-SOY0Wf3QXPJromjCxIhZDTOlJhfChOaCeEaJvH6In80fbtYbl64JAjCojEg2x9eAOvdeEsARd-WiMze9hLZnC0Eg1hTURhbowkqrKbtPCjUDSQTTFsb2hIHZC1wH_H_6GpVERBH4mBSCLRrP-cf6LvlN0x2BsjbSH8KZskOBD3yx7m9cyQj6bJMnwivLre-Njlp25P3NGzUw1RVXX6De-DfGmJFO0VF7uUa4PsBhOd92zD0Boy3IXxqyj2YmQQanzYtrxEPS/p.png',
      'https://previews.dropbox.com/p/thumb/ACJLPRJpGZA_2Wu51ZK8A5PHsgGoxS6bU1A1np0tVGU8EzPljoEJXeRCknwPNLMhe-Q2nuMcCgaxh7zE5R_ivCmRMwwZbaMisRMUyJai41d17nFDeMw8h3WQ9N6oWlON41-woCdbQnkTf5N0kX13q1FATSeugBEs4tSXUUi_Q4q-xkmy6HEdBHnWO_nHGZJjjK3bpLEIBDKWnIS9zuBbHpzo-LYhR9zhVbjtJ5rJ49r840ApjDYsAhj3PE84pkjCmiayorUPWlbFE4qK4hPyxTf6MgYZ_85darqsR8DavxtAjjzf8-l23GFlJTQYWXv8PFnGsqZSOaI6fdqiKU0xa6fjTNL4XQsTscmuFtqmNCZD-g/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ5v7oQ26EY0JRcI9d4v4fH38JOyRkoSL23sgIh48RRITimG6z7IoFk9jfbTQUg19zwSYJ4QOD73wKYrt2fdTTTBTHmF0Wzj96kBcNe4T0FAqtxux7In9trW8k_FCMwrVowCuMUtVxbPUWy_2McByqe5nlnjI9hJSYmtUljZMd5kuUAPllqHU2hgZlrQtc51v8xKHji0_RuhPzplubsrndPuIGWRCEc1eykbXdl7bfensz7YRn3BxGsh_8ow92lxLUFAK-Z2iS9FzL2RYmlhGXeEAiCLfqVcE6WNwgomarf6rFBJ0N-oDoVm5OCo8LlpX6ygv8QkI4rmqNUP8CAVsJ8/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'g-land-tower',
    title: 'G Land Tower',
    location: 'Thailand',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACK6dH6c_XAWFjG0vI5R-N4NLTYLkYSPal3lH4t3kcwfCZzxcvWWLtED98IdcnEL1xJzVhEgJOT4mslEfM98PNhoicSnejS31a-DJX2McUg3JH45ic6Yapy7S6S8w3KigU4yQUjlhQmjtZmbKWlr_VuQ6H6cPfMZqTWtpbdD3LUm8nAyoQW0-vjhMcen4EiLKEncsBxiOCRJOOMbzWCOZVbj2DZW38DLiYMLOXwQDZTl08l7Ubxc_y9F4mYnCftZCfIFJvdoHQ-HmYBw2IJ6-KPgvQEwFqZZmmpguou1TTPSqGYWfSS5hvziKuslPyd4DINPEkAoqUFlnPcA3d88-1CM/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p>Lightbox was commissioned by the developer of the G-Land Tower project to experiment with creating three-dimensional effects on two-dimensional surfaces. The idea we hit upon was to program a complex configuration of light effects upon the facade in order to instigate an effect similar to that accomplished by a hologram. We specifically worked on protruding an illusory ball of light from the building facade and replicating the pattern of the Northern Lights. This project was initiated before capable technologies were available and it has remained in theoretical incubation- until it can be revised by another patron.
We experiment light with glass. The eventual outcome was surprisingly a simplified 3D effect through the use of resonance of lights played in rhythm and light expressed symbolism of city movement.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIVyPrhDM4K7gaKkpt9oIKO64F9HtkrtEJCqcwoDh5oJhTAH2fy-b4lPCYiOrcwQmtyseik-rqnD5DONMSkz_ojPrjQDHAAPCC2kdCLRdyub9k4Cax-hGd8gelscb7XggyJRXPj1c0Sxc0_8-esno2-yOrqBkOWTiL8xIuI1GN_EJJolNfKwwsnDetv-hBd9QGumA1Oly9RC__mk-_rnQCIT7hR6jT4iKw7b9ykMQzP1T2UQpawblk2TKt6JF7GCjwX0OOB6WEl0WmQR9ZgHA-70_dXREPYbxYjZnnUpfYchqpKk-GqSH8bAYvYjIF0H9iuo9UubbzioJZQbNqVt4FX/p.png',
      'https://previews.dropbox.com/p/thumb/ACJc1X_mqQzJxK_O3sOBd6uCIFmRX0Yer1i9c9sOK_h4TqCSOZ3W4LPhHOuGqVM6eZBjPFjgJESK7LlZj4v-RCNgOc13D2NX0ZsreJOrHtrT8EtUwiD7fdx_0gJYkYNDy22QNVifjLv27qC4IiyHgxnwWu1S4XtYowchXLjrzENaEZJ4kL0KpxZhqDMB6nmkQjnviJlxp3ZpFMPOSRLqTVhexXDkwzqu8SyqXw1QYsL-rhg0X8g5ZhAHY2QEPOITjWwnOd_BOw9GOEI7SRSFqrhcSoyfYDfw_wFQ5-6BUgiOS2QGhp2TLcXbZ8XRFdDT5FqPOTDcIMxCDs7w2ehQoa04Yu9dFaVOwfkfkX41oru8lg/p.png',
      'https://previews.dropbox.com/p/thumb/ACK6dH6c_XAWFjG0vI5R-N4NLTYLkYSPal3lH4t3kcwfCZzxcvWWLtED98IdcnEL1xJzVhEgJOT4mslEfM98PNhoicSnejS31a-DJX2McUg3JH45ic6Yapy7S6S8w3KigU4yQUjlhQmjtZmbKWlr_VuQ6H6cPfMZqTWtpbdD3LUm8nAyoQW0-vjhMcen4EiLKEncsBxiOCRJOOMbzWCOZVbj2DZW38DLiYMLOXwQDZTl08l7Ubxc_y9F4mYnCftZCfIFJvdoHQ-HmYBw2IJ6-KPgvQEwFqZZmmpguou1TTPSqGYWfSS5hvziKuslPyd4DINPEkAoqUFlnPcA3d88-1CM/p.png',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'International-plaza',
    title: 'International Plaza',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACL3vXbwHuWJmfoRMFemsNuJNhzKkJzwJbZekotel9qouADRHlFscB7umhQNFcf4UGJjEC1vLTjY5m7mQLu69jN1Sn9jvKTWSWhol38Bo4SQWACVQuQwPV35xYCn4R5JMw0dTBgXfnA5c-KET9Y7y2bwJO-LnIbpK715DGLKTE-gtDSl8fDkEUP2HQlG46MpB_LsDSHg0G9OZc_FAlNZ7gCqvEjB-tGsepU-9kouAbvAIVwBEM7_vKAlnmtP-f1mHNIO-gaAJHu3sEZnuId668TwUg1UPH_7LUtGECi00_tb7urN7F_EDXrPmQrxsOGLfwxR34SecB8aUYz7IRxBgKUk/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p>This combined office complex and shopping mall uses the versatile spacing LED lights to create a vibrant facade to the new building exterior. The light playing across the aluminum cladding offers an urban fiesta of colours and beautiful imagery. Taking advantage of the flexibility of the LED, the facade can even be transformed into a video wall relaying information to pedestrians. The lively display of light undoubtedly draws attention from the evening crowd and attracts people to the new shopping interior.</p>
      </div>

     
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJBwQrNaJbMDP1diGo5aoDGQP0DYuXceLyfjdAB8EHEeYjgyEzZBW9J5Fsb73zt3psjwmlQ4W27QRuisXAeERgWrCyRbTI1sXT1b0JYWBlavlHJ5ir9wBfctdv_Ne3ZW2e55K7fBKDkyggRb3TXDWKyvcNkv_Y-pEW9PhhGVjaeMwvdCE69E6KRNCGAv6rzdl7tEBOVXubOj6DzyNAfiooGEJ2os2Xpb0nop0qqau4rJgzsJYMJVezD5_jdRATYZSO_c-dZICNyYExQF0_pZ2p8dtWPiNV8u2jiiF5knFyP4HeFTXjXqCG1LZVCKuaOcEVwCHztcw323D8DThgmBkgP/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ6XRZOQHQkOB9fYalwvupSnkggoeGa9LHM8lxsO9zPP6CE-sfOk4lbT7mLehW_FuBjIUYfrNqK5uojLZtx2-W0B6x3FEOqdlEdMk5fzEjgYNBk4eNSUn-9kJBE9rrRW5wNtHwPksSPXAmtdzdnVeFKXxw885sNm9ghdVafCyKzxkQwDtSNzfGgu06-zM2_o9RkvFvGYwVF1Smx1Xqd6ExdmgHPNbF4Y7tzSKmLtXYr4q0AlTPsKUa0lMGTPSD8ARsaESaqv0a3OkpK_UeaLnWAuGXpFU-pqFQVZBYuposFR3jBNlWxG-MHPBs0wIxl-nkgxSHokvsxn_Dt_-BaVempMs-EVixUXXMR8DTJrXP1rw/p.png',
      'https://previews.dropbox.com/p/thumb/ACLWITZHI38w19tYdZm22p0o-bDzQ4uvwXK9Fq2R_lais4hZ8x7yg13n4551R03B1Qx42kkGDClsS437rRDDGzSHvm4MKeSktUfrfNVWfBb_R3IfdYQQHXGJOBb7m-f5Bh2NAiJ9_u8UoFvm9KyNB-rQ-J7GruX0pI-JmbksJFzAmekGfKxd3IvrjcZk9cG9h95g4drFtf_jpmMTnXdFSfdS9vORGPJHcxOqX1EpJGNL40c0oyb94-JvDkMcpWRaKfsKmM61ePRystO8WO7b-jCINCqUl_j3vrukxj1Pt6Dz-XpxvkMG30sp-ZwAVXKvwkoc9Cu8scFiWaJhj6hMZWu1/p.png',
      'https://previews.dropbox.com/p/thumb/ACL3vXbwHuWJmfoRMFemsNuJNhzKkJzwJbZekotel9qouADRHlFscB7umhQNFcf4UGJjEC1vLTjY5m7mQLu69jN1Sn9jvKTWSWhol38Bo4SQWACVQuQwPV35xYCn4R5JMw0dTBgXfnA5c-KET9Y7y2bwJO-LnIbpK715DGLKTE-gtDSl8fDkEUP2HQlG46MpB_LsDSHg0G9OZc_FAlNZ7gCqvEjB-tGsepU-9kouAbvAIVwBEM7_vKAlnmtP-f1mHNIO-gaAJHu3sEZnuId668TwUg1UPH_7LUtGECi00_tb7urN7F_EDXrPmQrxsOGLfwxR34SecB8aUYz7IRxBgKUk/p.png',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'jurong-point',
    title: 'Jurong Point',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACL_YShK0eA2nmUqZ_X-8BSxDe3Hfw2Zpue-0HCy0AARMB7Hp1fs3H6sdfrZ8N6NF5x0j7trwjZpp0m4P8pbNOtv95ffWTroOwITxdIb5pHDGBsd7ov9tTk_JM2S2ceQKiC9hE3DHYCDyDomqV_6OJ8_8N7f-oU-AYGiCL5Faf0lABC0HFzXmEFQRXM7oRsvq9eGnjXOiy6-GgpoCTDhzWzizPa-ws6V1SQ_nQKOVcdp1zXJ8MsEEbblpnkZBA3aaS5LHoIdixTTHyqR6FjitBHFpOs31cZtTZEXj0LHPuxlT6GfRFibcmAmVZFggAws_aS2ljNU04epg_63pyGqQXSod96P4uPNaYgZ_G3K4jOfsQ/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The Jurong Point Shopping Centre Extension Building is developed following the overwhelming success of Jurong Point Building 1. LED lighting is used to accentuate the evolution to clean and modern materials and this is expressed in the form of a ribbon wrapped around the building. The colours of gigantic ribbon can be changed to suit the different times of the year as a form of expression and interaction in the urban context. Similar concept is adopted in the interior design of the mall with seamless line and contemporary space being carried out into the ceiling element as discrete as possible to keep the identity of the space and architectural facade intact. Landscape lighting at the entrance of the mall is subtly lit with warm light tone so as not to steal the limelight from facade lighting which is the main theme for the Jurong Point.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLXWMfTAsRqeD3Q7WCW5wfvhL473XemLNwZYPfPDzDb58jJb0Q7fzzsKj7u2BDrKt7GTi0103A_B2dCldaeMZf5CLtHpwxh9NtlEd9owNdrfnocdchHWFo5RZ3gRGLqXLbPNZn3bAnTj4zOtzJrYzzVY1_-eFH9FJF5cwwH2GzS5eY3jCfbfYlXXIG-VDaJixxx1k4wyHMh3cNZ2NsV2nmU6n0yBJc1tVzDkwJ7BNHpi1qJYzW2arZILOxm2klb0-68QFUySwvrDlD0BKCuVSk3gY65Fs0zYhG1oGd0jy4H4r6RCtetF641YI61fAVKgLr6wM5QqX6ZRUSvveuZpFFw/p.png',
      'https://previews.dropbox.com/p/thumb/ACJB8yca5FjyA6ObiNWzYlllyZnI8kw7A6ZmJ2mfEl6TN7_8JGmwf7YBDMy08mM2XYPLQpCSuO3ClsETPyTdRfYAO6jbjYrZ9GquOohtC46cX1sMlFu4RRPNiXISTdBRoWvOyODxqm2_aZa66F69yAnv1Mz_SWFp5sD9sI4mubCN2OUeulVchMcdqZxHKAkRmsVh6sSMwSFSjUA7S4dPUm5sa8uL8sFbdIY4x8p_KYYO-_B-qT-a2fUsMa_kqeo-w1oMqb7HvVfc0s60CVzsw4ITu8vfOuiXnadrLXCcPun8g4PCnjYYg4vVNFjJ-2DhiQHRcddGV6lZDfyb9fiHy0nc/p.png',
      'https://previews.dropbox.com/p/thumb/ACJKO2I15WHb0cdkGxGgZjgXtKj3Be0t8oL252oNqu8jeepWhQycaFiy1ux3TW1g6IqQYwy5dgMYlwZiDg0HuWkjBba0wEnjagnX-XGiH_KV59IDnFCAcg3OnnsyuBU_2WfZKNMWLFwJzv8JaqYJa5mjllgwjne-Kgd4e96n86JegzW7oQ_FXyHLfAaZm3Lc92eO_3vVv5qwqGk2jx_2kFUyReXppmYmAQjv0j957vVFGPTRG2yPwJoZBvvrPUuCz9_iJTac6wGNsbEdY9k3zJWtGaNCgMd_lZOeghCL5VpsNsMfGhRezWUII8VcImxilZaaNZWVuGLUQXcit4mpeds1/p.png',
      'https://previews.dropbox.com/p/thumb/ACLN6o7GzQMp3xYgaMMN2MHVSjmQ6g-E2VCa4OFG3So2WUGFNugfNG-en7Yjk_h2c69ViIABvbN3k_oJ62VvXLYIear-BWPeNEVeE_pRC8vOz7wgDa78_xvaC6mMpMrdsAQA7XLLq4yNQCrZW5PglgDrIFtH3awiVvDY6QrekyDUsJrbaNwAK_lTQuqfbkGpURhqcs-GS_axvk-VW-pitimJMkyY2SUP9Es6ZPGSLKkfS5Juo-LuSFvZpohoCpysTeKOn652-nuKtKs85nKEcyozObbjDS_e-0pceoSVmfhg6C01ZV-NEtiPxt9QBKf57Lq_IlxBzzYRrWnKKm8M3xtz/p.png',
      'https://previews.dropbox.com/p/thumb/ACIK0z6ni71_IcGlsZp1086n_Qpnj_ozYb9xPetEV4d8s5tcd9OswGhJc3tGMWUoSweUINraPXpHYfV5FCT9XQCEonXLBmHz4nOBs3heHt7_uMb27je0IuhTOgxIG7SH1SNdZYFdNxQacxh-aISANrXtmT3y3SpLCIARSHWn0IKg9XjygJ5RDptRxjnANJmLV8ooj-11WbWQPNullvg4ba4dJBQoXd4Fft6oetR5EB-xNmQdbOD85IwCRpn6l7GWdK-1A_iAjC7xma57zwSJa-wwpHkz_FrRmXzlaA3M9ReT73UA0XU-PQq4wtXJi2Liy05eIeNE-mU3bjH5z4in4HFQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACL_YShK0eA2nmUqZ_X-8BSxDe3Hfw2Zpue-0HCy0AARMB7Hp1fs3H6sdfrZ8N6NF5x0j7trwjZpp0m4P8pbNOtv95ffWTroOwITxdIb5pHDGBsd7ov9tTk_JM2S2ceQKiC9hE3DHYCDyDomqV_6OJ8_8N7f-oU-AYGiCL5Faf0lABC0HFzXmEFQRXM7oRsvq9eGnjXOiy6-GgpoCTDhzWzizPa-ws6V1SQ_nQKOVcdp1zXJ8MsEEbblpnkZBA3aaS5LHoIdixTTHyqR6FjitBHFpOs31cZtTZEXj0LHPuxlT6GfRFibcmAmVZFggAws_aS2ljNU04epg_63pyGqQXSod96P4uPNaYgZ_G3K4jOfsQ/p.png'
    ],
    featured: false
  }, {
    code: '14',
    slug: 'marina-square',
    title: 'Marina Square',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACL2rlcw3tTQQ0mBifnd2kwvnXEdaotswRwSF5G8XQHbbH8XMz0Rd1ZS05k7xaGjMwDYeqjXhCp9C9Y2mwy0SYVYFaM_PZlr1RR73aOnmsAPU1Of000EjhEfqPQluqvoh80jEmCANr9v4r-cvn4WUvNS6RIjES90QEkd6QcS3UvHj1LdHAK2Cs8bx6bZFQGCtkt_u5zdaNzQgrOcoP6jfTwit8MiD3FesjJTVf8ow5PQ7sDZbbwO1XSZXKM1t4G3YKbTf8HSXHjUcD0JrDLgOTpeI4Pcj0KXodOgSRe9rVNl4E8Fa7asXt9iABaRi-frJ2ArSIyIi0kzy9b24pv4IneI/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Marina Square get a facelift just before the Singapore Grand Prix started as part of the city makeover. Lightbox adapted the idea of fading rainbow to add more vibrancy and colors to the energetic life and speed involved around the area.</p>
      </div>

    `,
    image_collections: [
      '',
      'https://previews.dropbox.com/p/thumb/ACIM4bG7Dzx73tZ2e7rWdsm4CbStyfSBBBLsG2kPGRCu1jjT0ykurXk7rvZAdKxfO5OdnibesKp4VNjcbfAAVlWtr3EOZ9m3YbUU8HcnmmNhvKpSXFCF_QeO3X4qmh2wjjqo5DvO0Jpet5lMMuAwLlH-MOlnFQcmCt5OdayMsYq0GWNzHoMRgJYMV4f7g2CLYyQxaicScBQLZScXEYTKPygVE1eSv3rFiuaGUxY1QTwBO_lxa5GDSFqrppM-tE2cCN6jnMGBC_3FpsZULC7jyHMrezaIs8g87nr90J5JQmhyt81KURfiMTL3Zz5hG3lbO0eaE42nLSw-gyoXpgKtESwvrg9FaBakfirervI-td6Qdg/p.png',
      'https://previews.dropbox.com/p/thumb/ACKLtTRDHCeGUaDj5WI0VyRE2c8HVSg-nDD8Fzm4EXeyHEWNhBtMKWXyolkcueJ09_cQZfbqMs5BMtpKF6MGrVYiXTAW-nDKysPgB7O9Nudg_f6bzaEUxB_JcFTMUtPEc0t39EkXnEZ-WA_hyqrUsySAo0JLTsnmoRVurqM1bfiq6NO4UQ7E-B0rfM6k-xJkw_7T6kCi5YvUv5vv69-nSOfMjrbKffSFa5Y0r8dWeW-vw1vQ4Dok1w0YDtMMtnjX1Cbne_qivAL5JYG6hD0xDggz1O5HxHj-Zv7SYhWPMrMwshW9wbGfChwILjWJLcZ5vGHM7jSh5FJSdX4fw4tjYZNE/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ9R9rdm2vXbpbMj1VUQ4pTR11LAtWtGOGdoYqCa3F1UkBZ_EXIEMC8khTsDXoDNvqhXCLMnl1vUbPTr4i7ApKJ8c1Gl-ThvmeCFP8xOm9jSLjXLGILA0kaTHAVsRRvMSOzm8wnRYOUXrWqVYaBXKMrgJQlwI3V86acF2uss6TZmq7rVkT6V4OQBCh7KR_hu4ONc7to2Imr_SnkIWEItFPxUhzeRpRIjX3fu0BcksDSI3tOgBcsmojANvuuY5KmUZk8BFzU2AX6Xdp4eh7EC86l1YRS38Ti3ECK5FNis3YjkjVPIF8aZTZABIBhcQWyO5wEpnpHQqdSytwy2z3uA4paDS8i4QEyvoBeTUxx5NR_uw/p.png',
      'https://previews.dropbox.com/p/thumb/ACLi_pjcUE7zpvUMVWNjyvHJCBP2OVajAd5GXpIr1a3IZp-l7ypKodGiVmYHxaYMpnzDF1a0_t3shi2-SCoeHsyzPNFu7aTuX-YyBKfvjajTA4xYud4Vhkn07RrM_zTREG1_tmyQGsaVVvFCEMcKdDUgg8GiKl8xxM1Gn5HlSkj1E6g5Df-uhxbpvxMRWL6-M6n1K6p-eDYS23S8F-jH6alvHRGN06Q-qRbyIzKdZkrMCPMomDIY7-HX3E8w8sJktIpTQ-2swO2w_92zG9p0kZCD0U8YKptScHd_SNy0IQlY_thp-eyICqIL2qFdmX7JQ383Us_fv-oabYIDnUNaNQ-z/p.png',
      'https://previews.dropbox.com/p/thumb/ACKHY_7MipmdHDIkv0aVTzbt5ecuTvgMXQA0YJReiG7wXNmABqXlzFjE8E2tHE8pHF8jAAs5kO0vr6JCdbUYpmJpGA9yMqcrSBWuPlN5p_6RwAc5bD5ZAhVJD6QQs-gzO5RbzZlf7OUFw8_CF2LnDH4cJxaFLxPKoZDvjlhoYv5vT7LiKaGJC1hQkKLNpvD6DQaADA7dhP7cegOcbSVfOoo8DlFNc0e-VQASVPv1U-hylWhROXfZPT07e7OA1TYkVEBogyiAlAb5PXbh71Xwee4E0OZiTLFXxV5yzZLKy_hfUvqR9QovkNekWHzD8qEYCArw5DRGBC40JxCSi1J4-_h6/p.png',
      'https://previews.dropbox.com/p/thumb/ACL2rlcw3tTQQ0mBifnd2kwvnXEdaotswRwSF5G8XQHbbH8XMz0Rd1ZS05k7xaGjMwDYeqjXhCp9C9Y2mwy0SYVYFaM_PZlr1RR73aOnmsAPU1Of000EjhEfqPQluqvoh80jEmCANr9v4r-cvn4WUvNS6RIjES90QEkd6QcS3UvHj1LdHAK2Cs8bx6bZFQGCtkt_u5zdaNzQgrOcoP6jfTwit8MiD3FesjJTVf8ow5PQ7sDZbbwO1XSZXKM1t4G3YKbTf8HSXHjUcD0JrDLgOTpeI4Pcj0KXodOgSRe9rVNl4E8Fa7asXt9iABaRi-frJ2ArSIyIi0kzy9b24pv4IneI/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'tower-15',
    title: 'Tower 15',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIgIGIIuEVdIwDBTrAHmffqv_Bah-fCrwJnj2fIVJwcI1lUfccqi28jwKSc3KuTlv592gGkz0ip-iGq3WKVSrq-3XIe4p3t3wU0fMIVPOxIAMH2fBxuPWZL1yTm20IOGa15_xy1kIfIIgsAWctvH7afj0U0nF1QFU260pF8rqFy4J57a_lV5EfG6Imc8KBlBSRwbC7n7OHVaEqD7Zv3be6ceHghgn_LfSNKfMlJa2dQo0ZJkexDw2JX_MhktNqbSHNn1lA4OtSMEdT-8XsyZ1Mrqarp1h_2ZjKZ8_Oi2VVSdWZyHzz70cVMT9qcgaigfrf5g6mAGk-Rn8VnT1OsOJ5E/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The lighting scheme proposed for Tower Fifteen located in the Central Business District is designed to compliment the architectural language of the building, highlighting the dominant vertical of the tower. The key highlight to this building is the two interactive light shafts which contain hundreds of LED Strips running vertically symbolizing the rain drops contained within the tube. During festivities, the light strips can also be programmed to change color , creating a different mood and adding to the character of the building. Tower Fifteen is also one of the projects that successfully attaining the Urban Development Authority's much coveted Lighting Incentive Scheme GFA Grant.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIgIGIIuEVdIwDBTrAHmffqv_Bah-fCrwJnj2fIVJwcI1lUfccqi28jwKSc3KuTlv592gGkz0ip-iGq3WKVSrq-3XIe4p3t3wU0fMIVPOxIAMH2fBxuPWZL1yTm20IOGa15_xy1kIfIIgsAWctvH7afj0U0nF1QFU260pF8rqFy4J57a_lV5EfG6Imc8KBlBSRwbC7n7OHVaEqD7Zv3be6ceHghgn_LfSNKfMlJa2dQo0ZJkexDw2JX_MhktNqbSHNn1lA4OtSMEdT-8XsyZ1Mrqarp1h_2ZjKZ8_Oi2VVSdWZyHzz70cVMT9qcgaigfrf5g6mAGk-Rn8VnT1OsOJ5E/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'paragon',
    title: 'Paragon',
    location: 'Singapore',
    scope: 'Facade',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLvvEW-9tuBhVGrbyyl1pdI90C0RMaXUOzxqa3A3KyHZkXn9sTjSO-HHxa84Wlp0BrtwtuUsTf8KX16j0lLL40Xb3H3qm_ydq9eOlX6P52ukp0V_5mCff2T3Z0KCzvdp8SnJ7rvlo6N2QfnPq8jUURJG8kHvvWK-ftgugT7xk0QKbqdE7xXSmGHkIt-V60CTKc2Df95JCYzU83pt7OCE019LvGOk6R2TfT4HF6Ddt2AZ7QvLrF7M9diSsFu_PNIXRsNy1vcAfMRD7OyrGCPiWbmw1AvHpMQVJnXEWIcG2gIAMZmIhbGZmcIqCD9Ftzv91q81FgbQWrPb8cddQx74iTM/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Situated at the heart of Orchard Road, Paragon’s latest edition of a refined “Three Dimensional” architectural glass cladding, which is freshly frosted with a shimmer of latest LED innovative lighting across its facade. A breakthrough innovation that was specially customized for the project; an idea brought to reality through close design development with the lighting sub-contractor. The programming of this light fitting works with a wireless DMX system which is the second project known to be using this technology in Singapore. The lighting concept works within the structural limitation of the building construction and was defined to meet the architects’ and clients’ specific design direction for the new facade. The grandeur of the building’s lighting will indeed be a significant benchmark for future LED projects.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIb_rXy85pgZVSYg5QC4IX1FKAIn5u_7I5Pml_bAF3GbOUpvRmnWTsbAbozuNsXpGjJy_7BCKC0oHC-Fe4x1d7LWAXW184ABroPgwwinuyPpc7RbwusJH-IqyLiLm7A3FWzEFpyKaBb1g7iSl0HQ-vuzBirgQKJURuUOBeOQX60t8M0NwhPKUT86wRm1Ea8hif5Gho6ndEQAcdYICh2bBInKE3MIW5NKCu-dzAw_qf94vO3Q-XR52ikwT3jkz2NMiNYoMF76qC5c0tt5TO-09Os1gcd_tUsIwPrRUSzfKaAxmVfkRxAjvPGHRsM1Me87TS0njwv6S3GQV3vP5dvJb4f/p.png',
      'https://previews.dropbox.com/p/thumb/ACIyd0Re9Vhq1XCc_VYiUCYjl3ttjcHdYY50I1BPfHSjj_AkcN31PFA-2lJ2swB0LwAh_mE0m-5vJZCrqFKI2aVyUVueug9zj2_bpkVcwNutXL9BKlMgMi32DfkGbL3E2utkXvWZQIrPHd_ExC1EreddsnQzcFmr2SCKqRUDQaJCx0I7LUxNvrPpyBKkMarhNX_HapNTI-7azgBB8gW0g3v8VEfdvuOfGNb8edu6RhD_caxz2B-1FsovvSq_E9FUAS2ySAnhz89_y8ScVNBPe3xJvfQBA_aEM-PrTFO1OFD8JJsi-HbYKtB-NXt9c7oGmSZ2EzbGeydmBow9JC7FsXnX/p.png',
      'https://previews.dropbox.com/p/thumb/ACLo8Lc2dS6UTt4GZ-4R8aXK-yeX-xvrdTqItXzL6hShKAUwOBra-ymQdM_pqaxiR0rw3RNJm_etrU29_9TQvfLThZgdrPrkSR-OacyUB63puZ3C-34WY8whwu3Zn0m0JPTXibVps8zcU8CiPccXxB03ShB1q5k2YSY9Q9BRX3plZ2OJVomYlJ6pgBF9k3IFqJIsRpzb6wWNnhT8Zz-zJ2EMYFWxgHS8AAhtQwhPC5PEydZxXjq_Zc6acPqQIVm3U2w5o2x9g4kAEYJIOXRZOKfBCBXWLyit8ouRE82Tg43bqx5g9vpM9SmHZ_xg9oWbWNDIFu3OFvtccQHCoP7RVoke/p.png',
      'https://previews.dropbox.com/p/thumb/ACLvvEW-9tuBhVGrbyyl1pdI90C0RMaXUOzxqa3A3KyHZkXn9sTjSO-HHxa84Wlp0BrtwtuUsTf8KX16j0lLL40Xb3H3qm_ydq9eOlX6P52ukp0V_5mCff2T3Z0KCzvdp8SnJ7rvlo6N2QfnPq8jUURJG8kHvvWK-ftgugT7xk0QKbqdE7xXSmGHkIt-V60CTKc2Df95JCYzU83pt7OCE019LvGOk6R2TfT4HF6Ddt2AZ7QvLrF7M9diSsFu_PNIXRsNy1vcAfMRD7OyrGCPiWbmw1AvHpMQVJnXEWIcG2gIAMZmIhbGZmcIqCD9Ftzv91q81FgbQWrPb8cddQx74iTM/p.png'
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'pavilion,-elite-tower',
    title: 'Pavilion, Elite Tower',
    location: 'Kuala Lumpur, Malaysia',
    scope: 'Facade',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIO_VWaNO9ym-IcbBohU0d9L3_XigU7v-NQ9BYtrJM-A6PkoC3u5i_9lA0az75oFrstfkC1fd-q0orfB209pELnpUzgelQAMZvqWU6M8_9q2Xae2VCZS6kH5DAfBN2K-C5BOq11gEoPtueYRMcVSsIpAs5MfHx2TyRho_PfoplRHrnYgS3XKIdVIub_XQOzs1vVmuzVD4mY6rZbAelpa6N3t0tk2VRyOOaqPWM8c_BQnoqrNYxZ8bNS7-_xBlWbsBwpMEkrhJ1YmFL6oo88JLxUGwTBOzG877G0UeInzcmR7SEJBmkGzvJRfCt0VEaOTBGZ0rntPVFskXSkgp1wkeuO/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Creating an interplay of colours and timeless lighting environment with shades, hues, intensity and movement to stimulate imagination. Facade crown lighting is designed to perform as a media crown where each of the sparkling spot light and linear wall washer is able to be programmed with different colours and effects.</p>
      </div>

    `,
    image_collections: [
      'hhttps://previews.dropbox.com/p/thumb/ACIO_VWaNO9ym-IcbBohU0d9L3_XigU7v-NQ9BYtrJM-A6PkoC3u5i_9lA0az75oFrstfkC1fd-q0orfB209pELnpUzgelQAMZvqWU6M8_9q2Xae2VCZS6kH5DAfBN2K-C5BOq11gEoPtueYRMcVSsIpAs5MfHx2TyRho_PfoplRHrnYgS3XKIdVIub_XQOzs1vVmuzVD4mY6rZbAelpa6N3t0tk2VRyOOaqPWM8c_BQnoqrNYxZ8bNS7-_xBlWbsBwpMEkrhJ1YmFL6oo88JLxUGwTBOzG877G0UeInzcmR7SEJBmkGzvJRfCt0VEaOTBGZ0rntPVFskXSkgp1wkeuO/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'robinson',
    title: 'Robinson',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJYJUTc0pJsJoANuHMtNZ9DQS392hPHm8_L5g9KVYLZAJnghfHz8EpvhC11E7xj19vSiTWhwmq-bZ3lYB5yYTfFxVh90OvAaAqkywzHern7iYYoNqz2UIfvbrLdob0NcnoEtpwCd-kP5e40ebTucQKijJK8wqUVwUPLWbu3zIKHaC98Bbj17uEY2Bi3pbjE3clTR07slTWX_55Uulwd4GLl8jiGZ-_su9DOneTMSqo6Fymy63geq7MaFIm-yHh2EOKggqg3G70n2La67Vq-ZQD1hwDAoIZ1__5yv9oSO6jVNtio6h3fkXgvVB8KYo2BhqWYvKt4eykfE-ag3ozLO4cLVQ3ILhUpCmU4qHgXZWnZ6Q/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p>Understated facades are keys to the re-positioning of Robinson Department Store. The concept of a ''wall of light'' dominates the building's glass facade. Lightbox projected brightness inside-out from the building, treating the facade as if it was a window display. Shopper's expectations of the window-shopping experience are amusingly subverted by the play of scales. The objective of the interior lighting plan is to convey the energy and the liveliness of the city beyond, and to convert the experience of shopping into a series of pleasurable indoor events.The central OLED panels hung in random configurations from the skylight down to the lower floors. These fixtures reflect the owner's intention to refresh the Robinson brand.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJQ6yFnvu7t5KHE0vNJxSw-Y2PlGcx5UfM3MnkmT3ESEgoCU1qW7qWpmezXJmWguocRuuEGRVGXr1ep_E18fTzeEe02txLBBEYtxWdxmEL2Lj5gY3phzgvnTsxWYANxI3lm1Mq_1mztmp4tOoxXFeIr-xSmSX7BbQBsxnCVKyJ3PgMkpaG9FqCHAKJevm7reeeYxjS5KPLqAbZ1KaDXSyRMR0w7MihhJsAz8DAslnopgG4k5G3fPljF7111-JKWwqBGmpuoBWR4FaR0EdIUj8dW11TZ7Ex5qMgvzl2D5Ffm5bTGudndHfJBck6IRI6rK8NvGGVXquPe-ej32bEQdgff/p.png',
      'https://previews.dropbox.com/p/thumb/ACJjmwwdC2o4focqXkIyp8AqNt_rRuIHe2h5DAnUc6Wzlym9Iqz0683htkwnmhyh0xLPf2VvD6FBVVusut_TOGpr1LGk-JBKVSHeDOZUGy4ZXHsy9fX6lcJZ1FL0s4nxtIGfgrYnDKcohTa_13zKUlOUzgWPXGMS7_DCSNhFQBi7xH-0oIEF9_dTx-drYdGaDZk7fogXh2KWNKdbw5mSaG9qm-SB7uJsyjF2sLz0IQdO44oOg2WdKjPLwzLeh3xELy68DSf945QAjfKWx2TQQR3JsG-ROjAiblVUZdtjNmGbNmQQ-UKRKb4mk6VfWz1d3B5QConpn5u7pSbe5aDBMAUQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACKDb_RRx707yDlOHcRqIIO4O3vz6fNOmj9yY3ZYQG40O3qpO6KGq3vAmSSPIXx0ettq7Sz9LQzZMv2IYsIe_YqAUya7IOqQtOCv4GRnZRKep1GuRwNmMBP8FdS24leTJUj2po2KR4A-mqITfEW08hLn16viFiU24mPjZXkKHd8o5CQk0nU20P-xfUumuiTrxs1gMXrk2FTZEpwG3G-opbf2WrgkHJzpwQuOaE8nCXqiZTEseJZFJ7BxUJt3IWEpzi8OBVL9wiCyykKaUVGT98uHdfI6pqg5rwNByq0db2YjcRmopYW8EjQ8OlCLDue3ycaRTV9fP9d0RpYS3I1NoIQK/p.png',
      'https://previews.dropbox.com/p/thumb/ACIx4zbJDjTf28_54cKd1aQM4JCnk5I0uqSxep_mLW4a47rfJRwGUkq9aZohIzK9j_Csy0WJBUMlwiLmTBZ-HcSn2FqjvsPoTMHoaPlKbJ1BdNzICxfnWsA8LrULpXwJtVPPn8Ed9LWU3O9Z25D_u26cDAjQ4yZtLYIl3fgJgacKKuQH_8ng17hE4BmofkrGmp-6M3kQUe6460cMzvLAi6LcpCCmRlQWz_aFvUBEp0nTgy_fsln9_O-CcFxjBNTUuqtyyMYzcMpS36AQ_ooR351BbjmsYBL38yDn5G7KgDcyUmtLE0t-8UqK22Z6Q7vwhdvhUnLlUUQz8YAXh906Q04V/p.png',
      'https://previews.dropbox.com/p/thumb/ACJYJUTc0pJsJoANuHMtNZ9DQS392hPHm8_L5g9KVYLZAJnghfHz8EpvhC11E7xj19vSiTWhwmq-bZ3lYB5yYTfFxVh90OvAaAqkywzHern7iYYoNqz2UIfvbrLdob0NcnoEtpwCd-kP5e40ebTucQKijJK8wqUVwUPLWbu3zIKHaC98Bbj17uEY2Bi3pbjE3clTR07slTWX_55Uulwd4GLl8jiGZ-_su9DOneTMSqo6Fymy63geq7MaFIm-yHh2EOKggqg3G70n2La67Vq-ZQD1hwDAoIZ1__5yv9oSO6jVNtio6h3fkXgvVB8KYo2BhqWYvKt4eykfE-ag3ozLO4cLVQ3ILhUpCmU4qHgXZWnZ6Q/p.png',
    ],
    featured: false
  }, {
    code: '14',
    slug: 'rohde-&-schwarz',
    title: 'Rohde & Schwarz',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJdip9ZrYNN9BkkGtF_Z3GLY8b4-64ANkeAX3SlDnUqd4FaOjMbFWrzv_4YVmmbiNDbVC-XXmHoYlYSy6FG9fXS8mXAoMJHwW5_i-Mgu92cmHWTM0WWnrVBlsj8ZzTrvtVgzoG1H4NA6k37quVQ6epGzHAURWzI8P4NODtyPXBrhHNREzm6Upr82Bi8ssrZuZi1EsyXRd9cJ4QlrLisZ63iaeDjKg_csZBvziVR5iKdQg6ZpL8uXE5RqKHeBIA--ovvlR8ecO93uQ_d9LhrRM1vtkkoGmrY9GtLM4gbcoRPfiHuk5OAt9rpKwUOnJ3v3K03qsB49VKZOD96_X2-oIWpzSevivC1YBuw00KZvcRYOg/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p>The glass cube that is the Rohde & Schwarz office building in Singapore is an exemplar of difference. The facade lighting scheme stimulates the movement of lightning- induced radio waves, filaments of which run across the crown of the structure. The building's ''electric'' figuration reflects the company's mission and identity. The facade lighting scheme helps to establish the design language for the project and clearly positions the company as a powerhouse of innovation in engineering..</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJ8xJ0wTiOtjRS5gm76hZ6z7VHF38O9Htq7H8zwDTrM6A5s500es2Oe6wR6bXNN0TwzSrPRIiEoHWmp_6rBkfIfwfa1jr60ojaf3KHEY4eHUBA8gA6YXACY96Ufx2COMLyGIU2Y00eysMpjtzN7kxH0ZrBUEEo-9D6jyzpEz4rr-gGwMuLiUrdLpEsaiC2O7BM8o5ScWBp5tRUCk5MWlASJZ7hEUfcbkmP0eM9E7-6xVKRHfbTSiXpfznR-zXy7ligYVTL39RcCf8Mk0BlLCnsuy4SaIvtOqscotpjIQgatCV_v7a_0mX0Gag31_M5Q26sONAC7K-6fN6z1mrNWD05u/p.png',
      'https://previews.dropbox.com/p/thumb/ACIN4Y3e4-XOqco1CK7RIk2WK_YxeCNdpxzr6pN5nbh5fV7vnzKP4tYKm6xxmxJnmpcQ0KKhn4L85IgL4nyArJwEm_Z6p3W28TF7Zefy7f1H-_UiTPK4auUgWovIJgkR6o0brFqc0UyjOE7xu_c1PMZEgQtD0Nkrgv9zxlZAYvP45mwSvrWQ4tMrtWWEAXPcyJAhugSQ7Efv5fiP5Kibg55XB5xGYPqj9d5jnKtLxqORGwf5cXiVBHK-csxEWf72k3ATBZ7Cy1qPpTMhNrdYBAOaZDJDgUGnWuRBhEV6vBbgxhmP3fIUiNVsDZQJeeB68aANcqpV6NiycvmdRV2Xp1co/p.png',
      'https://previews.dropbox.com/p/thumb/ACIW8summwYgh4hNpb_AJtAfEqVsf57N6ZbMSJf8jY1gyS9XoA2S5hR-S2q9JR_tz5VNC6DJ0WgzB-un1ZCFUuyVLlfS05t1JYuM646IqE65Diu7FZUbArPQkPuMxS-0ew9G1XeJ25Sn6saav5UoXQLnegkBaCBZzc2-CS7Lmq5wVAr12I6O5BVwnSeo9IDq3BZWfd_9rewSCM8GYEKUNT5uKSjruqqLYejevIxpsju1RJ6qVcsu9a2tzYkryj8phk6Bcmb5Hp2CWU3n-fYBKpJdIRZu3mI-r-dF7Ypa0agqQLusFrh5I2OTWIH5RRRwLA9ILescQWbdaRHWeXRNIruT/p.png',
      'https://previews.dropbox.com/p/thumb/ACIhE-ehco0riZTut5uXPJ5Gu7jj_Z4sfgoRocpUdIYcYB1261lkSW2kgePSdV6Xyd6r0mo5Asw6w-y6e-xB1MPPDff-8lizmT1OajdgEiGO-crZt7_9zu_8mX0oa0imX-JfltOhUSiKWTYc-rTfU6B1k_d8-53AQ1V6ZeG35Iz25rpU2ooQz2rOSMRrysnExKNYHhNj0S41H_-J7XY6E1ZAOyzOjGK5mj3MHosXCgP8RB6R-iefVmFGvyG3TJKoCAsRxCiqSeOgex7jB51VFvf8tsE0rIyW2if_RT897JF5-J27G2latl81L1T06C6kDi0D_1r9oNhqCAfZ2Dh23-JE/p.png',
      'https://previews.dropbox.com/p/thumb/ACJdip9ZrYNN9BkkGtF_Z3GLY8b4-64ANkeAX3SlDnUqd4FaOjMbFWrzv_4YVmmbiNDbVC-XXmHoYlYSy6FG9fXS8mXAoMJHwW5_i-Mgu92cmHWTM0WWnrVBlsj8ZzTrvtVgzoG1H4NA6k37quVQ6epGzHAURWzI8P4NODtyPXBrhHNREzm6Upr82Bi8ssrZuZi1EsyXRd9cJ4QlrLisZ63iaeDjKg_csZBvziVR5iKdQg6ZpL8uXE5RqKHeBIA--ovvlR8ecO93uQ_d9LhrRM1vtkkoGmrY9GtLM4gbcoRPfiHuk5OAt9rpKwUOnJ3v3K03qsB49VKZOD96_X2-oIWpzSevivC1YBuw00KZvcRYOg/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'select-group-hq',
    title: 'Select Group HQ',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJH3V7fdty3sHTZ-KclLYm3QhigkgPL8acMXUEUFgaO50F-PaNcKED4CzJ3AMzct3ysqSu3aWW-NAgH1v9MwhXy9emyuqmEJro6ndz6bEcdynCVJJTF16TClgo8Eu92HuG9Cj0mNYnpmvGaMyasT_ZHc7o6Lhb88ekQGqNHzVPaR2U89NQZltB2P3jd7OrEnR-oEX9WNmfqLwIJb1Ls8dSVNpdBLiCocRe0GmTOzyjtpuX45Dwo5J00hZSB8mpEi-ztsM8B8H2S12zBt4Vp0Aph246oxpXDnpHqdxmhZ9LVCaPMnTsTmsQY1Y8VV2vfc_16PcYdAPsQipgAQ0--8Vnz/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The lighting scheme for the Select Group's headquarters combines both cultural and corporate values. The company manages food and related enterprises and selected the weave pattern of traditional China baskets- used to dry foods in the sun or transport goods to markets-as the architectural motif of its facade. Lightbox derived great inspiration room the concept and extended the design narrative further. We chose the colours blue, red and yellow as the project's light filters as they are dominant hues of a flame. We projected these lights in varying intensities upon the facade.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACI3kOH2OqdthEFmUYrmZ5V41uX4TjorUKs3mweFfXu5DrZ-PmozNJxW4p6f3uK3mLIy0xNBtNEHkixRtRhxz0LqcJX4FZ53oOXnk9cqen8gzkhXZEhVM0PtpgD1steGIVBXGogswSiIMiHoh6txw6E5UgeIH5za-41mA4pnLkrUBkjz4XOryfQbKEnUaD2UYz4EijtjeoK1snqiTW-zjAXrSj98jshE5jLUz09RsYHU1lHSlY6tmFeWjUFc4rpQ2qe-74dyGXAFl8cdktsp41b1QN0vhfhv02FNz89N5jMsCpdgi7P4BjiBb7LRGEuITpQY3eIh4ZlmxEIGeQNt4r7R/p.png',
      'https://previews.dropbox.com/p/thumb/ACLT9HLf9Zze2ctTtYs8XJVHvCYAI8cH4qvh0WOipVVCUkdtj84zN635B8vU5eGN1znTRHd591RUZ-VmnqN4IxChaQLU4lZoJPjQltADsEMlNHlR9EEgoO_1lKjKrVFwA7HbDPlqc8sVBmcLOXy1Wdo38d3LLRwptdlNI7lFVUui1uV_1mb1PvLYls4oF7JuQ1SIffSv4BfNESZJ0q74vV4eEPw98PKMUu-Oy1Q4nLoCQpOHybEh9qTYKE0pX6dIjM14KOXgdtF3vEs0mOBb3iUaa1swmfYMMP3EC9j7ym16jkJvsnQ1nK6rCe13hO8N8PU0zR_UGnO7owE2MMF3bxGt/p.png',
      'https://previews.dropbox.com/p/thumb/ACJH3V7fdty3sHTZ-KclLYm3QhigkgPL8acMXUEUFgaO50F-PaNcKED4CzJ3AMzct3ysqSu3aWW-NAgH1v9MwhXy9emyuqmEJro6ndz6bEcdynCVJJTF16TClgo8Eu92HuG9Cj0mNYnpmvGaMyasT_ZHc7o6Lhb88ekQGqNHzVPaR2U89NQZltB2P3jd7OrEnR-oEX9WNmfqLwIJb1Ls8dSVNpdBLiCocRe0GmTOzyjtpuX45Dwo5J00hZSB8mpEi-ztsM8B8H2S12zBt4Vp0Aph246oxpXDnpHqdxmhZ9LVCaPMnTsTmsQY1Y8VV2vfc_16PcYdAPsQipgAQ0--8Vnz/p.png',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'suntec-city-galleria',
    title: 'Suntec City Galleria',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIw1hfEN2eLUrDPSCEpj92_uIsGFEluqvBorIWb2BHXR3q4JUw_FaBqLcShaJOO7fqDhj4dJmBCL6eLGlQsbs3g97VusYFBxox0JGCvxWPyyqhdmVsUea5QNRuIJgELxk_blUw_lq2R-9P4nA5s-GqfpjRHKLADvKr30n72YKeoiTzwQpsuchup0-YLM7iCkRmAvfI6f8Jwxy-AFt9ws7xuS0owUUpRkArVgdpc20pim90hNqO4KqjQli1d-jSDW60iExCieNVnoB5Yg6IumKV4DdR7MxAYoN37kNiT-hh2Le7mTN2QfK0TKmwWWeBBNQUnso7RHBgH6Cc8SuvaKJiu/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> This contemporary shopping mall sets a new trend in shopping lifestyle. To give the architecture supportive light features, the interior lighting scheme is built upon a "Sense of Time". An integrated automatic system changes the colour temperature of the light according to the time of day. The colour will subtly shift from warm morning light to a cool, soothing evening light. Furthermore, the globe pendant lights have rhythm and a beat pulsing with coloured light to pace down the flow speed of the shoppers and draw attention to the retail space. In the centre of the mall is a feature fountain with light shows playing every thirty minutes. Vibrancy is regulated by function of the retail space without compromising on the visual impact of the mall.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACKEC1vohYQveyecpD9viPAgvC8WzB_BoZkAqDYEtSqSpOIB2rRKnrtEiFsP1rwodNFOEwwcUR2AHcHoz-0locVEbPWKCiNSz4YrD3K-wITjeT1IbbQm0L4VI9MUT-Kv72rBL8w-sV4DSF6RcL9ozRNiPf5Eb3O8jNlf7i0dnu_x4ebjirRG2Qh7c2gMlpS_QhtRvjkcldJEW9pnyECwwam9FMuoRt7MngWegVk-AcrfVI9pB2ojKsOs1hdLPOohqPQVXAbO5iz1urBUSD8BR1W3t-djf-l4uNKRiKDh2zn-AsS1ULT3hChTwQjMfgMkF6AUFRKmSKwFaUIPVArijkGC/p.png',
      'https://previews.dropbox.com/p/thumb/ACKRtCZVC70g_VUCRRz9M7QOJks2JTsSB5rYajEfMcNGQhNyFb1vxhspkndHMaUca9amwYE3u6WrCIvhO2B5GnH4xoUZbAVi7593qzr1ZWobfafs9LBu5CLW_T6hK_TcUsPsIzv0mkJD1ZTkvuSmhJ3G0VFgBL1t7L3HpS6osmBWPzuYUt-UJnYJnUeDdcSFKZnCyEBsUaIvwOJ90ujhSQMkjC-9Aov5kMb7vkbDdNRVdlJm8blEz35Kel1K4t32HSOARdmKGeVtyJ2nN4V1myV0pJ93PjHKKhfqObJn_FF4sVP47_CU96xvXCXTVk4lxZpDUfS01gsrEXPW48D71LXJ/p.png',
      'https://previews.dropbox.com/p/thumb/ACLmM4n1n5l6ixQjnLGMLumFjApw-498nPrFxgPAzvlJfr1bl5ucb2XWgHDJcPN7JxRD-wqAbkvJflQkrWX304trIO0yk9wcQNTXlUt2_Jo1KID1HifXGbzbqyKblqIQro42Qx6cSegNmN9Wq4-7MIrlVTmq3DQZZdZb4Re0QaZzrDjVsl3C_XBgmxjq9uu22dFgCpa9WlnOLuce6ZYPIKudWYMM6V2pP0xPBEMcB75kb8uIx3XoWb5vgTKfFQY8kgzqYUFMnhHGfaMSuhaMsXcBK1sGE8Dz_ae1zhVVChIj3i8BKGA3VicQaNmeYna6iSOFKWdhWBSRwiRlLvdlRNQj/p.png',
      'https://previews.dropbox.com/p/thumb/ACIw1hfEN2eLUrDPSCEpj92_uIsGFEluqvBorIWb2BHXR3q4JUw_FaBqLcShaJOO7fqDhj4dJmBCL6eLGlQsbs3g97VusYFBxox0JGCvxWPyyqhdmVsUea5QNRuIJgELxk_blUw_lq2R-9P4nA5s-GqfpjRHKLADvKr30n72YKeoiTzwQpsuchup0-YLM7iCkRmAvfI6f8Jwxy-AFt9ws7xuS0owUUpRkArVgdpc20pim90hNqO4KqjQli1d-jSDW60iExCieNVnoB5Yg6IumKV4DdR7MxAYoN37kNiT-hh2Le7mTN2QfK0TKmwWWeBBNQUnso7RHBgH6Cc8SuvaKJiu/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'vivocity',
    title: 'Vivocity',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJFC2j5vJcUQtqsYwKl6DvoWTRFqHgzKbXMtgkkrqj2_If371guoXOFqm7CYUP7w-J0VgQI4sMkB-Woe9MDg7JEMuZw8WAm4y5OYsR7Mz-B444qowP4ICO6h4cyhdho3Y8OK_GqqegKl0gXCYwYeahrfxWxuYoEXwn18CQMYBS3P_y1MQmWy13bdwzogiKJ8OyQEXzNepy4L3lk4YMyCJcjGrtAe08-t6sVcfS5FNVS75qvEQodZln0GzJoes7t0wiZb3HxFLefHYdjDnmryXSMiL6GmfKlBeZ0m-Og4MI7cO65PRIWmqHA0b6enh0aQLLrhO4NJ9IeWY1FceGqPRw2/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> N/A</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACL-1V92T-_YWms_EGox-D1ZppsYLozvAjpX-HN9HgBhHfmewlUW7a6zzpSATb813bfmff47lUWAzvsW6GtM6VIE9UsNOtO62ZGUlyHbOra6wfPg83a-gWbebW_r0hYKyX9igQUAXEJekP5d5fZmLC80HrtAx-Wxb1e0h3b943ypTgwn5XQ3TWE5x4-GcF0a6851yX5-cRvE0zAnPEr5VWcdA3lE7b9X-cjEbRhXC2p1RGFPNT4cDt6d6Zw5jkon2dU1i4MdeTYS0LXgHzK2HD74bBZZi4i-L0fJ606ZMnSVKsppbKzGrLxKvv5Wi4KE7fPQMRttvgUUjFg0ufhdLyQM/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACLclQKaGoI_FPl10iGxM809VCKOLEMN432RLBaezaW_4asExM4whQikmEbkuN4ekscs6GKD2i4oGTbqWcvV_jMO_xWmwEJYq_O0f21pZpKPyark6h7kzaQeZVfHPL-siA0huTYeeaFqzjPoXmlXte6B4XDLKe-bhCL-giGzlHe4NGX-gIdJnei8qSr9TXLuDjcDAQIKkR1yP_l3jneyW-n2Pjc_-9l_6ZeXzEx92I3ztHUwBOTmVoWOu3VV0x5rerw-e3X0cF6arR1c2quMgudVC4aFxp64lUOm4I14za_oM_S6pdcZqK5f8BPIdZBi8Fo2fjntOJ85ba6zM51mfPZP/p.png',
      'https://previews.dropbox.com/p/thumb/ACIQ-XKJHI7KYJsY0HCv9kmBz5HOXQaFeDRjQLpZroTlhxEoZscqKZYmcTE3V9Uxrw-luQW8uQLg6DIAABfOeliQ7fLuAOy0_ODo3nWGOnM2n2pWaxvCxsqC2i96tKIbsyQVQjK8-Vx6ooO5UdDkHSTosP-RkZJo1KitIJzS6taHyXf3HUXddnQqIyD76MWOFN9l6Q20BBQI9hS5I-vNY_btny64ndTaPGkTI64ItCNz8BXyyf6rEIeDqydsjfsVZ9qI6ytj4laqff7CB1-xZ3RB6LjMCLEkv5SyuGSrvVT1ExDz6xUcNiVK9BKRmclqMoYyFpq9owghpGAKR467lwH9/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'vivocity,-saigon',
    title: 'Vivocity Saigon',
    location: 'Vietnam',
    scope: 'Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJALEDCvC0QTW4d_U6jZEhe-abFiPvh3OP4Fpb9XJ9OMhOGGPG_7fB-yMppIJqQxaK0eIM0iXX6ANlLKtU2xew7nHRWH-b3rUzvhi7r4pK0fd6w_dYhF-PzcPz8gdBaQgHI7VtCaW4xZYxPyKxsFOy9c0MtCNrc_NlWpf9gTdCQvpKT0YX6YoNwhpdliWtnHlEJenixoBc0Vsr0_WVvjRrvGqcHb_wbzWfQticRMq_p6adVW0q4iCqmMWnro_Rq1nQbjaItdmzBg3MhamzmmtXLEh0TLAQ1OqsoWVstKHEJumiepi3pSIZSgIeSDdDQ0Vb-nsm-THL_ZNq_HcIT19SZ/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Vivocity is one of the biggest shopping mall in South Saigon, Vietnam. The lighting design created the identity of the entire mall by bringing the concept of CITY SKYLINE as its main theme to its fullest. The clever interplay of the window display and the layering of light outlined the image of the future city space. Lighting strip embedded with the facade with programmable intensity creates more depth and a magnificent sight. This project is a clear illustration of the paramount role lighting plays on bringing out the character of the building in the context's of today's world.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJ_pkI2P_l6FzHF92M6fM482H1q9S7ZLOQ3LjtUyaAXKrC9LLHjZjYn-zNHVIYODaCuUAC-yarTqV69gJp1acjykoI2BgAqFp04MhO4HL9Lbe92v6u4d550-9d0ZQ69M3iXClLilwU0SCM2e2Oix1ztHfYcv9-Ej3vpaL6ib1sTO2GJ58IWf8j_hIlZhK3DZ3kdTKVr0enNbDdYmd1n7EziAtVSV1hCEaXhVu_kZdkuQsqaQ3eJeE36z20UUMeD8vOL285fA9T_WglJN42mh4W4wtCDRqBPxFdoOFgvCzhI4fGKkSbwbS1R_0Hdi7eUvi3DOLPsxhP6yQU12NHWMlh8/p.png',
      'https://previews.dropbox.com/p/thumb/ACKTwgkmD7QIwPgXZw782kYLe9e7KG0uKYPbB2zp9vHZ-fL163SJi5XTecg2sr-P-NU-KompVTdPYdxKMbiPZjFZOhILXqtwV2yPLs2PuLD_qNGA-6yU6OFW6s4Z2D72kMIoFK6jzEgKAQnLmgy6WNAH5A2gniQqHkzMspyuy2i17dyrClfyFxL7tm7upv3JigehxWsu2UaoEYyjnbhfFrnyHRSyHenhWvPo6eyVM14JWLWaPPmUdzYlOTqzVPRVhyq5bNt-JnmBgQXv_oq5WpEQwRBrFn03SJJ-VCUvWfNCIKvtmLmwX73GPJzQvnsIlphO5r3u2s3SebX4nq4sdySy/p.png',
      'https://previews.dropbox.com/p/thumb/ACI4HUUsVEIm6GMRyzbYr91Mo2K5GOqS62OEaFLv6mMATvI7hC6HX_cND7L057yzxoU3lrgs1NoM4YXhLId-oQOwlRDKdEUdV9dtJ8Lh873nQe1InmZo_LSPKgEgpxY9d0LNclUb2-ECVqlo0vutbmK5uVv0xau1WJX6eYBCmntUZ9fkvSIGQ8pNHVLt40d4YDkY8ihdnNduyzHwFFfTVSbhq9hVkUTznUfByDFhKSOYJzWAWMrAkkCaHYtFM3_pFGMxBtN6yfIBXmPwKcxXMKpfiU4RMuTKJVdNZ3q_AXqAUhXcxfwZYx2hn_3k-iNVa5Gv1ztKK3AeYLpz5fp3-Yza/p.png',
      'https://previews.dropbox.com/p/thumb/ACJALEDCvC0QTW4d_U6jZEhe-abFiPvh3OP4Fpb9XJ9OMhOGGPG_7fB-yMppIJqQxaK0eIM0iXX6ANlLKtU2xew7nHRWH-b3rUzvhi7r4pK0fd6w_dYhF-PzcPz8gdBaQgHI7VtCaW4xZYxPyKxsFOy9c0MtCNrc_NlWpf9gTdCQvpKT0YX6YoNwhpdliWtnHlEJenixoBc0Vsr0_WVvjRrvGqcHb_wbzWfQticRMq_p6adVW0q4iCqmMWnro_Rq1nQbjaItdmzBg3MhamzmmtXLEh0TLAQ1OqsoWVstKHEJumiepi3pSIZSgIeSDdDQ0Vb-nsm-THL_ZNq_HcIT19SZ/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'westgate-mall',
    title: 'Westgate Mall',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIwaEwkfVd4w9OK7ALjNxKLYxp_FRtXgoUYyXzZSSIRgC8ZFSc2tVX75JQ8FQ7Wl2goIViYoNwqqm-vjTKDMtA4NNqDzo3N88wgsXHmmVCDCo5O_6Q5_2gTEruNjFjF0UKBjNeYWDLXhL6LOsPozjkzIUejgnmR8LD7I7Ey0zjwyXrLY91agwxt41OadSboP-hiGkdXSHk6mH5zHN9b6VcJpPvIWfhibGf3fr8s_6kuIG7HCMaNpFM8lbY2e98XfKCdzGBnSYUOSWrNqyoj2JyQndydvQC5VdwpTBvHM0uabBQQByDF_J6CxS-BqnBl89dirZqNKsuYW2hOKIkNdEHg/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The iconic wetlands and swamps of Jurong Lake District of Singapore informed the lighting design concept of Westgate mall. The bold architectural move in the project was literally fracture of the block by a vertical canal. The water, cascading down across four stories, collects in pools and minor tributaries on the lower level. Lightbox's task was to position light to reflect pff the stream of water and to capture the motion of water on the atrium ceiling.
The mall's facade was inspired by the irregular roof lines of historic Singaporean shop houses. The facade lighting scheme also adopted this architectural intention, taking it as a point of departure the varying heights to accentuate the elevation.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLdPYnWn7bLISDcmEkHnvwhHrE4QW3j8nIjFJBPiv5DXrC2a56DIOvrP0YWsb_6B2nGOsIQebkze1XbUDJgQJxyICKOVjfGAUmT1Evuz6os_3kJSupcAGZMtTd_4em3yUMo0K9-amQKO5dBVoGwpd8uOR5eTRjiWzZ3RGAeKRvJbhJwu7ZqIHLFc2HNTeqG5emjuEVa9CLw_jKQVUpDzGs2bpRxsMuJjGCIzVCfbXHctaBY_ZQnJOcbVacGNycnL2PrvEP5Ldgg0g7G95AVb_dZJMVySNcyEiqJ7Kha3r3dCw4gjlcwgofGhDVk40G72kGQE2tBzn5tNfJeQaOsarMc/p.png',
      'https://previews.dropbox.com/p/thumb/ACJEwVizxRMJXuJ7ZXHz0_yXEs4NGnJwwQXOAOWGBz7oT9KuVFuMgTJwnkYCrXxkntRoWsiS9Qb9vzjZ-XtxPFqMFNdTAwyYIsnH8xfOWwFEJTqcCv_Ucz8afKM4ibV6CeNb-369DQ2p02-lDQn-gJTx4xPHA_j6cBW5l-6XJndEA0wE4cYIhmCQtkkzTarqTxqkgffsqjqtnLe6DQv1wDzxcwoGxOzgXN_w8jZ7jm3lp0f5mPSrcrWAlAkis1vW4RuciXhZJRNc6wFyZz9HVqvZRWV4benZZLIH_fQ2oZGROaJAPdx5Qnh2zHGv1RDZ754H1mH4nEkABXgqZxZu3lw3/p.png',
      'https://previews.dropbox.com/p/thumb/ACII1cmVQ9bUplV0-kK6i-D-oJtE1N1dka4Wdhp0Wa4SGRR3cOb8UoIMtP8EWpaKtSNxExszSIAjM31BYx0B-NW15Ij7z9xm4FqqMSYgbR9NuW3_-xhKkYpM-SDmd_9hTFIUQrUC_6Gua06uZqXzTIMV_RN2g0WswlBj-smPWx3Su1m6Rc5OnVpjBEqJRiyEmZnLr3wb81f4Zl_QVljXdOkqPs4yYiby_lnTUK4_4xIZmNy8j72s6KJIpbR0eAwZ-KUi9eKISyPYDq0TUCKyWwAce0Z0o2t81uGTxl4amkuGuGvTu0mOunzjkMr94b8Zxog1nk1d5YFuQ5Fs0KCyzC8x/p.png',
      'https://previews.dropbox.com/p/thumb/ACJAPvVqQfc3vd_1c0OUnQW-QjdgEqU3GfQYLGWbYj5EyerlVUGlyje1az9nWofx0CMT7vapof3pCLwYFOzl8JRK5ZjB9JDqXvWYWEAik91xBNN8Bnn3d5bmK189_dnBA27JAIFdBbxHDY7NZujhJDW8jqynBVj1clO5SV8fSMg5hNvAUgDyh9uTu8o4zyFy_iq02gi5DlB-Ee5b4u9HBRXxwghZquU8uaEgI7AMiSfdvkQISzi8ZO_lPs69DEgi-ApyRXshYzBTJ1plTaHLUck6szqtyLaSaCUtIfvzlhwaAJXWZRq9PVrK7DYpTipsW3tIOMIPzg4CWJqGpkpnoNjD/p.png',
      'https://previews.dropbox.com/p/thumb/ACKb1jt9RFFCj4hzTO6-AmNOqoZ8Nodr09_nWvVA24TxwXjj7jurVrXx1NAJMyj2yeX5WTq7-84G6EgGSMa7tHEDqT9d3IXIXE-s-X8E4GiuMjzUio4aLhxuiCF-CC5vSySy-HAi90BNq89BtiwFU12MDhPWavBUEngtbIHERibWbdh6vH-Gp5mRwbkZp75oWSs-DVe7Bq1J5DoQRl99bi7H8QK1GYLQVmkj_wg3nbaOdH0WaphqKKmcuFipab8CsGkskMzvAxFM96yRtHdsxE1QO685TWviim1SDadEfsPLcY7A-X4VEba3hqX6pLiXzSMU_Wzx6BpSDKKCKS-eJi_x/p.png',
      'https://previews.dropbox.com/p/thumb/ACJgi0vaIhTxRiwEovMDHXLfZqp5dqqrL3mjlCkjrESiE6S6SBBDJx6SjRqpI6Sa1v5irYh8cJpKqswuAbQc6_JMTZVDhHL1J2Z5-3mF0-ch9n9ntYRhc0BtNfwFceFEQlOW5R0f1S99YRZksxD4YAzFVRR9Oct0G5Q9MHZrY6fo2VzgEnlDvh7hZurImJDpl1lqcbNZDzIBpqtZ3wDmVnhUnDc3YZxhQTUMLPU4amh__k_QbKjm49y-Q0eMbdPsAMRQGF9pPyoRSbgUA9i2rbizptPou7ugU85wDsJQwTcOtxZKYqJHMUrzMY7op5bM5mMaJHTTzUfXRX_gLahGrxv0/p.png',
      'https://previews.dropbox.com/p/thumb/ACIwaEwkfVd4w9OK7ALjNxKLYxp_FRtXgoUYyXzZSSIRgC8ZFSc2tVX75JQ8FQ7Wl2goIViYoNwqqm-vjTKDMtA4NNqDzo3N88wgsXHmmVCDCo5O_6Q5_2gTEruNjFjF0UKBjNeYWDLXhL6LOsPozjkzIUejgnmR8LD7I7Ey0zjwyXrLY91agwxt41OadSboP-hiGkdXSHk6mH5zHN9b6VcJpPvIWfhibGf3fr8s_6kuIG7HCMaNpFM8lbY2e98XfKCdzGBnSYUOSWrNqyoj2JyQndydvQC5VdwpTBvHM0uabBQQByDF_J6CxS-BqnBl89dirZqNKsuYW2hOKIkNdEHg/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: '20-anson-road',
    title: '20 Anson Road',
    location: 'Singapore',
    scope: 'Facade',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKr9hU0WjbK-3iSgyczuAl48O_u5c4yzrMwMKoPBN7Ie7WkwvlsgpXpg2243L7kwfgHKTUzujGDU6oUpuQwGao4pp0V2i55vfVUK1oO2fONJiX0Jp2mmT5iRf6PIVHKqvNJ3Ea9aZNXoBOh-PnVJFlospHXELQWDKpbRvLmYm81rbu7AGNF1Xd_nWKO2SFnmO3NIsMbsX0HQVgZUQNaqgtnZ7dmWgwCbmP_jitbmpcaR1J6gTXOAvBOCg_P2HBk_dkOTQOV3efydoBwhiUu0igT_rstXQ1nn40SfMdA9Jg-ciKTDonXk2w0gfWfTbpoAmQI7QL53m6XQw-tqBZi9rC5/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Batik, the Indonesian word that refers to a generic wax-dyeing technique used on textile. This language based on the inter layering materials that was applied to the podium block. The outer skin of the podium block is defined by the intermittent lines, forming the texture and building identity. 
The batik is formed by the second layer of the podium block. Co-existing yet never merging. RGB LED lighting is used, in contrast with the white LED lighting mounted on the lattice detail. Guiding and gliding visually around the podium block, the blue and white light creates a movement to the design.</p>
   `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACKpsmDHni34ygUX1eTkYLn5pVOz1Ltv7NCHb0cVrDhntTPU9Li_aBSYVrN6_3JxkjHx56yWalnJl8nogmabz7nmLSvX4Aqnv6WXAnPExenVu6QBCoNuhlBUpyMK99x7KBib9KrLxuGPDRyYesL5hy3EFZJeLPMWuy4tle1b_F3YyNiQNRu7onxbTb0YnZKk5MAjz1wWp95XHjSVOFzEenuaFR7rek04YRncnlOOOMCup1h2Q99narQgsl4GO4i_jqvtabPd8agc5WNgM2eMIWrejJj4Snq8_o9S7OPts3oxOk0mBWHagMAFM0rQ-Ywtl08YkCVJxuilUdo2drscU0BU/p.png',
      'https://previews.dropbox.com/p/thumb/ACJwlyzkTGjbDgRfygXdU10bEqU_xQzcgrOvtoZ3OBInflfaq85E5U6uNpc3iVXAKJC9467Cbyy538NuMpS-ScYm5kPMj_nkdG3zlOuEcQctmAE5zOIHuK-dwBBzt8O7w6Ozx5l-TMoMn6IWNreJJwjityqOnppLqK2jGa6gcbyHJIxhbzAfNyuOXq1D7IaOVnUZKiBJPZHEBxKv3QM6WF0Vjv_FVrxLn9cEmAS_iqYFNtmWTYGLE24uTnEohuYbjr9rW-UkqIP4itMEZBL21jYjHvCe4DjMgIY1DoLJnleWBESdtt108oEIoQ-ldvdpjGxmJckbsXYJiFJt7RPRS3c6/p.png',
      'https://previews.dropbox.com/p/thumb/ACKr9hU0WjbK-3iSgyczuAl48O_u5c4yzrMwMKoPBN7Ie7WkwvlsgpXpg2243L7kwfgHKTUzujGDU6oUpuQwGao4pp0V2i55vfVUK1oO2fONJiX0Jp2mmT5iRf6PIVHKqvNJ3Ea9aZNXoBOh-PnVJFlospHXELQWDKpbRvLmYm81rbu7AGNF1Xd_nWKO2SFnmO3NIsMbsX0HQVgZUQNaqgtnZ7dmWgwCbmP_jitbmpcaR1J6gTXOAvBOCg_P2HBk_dkOTQOV3efydoBwhiUu0igT_rstXQ1nn40SfMdA9Jg-ciKTDonXk2w0gfWfTbpoAmQI7QL53m6XQw-tqBZi9rC5/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: '100-am-&-amara-hotel',
    title: '100 AM & Amara Hotel',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJx6afvhoJuk9pRDTW2WvXpKBCHCBDq5p4UOAUaOCNrZy6qt2nbrSj_Hf8k26YR_Nnq4kZYAKwJ2qlnJm_NKs4iD4H-UEia4o5tQ8V26EzqNVvqScgDf-000-_bmqdHJ1qg0gAdmxSHaJJqoPj4RkVQVCoeneRoK4xXbPyJAh6vzqsFK-qvRne3Wr5XQUf3AVPoSZ7MdFb81j1UnrkewuAGseUYsYSlVKwNf0dv72-Tdn5Uc-dU85-_FzHU6IvAWGLLCSxEtVXmWgz9O-DPbZH0uzfead2gxol6zpPgQdaVdmFe2V5Q8d_JYny0-G-rJwaFL23vrvLdeDutL7jHzay1/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The upscale 100AM is located in Singapore's main shopping street. To distinguish it as a vendor of luxury, the developers asked Lightbox to develop a lighting scheme that would message the exclusivity of the brand. We used pure white LED Technology with controllable facets on the facade and programmed them to ''blink'',imitating how a diamond's facets catch the light.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACL7YBR4Xp3xkdROJLJOd87X4iTQGuuVYPndN4dvVSj4MDWc-LHZ_ERNC9QXLfRkYZAkQqNQXoQnbsJgsaWG2FBZSc6KVzONfFxs3qckr1bSQVP44Wcj31bGZSD98e3J0MyhCc6OBreOJ0rDywGYh5Eh56mbaYR48dmkFOj4M3obGKMyGGLLHhroGSpnw5-Ajc4lConLh3VeDkD05fO4G-41WOaMaavE0i69_WSBLJCbUB4tIT0ZtK0UDNT2FOn92M3URarRG3u5AokL4xT55a4t_ATfNTRIiY2rvX4Qj9yAdZbXoqC9zzOhmMlxqK8RG5VfO4GtjuOm6DCxLpoPAPDe/p.png',
      'https://previews.dropbox.com/p/thumb/ACJx6afvhoJuk9pRDTW2WvXpKBCHCBDq5p4UOAUaOCNrZy6qt2nbrSj_Hf8k26YR_Nnq4kZYAKwJ2qlnJm_NKs4iD4H-UEia4o5tQ8V26EzqNVvqScgDf-000-_bmqdHJ1qg0gAdmxSHaJJqoPj4RkVQVCoeneRoK4xXbPyJAh6vzqsFK-qvRne3Wr5XQUf3AVPoSZ7MdFb81j1UnrkewuAGseUYsYSlVKwNf0dv72-Tdn5Uc-dU85-_FzHU6IvAWGLLCSxEtVXmWgz9O-DPbZH0uzfead2gxol6zpPgQdaVdmFe2V5Q8d_JYny0-G-rJwaFL23vrvLdeDutL7jHzay1/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'atrium-plaza',
    title: 'Atrium Plaza',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIGRg81yXjiC2BhyU2tSDzCJfrKzZWW1NjGJTfh445axmWP5cH4RZpJlxGJO7ElwIrVix6uUDU-uYvtlb67MJKG9u73F72O6P4_YT7xGRemXQHEPoEk0NuN_hntdRhftPX-QTRxtQ-VtIe72Pl9aQYS52bk22oLpoeZ2nBJwzVEtXkJxOgs7dGFf4v9hkz7iIUjUugnowMyzOikHi4_T5pGU6MAVDCgItn6JfaMmioO_3rZv5eVCjXOswoQBqKk5mVxYznoLocoeRV6UQLE6wu2_85quB-M8XvD14A4b6B8z5SYZwG9Jdran9K4gcROcez3ftrezm8vRMDgwUzM7Bsx/p.png',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The proposed facade lighting design focuses on creating a fresh and vibrant looks for the future renovation of Atrium and Plaza Singapura shopping center. The newly renovated plazas will merge its architectural boundary with a subtle flow of ornamental LED belt installed prominently in a horizontal way around the facade structures. Proposed lighting effect of flowing LED movement by installing small LED fixture onto the facade cladding. Numbers of LED light fixtures will be arranged graphically to create 3D effect of movement that would simulates the overall facade display into a living organism at night.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLTvBMGWZYvdPuH8BIQdtVjJ44xoxB_vO6vnvyt1cTNqXRqOJakCQFJL1RnZsYuM1J94vtF9ohrtCGfsyEx9IDzhmWn3N0YedceS_EBE5b7_eE_bFc_tYE4ZXaLUll6ZM0MC4TvR4rRNiBwPZzCYmKCKchpTun_Nx4ilhbxUjau-VMsq6yoOxT8vUYUjk6_R58KwJ2qYmNC0UdIS5la4uMgRNuvTHJMU6AQjROH8G8S8SxG7zFM2oL_uXEiFxJupaZ7v5JuUr6M96-QxTtm5W7ymAiffBgnibU2NGuO0QxprH4EtzzHmKWprqCRYYD5c5aTKgdbfQ4ctrx9sdyOV4TfmiTpLBxwuj_4S9SfQt5iFQ/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACKI-I8iJ6tpPnpmXD6UvKztz7TbC4zhTnxtIR_dy7ipfQtNyeEp0fmn6beW8Arn_XbyzvFMx4zghmKjIXAweiK2sFkGogVTo92v0df2LtCdep9MHvN0BE_Oxyj3tjrSj3Nl8Us8hCbUrfDUwAdYdrX8XHhf69zo33KmVe_Y4rf-DXSfko9nTZfLZ4py0evaqlsA5sTG7xYuIG9baWFT-yLVBLnxBTD_KinkPvXCdK7yNhb4Nh33M6EH4QoJlAK2tWjI8SvUCyXXIjiXWq1ODU4oJ0bPJFwraa_2hpeJHmsiXHrrwqqYkOAd40GEBjG0eNIEitpchcZN3U6w9GGj1_uC/p.png',
      'https://previews.dropbox.com/p/thumb/ACJcoDmAzXhzv9nydL0Yos2-mLJbBQZdajjCClrrwr8CRu79wV5iid7GAJdj6sj2b3hnO40HqHFvALSclw-dxdMlxo3mtg2PRCxDjCUE9C43qNDBIEHSVOove8b8kNzyz0eJT5LfIa9JqgZUmrKS6o8QAwnJqUq2eR6l7mKbZu4A0cdSNbsYydu-HoTYv2r8t6IChsw6qEcoaM713-aCJFROPAUuh6ZjUaULST3TzZhRe0gB23I4W6IDcBIIanuKNZov1QX6WqYuL3hbpIr87sKuUJlgTFJOEIZBW95TpNXaN7C1vIDU748A4QAgaSCqkhR73_oUcy-WKDSEskn9RCI4/p.png',
      'https://previews.dropbox.com/p/thumb/ACIGRg81yXjiC2BhyU2tSDzCJfrKzZWW1NjGJTfh445axmWP5cH4RZpJlxGJO7ElwIrVix6uUDU-uYvtlb67MJKG9u73F72O6P4_YT7xGRemXQHEPoEk0NuN_hntdRhftPX-QTRxtQ-VtIe72Pl9aQYS52bk22oLpoeZ2nBJwzVEtXkJxOgs7dGFf4v9hkz7iIUjUugnowMyzOikHi4_T5pGU6MAVDCgItn6JfaMmioO_3rZv5eVCjXOswoQBqKk5mVxYznoLocoeRV6UQLE6wu2_85quB-M8XvD14A4b6B8z5SYZwG9Jdran9K4gcROcez3ftrezm8vRMDgwUzM7Bsx/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'pavilion-shopping-mall',
    title: 'Pavilion Shopping Mall',
    location: 'Kuala Lumpur, Malaysia',
    scope: 'Facade',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACI8TTvja8qWZlOE4_RcZWpCeSEK-lHnciCYG-zC9KOwX76E8cK26JZ2bfPMmX08b_vH33FlkYcl1qHZuXasINT_Zv2l9VnVRpD3uAe4sYQirN_ndng_UzMwlruLzZJwBAI7qQ0YcSkTphSpnBBLxD_QVt0DKNxb2G2oMZzbd82a9UjL0PZSU5J_WHN4eJVMFUFetCMssZZQvbtc3fCipubi9pcL0i6vgzGpydXnctL-y953R1gz_s_uv0yeLfXEm6cXyFYbeR66jrPAo9s_RUjmnsEVAH8unjVc-OuKTvGgcIcRZOdRwPLnstONgK5esqkuVZTXl7Cby5mm9a94JI_M/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> As lighting is designed to be part of the building elements, the collaborative efforts with architects and designers resulted in the creation of triangle fins including the 3 sets of LED linear strips.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIvk5icL2nUVCmO59eQC70-LqdKDFJ4eWpWLkM14pCxTmVNOF8hS-DmhOAdTPmk-94nYrQyvjiImSNS_xx_RSmBDiB2QLT9Zu5lpZpaROEC4Ogymnq02KRbGq1IIYaD87JAzZjZhFzoJ9dmYOPeBuOH19LNzD7Aj15hr9n3d5ZHgNXfPatC7n-7MnLpA2BGra744fd6OxM4UCtqyYSlm2igFxoclbK2YetzA1_9U-E3pocTWSlFHsyjItGWkts84S9bK9hJ0Of2ySDLjyMT31gUdhl8Wzmx_w--oiBLHKxpkURfcbcF4-b8XvVHjeSUomeXV1gxOh2A06-t9sieePgG/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACKN73_3TJAbtWBWTWWuXKwpzuvoY_TIAzRJlSvzx72YqIWk8se3TpRPvtQPkpzKeMLjxg2IMDjUSFsbaW7B2sW3Ebuwe1N3_wdvFEO7U5gQ-3nNVK_CS9I1PTYe_jinXPZMJ8p1HOe2-eQnuXBuIe3hp-bNXmL4ojoqMl1LZKu6hiZ4cyN7Fv7Vyv9DBMbkyv00AKuCx15zIOajDxPDBopGTeZeuSISTx7oruitrhMR2gUIiNvUrtHvVHRol0WzbyXgVTDWOJRC0PhF-ezP1pLFUFjclO6zkMNYosO-gLIOJCNrUEWTR9OYu6nvGvqHU68-ZTuBntEm187QT71zNj35/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACI8TTvja8qWZlOE4_RcZWpCeSEK-lHnciCYG-zC9KOwX76E8cK26JZ2bfPMmX08b_vH33FlkYcl1qHZuXasINT_Zv2l9VnVRpD3uAe4sYQirN_ndng_UzMwlruLzZJwBAI7qQ0YcSkTphSpnBBLxD_QVt0DKNxb2G2oMZzbd82a9UjL0PZSU5J_WHN4eJVMFUFetCMssZZQvbtc3fCipubi9pcL0i6vgzGpydXnctL-y953R1gz_s_uv0yeLfXEm6cXyFYbeR66jrPAo9s_RUjmnsEVAH8unjVc-OuKTvGgcIcRZOdRwPLnstONgK5esqkuVZTXl7Cby5mm9a94JI_M/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'crescent-mall-2',
    title: 'Crescent Mall 2',
    location: 'HCMC, Vietnam',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLjzDR75OSiTlHwA_PEUx05c6osYsUtVJfA5dltwlZMKk61jMDndGwb53O4QI_tMiJrtrqoI7piDNQHUVmQotV5vgFbtX-I3di6r4nPvHs8jqgRznvBAiBcGvP_Sug5molw-OBQLq7EBn-rOeAITAWth86bKTW_RmuLi43wtq4UDdZMyIPJIUiuT6WWSoglR7vmOHWb_NuAfuL9fiGnencXyXxClw2a-slnocQsqhhf9USdqUZJ25oBECLMoYOT4rj2ZA7jv4hbkhOs4Plbbmt0XLPxOys8tyVRC855_2BPYyx6lpjeT214ItzA1_fYCSowSN7jaUnMU1FvQd1nauKR/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Crescent Mall 2 is an extension of Crescent Mall. The lighting concept for new extension was to bring the essence of Crescent Mall by including the existing Mall lighting language in the new mall. New elements were also added to accentuate the facade elements of the new mall. Color change lighting were introduced at the facade to celebrate events and festivals around the mall.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLORE_wJsuJUA0zthTm8YYMnq2RtOMdgDK5PooFCnDlK-_EJrC_4KHc678EY2eeG900ujrf3o5NWS1auvc_hXtqfcbz3-fd3F4Y5EocdaKMTcSKOX7asQMNbkjxyXaaBG4W5b7C9TTdwe4C7FY0CKz3qyjhcHAsnvDkCWIc-yUQ_5DvCMgVxBZG9O0U31_gxMqa8vT7JFY0HmXwYC9Ejv4Dux7JCgG0cQW8V6kYAY43ZlCaxLJPHms70z6ITmrp7SqECnoIc5lcO-PCOy_o_63E0o01TSDyNDDE4cXpKEp4G2XPdC0ESl4iZsB1QzryfzpNkTWei6n_HMX_bhPB7GG5/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACKAYOX8YX0asveOULkbPBnRVq-Xt-kGpkQyLaG8gbgqNeZij5T0Hbjp6SfUQJn2Ev2DJkMup7WoTYMPuI05X9mIRL9cq-sjSpjUHEVur0y5-_PbH9rUYjz4DTmo3sTOI7Kl4oEydOY64amDlyszhW_2VC3atFtcg-aCNQtb-ROT02xwBA-3CZJYxuB5Jan7hAwPNZ9NGa5c-BLxUxPeVBZaRZv-6vDP79LSAutIbPjIa4fJcO3zw3XxJZ4USJi_jB5hNMBlCD2EkGzFWDIdW55vOayZZHYrd3X4cPF6SHvOXdz_Yl8N_ojIXx2AjkTvpfmBNpSeFqQ4s48nuW3Fu2oF/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACI4mGDQJvhT6sk13nTguah-1yT4cBrKJrserx-sep3oJf31aYHTBF64DGlIqkpADAqxOC519HERkxJHmeC1r3FI6fmZC58G8vY6J9cjdnUeM8G7HAi3o6eGYG8OwGk_lHyJuam6MXjxrFLUy00oMtKsWCyG_msNy2txWo2ZOuWSrzZ0WN0nt3Et84a-n11VyxaTtLNtJuB0GefKoYzTWXfcC8Oq5iPOHaNTg3yqw78ghrb2fOQX7g993kTZmGIxZ-QrFzt5ou1MEsRqPxWwoaPPRs-h-i_1Lhfwy4sWHk6gWaKKOYsemi6ECEJ9FuWu13TIPvi0__2CXkg59EP4UUwV/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACLsNcvUs8bK2XrcwRw5V7QSHRuR_JVnnS816jBcEaioUE4ZLEcwT6Qk9WZ4e3QwoixRpWRmSViFhwp7-W6TbRlhh5Wz882RyWeKUmiEMeynZsPT0NPWxvMDPc2mm1lEGiWpyhsWvSH4hPwuOiepsgNQAPjZbQepoXbclZ-ieQoG0rQRnn9KhsthbDe7UVjkN2GO1MQ2ZsXggTHVoYLgoZX_taYM-644V_r2vBxUrKHVomNm7vb4Bx3kMJMXdYBEtQ-l4Rgr5SRF42DOrnvtPEQq2Dk595BYyEO2hSc0shnfhm1iivgD2O1pjsSCKxv8TL4d6pYeFlp2Q5R9H__JmccO/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJBkmQdE2cai_PCK2Cf1TpnQ7_zR5TKjOBEVgncgzi9sDvHXz1JOPARK4mg3fcBK3nEUmIcg0GGVhfD5ydTkNCm2n2Z-m3T8NVeBnU2J7dq2lW0gibs7tfbkCFxjJ5OBmo5sJ-mUOcKTDIzKrkay_BnXim7470cRIx2tteHQ973v8K9_MfTsImjYnZZOG8SKXiC1ZG1F1W3w1rceO_YPiOJ4_ZpwRnIL1vVidXu4Z0mwip1Lxi5Kk4ZjGqBaiwI8iI4o40zSJvVXWJ2SUksn1nN-iYFLZLFb1M7DZ1lCdzbsBubwuKutvD5AUjZ2Pb5KBEaGnsYJjOV-JQxft_ity7L6dRmoKsrFEPxInXLwcsN1w/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACLjzDR75OSiTlHwA_PEUx05c6osYsUtVJfA5dltwlZMKk61jMDndGwb53O4QI_tMiJrtrqoI7piDNQHUVmQotV5vgFbtX-I3di6r4nPvHs8jqgRznvBAiBcGvP_Sug5molw-OBQLq7EBn-rOeAITAWth86bKTW_RmuLi43wtq4UDdZMyIPJIUiuT6WWSoglR7vmOHWb_NuAfuL9fiGnencXyXxClw2a-slnocQsqhhf9USdqUZJ25oBECLMoYOT4rj2ZA7jv4hbkhOs4Plbbmt0XLPxOys8tyVRC855_2BPYyx6lpjeT214ItzA1_fYCSowSN7jaUnMU1FvQd1nauKR/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'hsbc-bank',
    title: 'HSBC Bank',
    location: 'Kuala Lumpur, Malaysia',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJ3DSZBbgT8Lg5fRf3rGW-AIvUTMvsTPKy_-LskZribFcwGvlge6NX_PSKdw5vhoiFGW6hwliS1g4ycUYCnqFVfF-eZXOK9VgDwzGT8us2iYFb17deSnO6WYRjmBFEUCdCE3Axm4qjx6lG9W8bS395mwA8ZIxgyWpILErWKm8pGrl9s7XK23VLGNtMvfZepG3xXsKG8t-zY10QcNYXAKJSjQh7qFq8Tul9DTgtJ-7Y1cvPf3hkKaQGgr3-8g_ZaQIOJgpHz3HKxdejPxobzbQnVDajhOCIX2rDkJnJdDlhTZTHDGD2hkM8znnn-i0E9GB74JQJsZD8FQ7pPmGRbNWEi/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> HSBC bank located in Kuala Lumpur, Malaysia.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJ8ECCoNP5tcsvMxvI_2L0Zu7KXXBtmOe7XjeqO6aXJKRctzwY3f_e19cFgKlifExjIU-BWNSrzbQmep4VRSw9n1tnVhMtUGQZ3RLN0wgMbqHYxUV9ipHyC17Zky2Sx1TKsoYleXPvjRHOBtYQX4UXLPDsYMgJuUxfRPZy5hhn2_dgwgyvNYGdTUSUwG0vzxVCF7P5DcpytA8VUHkGxufzcLOwu0Zra6ZFmvvph932sZV6wt3aXEGZhZ2EalXCb2B_eoDvR8mMX_D5Y8kzgLNy2-0PJIL2FdKhtuTtzCPpI8CqKdpITJ8UiXkXgi2Bv85jIpeG6IJjKwrtnGFnU_Xpl/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACL7WEuEYBe0ycIgEAM1XdsTMExG3G22ecUsFIjv_KBset2l2R_lIMKYMO-1LXdX9bbf_vxWOSHdVDmrvVgvyUl5uTgFNjrk2P4DwTJktNyuT1vIkHvEzD6ZYNLhFogl-CRl-8HT7DRPgmZ-hJR2hjUuYVBLhmh5HQhv1MkCthZ4syoDiREXtNEhPkaPq0kFTRDFdGpPt7ognX2MN_FlQ5Yjnh57SZI1upF_hab13tDvuWqlMoFFIdzwDIK3bmCKsYj8TNlQNtS6WphIlwujgzJyqtVqaoyo_kWqroaydBKUJ5H1Ke8coSZI8DL7Y1LNtg8Gyg7Xxuj1kpoLaWXtyHNV/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ36l0lxv_I1leRB6qEXeVVCzJ_Mu8Sc9-_D57_jstb0x60SPJTp1QRNh63TgUzwyog8foknOtXTSfbxZbnMigrA2y937A_Qu-ZhxMg7tAPJdDxdwOk-V4Cve4EpPyFULlvyphZBVCdV95qiYQlqFMONttQn_q4LB8FUw71PwG3SILZGxzOGNE9Kda1niVGt8aW9pSZQ8nwLR2Ynj1jIrtRvOioXc87lM9YmvwHLU0J6SeNRNHJ-TtdOzQuQNG2MTKg7kInJy4_-FswHHNvwThkjmcWor8ud_VQ374m2odWwOPmJPRseMgOmYy5Qfy7YrbxqMTkwjBNYWkdnKrILJUt/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ1IPTd07CU_o8dx7sqojv9aGngZ7iDY3s1oa6gGIBjMXA8o-fXgDtVgn8lWP6r_f3FqjurJcilttr9Re-O1LyX5sdCMJUVySuPsLxzwUmZ1p41MD-WQE6lDdUs5CDkDX-wrGEQyVavBviffPdLYw2WBThi5huP-oHB5a1MaUbiOInkP9VGKdzRqp98umU76qXx9VDMadkcMB9Ko6FfWjOgNP2LPtl1BlMjBTQWNqioSmNY68ez21nwgZTna7nFDz4Agia71NhNCbF11g07kPL3X1UjF_hlLAFOCrHKbNBcAejjmU03EbKLqnj4rUKEkb8Ym1eXV0mnmJ5r5JKtqYf6/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACKPoBYzptHpmhUoYQoW9-puHiVQ92DsneiajWmkRBA4iw8xdVP12ErvChhSvxNAe5jq2ueGw_cq2P8SzT2u27zcPY-twriuFLewDz1nwr13hMIDzYNmJB2dEzGTc8JYe4jMqBrnx4fyjotQ2obblpwNmZTkrvMR7v1crve8WEFQijnZHlzdehgQKHzv4YtXUvgXUQlL452AdU5HzkyijJZZoPnSmjtJwqNBTYhvQxf5M6fznV1IvUeHCH62AC0LqBCLx1XweGFn3PZAqwj8U27fwU73D7GUWZJYIQxaSehQo1Dsbw_2usaLC6WQAQ-RWXAdFtcxMjMs0aVgo_R0HAU4/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACK0H8zRMXqHchkKF0tupzgLZf0jUaSE33JAtWouboLmsr6gvulw3Ejsv3QWkazT2ouLI0_vC8rEU9EtpuHj_kihQNR0MnCn7St9Y5CRKn7d3j9bG7WRbcTBgdpybNOJ4aN0O7cLybj6gBinZKNU5kvpsmAaXTxdeVHdO3NLrOYlN1zTXjeJ1uEZNatdFj5PMX9eoukvkptDQeCXWXgLijCDapqmyDjHDS_MIfpX59qY6JI9oNjBKzbnO9wRYgkvzps44lvy4odYzE05FO4HWyzNqPhlAY9isOHt9KVmAzMVz0hI39wDRSf4cTDUg011BaN5DbHDhIP7qwmFXGvMFHDsgL_ogo8oqxEBbLan9fHrrg/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ3DSZBbgT8Lg5fRf3rGW-AIvUTMvsTPKy_-LskZribFcwGvlge6NX_PSKdw5vhoiFGW6hwliS1g4ycUYCnqFVfF-eZXOK9VgDwzGT8us2iYFb17deSnO6WYRjmBFEUCdCE3Axm4qjx6lG9W8bS395mwA8ZIxgyWpILErWKm8pGrl9s7XK23VLGNtMvfZepG3xXsKG8t-zY10QcNYXAKJSjQh7qFq8Tul9DTgtJ-7Y1cvPf3hkKaQGgr3-8g_ZaQIOJgpHz3HKxdejPxobzbQnVDajhOCIX2rDkJnJdDlhTZTHDGD2hkM8znnn-i0E9GB74JQJsZD8FQ7pPmGRbNWEi/p.jpeg',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'affin-bank',
    title: 'Affin Bank',
    location: 'Kuala Lumpur, Malaysia',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACL_c1A7rmcBJo0Fm_poPPudaq1_9A2k3Nx29fNQ-J81O6bIo47xI97Nl9yzVbDotbfNyAZNyrEB0dfc7k0qnENrNMvk_WDTnOGhXooWKxAGX0QxI4eKhEjq1UD6KFNKhOP59LCNxlZOgMy68Nbwm8YE1Ci89VBctbYu8Ss5oz7NgZkdwW13w2Snv04fNZqlv4towisZmAzcT9p-qd2m2h8jAhGsDsYB03blaVDMcCWHuUKSn19FUMOIyXd4FP6LxX1sT6ljS42l2UClTaeBZ7DuJZEnOllPsh4cCq0LrmrTzVDKvJ2lvoGu71Lqs0uJ5P1UTFEyLuj6h9YVIcn31MlJ/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Affin Bank Project located at Kuala Lumpur, Malaysia.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJHhcom1sD30Vb7-U9Wf3jDoNFOPt5tZvoElq8u3SKjGfb-NYiO_33oT09rgsN6o-AKb_ZPdBBslh6Mxahq9GimlxAtpasr3E_3eYi8cT2d4ZKz8sF6tHp50y7KCQ2z63YP_jGAIs9FN3l5dpf-vW2S90iBd61TbgIjINpjhhy1frhGjS4ON71RaREryokvtIKj1ltGjO4hk3aMdCucMDJQ-h1btC5iwFTXSxSKmFEEONJW3wW4x-MPDou7-k0uu5dZoQE3lSej3vo55liOpKCjS3AOItEU2mmD-kf6em1Koea_KGesMMy2JEWYtXUGntqffVjNnwUCteFanCrUEsPW/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJHhcom1sD30Vb7-U9Wf3jDoNFOPt5tZvoElq8u3SKjGfb-NYiO_33oT09rgsN6o-AKb_ZPdBBslh6Mxahq9GimlxAtpasr3E_3eYi8cT2d4ZKz8sF6tHp50y7KCQ2z63YP_jGAIs9FN3l5dpf-vW2S90iBd61TbgIjINpjhhy1frhGjS4ON71RaREryokvtIKj1ltGjO4hk3aMdCucMDJQ-h1btC5iwFTXSxSKmFEEONJW3wW4x-MPDou7-k0uu5dZoQE3lSej3vo55liOpKCjS3AOItEU2mmD-kf6em1Koea_KGesMMy2JEWYtXUGntqffVjNnwUCteFanCrUEsPW/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ8dtPa2ojCp_vK6CYucioli8-ezxcRHGvGcwppZT6v_zXod4hWAH6UMaUAut9wOlRA7bLrIlHweeHOGRmNvOvZetjmFy2qlxhDKxijzpqCa0maYRlTzJJwP-FOTPX0CjNfYmO3p7SR6GVpRp0gub57hbOMe9XruRcipm76mj8_KugP71Nnpxzm2OZsvhHDmxjgW4La42hXB95pl3Ke33IhcgBVqDUJbc0ghitzvdtXYFhZoW_L6IqNq9OKKb76JJCDd3jTencCqQDWkK8wC4ThQPBzF3WA2_HKeGyHaUmxhPy9xOoFo1aPG4IaRk4PFyuEP5fjhB6hoYisC7RaQXcT/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ8dtPa2ojCp_vK6CYucioli8-ezxcRHGvGcwppZT6v_zXod4hWAH6UMaUAut9wOlRA7bLrIlHweeHOGRmNvOvZetjmFy2qlxhDKxijzpqCa0maYRlTzJJwP-FOTPX0CjNfYmO3p7SR6GVpRp0gub57hbOMe9XruRcipm76mj8_KugP71Nnpxzm2OZsvhHDmxjgW4La42hXB95pl3Ke33IhcgBVqDUJbc0ghitzvdtXYFhZoW_L6IqNq9OKKb76JJCDd3jTencCqQDWkK8wC4ThQPBzF3WA2_HKeGyHaUmxhPy9xOoFo1aPG4IaRk4PFyuEP5fjhB6hoYisC7RaQXcT/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ7hs43oUn6cr8HwO97qyicxL-FR0IPoVByy1z_sDWQeMdroLFPyyTXMg8_MjTk-xbGLn8y6NUgLUouRYeNJuQ-jcr1MTBCuKnueJFfuC5D7_XW9CqYf_RgAcWFKud70mgUhvBGx3AHhwNbmO4fBsV3fqRlrhRLzhCXwhj1Lq4TqwkSMkq6jzRaGSrlKkSyRpns1kYE5opE_RWlulydxd3h8KOUVhuPzkZcBduUaCJpjuSU2m8f5q5SsKjaC9PbnO-aSvipZkGiUUh695M6iqnGDt7uCcH2dbC6WfRUTggkjUavfTZXer2HA7IABWyhxxFuuqMvcQc8Wi5jZlpZTVfw/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJ7hs43oUn6cr8HwO97qyicxL-FR0IPoVByy1z_sDWQeMdroLFPyyTXMg8_MjTk-xbGLn8y6NUgLUouRYeNJuQ-jcr1MTBCuKnueJFfuC5D7_XW9CqYf_RgAcWFKud70mgUhvBGx3AHhwNbmO4fBsV3fqRlrhRLzhCXwhj1Lq4TqwkSMkq6jzRaGSrlKkSyRpns1kYE5opE_RWlulydxd3h8KOUVhuPzkZcBduUaCJpjuSU2m8f5q5SsKjaC9PbnO-aSvipZkGiUUh695M6iqnGDt7uCcH2dbC6WfRUTggkjUavfTZXer2HA7IABWyhxxFuuqMvcQc8Wi5jZlpZTVfw/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJfijw9C-C5S52zWHQLz0nEC7-cgYhRW_FpTjPhMgpI9tZEDl4-SAQ138Y7TxKKUCpgG0LsKU25rEtI2cK-lG8VtNoXA9Y8XwGIruY-OYzKkZlWnZ9ERzqOaLVrUE40cqq-whE6H_pDI_F8bzeFQUodZpF0z1ST8uLS_RNFRW11D9wRbixaJSfXg3f1K21JZ_um5EhxVvEjT3TjQurR1zEoe7jIOj4gIdppLXTMGMvknD7GdNzrTQiyXO9_sogQZwElmSXJ7U61ZfjunOrv6bv6UaK77BFPbt3C0ylaWaN7GwvJVdyF1v-_Cwec10cyJdfmMc29XQWamcVba39jTFlt/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACJfijw9C-C5S52zWHQLz0nEC7-cgYhRW_FpTjPhMgpI9tZEDl4-SAQ138Y7TxKKUCpgG0LsKU25rEtI2cK-lG8VtNoXA9Y8XwGIruY-OYzKkZlWnZ9ERzqOaLVrUE40cqq-whE6H_pDI_F8bzeFQUodZpF0z1ST8uLS_RNFRW11D9wRbixaJSfXg3f1K21JZ_um5EhxVvEjT3TjQurR1zEoe7jIOj4gIdppLXTMGMvknD7GdNzrTQiyXO9_sogQZwElmSXJ7U61ZfjunOrv6bv6UaK77BFPbt3C0ylaWaN7GwvJVdyF1v-_Cwec10cyJdfmMc29XQWamcVba39jTFlt/p.jpeg',
      'https://previews.dropbox.com/p/thumb/ACL_c1A7rmcBJo0Fm_poPPudaq1_9A2k3Nx29fNQ-J81O6bIo47xI97Nl9yzVbDotbfNyAZNyrEB0dfc7k0qnENrNMvk_WDTnOGhXooWKxAGX0QxI4eKhEjq1UD6KFNKhOP59LCNxlZOgMy68Nbwm8YE1Ci89VBctbYu8Ss5oz7NgZkdwW13w2Snv04fNZqlv4towisZmAzcT9p-qd2m2h8jAhGsDsYB03blaVDMcCWHuUKSn19FUMOIyXd4FP6LxX1sT6ljS42l2UClTaeBZ7DuJZEnOllPsh4cCq0LrmrTzVDKvJ2lvoGu71Lqs0uJ5P1UTFEyLuj6h9YVIcn31MlJ/p.jpeg',
    ],
    featured: false
  },
  {
    code: '00',
    slug: 'forum-tower',
    title: 'Forum Tower',
    location: 'Dhaka, Bangladesh',
    scope: 'Facade/Interior/Landscape',
    year: 'N/A',
    image: 'https://previews.dropbox.com/p/thumb/ACJHhcom1sD30Vb7-U9Wf3jDoNFOPt5tZvoElq8u3SKjGfb-NYiO_33oT09rgsN6o-AKb_ZPdBBslh6Mxahq9GimlxAtpasr3E_3eYi8cT2d4ZKz8sF6tHp50y7KCQ2z63YP_jGAIs9FN3l5dpf-vW2S90iBd61TbgIjINpjhhy1frhGjS4ON71RaREryokvtIKj1ltGjO4hk3aMdCucMDJQ-h1btC5iwFTXSxSKmFEEONJW3wW4x-MPDou7-k0uu5dZoQE3lSej3vo55liOpKCjS3AOItEU2mmD-kf6em1Koea_KGesMMy2JEWYtXUGntqffVjNnwUCteFanCrUEsPW/p.jpeg',
    category: 'COMMERCIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> N/A</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLndN9Wwpc39wL-yuLRHT1LGZCBB23xoDg15RrPSUWW7qWoKbMuB4yz1rG8H6d6gOIegXL8sOpY0cXTZr9UavOs6WfqtNsqHwRvsg4PoI-BwF4kWQJbET79bnG_lVV9fttdjXeDXdAji2zVOknZ4KnZC7S9Q6N0OVt8jt1PlQPl9E-evhEKXFIx-zab9uZqo_WyTy10iyrGTLMchgLcbemzHxN2BYv_3ejwcLEXHuNAxTSJMevBhVP0F8RuImv8tUJ3Deqdtp4tRCu_IDD-sa3nL7iAI9n5ountgdmIRzH64a3ezakYd3W32NQ_maJ4SRAhvDW5QJ5dUirXIjfSRVnR/p.png',
      'https://previews.dropbox.com/p/thumb/ACJjd_IiWOLYz4orJjAp8aHq4hnAfhRlJUUvEWvIYvI_ZzTyP24TKbKfbGRphe5KocYEujs0i6LLpDrvTYoxC66r-fNBLCCpIyi-jmWlJOyEs3Mt5K-oPjOOevNZHJ-h9fQkvq62asBeKlvHz3wzD_QKpV_Z9Q8KmUbG26pWRq5_2D4kpTXHICmjtb3haJTtMolvoCw8TYBfo60hreyQpLCNm6F3An0KfKVYUPRxhcgM9ipFvH4hT0NVN7oU3tfo6gpxfGRI4mYbCLYWtVGpv9fnyYLw0P5uW-Rv0nwl0-CUVEggVI4mcVBQQK0bU9TE_pxBLg_ax3qCPVPWajFWhfjE/p.png',
      'https://previews.dropbox.com/p/thumb/ACJHhcom1sD30Vb7-U9Wf3jDoNFOPt5tZvoElq8u3SKjGfb-NYiO_33oT09rgsN6o-AKb_ZPdBBslh6Mxahq9GimlxAtpasr3E_3eYi8cT2d4ZKz8sF6tHp50y7KCQ2z63YP_jGAIs9FN3l5dpf-vW2S90iBd61TbgIjINpjhhy1frhGjS4ON71RaREryokvtIKj1ltGjO4hk3aMdCucMDJQ-h1btC5iwFTXSxSKmFEEONJW3wW4x-MPDou7-k0uu5dZoQE3lSej3vo55liOpKCjS3AOItEU2mmD-kf6em1Koea_KGesMMy2JEWYtXUGntqffVjNnwUCteFanCrUEsPW/p.jpeg',
    ],
    featured: true
  },
  {
    code: '14',
    slug: 'mount-elizabeth-novena-hospital',
    title: 'Mount Elizabeth Novena Hospital',
    location: 'Singapore',
    scope: 'facade/interior/landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKwoK1j3R5P73GEq879swFOLcA_TF-rO2QnCVw86pWOcfvaZ7Eewju5ed56pgeviDTY02abh9ZzFElwMlmf9t88vxjXZakEPa2FhhoGiyGc_9oQ4ubgITHOv7nzNeLSngaBZzQ9xSJYasw7NEl1hmeOV0AMbIBI9U1HjdgualEBeobBCpwMAgiY1T-AEh0jEpzD805ok5qzfwPtewRWwwVvWNZTfL7XrQgr_1LtJKL_1NPe6gHWGzwwWLvet2dTZx-B8oLhhUIfPfJOojDZUAQgSVePz9QfJBpIK66yuGJ_VBCzOHQbGibYcnXKJzi20csh1PIEqmhK9UKKMh2fuQEP/p.png',
    category: 'HEALTHCARE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Lightbox developed a lighting scheme more suitable to a "Hospitel" ( Hospital + Hotel ) for Mount Elizabeth Hospital. To achieve an ambience that conveys security, comfort and a touch of luxury, Lightbox proposed a wash of warm, welcoming lights in the waiting area that continued onto the walls of the wards. Carefully designed, multi functional fixtures were introduced to the patient rooms. The goal in designing these custom fixtures was to united all the necessary lighting needs of the patient- examination reading, relaxation-into one device: a triumph of quality over quantity in the lighting context. Color and temperature adjustable lights also resulted in faster healing times observed among patients.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACK1RzwPnG61mo0TVhFz_7Vq8MGZeZF4y0B1alkhfk-J2nQnul8sshvOPZv_A-x_NgR3fjHNSbprxon-y7m_f9z0OXJ3-UMMw40XdbwS_fCK3K3TLoKzeQyTR7n0LtNCZU4kfmqkP1xtiwn0mGvLbdlVDUNNtSchW61wB7VzW1qK0asMZlQd9Mejlscs2K-0UUcuG-nIIcr5wicqYn4i2xML63azzVLtWnktDVrfuTwfTYXGjpQS-yvwQv9qhIzV44aNoZZCBk07tBFzUthF26v2jhdqhsX1rTBN_O-5CtSo9ywiYRvmICTgqIISztCs4B6amw7KfsnG9deFOQQfK3A-/p.png',
      'https://previews.dropbox.com/p/thumb/ACKc5NzRxZwb9s8JFMdi16nn-MF8LDm_Mzadegv6JX21Q7TZNuC-WQYNqkPf8v_HwvoXpfcBPpAiQ66YLdSIz2hX7VeLpbBly4aEShCB7gaGJlw8lca6k66viGC8mXIW_bdyWaCydUWPSnhz_ROcnZJFezpubbPV7rSGN1oLrYeHIFIjivtG0Ho3770FMPbPzQVSFArD-ZFvWiCU49mLqKKTuSZW3QhuDLNZbVQKG_dRjDblu3pLdZo0uLmZ_XjFug3bt47Y4c-Iify7Cp1rc5hRKWhGvUhY8MTIE9m9bRY-auTN3WKNHswrEO8it-qboJSkQtF1kxhpvA94PFHUV-mx/p.png',
      'https://previews.dropbox.com/p/thumb/ACJe6-5l2I6rGOiVciPxDIDoF5KHQKeN2GmyNxlgaMoNfIQkPjfSumV6cWWqjQFX5OH5YZEYbwsHO-SLXDNxiT0Ai01TIY1Ws2wIDqPpm7ndl7MDnGNDxrixl09IyVcHsc0n_o_W8fG5cVv0Lbd4Z2w_rJYn5f9Qwrr-cxD6u9OvEXInJ3v32BL71RhGhmtpJLZ2Dh6LwBaaPMhEgZ1uTFeckDP6hg2HmqUeHfBgbupasjo-NNqUZTHj0-IaYth3Flf6ZSeX0dm4ZSKzBREXxg3TYugSNFdy0ggjAv0_WEuvuxMYOJggGXZND7WV8Z4ewr6Izvw01VRu-0hxUP1_ZMMN/p.png',
      'https://previews.dropbox.com/p/thumb/ACI7PGGI-TGv_oNBIFKTVnRoSCOncoYHVIBn5fjYm_Xl8KUM6sWIXQYWSq8yLN96zo1Qpg66mCsGzSXFPbM2Jv-JEZ5t2nbzh2S2W8mTJpMja80BN7aVVHg8U3f5Byvv2VCQMvHOZB8tF842u9R1YSAfxI23i_ErCkbZuG3DQOGZLPLle2yRauj-_jzf1JWduDjhR23zc14ZwCUCQRRPh7vWD9Km__bzmgZMqz0WjiW17i4N3y0763FRShkMGD_452dis0crsRulyikqweqd6m6BxKouOaWQH8bOVIP2FEvq7QzkMACdo-9FXrwoA7wZByt5hKLladvoY8lHyZ12_M81/p.png',
      'https://previews.dropbox.com/p/thumb/ACL4Nea2JJbRPVlu0XL7RNrUANaMoJ5OJsYCkjnuWr3z2a90OJex1iO7X-w0jZhUhD_Pl_YmVGO8jgPNybk8MQLoZsjAMLJ4nu8s8f1VCPttC47X2wkm6kLumqwU26f-D8avxsaUaWM_YQ81NT5ysLHkUBuLRGPuhg77smOqpx3xpPxFSU8q0dh57QBUXHFwiigJ96HewuKpDen3unbS4QFrZYWbTq35MUq42qWsoT2oOGMtsdzyjg96Z7hzudRVJgc1yHR5HMrYg61bSTNFUQEpL_W1ahYIWg9lO8rIoNNNsNOdyKojMvwzmrgTTsQx5UMLtqmrMOF0DwiXIB9MiYMU/p.png',
      'https://previews.dropbox.com/p/thumb/ACKCML-R85Nz1f_BF46TVAJ9_oa6pvoze0KaIWsCz1YOEJkKPPXEele51GJj0_vWf4kuxH2gYUYyeR371nVX1M51NUqTMvAoJjXBx0bnAeszRp2pSACUpHbBzj-sW2yNGD7FlUPX7ZgBq0shNqpkFt-VvgzxMquNzaFG9RkURn6VtbZ6_CitzDvgc38YCy9ml2UD0LuGrBstETiPT_fofBJ1aci4thRjatjiZgPlAiUidy02UHT_PqT11bO73kvnn-BeCcoOMWGd9SaK_3dLa2GpAWdxTX3F9ABE_YoNGe8yt6QrD0taWpLsV3_vOaHvUtLJCOq_wmfD7BPumFfb1k1X/p.png',
      'https://previews.dropbox.com/p/thumb/ACKwoK1j3R5P73GEq879swFOLcA_TF-rO2QnCVw86pWOcfvaZ7Eewju5ed56pgeviDTY02abh9ZzFElwMlmf9t88vxjXZakEPa2FhhoGiyGc_9oQ4ubgITHOv7nzNeLSngaBZzQ9xSJYasw7NEl1hmeOV0AMbIBI9U1HjdgualEBeobBCpwMAgiY1T-AEh0jEpzD805ok5qzfwPtewRWwwVvWNZTfL7XrQgr_1LtJKL_1NPe6gHWGzwwWLvet2dTZx-B8oLhhUIfPfJOojDZUAQgSVePz9QfJBpIK66yuGJ_VBCzOHQbGibYcnXKJzi20csh1PIEqmhK9UKKMh2fuQEP/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'christ-methodist-church',
    title: 'Christ Methodist Church',
    location: 'Singapore',
    scope: 'facede/interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKzOH_Cjb9SZpV2G1vwNVIQV2de3y6VbLH9q9qD7cAFDSk2mtUn3A2mGV4klFkxmjNDvX1w5RVEZGjEB3x1cNkYDwdoN64xtXPLPxWsYbuDExtCAwN5V3z2YrmG1Releg0SOwWh0UG9tsiIQ2O3ml7QA2dGiGMY2gzqln3GGKhrZeJwuG1dBX_N9AW3C_qDyEUavmzXg3YT2Tjfeo6STpqOqlbW_BqTHPDg1G6h3wvcmcywhwsyitXvSas88Obxc2AVWrxicH_8nQvoq4xRhgufOBDVRo7xDi6wBp9HuhLwdEdRMxXY4UXtqETjCLt31VS4yhqPSKNN7TpUwd7pLjW2/p.png',
    category: 'INSTITUTION',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Through the use of lighting, the contemporary interpretation of style of the church is at its fullest when the iconic crosses shine their way into the night, seen as almost angelic. A very different effect during the day whereby the crosses are perceived as an aluminium cut just like the symbolic punches of the wall in the olden days. Interior lighting in the sanctuary space are warm and indirect with layers of cut folds. Through the interplay of lighting elements, the different characters to the typical church itself were created. The detail of little iconic cross integrated with light makes the small crosses seem to be floating over the facade elevation. The overall interior lighting in auditorium and the sanctuary presented an uplifting ambience and change of atmosphere by lighting when the different functions are required.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLJ6UhTDy3VW2X6be1CflFLLM1Fklp-Pk8hxRdtWeL7Vt98l-LXoaFL36XRUIAaSF_kn1UnkixIF01ldzo-WlvJOAufWgRY00_6TcPBBAYByl5e2v7zKghIVlhBKbcfNh1UuuftdJs7gITVwyIirwMnZSygLV18QTh8qidubWbOT8W_KqxjdH18NMj-26_NZ4rSkWaOOfCD3k2PIVUavW01rT1V8iQ2iEz0EcqGJHn1R7zJUcIfT8rhCOZP53ZXglub6GS0yZhoa_DvvcEyTD4w5sIgeDNr8dSuW6g6SUAtjY4Eoyu4QKE4FfElAlwcnsDm7Jfcs3o4n9MelDWNGlGV/p.png',
      'https://previews.dropbox.com/p/thumb/ACLq0qeBt_okSFeIVzGEOPiKYT-yMa2ngkDvHX4zRbYaQye0eQ9vclcwk7HvBjxWHool7uopGgQnwUlTAi1fmNgITPCkAwdian9H8Op17_x7hPfrz4UKzt_eilrRs3xuzLbV8OGSVBQt1SS3v3p8Jdrdt-B8SgFPNmKaN69AuR__LScJB6m7QelPcQw73m-hk4oKA_loRY-_0Qf16Et9A2jfSfYT5kbfAZUWo9hQelDKh6bkowyiJo5zbmFwr-vPgIb-iCeM_tu9NO-YTQ2RH1dbsEIABHkkhCFPkRLiu37CO-vYC4m0I238YWOeUVRNQEBQ7y_bwND2lw32nWIw0soZVmkGnnlAyyBcWBhP4YMyrg/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ4_-HiPyLTawwLi3D07ZZ8rt5dIifhHFtEiPEZzfBEWRLk_f106u6I6BJt9lWy3GaDaK-UR9g0BA1nHeVwh3azyXbk27h9QBzBoN8kzIDvCZOBu5gvUHtOno4RBz2b8JbaIxq6r2lkvvQ86SF27faucBtjelNAiDW-XiLOAZ3dm0A1k21FbC9eqaqY_qHoqUrKbsMloJcDFfw2nIkluBjIEPa9bjOuUN9xJW5AAB4y66TIveFiPpuuF5tSJG3gGPg_Nic_aaVWKuiHrniGXJz95Zjx0Af2cta9ZpmCajo7Zkicni_TFQSsC2469ISs_4YsVUb6fbNyjPVB4QkW-vZYHgAOeIpxrqobhcuLzh8fjw/p.png',
      'https://previews.dropbox.com/p/thumb/ACKzOH_Cjb9SZpV2G1vwNVIQV2de3y6VbLH9q9qD7cAFDSk2mtUn3A2mGV4klFkxmjNDvX1w5RVEZGjEB3x1cNkYDwdoN64xtXPLPxWsYbuDExtCAwN5V3z2YrmG1Releg0SOwWh0UG9tsiIQ2O3ml7QA2dGiGMY2gzqln3GGKhrZeJwuG1dBX_N9AW3C_qDyEUavmzXg3YT2Tjfeo6STpqOqlbW_BqTHPDg1G6h3wvcmcywhwsyitXvSas88Obxc2AVWrxicH_8nQvoq4xRhgufOBDVRo7xDi6wBp9HuhLwdEdRMxXY4UXtqETjCLt31VS4yhqPSKNN7TpUwd7pLjW2/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'galaxy-development-chongqing',
    title: 'Galaxy Development Chongqing',
    location: 'China',
    scope: 'facade/landscape',
    year: '2020',
    image: 'https://previews.dropbox.com/p/thumb/ACLJ6UhTDy3VW2X6be1CflFLLM1Fklp-Pk8hxRdtWeL7Vt98l-LXoaFL36XRUIAaSF_kn1UnkixIF01ldzo-WlvJOAufWgRY00_6TcPBBAYByl5e2v7zKghIVlhBKbcfNh1UuuftdJs7gITVwyIirwMnZSygLV18QTh8qidubWbOT8W_KqxjdH18NMj-26_NZ4rSkWaOOfCD3k2PIVUavW01rT1V8iQ2iEz0EcqGJHn1R7zJUcIfT8rhCOZP53ZXglub6GS0yZhoa_DvvcEyTD4w5sIgeDNr8dSuW6g6SUAtjY4Eoyu4QKE4FfElAlwcnsDm7Jfcs3o4n9MelDWNGlGV/p.png',
    category: 'MIXED DEVELOPMENT',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Chongqing is the new developing city in China. Hence, the concept of modern contemporary style architecture enhanced with lighting, is best for the atmosphere we want to achieve in the overall development. The context of the new city uplifts the emblem of life. Inner court of shopping areas and offices are designed to be the spark jewels in the entire galaxy development.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLsZIOt39YOwraqvGqhDYNXVzfAN51usqULrguOn6gWROrz29rQggkmGrJr0Yv2cClyH72GxLVcEKP-ltquWwODWTd8NDWqid_NEGev9vK9_tpuy3YQAyT8D9Etr665STGL10NzoT4iaOFrcfQAmN_3Jl-y4s8eK7Kb4LHAQVwNGdugU1OJF4IDizkHArE0S8spJA89XhT04dR1YR952g8dEdClZ-6TD5v40nnr-nQx7AuxtMcN93RViryTfdElNn1pQYXflcdlbfaoZ5MaQ7BSAnhA_gHbarj9hLtIWs3cvd7tQNWD7MdeuQdh7fBzNMeccKwPwobZ92OmwELEDjlWQoRAvVrKV-2SFrWkoJ2EVQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ_EBYyNrpRpQfPHpk-Q_CqlMoo_OU8aGwbqBs3mHzVgDf5GHUaG9lMaWN8X7x0FQlKgrfCUnu9qJiiw191i2yYn7ilL-gfvhT6dySh_pDMOCCo68lUFc-aSHSUjeiw-s1MdVTRkxMBe7b1bsQhMjirIh2Heul5JnHY-pIbjOYGZP00YxgYUko_SUHK33OzwyuECLzvWT4hY3vlzVhCF-UPRZXOZqD48McMfbunuAt-IOwmxqJ18mV0Nfkawkj7XnjNrKdmT92iw4V9onEhUSKU1IqryHhw5EcZT9EosCpaQO5dlbbIQXg5nHgDtGHyuylj4ylD7-8mUTSSGl1XjHEo/p.png',
      'https://previews.dropbox.com/p/thumb/ACI7HhCAX-MJd4lcFhvjn9dKy3v69KfwsQPPp8kWYeYIRjDGnVPM6gI-gemN5_m1ovX4R_5rHv7LJao6YJHsQ7B3aqLIAf5fXiJIdkstgjEqvPQP5iQj8J4Idcva_qbxDhuf2Syc41_qjW1LAVeneJs5bntbMd2yI-aVTxNQhBQ3f6bCodGFmKe1hOvh4Dg5Cb19uwF99fLNmG20wog10ZrQnekqdHV9F1O5rna1pRppXdjiTXJ0KCxT1SYX4ILY1URXoQHLPV61nkZaUQVOa6l68jILqrUUuayNduCQoSHrCbW8bDXCEGsEZRQAUeUFO7sNK9C1a7VY8_b-J1dJqm33/p.png',
      'https://previews.dropbox.com/p/thumb/ACJpTGH6xLJbh-qZupjNJQ_RdbGBnEQSNbmUqyGjRMKp1qSkd2jAFT4JHvffIzrqQt8SWYwDuvpqmzpsZuB79hBUAfjTcwcm911hjlsLwZcAF3fzMjcfQQ4vEP0YM7oDWOTSPc3chKqhiOBZlzEVAomwj9dm59QSN1zEMzpR3oDFSvu7LctwKxJKz_IhUWoj1dmHEI4N6GkvD8By1eFxYVicx7zZnnqnd0pHFWyXL5ifxq4uhfia9oGmDzvyzZ2zYCOjMWuht5NyvdXu7Q4GRtBsXJe5qw8wEBj-XTFj-V9yVbTBpCuXP7cR_EmeksNzW1oOlNkiBtXorultw-w3g3BS/p.png',
      'https://previews.dropbox.com/p/thumb/ACJpTGH6xLJbh-qZupjNJQ_RdbGBnEQSNbmUqyGjRMKp1qSkd2jAFT4JHvffIzrqQt8SWYwDuvpqmzpsZuB79hBUAfjTcwcm911hjlsLwZcAF3fzMjcfQQ4vEP0YM7oDWOTSPc3chKqhiOBZlzEVAomwj9dm59QSN1zEMzpR3oDFSvu7LctwKxJKz_IhUWoj1dmHEI4N6GkvD8By1eFxYVicx7zZnnqnd0pHFWyXL5ifxq4uhfia9oGmDzvyzZ2zYCOjMWuht5NyvdXu7Q4GRtBsXJe5qw8wEBj-XTFj-V9yVbTBpCuXP7cR_EmeksNzW1oOlNkiBtXorultw-w3g3BS/p.png',
      'https://previews.dropbox.com/p/thumb/ACLJ6UhTDy3VW2X6be1CflFLLM1Fklp-Pk8hxRdtWeL7Vt98l-LXoaFL36XRUIAaSF_kn1UnkixIF01ldzo-WlvJOAufWgRY00_6TcPBBAYByl5e2v7zKghIVlhBKbcfNh1UuuftdJs7gITVwyIirwMnZSygLV18QTh8qidubWbOT8W_KqxjdH18NMj-26_NZ4rSkWaOOfCD3k2PIVUavW01rT1V8iQ2iEz0EcqGJHn1R7zJUcIfT8rhCOZP53ZXglub6GS0yZhoa_DvvcEyTD4w5sIgeDNr8dSuW6g6SUAtjY4Eoyu4QKE4FfElAlwcnsDm7Jfcs3o4n9MelDWNGlGV/p.png',
    ],
    featured: true
  },
  {
    code: '14',
    slug: 'tengao-project',
    title: 'Tengao Project',
    location: 'China',
    scope: 'facade/interior/landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJy7_k67XSG1Gw0eEQ70BHocVvQ8KAgJrfZW6YHAZU7c10iQbvL-ReA4mYRDee3UInS1FfffMH0Cqn18IS5IXTb2PXRHM-UqYFNNCXrDv_iUhBHVyLEeNE38pdH6Jrr944MERfVRC2VYcr6j4Z8zwzLT-ARAyNz76t29tb7ogJ3KdA-lcitMh6uF-13pQh7bI_wwTF5hrc5y_ZXwK7OjEGtRNVkexNo3KwETllaRDxi1frrwyMXoq-d8Om0UeV8OGSKQS8oXEUQS5RmCSxAs7-kqL-jNBGZaThQmJLXtyD8OiRCXg2g2kCM7YPe73XBSd1HzlEQJNMI-4mXBw-mmg65/p.png',
    category: 'MIXED DEVELOPMENT',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The Mixed development of project Tengao comprises Retail, Conservatorium's, Spa and Hotel villa. This project envisioned to create a new way of living, but at the same time preserve of nature within and create a living space surrounding. Lighting is essential in many aspects for this project as it has contrast between night and day through the utopian concept of dwelling in the forest of tropical Avarta is sparingly emphasised with the aid from the lights. Surrealistic lighting mimicking the nature in the cynical way with exaggerated lighting effects and colors. The elements of plants blended themselves to be part of the lighting fixture light the fictional lighting.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJKoja1RCYxFsp6byLekd8rVmfGYoRmfZUlP5oiE37Rr7cBW5U8eVDMSwIH8-Cb7Chb94Nz2C4IdLsigrMcKi7cm-mKasd92dK-FKFyFzm8vvI4rsssLHTasiTztV5uoauSYuuHr3Czq5qKufNl-k9YmvWlI4g9G0LocxUokmv2AXmZPhwHzOsOsAShaMpJS-AAImKbxGjqXhFAH_sj16ZpXTIqh6zv5VbS0MLvyom8849cdsc9m3SnxzoGc2CvOFdeja1PC1kxSTjtE2JXRIjO7oDv_uUIX8SrGULEX0l6P9et7vlk5-5s_6q1YbZVK--cyD32yaTl2sC0fX1_bFQ2/p.png',
      'https://previews.dropbox.com/p/thumb/ACJJZKDIineOH66i4pzbTYjgkOm1mbbixVYmmjlLqLriCQDlb4lcIEvoW2IUkaa-90UwyLdIlyZW0R9IrVO89XNvR_36TzHlX8rKbZUzO9mZCzm7dPfmRaVo8oZgSGg5o6F-CF0Kqdb58F6ewWuR7tOMu5IgR2L7mUBpQLEHRHQlU10-dhzHlL8xTbkr9bzvAIcUbeH0Y6evdrUGF9_TMOU8cq0mC0eebyRMbS1_6VSgbS8JdoJ4OkZPbbCJf2HpnOCKUdLiiTc0BiGiezf7GutYHjQkwsAZgfJ2mz-klzr91voQi1ybzTpABPrJt6WALSLh2p0lxYr070fWAqeZQX_p/p.png',
      'https://previews.dropbox.com/p/thumb/ACJy7_k67XSG1Gw0eEQ70BHocVvQ8KAgJrfZW6YHAZU7c10iQbvL-ReA4mYRDee3UInS1FfffMH0Cqn18IS5IXTb2PXRHM-UqYFNNCXrDv_iUhBHVyLEeNE38pdH6Jrr944MERfVRC2VYcr6j4Z8zwzLT-ARAyNz76t29tb7ogJ3KdA-lcitMh6uF-13pQh7bI_wwTF5hrc5y_ZXwK7OjEGtRNVkexNo3KwETllaRDxi1frrwyMXoq-d8Om0UeV8OGSKQS8oXEUQS5RmCSxAs7-kqL-jNBGZaThQmJLXtyD8OiRCXg2g2kCM7YPe73XBSd1HzlEQJNMI-4mXBw-mmg65/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: '77-town-centre',
    title: '77 Town Centre',
    location: 'India',
    scope: 'facade/interior/landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLfqbR3AASexS5CaOVMVMrXWDj5QHMxg96xeTnVQt_7d3D6lcO6Q58zDPSVbF5tQZ8ssRiCHv_eQjrbDQjLespqYmT0pGNSKj_XG8D2Yku0z6k0GbMox98dCz51vb8bfmSO3SZir5wM25joXrs44SCPiKrxwKwpwXBaYMiXTof8UAx-2arUSCjITjuGTachgdh80kmKmtd9Gcd2r74a3w4P25aTnX8YWaCymZG5kmvM-922mTkvO2od72VMeoodb_fAs-sPFB-YvQP8o11OTmRkMFbrwKjiHgjl4UD3N7UmtzGWN2_n4srKp2iDV-HAIhvHiLO2dx5S-9Y7-cRNSWIRpSXgmIqfHVlt-LuLF3bHhA/p.png',
    category: 'MIXED DEVELOPMENT',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> N/A.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACL2oa60-znMbcM67fRbV4B27SW27aP4xd1UKa-xtWazURHq8XWejJrLZrkChMSdzQ5FyDUAEyyY86qnDTXCBfuejLLB2cQrXCFMnY3UsuO8dfUFn1CqnglD7ugeSyHwAXVGOhqFVpPrG5G4WJ2Y4RFeC69AgDq2KKO2DPRs3OYTWbYFuM9zVsI1UDgGWHhwW1IPbR-GGD0ioUEnujXHaMKNz7qjxR6RFyH1_UQyaj94nQ6OgLaNRt0pNwidYndZZJ0OUS3KBVW89N2afHTeEF9kUHioyAzBcSZ7vGK0ADoSXLnPC1AGS63Z3vUJ4cbADwqcq8RolBirTE95YMgoM2ky/p.png',
      'https://previews.dropbox.com/p/thumb/ACICDOwmZaw5rw_EyfRVgzKl-UM-5MKmoSv3igo3YHUnu0dlPOa1dFx7dHpKrN6lxE8fpxmYKgarJ8mHX-Oo101_X0oeJ9vest6udavmlVLQ1nVFP97kCzkRo9ac-XEOMMboLkHURPc9qTNqDPi9EIUMjALnqKHsNUmeQsdfQcS-RXwqqD40i07F1FVmnLzgGEmKRbH7F4L3spkcFqORA_vZycQSD93LqU4dVJNzGU4OXcXHO6b54qLnV-CLCQ4YUA8m9C46emqTkkwvl76kP7tIva9oG0pD09pe_PF6N8mN0AhHFg87p9JlBcQzUAy-KXgna244aM3M9QBZrgsgh0Hs/p.png',
      'https://previews.dropbox.com/p/thumb/ACIEPXWTHzLLrMM6q3sf1OpxDqCaRfZ6-Gkaxd5ST90uUv3z60m6f5tVe9pw-BV2FYJz2kRgGzn2nPVWW23G0Oc5sFC2FcAAAqFw_YYpTqBeMNSqoYu514_yY5XGm6ClXT7gxGT6-mpHDzwkmyZw2xB2okNz182BEpJumPtpaWJnU8yPyE3y-GZLpSARvxnWeuI8T9sQgegLzD9iTDCo3CKUsjEX5iVPqZ-6SMjJw8ow5_dAc3YYkpvf5291XTvHdmyeuQU196wu4-d_wLSJpf9Q2GZKYDWybsA92Bw-JleJcY2TcwCq8arsuY8GZXg8-8RweHPsv7Oh90D8lqVz-2-Y/p.png',
      'https://previews.dropbox.com/p/thumb/ACJWFSEO85coFxJnU7KbREM_3vjsY5W6CyYUq7lLZiD94stKcnp6qQBMZH_sGfuwz1eUe3S5yVSTznOHIAZJehjJpxj8uF_kHwdHLm3jhIIwSJFfvOu3mcSdZcMSzpLobmCHG9DUhuq5bqSMtrp6UiH0gCjMYIc2_c9VX3UJMAwCw6zhFxtCxx5B6Ctc34Aa4eV3ZNTyf1B_3Z9JVTOFdCLhSokWHu2C6bJluETe7CEKzCJMiNdx-KI_5u6Fyo2WBgf7Cl7aYP3IOfbTgPO5uh3xLhMdqiHU3CkS8S20FfZ9pNGqez5c0KwrBc_DEQkWvM6ue4Qa2SvNtFrXlYFYhM_w/p.png',
      'https://previews.dropbox.com/p/thumb/ACLUL8LYSwFkDQ0_5ZCvVgoIuESpZ91tbLHf-5xhy7KW55cw9saLwYEeZcE9vS9D7UC1D1sWBYWZiWdq9JpywxOiGCQkcWzIykDyU-wTQFS65l4D7n6yyX6GS6tLg_-7reDY-Uj3h9sb0qd3ODVcLuUioMFQMSixOCyxfjKIbfRWEqQXyZJCKVUwJapGLG8kW6J5ZQilSiFbAyDzD63Qv5GrdhBBNReicj7vCtgLgo8t5TUkSC9CeCroKkxakXm-xPErPkjgJI2MyOUGH-kWuGuF1PjmwbomrCj_Dj5856opXnRfzplP-250-dT_Mp6LRGevYxrMEy_VqrTB3S2lMavR/p.png',
      'https://previews.dropbox.com/p/thumb/ACI2VfAPg-uj4NBSLyxAzoTmcVIviKzRcAgcGRIS402k-JE9sA-m-boBpGXtMZuLpiDUjGQptQEMNtjBNQEIK6OY4Yhuafo3Xk3Fdt9cBXSH2M3uJIgYovYsUVEgwNc4_dAbT62FGiko1WoRcnroHUEiqd7yN_fBgAC6uuawu4-USmkTt_5uBEfIJZuL0sZcZFiDSn0R2NbgiC0hkx-eGTe1RNupOUXD-MWZpPnkuHPEX3JTpHHT8j5kNxkHlJ4CPtofVgF4zdfrLx0lWxZeYYiGLTSYwUZnHO4y-Oe8AnyUllT9hrQIeVid_Z5Q7qCuYhwMXp_fWTFhVCWJMMdHcFzn/p.png',
      'https://previews.dropbox.com/p/thumb/ACICZzaXiWmk1S9QVYZz7j0AWuCyr9sK4Llt3pWqHLyO26vSosroNustivBJVmaJcB8c4s6WGbTopDsF6--YZpIFzAHEX1mGVFHBu7BVFc6O-UAXITPNIPP_QfwuB9Fttfv5HCWn8W-kGgXTZmoERAy9J7suhTOwXxIDm7g9Os71mrqqlRZ0ev9-Ks6ZV9f7KX5XmeA2UtpolDrCTcQP3bdVyjiZjZgpBV5WSWnA7GtiafA7miJkahapl3zmCD_P8nlYZEk2YGJjHeS2qRsXjQ3PV5UQi9U_58V5-G0jOqBj0Mq1xzq25EBOVGpfKwyHF044UyOnokPb48cD6VFPsNeH/p.png',
      'https://previews.dropbox.com/p/thumb/ACLfqbR3AASexS5CaOVMVMrXWDj5QHMxg96xeTnVQt_7d3D6lcO6Q58zDPSVbF5tQZ8ssRiCHv_eQjrbDQjLespqYmT0pGNSKj_XG8D2Yku0z6k0GbMox98dCz51vb8bfmSO3SZir5wM25joXrs44SCPiKrxwKwpwXBaYMiXTof8UAx-2arUSCjITjuGTachgdh80kmKmtd9Gcd2r74a3w4P25aTnX8YWaCymZG5kmvM-922mTkvO2od72VMeoodb_fAs-sPFB-YvQP8o11OTmRkMFbrwKjiHgjl4UD3N7UmtzGWN2_n4srKp2iDV-HAIhvHiLO2dx5S-9Y7-cRNSWIRpSXgmIqfHVlt-LuLF3bHhA/p.png',
    ],
    featured: false
  },
  {
    code: '02',
    slug: 'amari-johor-bahru',
    title: 'AMARI JOHOR BAHRU',
    location: 'Malaysia',
    scope: 'facede/interior/lanscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJeqCvY9N9l9TZ3OY3ywLdnK-xpnD4bkgfsWitlclwO3Z3manT90um-ICGIiR7idQ18k66TPmluG3a4r0bMeTAujhLWnKN2E5mi8i-sQvLevrMHox3ZXGTscE6VSA6ORc_TbVWI0CmEo2GFE30KHQTs6hIwZnbMmpy5IUhSW2X10gzuFyl8tvfJNVla5LnWN2gB-_acxV6I_XPasKpv1kibzsL6-dahDSH0sGjpBqS-a76c0rY96MLczaukYFVfWpVir5s8PaNYMJAi2_4c2qJnb37Lra3dmODByaRzWgC-ZhtfZHcTZip1WmlzHSERNMR3cWXPzYyCPOlOgBNNS3DF/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Amari Johor is one of the most prominent hotels situated at the heart of the town and near the causeway between Malaysia and Singapore. Lightbox has proposed a lighting concept that plays with the balance of light and dark that inspired by the mix of contemporary Thai design and traditional play of the 'Wayang Kulit' show';Puppet Shadow Play. The play of light and shadows allows prominent featured spaces to be highlighted attracting one's interest on its purpose and visual aesthetics. As beautifully litted in the interior, Amari's Facade lighting has made its recognition among the Johor's skyline where it can also be seen across the other end of the causeway.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJeqCvY9N9l9TZ3OY3ywLdnK-xpnD4bkgfsWitlclwO3Z3manT90um-ICGIiR7idQ18k66TPmluG3a4r0bMeTAujhLWnKN2E5mi8i-sQvLevrMHox3ZXGTscE6VSA6ORc_TbVWI0CmEo2GFE30KHQTs6hIwZnbMmpy5IUhSW2X10gzuFyl8tvfJNVla5LnWN2gB-_acxV6I_XPasKpv1kibzsL6-dahDSH0sGjpBqS-a76c0rY96MLczaukYFVfWpVir5s8PaNYMJAi2_4c2qJnb37Lra3dmODByaRzWgC-ZhtfZHcTZip1WmlzHSERNMR3cWXPzYyCPOlOgBNNS3DF/p.png',
    ],
    featured: false
  },
  {
    code: '03',
    slug: 'amara-hotel,-bangkok',
    title: 'Amara Hotel Bangkok',
    location: 'Thailand',
    scope: 'Facade/Interior/Landscape',
    year: '',
    image: 'https://previews.dropbox.com/p/thumb/ACIW2goIlUIRQpfIPyWUrhQx7VX40Fuw_MKLVQhyhLVoOswacfl4tySWu8ARqxGHBfjYdXUispYJZc_EmCUQC6nuVl3R2lrXGlB090o9Ik0Bot4RT4F5On36BH7kWAhB0IARs_4xoYJzOimTPRKKBDL3czXUjVeaT7X1-JYOQTuvZfteYqxGbaw_s1W6BdfOY4S6y_TeTy22ayqyCj_4EP9HMEud33nC1P48LmUOWUZjk45jogC_QLcm6AL2JcW2sUmrJgf93_M9h2OKFX-MQeQKcRr2DVPAU-r9U-LpuaROryrJw5JaGPpA0R5zPqbeQQ1VLyN4nZR4JmBrK5nYJ3Z-/p.png',
    category: 'Hostpitality',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Resort Hotel in the heart of Bangkok most busy street. The podium facade design with wood finished elements as representation of resort style. Lighting created the effect of warm inviting visitor to the hotel while tower facade slowly transform wood finished to silver grey alumininum facade integrating the modern language corresponded to the Bangkok modern city skyline.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACKwSUJTj7pcFjrpMJIifx24kJpmM9VrSm4AMzdntPbxTvHjTo8viwN9keHFUj4FW0WKLOS8l_TWnJ1GbLgWm1vd0qAwgEAnwuhr9vSjHroUJOLhb2GmSoOgSJRM-RUH-Vqash2VlYAJbrfrQT9pG-m78aUlIjVOfgSC_SSuFAxH-LZJw104mtK_-fpapqPLkstuHX5qdv62TaPgj6mbiNtTOPUgeGarh6qz0M-X9njQMY8ffSqvyuZ2PajAU3Yxwv9QO2vzHWHyaWvoWYKQDq4YI2gLwgD-YFu52XjM9PdFUV4SaGoUaMa57I0MuYJDtcneXaZPt9RPtr4WJvkNWSjl/p.png',
      'https://previews.dropbox.com/p/thumb/ACIW2goIlUIRQpfIPyWUrhQx7VX40Fuw_MKLVQhyhLVoOswacfl4tySWu8ARqxGHBfjYdXUispYJZc_EmCUQC6nuVl3R2lrXGlB090o9Ik0Bot4RT4F5On36BH7kWAhB0IARs_4xoYJzOimTPRKKBDL3czXUjVeaT7X1-JYOQTuvZfteYqxGbaw_s1W6BdfOY4S6y_TeTy22ayqyCj_4EP9HMEud33nC1P48LmUOWUZjk45jogC_QLcm6AL2JcW2sUmrJgf93_M9h2OKFX-MQeQKcRr2DVPAU-r9U-LpuaROryrJw5JaGPpA0R5zPqbeQQ1VLyN4nZR4JmBrK5nYJ3Z-/p.png',
      'https://previews.dropbox.com/p/thumb/ACJD_zwQ3iQHNT5xj2uHH2CImFf8CgwNTkehhhpdq7dIM9XJ2qJoQ7-eW8pOhRUIuvBh6nl4l7046oN_TtslxEG0RI94BqkFUGJZdJ9_HUocIKc4IFcrdFulCSx1Z50aOqL1R5s51LvtMzHVz4r7qFMSlo9RmBaTjo8weGKwcTwdDJ8UCyQX3fcda2Jtx8CdjcL3W4cG3KW8uiO7TCdueUc5lKt6TVCq6r1PAJ75jBHWBHE8I_qVFx_WGJMTLv5JFwlGhVUMGeS06dY1TkajpdmzF9gQFRdFVOi1GQRov3FDrccsuLRRXPV7QP-S0bl4qAaDJv9qFJq2PasB1wF00-tH/p.png',
      'https://previews.dropbox.com/p/thumb/ACIW2goIlUIRQpfIPyWUrhQx7VX40Fuw_MKLVQhyhLVoOswacfl4tySWu8ARqxGHBfjYdXUispYJZc_EmCUQC6nuVl3R2lrXGlB090o9Ik0Bot4RT4F5On36BH7kWAhB0IARs_4xoYJzOimTPRKKBDL3czXUjVeaT7X1-JYOQTuvZfteYqxGbaw_s1W6BdfOY4S6y_TeTy22ayqyCj_4EP9HMEud33nC1P48LmUOWUZjk45jogC_QLcm6AL2JcW2sUmrJgf93_M9h2OKFX-MQeQKcRr2DVPAU-r9U-LpuaROryrJw5JaGPpA0R5zPqbeQQ1VLyN4nZR4JmBrK5nYJ3Z-/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'banyan-tree,-bahrain',
    title: 'Banyan Tree, Baharain',
    location: 'Middle East',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKoqkK9RLZ1OScqnad0yn2rA8877z_9eg5bsYuPKiIVENbH6SUwKRGlYWa7tmmy4da6pYDt3tuRdd7Y3CtPmjBDwuY658Dt04sD4BLLWNh0LGyGUy61H9xhI-HEY-eKbgqv02ihquUViLRfbrngWbsSKTNRoHHMsnQhs8xc0ixkIWCiUIAi7I0bFfQJQGYP-Cfx3M-YkJwXeki7qMA4ypBSsKkSZgYs41syhR1fAOyLutvdFcLq71HcQy1-iQuPTPow2FPQLqNcoNDWlFtRQrWeeBF0uRD6abNzVfq8Shr_yLH2QV3TmlrkU4U9Rp64oAzcUhj7_x3XaPPHdG4i1Hl6/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> This unique resort hotel and spa, is one of the Middle East's most luxurious hideaways. The lighting was designed to enhance the elegance of the architectural design. The elaborated perforated pattern panels are given a warm glow, staying true to the Arabian theme of the architecture. Warm tones of light are used to enhance the richness of the materials used, and to blend into the overall ambience of the resort.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIa-Xm5fWIGT7YocNzIKPbdVg7f7rbcOHsQ9UoZUWLwHHUYWssHiYbXCu5GY7xiumefFfYwhNXyf5wkWTkGIIBJ20s3jrxzlw5J5oAZYpGLkboYJceNbiB31f7x5o6xqnHFTkRBbwqIvdo_gGvnP8WRLGV1RxCkJHUQxwQZENZ9gGGN9Vll5U7KOvMOibFFth4agjvDbiOBmSTNIu2W_FvrqGgGKqqBgevBasD_1XH1ptGDTV3VySH-cQ4eA-7K7GcZqhK2ye15jH-0gmsQOt8s0s7AaSQ-9vczFZuZnsMBGQE3EZ8QngAnb0Q_JDH_Nt9IOnNs1XT6eO_ap-cNkH-y/p.png',
      'https://previews.dropbox.com/p/thumb/ACL6rgPpYaAV-Z5Nfd8OkzJt8Xg944gO6nRuWtxph3aFyMrGAdgSxXG3eHay08vBIasKFiKFSoiXHoN9WxO1MDMFE8wi-B72ud82SKuol7Orv89BWaACQv2p_V0fPs6eJfmx9C9nHyhNDSZiMSkMCiKjcF_z7MTlIcjapJFQtQpTwy_LBxVJK9IPbnY-qIRXYEi7iAzqUzAvCoyFzl9NoMHAG0vcTJ24KP5qvwZNeFaktbMNv7gK6FfGXeQmO-m4PV99Miz9Q3Lxhi0VoTXcfZgXRF1gnP6MnHihVDUT2bEZ7QZKC97pOO_EmUtraeijBLnwzqWggF07sSA5TUNDPSWv/p.png',
      'https://previews.dropbox.com/p/thumb/ACKoqkK9RLZ1OScqnad0yn2rA8877z_9eg5bsYuPKiIVENbH6SUwKRGlYWa7tmmy4da6pYDt3tuRdd7Y3CtPmjBDwuY658Dt04sD4BLLWNh0LGyGUy61H9xhI-HEY-eKbgqv02ihquUViLRfbrngWbsSKTNRoHHMsnQhs8xc0ixkIWCiUIAi7I0bFfQJQGYP-Cfx3M-YkJwXeki7qMA4ypBSsKkSZgYs41syhR1fAOyLutvdFcLq71HcQy1-iQuPTPow2FPQLqNcoNDWlFtRQrWeeBF0uRD6abNzVfq8Shr_yLH2QV3TmlrkU4U9Rp64oAzcUhj7_x3XaPPHdG4i1Hl6/p.png  ',],
    featured: false
  },
  {
    code: '14',
    slug: 'banyan-tree,-Li Jiang',
    title: 'Banyan Tree, -Li Jiang',
    location: 'China',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJ6aOLowIGuE87peHkodXr4Hw82BcuQuDMNyADdYcSAk3k4Fw_ZOb_jg5C8CVVz40if2ez3f5-WeSsobycwU-HwImCdGY9q3JtKq3MAGtlMyPsuowB3kw6aCZYJMjGAM5Js2ynahKdn1Fspaq5rI-yl4yFKxFuOOQHuWQu_sC62aKIVQFlu2aebTVDVg72yHP4IRXvAw9PVwz5yPV3k4MdmXQo6-kQ0UdbWoz1BpHNSkjmUAiDrTXfKjtoAr8gunqiNXXNsQ0gh-7YaFiBruqxDMqQp23ao7Yp6dXQmKhr1UAN8Rc7ldpiAsVX5Cy2WdJNnJtT0cu8_r2Qy734EazP2/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The first luxury resort in Lijiang, contemporary yet with the traces of the Naxi culture that can be found in the architecture of the roofs and courtyards. The lighting concept is designed to enhance the depth of the architecture and the space. One of the most prominent features is the contemporary pagoda which is stunningly lit by night, giving a twin reflection in the tranquil pond. Overall, lighting is used throughout the resort to express the texture of the local materials used and enhance their richness even after darkness falls.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACKd8zquS9xw1DUgDMpckQ3WiSXd0fww-59I5JlEYRoqoFDvgFHtAoKb0AquY1scBhf6R54ySSYNW1enQ2EZdWu9hT_bYbgqPiK7tY8YGXtQcOu6iqKznYQ5lzm3JEkjLCYMepV-z4DGq2Xu6P-nvCby6fpAiMlDtFQ9XICsjxVkOwxpaSXCgzrCSIduY8LEJfONLWgC59O0-vEeNyTrwuyrQzo-kkKp4cDArTWH6CiPun3KCaedAMCvzvqUJ5irb0Jq0kQeENPg-9QpJBZIvulLsXbxYUeKe3W4327yHNgmAwfdYvhhSJoylCx6g_acsuXh5z51eTxIHugwpdtWpYD8zB5pn1-EnTVTcUa41T-MfQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACJbKCmWPnP2mKdW5FUv0wt19vxNl4L05Gc06RZHuEGrvPmK7fky0JGSoh8RCqQqUMC7cSTNA2hj5rXT0vxEKCp7MdZZqwdECGaFP9Cn5aAudMg08NZo6IdzleC5jzBex5isUVtxSp3UM2ZO4HctQBdMfn8rJVnMwQMmkIku8Wv_vCyzFbZYeqDXd0N-WEk_tw0-7NbMBdmRD5cBLXSirvudsY-Yo9n6R6Z3B-L-9dUgOZ_cgYAYjVlz2rnX9Q_xvP6oB5HvBr1XG0lzWsjIrn8whhDieO34RQD5-MHm-f8AYIQ-UtPmE2bxSMO6KFo2BFImcO7oI5wMI6Gecld_KP2C/p.png',
      'https://previews.dropbox.com/p/thumb/ACL6UzOiYIcqXo-PBhcIpg99TNcTeB77qBEKQX7nITov5pvNKlmDup_y_ptIl8ndzeXZhyvUcdkyNCQka4wK_16gD1RvsGrLNWQNa8BsaHvTcIzXzw4mMSS2LWd7xqABySFsA3M7KLJRIyWvRNUBWu0PqUcbAYso2HFw0OWqKuo7iBcfN9RWb0xbasm4uLeKG6wsoERYh0SLSrq18XO8yCetej0tgUiCJXcyWT1Qu-BTRgf7UGBEIq-WTJH37LdvgDLii86CXUbhlSvhJvy2-KYsNqlb0LAvz9zgny7LbEKlO2ki4LVncD_MDFyLsnoccSuYYvZxpKluu1jRr9ml1wqS/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ6aOLowIGuE87peHkodXr4Hw82BcuQuDMNyADdYcSAk3k4Fw_ZOb_jg5C8CVVz40if2ez3f5-WeSsobycwU-HwImCdGY9q3JtKq3MAGtlMyPsuowB3kw6aCZYJMjGAM5Js2ynahKdn1Fspaq5rI-yl4yFKxFuOOQHuWQu_sC62aKIVQFlu2aebTVDVg72yHP4IRXvAw9PVwz5yPV3k4MdmXQo6-kQ0UdbWoz1BpHNSkjmUAiDrTXfKjtoAr8gunqiNXXNsQ0gh-7YaFiBruqxDMqQp23ao7Yp6dXQmKhr1UAN8Rc7ldpiAsVX5Cy2WdJNnJtT0cu8_r2Qy734EazP2/p.png  ',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'carlton-city-hotel',
    title: 'Carlton City Hotel',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKipBHEnJ_ZA-f7yKH9jiIZVOMPBBxQJUAJN3a52y-QNFiLCoJuDVAbwBPWBwlyyXQ_Ogee1uiGc0Onm-2bRMrgPM9mjx6Q0mPjQ69ZZt7NhUDNgEtokBNiQlHdzzIY7LKsIa9VVyUiP08NeKhFmA8WT6Krqa5Nx87SWdfkYQfd0VUOGiPQuPht9bV5zTmPfm-R9FSiRO-JgCDI5sZL9mCxb7S6eFWc0uX3CqOffhVpetcTx7EY3I5B_CxiAAnlzdcDTchvmvZAyLoMlt4Q9RGXCXRG_qjyigB_zX5Hi3l5z5V4MjQf1VvlhF87jAoWrOR6tEBfhvdBIN1uzFs4eHuu/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> A newly-renovated business hotel located right at Singapore city centre; which considered as one of the busiest spines of Singapore. Keeping in mind that it is a modern-looking business hotel which targeted most of the young executives, Carlton Hotel lighting looks masculine and it give the sort of understated posh feeling to its guest. This understated posh idea goes well with the concept of the hotel as a place to rest after the whole day of tiring schedule. The Carlton Hotel is a perfect hiding sanctuary to its guest and a beauty to the passer-by.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJANKjfcBeggFwXLVN7uag0zYrcbUMYGxjt8SXRjv0GtGzDzaxeMydkknBfrS95n0LWNbKhQlUaX1Ayun9q9IkScmVTRb_42ndRCAiUtPxRVlOByuGrZKinzLchI0bW0qAiJHeF355UjG0TRl0t7TRuHfUAca3D-x2WbEKQU_RMnrC8kgDhqHaYiJ_cIY0b__cKFiLOIWa2Ce18BGm7d40m4F3ucVeFJIWn730GZ9lAWLpbuGdRrQ7xRVd9cOKzljlLLlLADnV6-XoFz86gqim9jJu5PuVsg2mxHqi3C-BX_oO38f-9SBmFWAXL3f3FL1ek1uSyGfH4zvy2HuJoOkyF/p.png  ',
      'https://previews.dropbox.com/p/thumb/ACJANKjfcBeggFwXLVN7uag0zYrcbUMYGxjt8SXRjv0GtGzDzaxeMydkknBfrS95n0LWNbKhQlUaX1Ayun9q9IkScmVTRb_42ndRCAiUtPxRVlOByuGrZKinzLchI0bW0qAiJHeF355UjG0TRl0t7TRuHfUAca3D-x2WbEKQU_RMnrC8kgDhqHaYiJ_cIY0b__cKFiLOIWa2Ce18BGm7d40m4F3ucVeFJIWn730GZ9lAWLpbuGdRrQ7xRVd9cOKzljlLLlLADnV6-XoFz86gqim9jJu5PuVsg2mxHqi3C-BX_oO38f-9SBmFWAXL3f3FL1ek1uSyGfH4zvy2HuJoOkyF/p.png',
      'https://previews.dropbox.com/p/thumb/ACKipBHEnJ_ZA-f7yKH9jiIZVOMPBBxQJUAJN3a52y-QNFiLCoJuDVAbwBPWBwlyyXQ_Ogee1uiGc0Onm-2bRMrgPM9mjx6Q0mPjQ69ZZt7NhUDNgEtokBNiQlHdzzIY7LKsIa9VVyUiP08NeKhFmA8WT6Krqa5Nx87SWdfkYQfd0VUOGiPQuPht9bV5zTmPfm-R9FSiRO-JgCDI5sZL9mCxb7S6eFWc0uX3CqOffhVpetcTx7EY3I5B_CxiAAnlzdcDTchvmvZAyLoMlt4Q9RGXCXRG_qjyigB_zX5Hi3l5z5V4MjQf1VvlhF87jAoWrOR6tEBfhvdBIN1uzFs4eHuu/p.png',
    ],
    featured: false
  },
  {
    code: '07',
    slug: 'chrome-hotel',
    title: 'Chrome Hotel',
    location: 'India',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKSl0eR9wec6tM_zNHsLGdZERRoqLGVwmt85qjYu7OmRmTiqa3al5FFnozmLL7PIhH5Bi_vm5rEk3Gcj0kuBRvTRQYCFOUpMM2WEikWgnrqPW_XhkGJMsWxwPfda-jYS8FRJOCjltMepPZvL6Gwmp5eMvg4mXSSPefN7F5YbLFNqoWEOW1_oG3UyzGEZFin4AvhKRiAVW14ciYvbJ_YSJMqwnQCTnsSNkoyPIN5kqzvOCcrpHyQoSE8VsBOv61UNrwPN69hm0vm58rZO3FG_XZ_hl8nbo-AMaqRv4WA96ic4bjxsK3751S9kR4O8Ve94ItE9fBs4QeaU6zA-ySYJg78/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Situated in the heart of Kolkata, India. Chrome Hotel is all set to redefine the city's landscape with its unique sculptural architecture. Composed of white blocks housing the rooms against randomly punctuated vibrant lighting, it ultimately becomes a lively and dynamic landmark. Lighting Design in an unconventional approach, together with its Eclectic Décor, transports one into a sequence of spaces that alleviate the senses. The signature lighting is carried throughout the four levels of the Edge Suites, offering different lighting element themes : wind, water, fire, and earth.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJSN5MWhqOn1aqylxaH-8abZJgETCXWHvuinHVntVxFHys5IrJEsDw4n6R7runJLUrSpHi6PM1HkPzsGRNDappw6ZUcHZ9ONHl0aD4vNuvYuTirfthPOb9aOnuORUdkYV7OAFpdGwhOf6itGq30hzzeFM-YqXHYtUMBk99rdmNThEXuxLRbCafL52ugYBbPb7NsB5h4N_bLwddNt5OWUcoC38uPaWAkYvrPLyoQYLa3KK03cOHTlbXQq1FDDmcT1F3df1bwfcIZigsyxcVAKMMe-Sc5APtvOJi5hvAd5NiCKry2MTTR1XrY7AN95iJW8AaN6uvLoXPcgohyDwF3fBuv/p.png',
      'https://previews.dropbox.com/p/thumb/ACItig7KUKwHeVq8dxf9pgRa1o7hJcZT3jFmVg0jDVAH51kZju9R-gD8MJNqbNUHu63knbG5teKTuJNHl6b6apjrTUj8tGkIqzfDA3h89H0dN5oUrF0RI5yKQWbZjrX3nvfZhI1tkaiyskCLddtfs2Z2ARPu2Gcgstn18QAkLcWp9gUzCzGMbmB4WwL-70tPQMFZcOeXzhi2B36C2x4w7wZ9tvP_gpWh_uVkdXd-k6K0Cem8nd6V5z1J82MKcsKunjbHN-sa-Bx-szGoMgdd2uL1o3WaTiI5iSpfK135p4nkhcST4-DQppWzqWVwVqUebHLb7-iRJPa7g5FNPVkVZfcv/p.png',
      'https://previews.dropbox.com/p/thumb/ACKyK4tp4SttTi1htD1roOop-BQrxPvz0aHdAcATthGiP_Qld964SaknfbFSvOqNCCdKuWJI5iNyq4rPsDqQDY7OO8ogZZa7DkIh92mLFI-stjQyA8bQ6rKGxh3PFXEqqlJTvzR-0Pzk2ZzBsf_QJAklGoSbewcMfHzi4kOvmnN3URXjqdcQvGdKN804xaQnbFldztn7QXcYyooK4tMExMJozOisKHBWvIo6tCyoY9E-tH9DHBS2hziHmFFP09I3PpXoDzvwplTqkXNeUVgRlYPrEQyRf2MfHJb_m3LqlK8k7UvaOZ7_KkMCiY3-nUeuYtr19NjScY5tAFR4ADuofsue/p.png',
      'https://previews.dropbox.com/p/thumb/ACKzBTWYoCnzBQdC6p4LJBCF_EOqgawnPnAH3T5DCQaf4uoQEVy6BXcgHB7xAV8SXZrQmkzb873LL7-pGN4izFB14Uo0mNKwzIFKjUwk_2_iPVIkkrZpYCd90WCgeUjMbOgdCyYEHbAU8H2ZD4AkVe8zaVMWaQ8H_SLxmD9uz5gOJt4Xx9-Vy3paTIu62axSA-z-270YiBGbJ_vefKxDrPX450dBh51drhV9DdEHP_nz1bhsmypHJvV-gLmEfGaaSr66LE791BNv_EsQwqyU-nuaupLeES5-3MLjci8OudkT-3q17AftK-THPieDq5tYw9vIRt3FAj6HlR_5tf96K7cu/p.png',
      'https://previews.dropbox.com/p/thumb/ACKSl0eR9wec6tM_zNHsLGdZERRoqLGVwmt85qjYu7OmRmTiqa3al5FFnozmLL7PIhH5Bi_vm5rEk3Gcj0kuBRvTRQYCFOUpMM2WEikWgnrqPW_XhkGJMsWxwPfda-jYS8FRJOCjltMepPZvL6Gwmp5eMvg4mXSSPefN7F5YbLFNqoWEOW1_oG3UyzGEZFin4AvhKRiAVW14ciYvbJ_YSJMqwnQCTnsSNkoyPIN5kqzvOCcrpHyQoSE8VsBOv61UNrwPN69hm0vm58rZO3FG_XZ_hl8nbo-AMaqRv4WA96ic4bjxsK3751S9kR4O8Ve94ItE9fBs4QeaU6zA-ySYJg78/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'fairmont-hotel,-singapore',
    title: 'Fairmont Hotel, Singapore',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJPpzaPSRHFGiau9HOfsogmfHjaiq5W7K5JWmab-CU8lzLW_t9SCRoJvFnZle7OK1nVkPKqfyLHycZSFceuF8iVyhdxqLOcqGaeBB324SKSPxKOQyF3zEUWIUbQ1O3HG5avsM0hEuAit6hX6ikQvb6s31-BuNKTP_RsYFCQoQSeJ184mnjjBzPq3onKYsf5CpPUrnIdf0FYbhgpXP2oMa7wENYXXgzvQp6Z_BqPUoxV27kUX3fC2auUKqpgmPWHn6OgGY4CoETiqX15_5bR45iRh7mDjoGAxU7QpmzaQizsh1bX8onXMXeErbTNmbHStoD92hZs6FecTNoAhuZjtOc7/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Situated in the heart of Singapore at Raffles place, the interior of the Fairmont Hotel was inspired by Singapore's multicultural roots. The blend of Chinese and Portuguese culture immerse the interior design scheme. Unobtrusive lighting fixtures are introduces to enhance interior details. Direct and indirect illumination creates depth and enhances material and space. The use of LED Lighting integrates the ceiling design with the furniture bay.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIBc1KI0Rf0nyqT6rHYYp4ErTSps3oXkhT5ce5bTJyYL0qjoH7G2v9k0TCOBuAParLXbnWrNSg0eNpIYi13-1Qz5ZE1y1LCuhdE1hwNPB8Ybnu0dB2pLP5nrvinfHNVkYufj8L9X_mFot0e9INFycgNRFLZrwvL3B7lSipOO9WQq7rb_bGFgOReuQgxo_CPUxz95k0sk8Ja_hKJxrla6m9AwB4np9QLRAUMJ8cqTF1XnP-5TIsNqOdcGMfJrV6Syug7DDpc6SDyTRWHCiN-YyARa7zjBpCsk-X9QxjN27k-R5BJySeE2I9YuyZJ-0d7gvHuWFRH1L2bjld6QDQ0PewD/p.png',
      'https://previews.dropbox.com/p/thumb/ACIq1sM_0k9DRcyffzYa2ac2QwPQRIUD2AjtasnfYx3JcgGYcZcWRwMcrgxGHhVDR1m0fGPFiVpjJ6o3t3_XPNbmiL583ujiBC_0uD8EkOay_w3fBVXMIsSC3_tA3sfy_NdSH-pO4IlvA9w6fhO31sl7faH_YgPRO0YEU6VElEht4p_L8jFhWRPav26b8mZC3AWTReO6u5RVmWmH_JigOSv_iwlKS5es-1BBMjVjikzdApuNdeNjY_Ao-6gbl2-J5FmuVMFxJehtt4ls6vpgB71Qmctq8ue02lsQOOP6XTHNMo4xhpRERYz2t2RBHT9_lD-x4gcF39qJ9JuqfOzqJ-wt/p.png',
      'https://previews.dropbox.com/p/thumb/ACJPpzaPSRHFGiau9HOfsogmfHjaiq5W7K5JWmab-CU8lzLW_t9SCRoJvFnZle7OK1nVkPKqfyLHycZSFceuF8iVyhdxqLOcqGaeBB324SKSPxKOQyF3zEUWIUbQ1O3HG5avsM0hEuAit6hX6ikQvb6s31-BuNKTP_RsYFCQoQSeJ184mnjjBzPq3onKYsf5CpPUrnIdf0FYbhgpXP2oMa7wENYXXgzvQp6Z_BqPUoxV27kUX3fC2auUKqpgmPWHn6OgGY4CoETiqX15_5bR45iRh7mDjoGAxU7QpmzaQizsh1bX8onXMXeErbTNmbHStoD92hZs6FecTNoAhuZjtOc7/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'falhumaafushi-resort-hotel',
    title: 'Banyan Tree, Baharain',
    location: 'Maldives',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKHbbzxkSlRkHoMW35brg3u6VJ9EC02bP34K4ZYAcVkuuUxnbZI2fK4qY1fZffb9vMKF9heZRhCJRDwEzjRqHe4DBWSg9Wj4xOVBQCuOIwctLW4jJKhkiTRBABu2kuu-Y9lyIZsU3cW9rhQUZatVEx2mWrZS-7WLlwnadmxgyPnEqXEvPF6ExObzhb-YTd9M7UqS_b_7ISmlYB-a-Tir62Q-QCQ0ngnO8c6Jj-Fd2juKZlh7L8zuIOKvcnqoeqEmOTwrMg3LeQkCB-AcAVg8xTfTj627SgyWN8yHiuJX0pR1cUDyamRicVc_uwQCN6O9-KPr2zRtob4jzb7a2B0pSUS/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The remoteness of this project is its magnetism. For this reason, Lightbox aimed to introduce unobtrusive light source to deviate guest from the outside world. The purpose of light serves to create atmospheric and also heighten basic senses of island living and its serenity. To fulfill this concept, Lightbox avoided all architectural fixtures and concentrated on the design of lighting objects, such as pendants or lamps, which served more than mere decorative purpose. Architectural lighting was only used for the functional illumination of public spaces.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIUbc7PUY_wed-RZQU4siidjU1NNmtyKfqN0a89C6p84XmrhKifEBo3eVpmkhePjIQYoyuJef7W4BQ8KJf2iObJpRkFKeESMCWVBzPc396YwxoNd6Ha8d5nAo-vqBKNp6ZzsFGjjIuOHZ1XjN8IDXIp0ATHSAUBmyn_LVOqVyQRstQ75G_veneP4N8FqWC3yqpo8appIdTkblq89gRNfWaCrfIpHVa4KbamPVoRhC__-JVo8Ecd8wkMpsNF5xBEXoi6nZawLZaCWBnYWJCIHfyNWnN4xkyQiMx6ogvR7PdibuO4jrXqV93PygxFUNbyJrPkQrt74Np0FUNTH3Q_fKPX/p.png',
      'https://previews.dropbox.com/p/thumb/ACIFKJNaDuNGeprybi0iXdTUNoPEJwv6tvsF_v_ZMR42fz6mxbW71KYZzVolNu2R3U-LMRdeOw20fn7yhqTL_M5nY6Hm3FkkcGkcR9z840l-1C59StnenswBgCtpusHDhYnNVurO9ZjtB7ziOk6lk4-oQurxyXIdIXkyHnEd-eGbbsyZgNA7ZQBMXkQr5aGU6QQDbpe19EKcdKLrmY72xlKBtxi-x6-ghjDLgj_7ZE4-TLZpOZieyIXI_u6Xq-3pc52BbI5kCJXVDlzi5MAilkpLreJl2ejREBrgqCM7zlwGo44C7hxLLk_yCnrSWpOpMhy74KTrUYjo49LerVzaFuxo/p.png',
      'https://previews.dropbox.com/p/thumb/ACKHbbzxkSlRkHoMW35brg3u6VJ9EC02bP34K4ZYAcVkuuUxnbZI2fK4qY1fZffb9vMKF9heZRhCJRDwEzjRqHe4DBWSg9Wj4xOVBQCuOIwctLW4jJKhkiTRBABu2kuu-Y9lyIZsU3cW9rhQUZatVEx2mWrZS-7WLlwnadmxgyPnEqXEvPF6ExObzhb-YTd9M7UqS_b_7ISmlYB-a-Tir62Q-QCQ0ngnO8c6Jj-Fd2juKZlh7L8zuIOKvcnqoeqEmOTwrMg3LeQkCB-AcAVg8xTfTj627SgyWN8yHiuJX0pR1cUDyamRicVc_uwQCN6O9-KPr2zRtob4jzb7a2B0pSUS/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'goodwood-park-hotel',
    title: 'Goodwood Park Hotel',
    location: 'Singapore',
    scope: 'Facade',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACI0tbLMO527hLCdXgzGDajLfhW8aBZVyDUAVFUe19JSgYc3SBY3rAXPlPuGiPCPzRrGB9Z2q0bHoMPiPVM-9MYryc94cimrPpD2xHG8I2R5F5fR2mw4EMXqMkUqsCUIoPe9WaFSfXCjXquTMBC1dyZOMvLkZ5vM7rz6CURIt2HjNkY7osZvfQX-uR_iuVW57Li7AHB6f54uEtYrhQLNwKaEJwPWd4XcIk8ha3Gg5iYHKTewvn4ugHC8JChIckB7OC58WoNF-Rnr8EnqQCO-LynWafrSfNSw5I-pCp6lXNgxty0ykr_DwWtrWnCaz_P_n2qDoVov_2QuNJCizfxIthZL/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> This prestigious hotel located at the heart of Scotts Road has been valued as one of the most beautiful conservation building in Singapore. The lighting design approach is to render the architectural column detail and accenting the linearity of facade in warm white tones. The result of the lighting has turned the building into a glowing diamond of Scotts Road.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIgMPhY6XsCKf-TP5YPLPZxhA0YXesgnbYU48UtspFLTx3Y8zub_MOhMt2zUWDeWVdENVuyXIj8vlJvgIhSan1XFjtaRKiuQMAdwsqj6Zu2Jx8Cd9MbkOokxgAHOeg7D0K_WEMvgozKJtMUg-5qiNJnOTr-tRygm59Xfnj5jlytRF7yHqj6ET1IF_APDixeKhXFiRp_lR1s5MHV98JlKpJQHla8KWLO48QaQQ5YyFemKst9er-zE_O3ZnTiIYKnoq30brPpEvNQef19bo9bjmrwSAos46qvnYfeSs1d3t0hVCUds2rzqwnnAVzi2L3dUQn9OXi29FjnUcR1sMPyy153/p.png',
      'https://previews.dropbox.com/p/thumb/ACLpTbptFe8cNdqrACJZYGEz7Uu1RBsZgj1BzdYH6x5JlDyL85vnBab5_ZZDL78KsjUMbAAlYIVkswkNWilgc3PbYiRiu_wMf0UN8XL3wkTpPPnl1gc2Z22bYbVj7V_RVXxggJUHGGIpQjZW5iu6pmoaTMSh5HqZJrQKTIbH3bxolTFjydQEvNeukCXR9lDXjE-GpFagU6V6LNe8EnRyLQsyb4uvK7M7FYK74ZpK1E8fNlB3yGhbeMs8M3BOv1igAcv6z5huIIkdTKnNqhE4O8hwXOOHvnQuvZdB4HVxmb7emW7IAF3yyXvts1022SXS7VBez0e2t1z1z7RHutyTPkeP/p.png',
      'https://previews.dropbox.com/p/thumb/ACI0tbLMO527hLCdXgzGDajLfhW8aBZVyDUAVFUe19JSgYc3SBY3rAXPlPuGiPCPzRrGB9Z2q0bHoMPiPVM-9MYryc94cimrPpD2xHG8I2R5F5fR2mw4EMXqMkUqsCUIoPe9WaFSfXCjXquTMBC1dyZOMvLkZ5vM7rz6CURIt2HjNkY7osZvfQX-uR_iuVW57Li7AHB6f54uEtYrhQLNwKaEJwPWd4XcIk8ha3Gg5iYHKTewvn4ugHC8JChIckB7OC58WoNF-Rnr8EnqQCO-LynWafrSfNSw5I-pCp6lXNgxty0ykr_DwWtrWnCaz_P_n2qDoVov_2QuNJCizfxIthZL/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'grand-hyatt-hotel',
    title: 'Grand Hyatt Hotel',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACJuhTlHQmC0P-S1YmV1uP3F2jb8eKVOowAlj27I1NqL-19AatrwFX_ax-XFpfMqRh-MIJcF2UNpykZd48ZK4pwDd-g0J2RZ_Mq4-KM88SDM_y8O5onohB1IAE_HX66BcTxKykE6ces6QM-zdcBWw6dhWWJhMrR1Ls7KFJ5LeXkJuC4uc1BfyXQnlttbXP2fZwpFJ0INQuk71XTbHoKiR3qa0YFZyMR9jil_DOhunISNfEe2eEqk1TDjGzY7n4aDTW9mjW5OoV-imc7E7YjGCwYa8gAKdWB15g6Hck8WdO7GzAN78a49v7oNqfwO4CtfXusHDU_tsObOmE2zi5Yy88RS/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The very identity design of Grand Hyatt Singapore is expressed via the iconic play of texture, light and shadow. The changing of color introduced at the gallery space allows the light color to be interplayed with the trellis design. Contemporary chandelier is being designed in collaboration of the lighting and interior design as a feature element.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLH5kPjvdPUcDp7TZ7vkGVzf_v2YtlcyCV_Q4x_gnVSNWjNkpOO8Q8no_Y8EDi3hx1xWj3sd3BFZw3NvicaHcnUC-H0gXe3FBCabq774Q5KgMkJxEBC-r-ywOlF9OQzVn1vA6PD0_0GiV-lj-Vxi53j3kt4Dyqm0kikeTc_vvXC-uDPAIFGvuj50b13PShJt3MeG62Rk1t2R6AJOuIGUj4495zQnf1icXybCosP0n3BXWtgdeTrO1DAyNvuq8pOKs43HpWneAA27EdOJxqf3tB97uCoymtpK1WLuwthaFL3ow2A9CpK6Vlu55Y07oB6Spc69-sgkUPMC7W6PSa06kGO/p.png',
      'https://previews.dropbox.com/p/thumb/ACKbrxRWOUyuXWz6xMednYkyykbckrcgbSJYdPZ7_x4Iqg2YBnl8_Tozv07WRTHG9ePMS6ZuQerBYddqXrOBvTc6OZFmiGUWMy_Yyununj--9QwUO8tiYt9YV_5bu9gACFYt-XYX5SrQTHIHnLvMtMW-qFMMSL4eoiVfdQ7clyrc2E4s7f4NTFpxvzuZeO8XwLQzj3B86d-s44I0e4X-Qj56PMMwXTHJlUmQ-Qbotp8GGxLy7luMpfwqi6sOOQdqiTCPaX2D_Yx1laDF9PHeMj7AVri1K1u3wqX71RWJgL0HwcBzndnV4a5VKl6gFxxX8kZ-K8fHSnq4a3FaRf4OwgUU/p.png',
      'https://previews.dropbox.com/p/thumb/ACLrgdXaMmrwdBkT4uh5pSwX2yL9Rlrql1uY9nVPmRBGm35sOtB7h68-gKmdYvr6nSYZ7i1yPbOLYGYY1AlYwhoCRsJ1MZVaqBv2P-qvYbk2q2dsf0VFKpAdW0RCn98-M-5j0XXaby0a_FyspFnvmSDIlcI5FRzoxsOh0VC6GpOIn-hIhOnXWF_hMrss6ipv0iGBOEK6fThSOS_t9cqjYRnnnjjMBf-GTcc-PMrX7ZNKAKAT6S5EK6sgAYaxMWaeq1PzZrDh55kRY62_hJ5h4njWla9J4sQHLvVqFvdR33g_mh8iKc9BuwinkqSBGRPnS8J1r_HUZmEsSfXP2DJN4-3O/p.png',
      'https://previews.dropbox.com/p/thumb/ACKcc-ZAvpfE3ra2dwJgu6-sFPik5ZP08m5PjkmP8-rzfQtRsFHiv4ZCwVxdyMEWMQPGW1XtIvOYup7lyQT-jeLWzOkfQUJ6AE0UtN1ztc8eGhgMSWtwhZKj3m8M5E79dGxn-kBIIoX8Jf7VJU5A5FG5ooViNRkbPhfgJXIWFccRV82PDy_-m8WTiNjNzIrElFTwM0y9O3FSq6sHj4qjm3C01XUZuKVzoLXt0saq7ywHGAWHBD9_HCMVmzwHP99QIm3jT-f9VbdDFkjFHsfH559EBDJ_mrLhzlwK429PTNlaeZQR6wSzlTvqXBWNS6RH18XXuNF6f_itNopCq9lwyeUV/p.png',
      'https://previews.dropbox.com/p/thumb/ACI6LDMVaXBPzEsK7izGl6OxNRJTQ2xV05Pk6iqJT4FJwCLMlnIQxrPakIzVLQ8U9Q4omS-nHyMNw9EDS8DCBm4mN9DqFQUfXxVM75oivvLSlWj5YZUAVgd9dT0TcMJjf71qbIKbwi-HTvROTtFk4cCRhFBc6BtXshd_3yhQ5dVaz7neN1O0NewkvG-7p8BTe4v4ug2GxZidfuy9hS_MJgJz9EkwC5pFmDBBJ5I5mxeBc1GR-cZmfvXtpty73l6nS1JsYgQ_T7AT4MZzzMKCneOSNQ84CZdqXGSUuY3dK52tIbFWdqOFQnYGDrRRR0B4EnMyHqRsaxw7GzpGQe9btpzy/p.png',
      'https://previews.dropbox.com/p/thumb/ACLp3Gr3anl_lm7eX08TW-ItGpbpEgdqTpuX6Fr_yBpLux_aM2wge2eqeUlrZ0BauyUtiGHXX_0uU4Y36VP-kL7dzxmDKo90IzycW24l3lEOp2O1Sh_iNhVU3AAfRIFooXFTv62-WWvzOX-ygI4uGi4CB3o0tYZBTV212CJnjnrsH1yOp4FHrB9BetgR9orz_ea4DbpY3-N8f52ooS6iZ2WfFA0GrD2r2VG26fjjuEWSUifoU-o14iZbvmgstpnjP8gQtnAZiDxzzhS5AisKhu6qk-QT8mZfEQCE_HvgpFFLOC65Ife70cteicm8n6XRvlQr5NlxiSSjfUs6BWjdgkMD/p.png',
      'https://previews.dropbox.com/p/thumb/ACI-CWTYG0zmA3KBEnBp8sIWt-4RQ_p7L4Di9kdRsyF6aaDGlCB4axvgo1RLFKvlxk4lwDIuI__CcKnJaTkijjk3BzkFjlNTTkEvWpq-xctLHcrbA-DFzW49wuhCK51XSIKH02u4U1ubxwV2uj70khP6MFHZLc5BAq9ERluaedendoFr37HydmzE3F1WvixLkEA4Fy3rjIUfn01fHH3jzCgMwxwAueOLmXG8zyqOcBe4XThVCPH1aIcEM_7nHaYW10ETQAgzgsznXNvDlr2ou1xvAoCLoUs0tSYSWPZ5-L3NxIxXIgOovAfZ54Cm6IVeXFkjBSbZMPegvYfGfDnBumnI/p.png',
      'https://previews.dropbox.com/p/thumb/ACLyHWnD8C8vMPfn_XTNAP_YpA1o5XmLMfzccWN_1D8lfhqOanyLwb_9Zn1DaH-rB6aQ2iJ2I3XprQBBY427vI96yhgjFtp5tGGzoYYOYDwK-LhsDUiMqagvMc1IGkmCskAOBbl5QPonpwM39zLYSbCl_z7Ny6dyOlGGy_kRv7rvC5xpAAOm4solVrgEKFkArE8EpdM5E3Lfd8zOZY6g-VLK2G-HCxYz8B6B-3f4AAmYz3RtpieYq-44y8wd3MbKumijqsuDgqU3TVSFWV5gHOLbNcXbpYzkOVCISf2n8QjpJXGTRlHLsRoWL6KMHVylYbdCVrKlNnOsdneSd0v3wFNX/p.png',
      'https://previews.dropbox.com/p/thumb/ACJuhTlHQmC0P-S1YmV1uP3F2jb8eKVOowAlj27I1NqL-19AatrwFX_ax-XFpfMqRh-MIJcF2UNpykZd48ZK4pwDd-g0J2RZ_Mq4-KM88SDM_y8O5onohB1IAE_HX66BcTxKykE6ces6QM-zdcBWw6dhWWJhMrR1Ls7KFJ5LeXkJuC4uc1BfyXQnlttbXP2fZwpFJ0INQuk71XTbHoKiR3qa0YFZyMR9jil_DOhunISNfEe2eEqk1TDjGzY7n4aDTW9mjW5OoV-imc7E7YjGCwYa8gAKdWB15g6Hck8WdO7GzAN78a49v7oNqfwO4CtfXusHDU_tsObOmE2zi5Yy88RS/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'g-hotel-kalawai',
    title: 'G Hotel Kalawi',
    location: 'Penang, Malaysia',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLPJ-8o-Lgb66yAbolPdgh7C4dzSuNnabSupqjPEwhBL6on2uDJTfgfpdG9ivdDqBVO0tSsORfxlAM6Zd_3KvHsHUojav30QbWS6Pj3ccn5HNxXx3ZbhQtIexO9YPYNuXIqnlCv9_vDaGBkAOgIKp0Ix5EBY5n1tTf0-uN6vmX7ZoELK4cbrvEnlHuIz3Xra1pN5s5vxrLpDDVh81kN3VhWo_ZHG3OplWUBOX1PmrHnjMLZAzTctck1RJY4AxZ2SeZk40moY2_5GWx21h4B64gpYf2tde9v6601_Lap3GLsrPZ34vfnpLMvISTdK8Vw4ZbEbTX920u00wm_XDQVXhD8/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Baby-G hotel is a second project embarked by the same developer after the success of of G hotel. The concept however is to be one of Penang's hippest large-scale boutique hotels. Its materials and textures lean more towards woods and metals and more characteristics of contemporary architectural directions. Warmth and coziness are the emphasis of Baby G, qualities that Lightbox enriched with elegant lighting systems. Colorful facade lighting catches the eye and boldly joins Penang's skyline. The facade's light palette and programmed gestures edge near Gurney Drive, Penang's popular beachfront promenade.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACKg2Dx-z2lftddsvJnrXbOT00elx62r02io4p-QQVMLtolG4enm47pSMpWgznQNXz1zKO-BOKPeG3Zl-T3iQDYQGTsGNojMrUQKeDI6fBrPzHEounB0CDoQixW9GsMBzHDtjdcZ6wNsrWd1E-mQDhE0a41pFnJSDmDzgdk1onMZ2iXU1JRQOx7cG8W9CNkV9toqnTnrgO_YvDd-GXdr6rdY519_S9OwLDNqwHODykQ0cS70otvs2SEisKGhyKAW4IrCYaWuwVTlgONf9BHeV7EdSP7mZ2F9TW4On-dkKMp8H8_JsD322wUXfqlrdlX0_EZv0q8BiiQKj-xU0M9QNIUM/p.png',
      'https://previews.dropbox.com/p/thumb/ACKDjIV5CiRGaF4zhUBrDdxEX7gTl49H08XqHFu6J3g4cRQ7S9D10Tz30y7gN_oE4OKdN6NgOzJcSXVJ-vZughNIj9eZ_oV8UxOH045LHV5StlDKlHoDHSGAi1d46ihzmWa8Ixw9It4izSTPJVBj9GU4qdCDjaXQwDc4SDNryDMOeiYUIx1tVBRFcNVZnb9LmMXPZ29VxPJG_QOgrvYvRrjRhR1fH3tUjJwD53F8Kh_iuCy2C9R9waXIaDi1qZVgCR94yGE7MEuLJT2IJYu4siFrm0sBA9KTqaW3JtrhN7AYjkwjKlYpeFn1FWomkNV2iApF4Gzc-yv7nqNqxQ77UlxQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACIZ54oS6zFOSC8Ma5mf9hGR55TOrKuRRfIH1qMAU2ndozP90dIV9yfUDjxIPs1D7Nzcm6XsadygeCpLWKVYn3vu10fYE7_Vd65ThnBq5zpdNVlF4R41OX27be5pEKexMvUhREjdIvIdrSi0r3sq6rs1g9Y77x49W-gSkID96cclHvfQnY5WQ2F_jUH0PjSvUKQzUf-DDjP6H5c2WRD_7zWjrVzgejU3iOiYHWCl65kxFdX-H2Dbv1u3scKjRAxm13TPzAyjY41VtE2-HE0RXA9PN5gn_DBKDI9Q1ec4S-n6WZjWWXHbI29YdOW5l3xZWQX1xBLh9gzOp9uMzLKx2GkV/p.png',
      'https://previews.dropbox.com/p/thumb/ACKH-V7-xm77KLoRaH7gtxiuMLIdPWD5ladhPLClPDm8cDoKmaAts65cTNHU5ijcQb6ObkRV2uIzTYgAP0ytrXDl1CvvXrjlK-uf1-8Tn1_bcZSGZnr087NvoHP2ObkPHrmuG-k-pXla_QbXU1ZBj0ySxTEKN6ViTkKk265BfPf2QlkD1-8aLbZB3oNBDoOsCIP4neUy2DsTyL-1lIwFElg9IX3nI3fTk0YHD8MMZaQYBTc96GMmcg08LrIA3iYuuPvU3CHjx-TH4ei7a_UchVTwRup25B34NHLQDYAv3ofdtC7X6PxycOTpXa123PjDv-cjWAu2OpkSzFAD2NCPCts1/p.png',
      'https://previews.dropbox.com/p/thumb/ACLratX7tPDLp-RwISPA-XwHqYK8BTQd5itJxz6PkSweQSoYKFQeiDaYd9sy0XI0xHCPi9ml1c4RDlYBkqEwFMqR4CuJvmh7HjaAdY-jP4iWhf0yVXQMZLKlmsLUATdfyVjqPvjJeLcm92Wm0pV_UymvbmtdJo7nTXOjAgh73SNx6I1Y1SKGD5NZ-xiv6FPREKf8K8EGnLFYSwIyF6snnWxhfFUgNanJF5kGYv3yN6MMd7x_kLrXX1F7ykAtfND7KInzk9zk9kIOzpVSSnOEsl59S1wbrXI6VdbsXVuMzagxSqEqgJbmGtWHhU6UT4n-ka58QooTTDh0IUztTLVvj6cy/p.png',
      'https://previews.dropbox.com/p/thumb/ACJZzVqAzBIfexNdI4wRJc4gC79NjNAfBhz6RW3oDA-t4GeKYr9ktphec4rg59BAFrTXF5f-MgX8He3Veff3YSQeF5SR9EMCbWASCBEA7_GSf0SOTd1ia2Tab5JrCWCPMfJ9KqZPA6RDdrI6GIic3PCg2n4BCKy129V207ylsDdwmj4YHbgS359wykGVQ5uLQydVdhP5S-9-uVlrpiakzcpm1aIzW0SPlmoZbm2Wb3fvSV9cdr6L0JdQuV3GlU7CDSDtd-i45ygrWGKyP26MUtKdAPF5P-DSvkJ31xxJ-wb6TQ4yd58ZZNWhMAoEUBGE2ul7l2w0mM7Elmwnk8X2J-sS/p.png',
      'https://previews.dropbox.com/p/thumb/ACJu1QgjeNRRVR0PEd0n9F0g0UvKJGyP3lKoN9H4TVibT7moePPvW1352cbt5I9DBVRPn6cbEQF6lqlvhgnNsiVJUOOR92giupgYBFLSJo_hx1Z5f2B_GVpUSipCR_qQjM1nkcnhiD5Z6-WnLr6eBfYOHmFyji3BZELYD8HtmLJUkwY2MV24-8hHd3J5M16SmPzNG_VwI_dv2ZvBB36er9DM15C26YynSHV3JNm5jTAjeY9ENF83W7SZlXXPkPhcjZyp-vbTDGHiW1Cuwt6ujUuNuV5ebkFrtjWBzmSicC4pFhyAdR-80DDeef0meLxWZ1EHkkSfP6owIbgZAj2Dz_NX/p.png',
      'https://previews.dropbox.com/p/thumb/ACLPJ-8o-Lgb66yAbolPdgh7C4dzSuNnabSupqjPEwhBL6on2uDJTfgfpdG9ivdDqBVO0tSsORfxlAM6Zd_3KvHsHUojav30QbWS6Pj3ccn5HNxXx3ZbhQtIexO9YPYNuXIqnlCv9_vDaGBkAOgIKp0Ix5EBY5n1tTf0-uN6vmX7ZoELK4cbrvEnlHuIz3Xra1pN5s5vxrLpDDVh81kN3VhWo_ZHG3OplWUBOX1PmrHnjMLZAzTctck1RJY4AxZ2SeZk40moY2_5GWx21h4B64gpYf2tde9v6601_Lap3GLsrPZ34vfnpLMvISTdK8Vw4ZbEbTX920u00wm_XDQVXhD8/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'jumeriah-vittaveli',
    title: 'Jumeriah Vittaveli',
    location: 'Bolifushi, Maldives',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLPFu4CeiybP0Q0RkYCEtzhcObzImys3KRylA5MGLPDZUmsB3it1uto82FlAlinFfi4Mpi74GRMT1JxnaVZsEWexWwUqHXe8WrEf7cvoqQmD4LxJy86rADG9C61txO7w5xKXGm4BMjb8P89lJt58vzjbxegkiOBZ-ST__di0yeVWhDNLwypgHrb3G5a8DlMwvqTUxMXIOxOqmWaKYYMqTTvmObEKInNVfo53-rdceeEkOqmhUlKRf8nQGyCyHj-IAYHgtsCx-klkF_aSnru-fMlMOaVYH52YOxNc0wbxd2yz5GY2oP60m4GiZSIv0w0Vt8h4Ro7esI9_zCB_HKQC8uW/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The Jumeirah Maldives is similarly isolated but the aim of the design scheme was to create a sense of community. The hotel stand adjacent to the small village. In order to honor the living heritage of the island and give back to the local community, the hotel decided to create a connecting corridor. Local stone materials were used and lighting was employed to reflect and highlight these textures, enhancing the indigenous building culture of the island.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACIfiRbM_qa_O3V0vKuLdPLRvmUk6F-mMxXfmizKRmUgCnzOVW1-5vHeowEKjOkUypvvsTA79U7Cw7BGIithPfwljyXOV9WeEvHJNETPei01MMZC0QJFBrFhJQExy23hVlYZs9qCp7khlROpjI6UxaX6kCw4VCvoJVWLuEN8rKlHFqoQKZUcChXP6TGXo-Kep9ZwUQ0IfWaU2QAbhQ_AdSzdTi3DZ66Or9JNMRYqeoAsYqN31k_lY5M3uDSWZTJzER4taFfA3F3f1tKBH0YifMzLLMoH2JGJhajPmX-M0gn7M9j5JWHkoQiIQs9Umtt2xEjxEEoY41MMZwIUTAkMGj8B/p.png',
      'https://previews.dropbox.com/p/thumb/ACLPFu4CeiybP0Q0RkYCEtzhcObzImys3KRylA5MGLPDZUmsB3it1uto82FlAlinFfi4Mpi74GRMT1JxnaVZsEWexWwUqHXe8WrEf7cvoqQmD4LxJy86rADG9C61txO7w5xKXGm4BMjb8P89lJt58vzjbxegkiOBZ-ST__di0yeVWhDNLwypgHrb3G5a8DlMwvqTUxMXIOxOqmWaKYYMqTTvmObEKInNVfo53-rdceeEkOqmhUlKRf8nQGyCyHj-IAYHgtsCx-klkF_aSnru-fMlMOaVYH52YOxNc0wbxd2yz5GY2oP60m4GiZSIv0w0Vt8h4Ro7esI9_zCB_HKQC8uW/p.png',
      'https://previews.dropbox.com/p/thumb/ACLPFu4CeiybP0Q0RkYCEtzhcObzImys3KRylA5MGLPDZUmsB3it1uto82FlAlinFfi4Mpi74GRMT1JxnaVZsEWexWwUqHXe8WrEf7cvoqQmD4LxJy86rADG9C61txO7w5xKXGm4BMjb8P89lJt58vzjbxegkiOBZ-ST__di0yeVWhDNLwypgHrb3G5a8DlMwvqTUxMXIOxOqmWaKYYMqTTvmObEKInNVfo53-rdceeEkOqmhUlKRf8nQGyCyHj-IAYHgtsCx-klkF_aSnru-fMlMOaVYH52YOxNc0wbxd2yz5GY2oP60m4GiZSIv0w0Vt8h4Ro7esI9_zCB_HKQC8uW/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'marriot-hotel',
    title: 'Marriot Hotel',
    location: 'Singapore',
    scope: 'Interior',
    year: '2011',
    image: 'https://previews.dropbox.com/p/thumb/ACK9GL13LCiQ1_LHufGOb1yrk3fTWnTbVQBl23smEL-KdNYIcENQO4Xn2yrT92q8eZxpuViWIO6RpvUmHrxJM24-0j3ONXktTYCqhpFyFSZX7eDb2oRpYKiPc_VbUGwoSGJf37qrfk3ZfLRvpYgAGASf0pETcS884wT9PLSwukKe84SQKXJ2CRjJF2JzkF9of7CHH4qDREemQ7VGAzsHuDHd7VIT9tWFCpuiVqb8-34wKkaiwBHrYYv_eIEe0tOyF5NIfsX5POVgXYtyyznzZ-G6d6en_DuvuOeQ53HwdyYEyCxDzPBRGCxFBRVPBR9QoFFwGPKpXwf168sjs53iEcFo/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Marriott Hotel has started its renovation in 2011 to re-brand the look of the hotel internally. From the old interior to a much more contemporary feel. Modern contemporary language has been blended so well with its "old" architecture style and create such an innovative relaxing space for the guests.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJ3V8Hka-6pjGPCDvPPKC43T2oe0GzRATc52MSvv1M5wBFhkwEn172fodVf8RLXJ2iRj61GC1qiP_A5fM0cMl9sFXb1427eVtaBQwOmLYC7HDxoQiZQ4hTvtOnLMzSEwvmGNDTm3JoZCgf9SD-ew7ITaEHtD7X2yYATvUlgcimG_tXJueSrprKLU14H_IrDpDVVwiO--Y0LakI25MjusSKMCesq2gxiVUTSC7_DwAh1bgn8K4rZEf5TPmS6gw_XN7178iZ5cMXylNj4alQTWElL0QDbwNpem2iH0DmsHhEZz9n0aA4mvsm4Yl9w6Mt0zz8RCejcrLxBrH_rI-Ze-Xua/p.png',
      'https://previews.dropbox.com/p/thumb/ACLFmQsI4ez1OBuAWB3ytBQ4uh6X4WJ73eZ_zmKMFo8HwZs98LgOA357X2CrUNc-PGcVH0oSDsnUNsmkLLQ6OqFdIfjgyXtJlcz44fhojkA_B7gMn7VbaKQjc0f4eOyrARVNeGpIZGNY1pIpEp__ApVM7VcRTaIX9hhKEwebPDaOsGc5E82YY2s4u6RV-4f6Uelx2hufafLxrwiHNQ_gjxQZLFEZDP4WuhcdWDHpu4wenl6ZQl5plWE29qcamQ8eRbRL7C9hM4gv2ytdR-aM5eCOBgVE2RC_s7IAUy0KA1ov-uJhPpHT7usAaKMaK_83biKHo5rC9UCk2tOxLM0u_Cka/p.png',
      'https://previews.dropbox.com/p/thumb/ACK9GL13LCiQ1_LHufGOb1yrk3fTWnTbVQBl23smEL-KdNYIcENQO4Xn2yrT92q8eZxpuViWIO6RpvUmHrxJM24-0j3ONXktTYCqhpFyFSZX7eDb2oRpYKiPc_VbUGwoSGJf37qrfk3ZfLRvpYgAGASf0pETcS884wT9PLSwukKe84SQKXJ2CRjJF2JzkF9of7CHH4qDREemQ7VGAzsHuDHd7VIT9tWFCpuiVqb8-34wKkaiwBHrYYv_eIEe0tOyF5NIfsX5POVgXYtyyznzZ-G6d6en_DuvuOeQ53HwdyYEyCxDzPBRGCxFBRVPBR9QoFFwGPKpXwf168sjs53iEcFo/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'laguan-wedding-chapel',
    title: 'Laguna Wedding Chapel',
    location: 'Phuket, Thailand',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIwCHEXYYVoC2y01No_7X45XdOQ4wRWbrVirdKDBQGUjSetxc5i1foRP69W9PAeyxDaqyYQ1FSAyoVDXtTccmUmoyLeRrrvucFwnhm8hYJ-ildWjdnvatdFQRDjBMgyfbExEWXgNYCXOmDKhV27NP1nnWLRbcOGcrBhZajJk6UU08adqZaD4ghmeOr-4fZNzSJEnqxO-MkZaLrRa2R9Sj2268LvWfesK6besPpW06N1ZyYOvUdL281XJ6wJrDrz3YyfLCcpBHiXEDmFdCXHN_DBeFQPbnEr7l63Rzj1fVBY0WNaou72gNDReZ2p7yQU_voilJKqMxVnFMqIDYEELhKf/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Light responds to and teases moods, atmosphere, emotions and functional requirements; it transforms this architecture into a theater setting. Lightbox's Laguna Wedding Chapel in Thailand recreates perfection for special moments. Its simple and clean lighting emphasizes the Thai vernacular structure of the Chapel. Together with the merging Asian motifs with western conceptions well-being and abundance of light the chapel which was painted while inside and out, carries a heavy emphasis of the structure's luminosity.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACJf2es8fnwMPKXZ6uo79KEXtPnVp_sYQ5quEGu-sLzYL_2B6Bw-XkX9HXDgFB4MPIhHWUmIgWXsqhggrHqRddXrPskO3jNUryPu8gHHg65EjIQSQuntu3YG0QDPZGS0vTGF5HHpE9ltEROeMSlcjGtZwFUZ5Zycm8n7CDTiDitDw-27sBmaehzvUjDypnfptBI8avg6mReQua6ebmQyRUxG6x_6G1YZtPsoRp7uAtTdW7C1pNEja18bUwd0TOPU3SX3lAYyzExP8JUdqwb6SrRZQSZSJn7yRP8_qTfHmKIanKKBwcA1qyys76V_nUz2pAPIMvQTzv20ztcWrH3GzhDE/p.png',
      'https://previews.dropbox.com/p/thumb/ACIwCHEXYYVoC2y01No_7X45XdOQ4wRWbrVirdKDBQGUjSetxc5i1foRP69W9PAeyxDaqyYQ1FSAyoVDXtTccmUmoyLeRrrvucFwnhm8hYJ-ildWjdnvatdFQRDjBMgyfbExEWXgNYCXOmDKhV27NP1nnWLRbcOGcrBhZajJk6UU08adqZaD4ghmeOr-4fZNzSJEnqxO-MkZaLrRa2R9Sj2268LvWfesK6besPpW06N1ZyYOvUdL281XJ6wJrDrz3YyfLCcpBHiXEDmFdCXHN_DBeFQPbnEr7l63Rzj1fVBY0WNaou72gNDReZ2p7yQU_voilJKqMxVnFMqIDYEELhKf/p.png',
    ],
    featured: false
  },
  {
    code: '16',
    slug: 'oasia-downtown-hotel',
    title: 'Oasia Downtown Hotel',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACI3Wbx6xmECZLZRO8-B5K4Twe5fF9oMxK9QJQ8C2hHE9h3pgv9bEY_uoofYGXREgAYvY8Uy2s2i-jgRJlDBVZWE4bVMAilp1cJtwqnC4EQjwbZK7QbPxQjF9Xgc5fvjiffjKoqUpV5-4kkI8B3fkmIQNK8Sa4-J6vj8wgUc8L4FdqhXRgEJfjvwvVqafntS_lhbMLZOcvVRP7NI70DvcBeAYQ686K9o9ieLBSOpoupoHOlruBmG1Qt_Fhoa5Fx83mlQmqvQ8HaZ94LgINIYmQZv5aDySGeF1i7mMgE5u1-oKYNC2V9QL8YMIAQkENphglS5QzkS7o8R4MCZFZO5RjEy/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Designed by famous designer Patricia Urquiola from Milan. Stylistic, elegant and playfulness of contemporary art-deco interplay with lighting that is designed to accentuate interior design concept and style. Lighting of copper patina in the corridor spaces are specially curated to give different experience to the guest. The lighting effect created by designing lighting fixture in the copper tone reflector to experience softer warm ambience.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLHBVcFzFeimZa_mSPR5dkoxs71La-OUli7-rHzTdYF1ZoBvlTqOGqavcpCUczhi6XSGdC57WhkPpDVvjymHpr60DFmscoLAing4YXUiwfgGSWhXNbowFQ8GGLGaSNxWsdI1zX_gZwgw0G0qcdohkjOkgbz19qWqmZDmh3eyjJZxair2BenZDA2k6Z0Fix_M3wgFrcS2QaZFhrxEua1Vom6VG1H7KW_LpVOS68gh1nFl6LZqBq7Rd9wP883SlOlAztGL-FINMfDiXcCVX_hQ1yxr9SaACoBrIM3kpwQZGGIkymEDFnkvbRgH17prKKE4w2iQS7IhOK7NLGQFaq6-Yql/p.png',
      'https://previews.dropbox.com/p/thumb/ACJZWl_8vNZcY4tNhdNS5Kq8Y1UdrIo6hRP3hchS_oo89iGBgHvqpgubQrjpnCye1LGHpyB6F5p7wRO6LKVF8y9SOfCvREMnQ5-ROr2Tnzy5RXwFjBv-VJy2-TjxeDsh-ihPFiNVsei7oGW1N6uXVvb3WYocKqltCaY0f6yGq3tEFrRUpk8lLMh3kiGjWghs0LZOXQSkG76QYfJj_w8PqZjPGwB0ta2kBrANYnAHxljJK1_10l0zK-JxYlmtYExsbnleSkLkydiXVd8LAqV-zpZkrxLyzsMhJR12ydpotdifk1KpklztALipWrHjHLIKjIb0Tq2Cg178kjKADN5pCCHr/p.png',
      'https://previews.dropbox.com/p/thumb/ACJTzlLgtKAP8ymQyLRyBqam_j5e25oJZKmYBMkWbqxx6hD9Hq2V1zx0hvHUch3P-jtXOLA0zZJwnQfVCqd-s7FgJmRLUuxrTeJdodNDGsxfKxWtPUc2t3ILph6dNPvNXQV_wAPr8tYm04wVxUzc0SPHCIY8b0kx-Pia7PTV3B8IEwEWE2Chgpsz5Pd1DMLS5Ke25O0AsYpvnO1Q2PO75HP6yoXg_9zk_ZGpZc-fXoSvITZ2Yb2hlGMc056AHJ_on_wTs31-Y-TzrIQ3DifoHFKDjpAMnADF_jFYRzZ991vIR49_tRr6VLx8V3hL0ahcgkUxPXV0Zib5UDdXVC6ZahqC/p.png',
      'https://previews.dropbox.com/p/thumb/ACJTzlLgtKAP8ymQyLRyBqam_j5e25oJZKmYBMkWbqxx6hD9Hq2V1zx0hvHUch3P-jtXOLA0zZJwnQfVCqd-s7FgJmRLUuxrTeJdodNDGsxfKxWtPUc2t3ILph6dNPvNXQV_wAPr8tYm04wVxUzc0SPHCIY8b0kx-Pia7PTV3B8IEwEWE2Chgpsz5Pd1DMLS5Ke25O0AsYpvnO1Q2PO75HP6yoXg_9zk_ZGpZc-fXoSvITZ2Yb2hlGMc056AHJ_on_wTs31-Y-TzrIQ3DifoHFKDjpAMnADF_jFYRzZ991vIR49_tRr6VLx8V3hL0ahcgkUxPXV0Zib5UDdXVC6ZahqC/p.png',
      'https://previews.dropbox.com/p/thumb/ACII6scY2d5r4c3lr_KK4KvYnSHF_CL9HOFtIuI4Jn0-GH0TEJyDnKLGyFnZpNLcJNGXlqDz4oZog_E60hakB4dvzX7qxdfqExMtfSG4KvLTM31Yy2_ARgBvklVD0pky4a031BgyxEml8j4qoPZh-SR4LTAdpwD3p__DEeD1ZkUI-GAbPrHHt9r1umH7JR5EbOVTjwJVZJzprdShUT8dr-bsIjpG5RNNPp9vHymShsJSyXSWhGL_NsEtPiEA18EqwIWR8PlYzoqDk0qsr8e3Tg0JETVk2LluRTR5jtp3VhIZ45wnCFXk0sf3CsQEzX0IKMuboY4z8-C8Zk66N5hi4z_A/p.png',
      'https://previews.dropbox.com/p/thumb/ACI3Wbx6xmECZLZRO8-B5K4Twe5fF9oMxK9QJQ8C2hHE9h3pgv9bEY_uoofYGXREgAYvY8Uy2s2i-jgRJlDBVZWE4bVMAilp1cJtwqnC4EQjwbZK7QbPxQjF9Xgc5fvjiffjKoqUpV5-4kkI8B3fkmIQNK8Sa4-J6vj8wgUc8L4FdqhXRgEJfjvwvVqafntS_lhbMLZOcvVRP7NI70DvcBeAYQ686K9o9ieLBSOpoupoHOlruBmG1Qt_Fhoa5Fx83mlQmqvQ8HaZ94LgINIYmQZv5aDySGeF1i7mMgE5u1-oKYNC2V9QL8YMIAQkENphglS5QzkS7o8R4MCZFZO5RjEy/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'the-novotel-hyperabad',
    title: 'The Novotel Hayperabad',
    location: 'India',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACKH4f0yqsLohJ5oI0G3mVMG5nJQ5zFsYKQPio1lpsQhWoFW4zabQggxGnwo3-rLhwxiH1WjIsR6G1vOBMnpWkOUayFBlx84UhiyTzvRfPiPf35CURZ-u-4HBP6sG1POgx5LGkKD1lcLP_5U-tidjHhd78wMsARffM2aCsC-dxFs1dd6ooxxNjOu4Za23pWtBBvC0HUBJN2kZyoTtedBqfA5ikJSa2UpOCr-n2SX236dGeapnzrufvUkY0KpDNiviZpqpboigGS-KURzNNzLanuJRIJGsJyhTxHDVlzzLqrn2exW7IKRpedI-_NKvVmoQRVe1w5hucDYUzSLOaF_O1q8/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Completed in record time of less than 24 months, this 305-rooms property has all the facilities of the top class hotel. Ideally located next to the Hyderabad's new airport Shamsabad airport, the hotel has been planned as an airport hotel with international standards. Designed with modern interiors and minimalist lobby, it allows the grandeur of space and natural light to be appreciated. In the evening LED coloured lights light up the lobby by accentuating the reception counter and the bar. Ring lights glow in the lobby directing the guests towards the restaurants and banquets.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLOjzbdveTNU6JceYPbxIe0HU-kLLU-yJtXjcuSDq7sPxn_8_Ycr3ruHBvRUYZmJyGwXOPviSvxVEY15-vr-K4mFtGHfE3RZam_zdnv2TmI6Hn2q9NjGrE08LZrhHD5vKYELKTNudaP3NSaWeNTfLDyhcLzB5B_7OnQ5AmG2BybY9t5-T7fzHgxVQ-69Ummfmc4LbyhFFKhRd-RXrML9gk2YKxbj6URVCuh8iEItUMbmP3VGUk8dnR9hHW9xwf3qb0m8eom8JIm4Nys4ZMWQw3StVyaVXOlN0WluVcCSFyEMt-adud4ZRwN-AihFBc6KN_4vylKLfp2yz6DA12Eb2Zw/p.png',
      'https://previews.dropbox.com/p/thumb/ACLN37C0of9MQOAOThk4CKl-n882jtNLdorN83A_DLYCs--KweqyxZT2Y2FwghK4NMulFR2hz5D_ou0_Kpfu00Qrkcv-9cI3o9hoIoNE0cNM_nXKE-L4o3GAAj_oxC1HP3cQ_tPhm9LwshZcfrrJleaZpzmR_GbjjMeAntt9jM2YOuWYh4PWUo1swoGo4tTh6JDZ-YlDXdA03GXHvX-ijo-2thPb8GG1dj65aqinGeQLHBSk0opBWY5DFQl_xRPtf9jIb6U-RreA-UOlE0WT7sxOHS3zZZo79LRsH9mgtgWhjFOm6QZkRSvdYQQ9CnLhOC8dbSUq0JGKQ9owHWloom7-/p.png',
      'https://previews.dropbox.com/p/thumb/ACKudGGV_a9tnqymWp1OuhPzmvrUJrFvB-mfZ1Y4nCX_1zF0cK8AfzaRRqpgBxFAPCAMXFU8hPXrEFMxFbuZeQGl11Ck5948uF7JSM0xN7G34ZzqDqo8DPMer-c_dtqCcp7HHbTXtzw4ztpJJlM0Q5kiOmh6nPFv8dnhdPxH-ihMMX1LI7a-6zSbl_mCz-5QnU6yYJmnrvlsWZpuhYx3U5DDzUO33L2VZylwlOD0DFDje3D5k2MgZIzNOwKOgQPjzLXXU7DHbvI1_u6XeuBO-Xr9sWvG9T1MpG8OvCflJJwzauKP7dqEAwGVRmLTlZVNjiQSLP5zsiAoMW9VNVkIyJvm/p.png',
      'https://previews.dropbox.com/p/thumb/ACKH4f0yqsLohJ5oI0G3mVMG5nJQ5zFsYKQPio1lpsQhWoFW4zabQggxGnwo3-rLhwxiH1WjIsR6G1vOBMnpWkOUayFBlx84UhiyTzvRfPiPf35CURZ-u-4HBP6sG1POgx5LGkKD1lcLP_5U-tidjHhd78wMsARffM2aCsC-dxFs1dd6ooxxNjOu4Za23pWtBBvC0HUBJN2kZyoTtedBqfA5ikJSa2UpOCr-n2SX236dGeapnzrufvUkY0KpDNiviZpqpboigGS-KURzNNzLanuJRIJGsJyhTxHDVlzzLqrn2exW7IKRpedI-_NKvVmoQRVe1w5hucDYUzSLOaF_O1q8/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'park-regis-hotel',
    title: 'Park Regis Hotel',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLV7MwLo41MYNEB09Jvu-dx6a-Xqy5qOodoCLdMlVvMxLUwPpq-EIUFgSD0H-o2OTereCmo-VugVvGhYIi8Z3Zuo64nKifUa2fFUw96mPQqU-agJfpZwozcqTnry7pqa6Gy1Oubw1_K-HBLlqDXiZx6_P6-aFgUVvskG3fLcBOhzd1ten_zH0SX4b_H3WqGNWJLf3jPbx-hvxArlF73QE_CDhjaTKqzzwKjnfzChJTGqcr9OXhnHyOM6iYadNTYLaTI8umnRxm8WYg2wOs_M19dNCAqtgNb_izgIM-QkMRWejjSaCBjfx-ldEkQMNF9sTHx4nCNXQ1zDCYjH1Kubx-izHi_1c2_64E9ADGf4SbwKA/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Facade lighting running along the profiled-bay windows are expanded from one end to the other end of the building. This suggests an active feel to enhance the concept of a business hotel in a city area. The interior lighting enhances the contemporary urban resort concept by introducing subtle color of light within the interior space; resulted in a dynamically harmonious ambience yet quiet state.</p>
      </div>

    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACK0nwE_fYDr-sbmk4052p5y6WC2yGAPg9XrYIrRp2MicapurqZW8nYJv9fHKmNQs4fTS52IwFRse8rC_ezq-yBrF1Ic4m9310232YRmawZs_zl7VC2jLRfRws8NfgnPBXgXuhTUkSlSLcwJaPSnf_9mAH54Ene10k7Pq5Xkty6p1_b2EVUtSh7UlEeCVD2WQ2RBpPN_wDifwMqCu5mGtOeHjGIervE2YKVvat4f_Q76KHivT0FovVqZa9VMTHhspRTqWAa6ve2eZEKCnFnJ5cUuW1Nz28c938c8w_Pszdgc45hFPab10r9rxzkpNEHA5QWi2L16sjV-59aCzKm2CGOG/p.png',
      'https://previews.dropbox.com/p/thumb/ACLxzqtZ4RdMWTLvC3-c5YiDqgrGEEWtgSFVFIX4EasYHzq3A6qIl7wFmfGF6E22_wt2DxhavoOhr9nnk9ZYZFejy7sizWB14k26DoWrPDQAYkY9EgsEO8BI1R7xj0T5SA8_5u5SJLNdnhy51SvvNq-1YfnnOSyX0cqXXlYQKnBOpzJgCbW3AejGaWXErVeSL4kL5YgFco_kAkYmiLIMlTe7hUMVmuZmz_ppKXtFygmm80W0Z9TTBeHEGIgUVKKyCJkzP0Ww2hT9Rf4r6gQXsmqPsFuQl-0RHYXWZ6ANg60TYs-k1trMwOrAA_YlO0Vj55lnSbrdDIK0ZzaEih8ImClUYD9EYGQWo3zJSya2XD5xCA/p.png',
      'https://previews.dropbox.com/p/thumb/ACLV7MwLo41MYNEB09Jvu-dx6a-Xqy5qOodoCLdMlVvMxLUwPpq-EIUFgSD0H-o2OTereCmo-VugVvGhYIi8Z3Zuo64nKifUa2fFUw96mPQqU-agJfpZwozcqTnry7pqa6Gy1Oubw1_K-HBLlqDXiZx6_P6-aFgUVvskG3fLcBOhzd1ten_zH0SX4b_H3WqGNWJLf3jPbx-hvxArlF73QE_CDhjaTKqzzwKjnfzChJTGqcr9OXhnHyOM6iYadNTYLaTI8umnRxm8WYg2wOs_M19dNCAqtgNb_izgIM-QkMRWejjSaCBjfx-ldEkQMNF9sTHx4nCNXQ1zDCYjH1Kubx-izHi_1c2_64E9ADGf4SbwKA/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'Qianshen Spa',
    title: 'Qianshen Spa',
    location: 'Shanghai, China',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACI1ciUC17WzUlulzsayGGPnyNs65SzSFSdu9PrjzpRqSEvUuteb-aNvZiNy0DueCTBfo8Jmag6MBSHOZ0mUQoqG-_xJ3oYE-TfJnb3e179xK9G42Rz19UWX8uhGKbjmMdEElFZATYzXC3xt25KFPvvSK57TnUPUF-ZbjD7RLBERHKVazYP1v8n5hwLRdVKybrsCNBq8V48bRKc7BPR8AAgK44ngOGUQXNpOIAxIQ5DQB3caksrvkYnq1tIonCUFLtuJgkrXUDZSC__z1cMVPToPf0KG66akrdF_9MAED4FfRGDv7wgw5nkQziNWRJhj36vCnWnkZt_DuRTxezE3rGI_/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Lightbox proposed a lighting concept of wrapping the entire interior and facade in gold through the play of colored temperatures. Carefully specified lighting fixtures also assist in accentuating the different material and scale of each space that conveys a grandeur ambiance. In line with the brand's intention of establishing it as a high class spa resort., the entire lighting scheme has dramatically enhance the visual value of this project.</p>
      </div>
    `,
    image_collections: [
      ' https://previews.dropbox.com/p/thumb/ACI_EPyx7eFB_39faaVUEC9xhOYz4sOa31bFWSGFiwU2lVPiiWtShKpPG_ZARuwsCfvfpAn_oE6aOTxwOtcKayFT32heFIXUA0UTiIi44LbJIyjw-4VYW2sQOLk-A6aJkeoCDR6pFvIAnHSiBStk8JmvY3FkofFEUrDPvWlcwf5SAu2b_zScJGREF78NBnpEJKE0on-7u6CwXA3j17Pwos962_i8Hyyy5hY_yIJhrA_ZLzf90DxPxHu6-Y3ueOh4jHFzRI0PkOlaYdF1L64YhRFEltkb_4Xor76u9N4BI0W2DoYXzPywqn3Ga9e6ILrZYYFBg5CE2alxKl1UUx9keZlq/p.png',
      'https://previews.dropbox.com/p/thumb/ACLo83DZUSVVz1f2cdiNLNocuUx-qc5wWqEh-68o35umkAJ9fPyaebfOVrWJoCvkuTc2RsAp1Dp5bSsOQlCni2Jw3Bpu8JbrCgaUZT9VDUTtBaQqr2aiG6W2vmp_-4nI1XN6n67qnPOh5SepJvifX2tpMT7bKfuNvuaLxW788oRgfvM_ie4PfajJRyBepWOuEZWSQzEwK5YuuQueBA1XKKdoby9fhd-B-knULX153yU_DDLUnt2yTE0K1hGqXoY2M599NKI811PvT1z7BdexEboU1DfZ4DKatjm3g2HBeU8xWwPCPYrvBPRBg_PQDVdYKGcc_CRsLD0XqhNgV2pftxTNTb3A-rzF6liGrXqpvn46pg/p.png',
      'https://previews.dropbox.com/p/thumb/ACIEWO_s3_ERrYKm4AzFWL4dxhdWf3shG3zv0yLU4MGobD1yqcnzbrFDeptoqnCzq7vEcDPPT8GQlIvs5G7cAWLEgmsKajSwarktYbUeHsZRjXt-ENQR5J1yksQp4oIKKNjbMTdGFPZMtqBSEpdf0Ed7qD01ydA291pgMwSgVPrhXTfoJN0rlOsYCn34VLT76Kb82UGgKXomufP18F2Do4Cde6BuH_SUeKXf01bRv8Bci91AgfHjt2igXUUTHOvAB80qBUT-jPL-oMWwRoL-eUgNJXSHyqx1vlYFIDvlzdauQVqXXDQbdT51KLXduGN1d9fSjVy4i_o5MLQJEGQUV05I/p.png',
      'https://previews.dropbox.com/p/thumb/ACLkV0fLMqtw-z-mapNmrnXDynzyVf-sLY58WzeqtxMzwVahohQTOVldm3IJxoqMh2TvXwi_zlfiBy-AkcHQwF3Of6DF9mtlCzKuCV3jWy55xLRMVlEoqBn52rozY_MBQVdYlWky3c-6rDLpL2OzYCL9uWDOuc4-WJOFG_tSU4US8MjL7l4qSJfpvZcH2jjrTBZs-iU-neiTACLAj3kgqvXwIQTs_2vBTc4kg2MFX4bMiGgJLBeLHsehm5OT1t5h6fXmggAK2X8jVPhPmdvBsQvwcvVPWI9ZctwmnnEC3tAmGZ6qdcxStpn8vwx78dYUkJp_IVFPtYUFO3eSTeLRnbQtAk3F7TqNBq0E_mZVL-JuPQ/p.png',
      'https://previews.dropbox.com/p/thumb/ACI1ciUC17WzUlulzsayGGPnyNs65SzSFSdu9PrjzpRqSEvUuteb-aNvZiNy0DueCTBfo8Jmag6MBSHOZ0mUQoqG-_xJ3oYE-TfJnb3e179xK9G42Rz19UWX8uhGKbjmMdEElFZATYzXC3xt25KFPvvSK57TnUPUF-ZbjD7RLBERHKVazYP1v8n5hwLRdVKybrsCNBq8V48bRKc7BPR8AAgK44ngOGUQXNpOIAxIQ5DQB3caksrvkYnq1tIonCUFLtuJgkrXUDZSC__z1cMVPToPf0KG66akrdF_9MAED4FfRGDv7wgw5nkQziNWRJhj36vCnWnkZt_DuRTxezE3rGI_/p.png',
    ],
    featured: false
  },
  {
    code: '20',
    slug: 'radisson-blue-chittagong',
    title: 'Radisson Blue Chittangong',
    location: 'Bangladesh',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACLNHRxDxs_4lczr3gvxuYdJXtoNqq26cI6t-JWCgqmN4qpwYeD54iMdixz_L13ewdNmLg555a1A9OuuWHch8EK1djo0yTzOd-Y06Ugjx5sZahZLdGsrSgkSzcxAQ7f7HctKI5skzHy8mPXJ9P0WuwIgpGLWA09VctZCutYIs0P9zIWjb5IMwomvS-IEFbCF8OwfSvdGYVRzqZWXBGfIB2dtKc30lPLHBIOMd0oLQoOC6jlhdUcYxBB4vp_JEbWL3EZqZxhOKu46DVC1pSea1jg_xJ9KvAw_36feZitxvHvc94TZhfJkKZ5rPYeGB2Wu_3LR4lhuOPC7iAC08IiboXqm/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Estalished as one of Bangalore's top 5 star hotel, Lightbox integrates carefully specified lighting concept and fixtures to improve its visual presence even in pitch darkness. The overall lighting scheme intends to bring a sense of comfortable belonging to its many patrons.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACLBZyHVTfpGljvqsqB5UIpTlAURaSZ85RlIeaoKY7m6A3fOigA2Z_1Q2vEqul-If30KGormjF46HOAavTK6AYpJJZgmBzcS3apg02ICZcRsQtY4PCh13YAuCtcVD1v9yOanbqYF7k0C5vAdWfy6wHYODheuGPqcGA_AW0TWK-N8Qs7a_yVAC2Hkk2RVCL3AnwAscz6R6LVO2jgI92etE54U3AW911nSGvPTg4BBrmaqC04yuRVMD8VcSQyJCi1HVX_3XhtkF5S0Ohw3FISdYFu4s_-hQ8nzhV1yEnBYrOnMBBWhKqZnRc4VV6I6dpUt5xX8gSxl7CDBxx4CQp6fq5ic/p.png',
      'https://previews.dropbox.com/p/thumb/ACLBcO10pfOilsKQYXoPxH5JotgIX4RdkaQwR0nUwrbDQ9CAEgkiVje2PjmVQU2RLVtdohtecsZ_4GvbLIZkntLiuOgMI6vUgl_6mWolcNgF9qgSVsJOb9wI3SJujTK_SYolr9_vKJhtJWlog9GRepcPkZZ83MeTt47H-rwhjbU2zb63gELo9C3se__gnAXvXP7V9w-bvW7Ly-W7ePxdstAUXSzZahZSrIArV3kwa1FdDDXTVB0MrFbsNzkgo2TGg8Lgt1g92FMFbxpLnyROArK5_RVKscK9DEZHLgbL1qPdK5lRYKHBjIAMplm8YGa2opgnvkQ6HXH6wrRZB0gLBUgyzgFcYPXvYM4kKgw2LUyrHg/p.png',
      'https://previews.dropbox.com/p/thumb/ACI2ujJnaFbL9XVI1zAjQOoK_AYPOMFpODpzQJ1HVNVodxtBt1WrUiuHvBr1W94d9thHmkFkUxfUZhKVAz1bzexBknd4NKK_Fd6DrD7J9Y43N1gpNpmojpLRw39D7XvreOKQuSIAX_h-knoC3hFIzro3L7p5kgVjZNbcQv5TsI8Wr_UWkTAk8DoHJhaRh6mcYcsFPBeJn8cXzF0-YK1MINxqKOgKckEh3UVUXHzB2PYSpc1GKc7LnKnvXGTRZN8mLSAuiVRO86EAG1ETbLD6u1in5b-cnS98DLSoSWCF-EA_htNU7yu_XjbpjjHIxM2N6IpTSkAvRvjxuhVnQ8Mwo3LZ/p.png',
      'https://previews.dropbox.com/p/thumb/ACKSjDoofJVmGidypS5VIvZW6c6ti3vk2qrOmxNbUNXCXGsAgded_5uZINBwEo1x-TheDJDbd6ln-rQrFpvSqOqX_spmvN-Jb8yMUuT26nQ9tB1i44RN_d-bNFR_xYRryZc0N5lVYHrkZdJ_sojgEbjzcciScOLjE0Gzjc4_Bkoqs8gWPmv2f_vslz9aHs2UT1D_bQlTxmyz_RSZzyPoMwo5vwDL6mXnju38KkaRy-uxEtIEg3YLuqbhmv7BlF2GpdUjW63EMXxzLk4SlaxyfYV5LIaVVkL8pwQxq9OCN03iUWpX7ISR7D6hAbtdICSPXRfFQrcHr65FSGL5jIaJz3BD6_8xKxAtSoD6GjaXSPiuyg/p.png',
      'https://previews.dropbox.com/p/thumb/ACIeOtbNFn2AXxMvJ2sJZ_p9OgE92UlaHUkHglnDOyTLYc7m37zBO2PEw_aXOyyEfSYJX0DvYMn30plxtNjl2l0Sy1o1SMJUU9hPCavU4ZDJJ29p7MuHxdyn8HuSuOYgOMBNZkMj69Ck9AgtKGdS3-Mxo2LeeAbDvq02D4otUK9NLDrxhfblzBU73H7kmSH7wJUwjMTXgFLzSCZxxNc1ISxsknr5T-LnZ6mv2AkSoqfDaN3bfkNS3Rgv1Yb5yBmi4OHRGhDQOROt5orQ-XxA-YMqO1mCbyCTBKqqw9YgLWBwmMafoXqB5S2ipFbbsoEfUGb9nHqU0551VnfW_DbG6ldm/p.png',
      'https://previews.dropbox.com/p/thumb/ACLZ_0SWWhtf6zqK_8IKd9iHGtcxGE1HVT9w3XorPIwA3gDHgWzGdkECAehmlSnjXaTPaXAIWCkK2YvOCmNtnty6Zxs9j8UufAcbiWsz8_i6wWkAEOULDYETvpzMnemozNuiI4PMbQO48i9xfYixfCVtP8h05qPMGW091V0DOodJb-rl3NkisMKalNycNw5jwjZBsqyLp1OKWKknMuL-GJPZCX5xAtZOn1dBgueRW_JMtWFuDDpJQvLDRYKe2dz5tn3q209Cod7vM3UnbELcRGjaSaR6qdfedw8ODKiTXAHP-0CDWPXahMWGyyzD9B7ClsCHQx7kfrG6GHjkGdSy-FO-/p.png',
      'https://previews.dropbox.com/p/thumb/ACJe0CQ6GgAtZCn8_Y5iaGQTMXij7TH5pp1XioDGvsp9t9KoxBPnUe9YW2WucTcttfrxbF1QVdCyPp1P5ZUc2UPbYoqsH1Gh0wfc_nVifPgfUph4o3AYPsjSWJ2ncKumgAZJTCRZ3PDuEuxk6IROGxoLBTJYeyQy8i2LVM1e1F0IEEws_PhQhpAOgmffcYcOq49qRYClPf99VULzaKpiTkdvX8hRPix9kAKY9_aIOvQSJmyIbqel_b0Ot0MXRVkbUbt3KdgLxjAwhptvyvlrcHIPPXDRcLuTJbtPqfqbHSrhhkmV7wknZDV5EUoDDl1rr1rjBG2A3eEfcxhbPD8pIw2B/p.png',
      'https://previews.dropbox.com/p/thumb/ACLNHRxDxs_4lczr3gvxuYdJXtoNqq26cI6t-JWCgqmN4qpwYeD54iMdixz_L13ewdNmLg555a1A9OuuWHch8EK1djo0yTzOd-Y06Ugjx5sZahZLdGsrSgkSzcxAQ7f7HctKI5skzHy8mPXJ9P0WuwIgpGLWA09VctZCutYIs0P9zIWjb5IMwomvS-IEFbCF8OwfSvdGYVRzqZWXBGfIB2dtKc30lPLHBIOMd0oLQoOC6jlhdUcYxBB4vp_JEbWL3EZqZxhOKu46DVC1pSea1jg_xJ9KvAw_36feZitxvHvc94TZhfJkKZ5rPYeGB2Wu_3LR4lhuOPC7iAC08IiboXqm/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'mapletree-business-city-II-(MBC)',
    title: 'Mapletree Business City II (MBC)',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://previews.dropbox.com/p/thumb/ACIjhnn-i6sGsue4nALpEYmPYwcAdEy1P0PMcTaKMIOJzvvhshATarbHqc9Hn9aVz-MwNSxPlwKLkP3AeGubjRYLQILbo2KDN9CiamdhHD02un4j6ggDWxWZdD1stl1jlTh9rjD6tUtCrVXht_U22BIHjpdoIZKfI0ItKnkeaVy4iV4TQps-q1byBX5AxiTPbbdbHHcXvbK-rfUKcmKSi8njl4YU94wKmczpCaSQH4Txu7r5TTjU8KWpefplmNQJVfLlTb7rhqxDDszLwocsC-IGwJyuoPKCD2rdw6C4WQkK9sFVl-FhwVQUJynmy4sffueeKIWacA6yjuFmPNUK63ev/p.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> MBC II project brings nature to several dimensions by managing light temperature and the play of shadows.The lighting design strategies was to mimicked nature's abstract elements into architectural facades. Usage of finely integrated color changing LEDS dots produces gently throbbing bands of light that stimulates the currents of the nearby sea, bringing that environment closer to the concrete pavement and glass walls of the urban office complex. Each tree develop a unique color temperature based on the green spectrum of its foliage. This light was then reflected off the tree, heightening the moonlight effects of adjacent clearings and spaces.</p>
      </div>
    `,
    image_collections: [
      'https://previews.dropbox.com/p/thumb/ACK2av27VFoQIDNnE8JyYJJmTwHW2kXZgF17Vz8jViBF15Pw0MQG1kIKwxLn-epgRkIPtrq7IztGXAWoMAeWrA7JXmD1lKcpaEjdurUXCiurHfevczY8OHH9M5usKfwtYdhyJ8VS4XzKOSaXmNV2rkv4TQMcUuL1M9hj_ZGnEk2g8t66joj28HlhpHKrcJRnmcVEj_VIsyM1Si92LjSDIHTuFNYkKz33dBk_sc45i90o0PqmrRWp3em67kYM6zm8OT9dlB6g1odv_U1GrddpVjvhqIck2lZEBpNwtAr6QLi3Isb99hE3Np8yZ398JypDzWsiaBG5IxT_ODp_3vEp8aFG/p.png',
      'https://previews.dropbox.com/p/thumb/ACIdmHwEjVa59K85YfVbF23GgR9eq-k8BHlh_UOW75CApP5THDweG-7eHaZurXG77jFhzC6vZcWu815ki3QIpe9VtZSRG7c1ZoAcaopWY__bGuKUPFulurSfYD446d8GEeVRu5gpR7U5TxU92nAQgUZTOYGexB36WTkiSnr7PzMhJIA9zhKVQDDRp4gJOSyy8jA3jC5iSJxOt5kZHawGx5W98mlOuQxkkmDrAfvD-FWXw0vCzaIviCLEmaei_Lr6egsEmOEAD62ytgNifZ1wztIhX7_IsdLRTzHPYYFEgvaY5LcpPweIEGcYitwuedZXugNt1mAelrwBCbVrPSN7JEir/p.png',
      'https://previews.dropbox.com/p/thumb/ACJ0PqObkNMi6slzUE38eunM8jcJ6CNzLNi1aMmzVwp6_Dakk4YzCORxZupJ_aS8tz8ipBVi1StH92SLs9S7qESE08gbh_XiYLwH2QuXuMpvCjE0RPhcVDHlUpVcC9OkzOSPHD5DK4PvjpjugmOR-Rai9ZlTbw8mTUPqizLs2zXSOLSfgvcpmmA9QOrE-3qKtf21BRJUDVuTc6VhlnEuTIttWdEbYmeKE85JScI0Bs8RXeCKFD6e4Ct-gn3Z2xZCchuynzJe-lQH9rTKYehnD5pNlFdIdCgR9ddkJEK_qKSFvSF1o8XoKKNjgAw-vzs-P6C5cgK1ju_ty7IXtzmimlr8/p.png',
      'https://previews.dropbox.com/p/thumb/ACLZHXETFPe4f_GJ9kt7NqBsr71iI7r71whgN5T3_xPMJkrVY7i0mehtIj7Z8ciQrbEdUdbGpn5YRqpNHQmHiDLQy0a-0oyAZc11Tei3Hch7sBGqV1HOCBIbmJa1-TxvuiXBb5shSLhgkfVBO5RrzFM1M-dNoFGJ-vvZrP9mg3hLLoloXSFMeTi3KbRzuoHLiuY1_h76PakSxasta313_zn7j3Co4oGUN1mlEM7ZBLL3X0OfEmfUfJWVvpP8or5Dsp5ZS6gTMi7zI5WoZOR9ln34zzV4ODNH5ippIgubPKnkr8UAU5oqS1D4N6RrtsH8Gr6RWw8sfMCOL7m026oq0Ury/p.png',
      'https://previews.dropbox.com/p/thumb/ACIjhnn-i6sGsue4nALpEYmPYwcAdEy1P0PMcTaKMIOJzvvhshATarbHqc9Hn9aVz-MwNSxPlwKLkP3AeGubjRYLQILbo2KDN9CiamdhHD02un4j6ggDWxWZdD1stl1jlTh9rjD6tUtCrVXht_U22BIHjpdoIZKfI0ItKnkeaVy4iV4TQps-q1byBX5AxiTPbbdbHHcXvbK-rfUKcmKSi8njl4YU94wKmczpCaSQH4Txu7r5TTjU8KWpefplmNQJVfLlTb7rhqxDDszLwocsC-IGwJyuoPKCD2rdw6C4WQkK9sFVl-FhwVQUJynmy4sffueeKIWacA6yjuFmPNUK63ev/p.png',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'sofitel',
    title: 'Sofitel',
    location: 'India',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/xk92350rjzy2u45hiatqr/5d0a56d16add0-1.png?rlkey=gmy793i21ln6w30gs90c25p32&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Sofitel India facade and Interior lightings are the reflections and cultures with eclectic style and cultures in India. Each Interior space is designed and expressed in different feelings varying in color tone, furniture fixtures and lightings. Although simple lightings are used, it is able to elicit very rich favor, color and interior depth to the entire room interior.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/jcfdnqgenmmk8py1bhtwi/5d0a56ca762de-2.png?rlkey=i5las32muh4fhrk7x9p56riev&dl=0',
      'https://www.dropbox.com/scl/fi/4qjg8mbzoq97lrqoiidc2/5d0a56cc18081-3.png?rlkey=nooy73cp8ng5ewvfumxfh21dg&dl=0',
      'https://www.dropbox.com/scl/fi/ujjpo3jj0mhqro2w6crf8/5d0a56cc33176-4.png?rlkey=43jcojp1kngyoi28d2qlm99wl&dl=0',
      'https://www.dropbox.com/scl/fi/xk92350rjzy2u45hiatqr/5d0a56d16add0-1.png?rlkey=gmy793i21ln6w30gs90c25p32&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'spa-shenzhen',
    title: 'Spa Shenzhen',
    location: 'China',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/z60wfw27it1ibl8o048w4/5d0a5799cb3b0-1.png?rlkey=fm7zmxxeftnmrxv0s58ghbtgv&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Spa Shenzen witnessed lighting and design coming into play, creating sensible space and ambiance augmentation. The entrance areas saw light projecting a water effect of reflection to the ceiling like part of a design element. This truly illustrated how lighting design is like a software, when added to interface with good architecture as well as the landscape design, is able to delightfully achieve an end result of soft ambiance and highlights within the space.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/u4g61g2t2w09hfakazqyh/5d0a5783c3933-2.png?rlkey=ji1ygwfgygpykhkefp6hvpg9v&dl=0',
      'https://www.dropbox.com/scl/fi/x9fi85zlyk952g95mqq8r/5d0a5784aa669-4.png?rlkey=lxoke84ydi2ndz86gr4m8aplc&dl=0',
      'https://www.dropbox.com/scl/fi/z60wfw27it1ibl8o048w4/5d0a5799cb3b0-1.png?rlkey=fm7zmxxeftnmrxv0s58ghbtgv&dl=0',
      'https://www.dropbox.com/scl/fi/32c5y30g0cs9deuxq0eko/5d0a578412d36-3.png?rlkey=4i0igydaawj5a8cf0kv1zs6o1&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'Sheraton-danang',
    title: 'Sheraton Danang',
    location: 'Danang, Vietnam',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/jo4kmzdbpyl9y45qx3z0p/5d0a594584ec4-4.png?rlkey=qfr200wfgxhh8eitwr28r6c6a&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The play of ' Light & Shadows" and different colored temperature of white light add depth and further accentuate the colonial building envelope. Festivities are elaborated with the use of different use of RGB lighting fixtures that transform the whole facade into a play of colours.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/f0qiblkikxabti1mopqn4/5d0a59567059b-2.png?rlkey=0as80l1pl7gxr6hdpgobyppuk&dl=0',
      'https://www.dropbox.com/scl/fi/qp9624km8mpb50ldfq1o6/5d0a5949215ca-206c92_e6f29613fbdb492592e62516ba0b1ef2-mv2.png?rlkey=6n0lzuf8sbyo6q2460595lqeh&dl=0',
      'https://www.dropbox.com/scl/fi/jo4kmzdbpyl9y45qx3z0p/5d0a594584ec4-4.png?rlkey=qfr200wfgxhh8eitwr28r6c6a&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'taj-yeshwanthpur',
    title: 'Taj Yeshwanthpur',
    location: 'Danang, Vietnam',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/xs1yq797k0rd2gehhr4nd/5d0a5a567dd69-206c92_e464c4c153c840c5ab03878fa81c8bfb-mv2.png?rlkey=g4sz3xr0izxdbhys61vhypu5j&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> As a modern and chic business hotel in India, the architectural and interior space was inspired by the modern contemporary style yet maintaining the cozy feel to its every room. Lightbox had the idea of juxtaposing the center part of the building to both of its other sides to create such a great contrast. This will definitely draw every single eye to the hotel.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/iee8o3ixdxch4ysglgqqa/5d0a5a499de34-4.png?rlkey=4vx2qmaelafnyt95pgkigxfd1&dl=0',
      'https://www.dropbox.com/scl/fi/xb5h92iscdlu38wyca58t/5d0a5a4e0ee91-3.png?rlkey=1eznxw8xc26qtqagrry1yjds2&dl=0',
      'https://www.dropbox.com/scl/fi/xs1yq797k0rd2gehhr4nd/5d0a5a567dd69-206c92_e464c4c153c840c5ab03878fa81c8bfb-mv2.png?rlkey=g4sz3xr0izxdbhys61vhypu5j&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'westin-hotel',
    title: 'Westin Hotel',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/v26botsy1lgmk0y5161ws/6077a8ee9f4a1-WESTIN-HOTEL-PHOTO_3.png?rlkey=99aynx5q1f8z2qmr0pm1yvf4t&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Westin Hotel's renowned identity as Singapore's 5 Star Hotel has been in existence for many decades. Located in a prime location and heart of the Island, Westin adopted a collaborative method to link lighting with the interior design of the architecture. The end result is a lighting effect that catered for functionality and ambience enhancement.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/v26botsy1lgmk0y5161ws/6077a8ee9f4a1-WESTIN-HOTEL-PHOTO_3.png?rlkey=99aynx5q1f8z2qmr0pm1yvf4t&dl=0',
      'https://www.dropbox.com/scl/fi/yrm4cjwmwx6cz91ftyd66/6077a80fd2fdf-WESTIN-HOTEL-PHOTO_1.png?rlkey=2ci92b5x7pnyqqrqhsqlagbeo&dl=0',
      'https://www.dropbox.com/scl/fi/eh4iv6tr87wnpcuq9pb1e/6077a81fb0db3-WESTIN-HOTEL-PHOTO_2.png?rlkey=7oq3mitj7a9s699xqv21cz5uf&dl=0',
      'https://www.dropbox.com/scl/fi/rj7v5qtot7cbsrml2cpin/6077a84af1113-WESTIN-HOTEL-PHOTO_4.png?rlkey=dxs0dd209hyro9kd9iniunijn&dl=0',
      'https://www.dropbox.com/scl/fi/nyuvcx8hy8gonpj1omay0/6077a830808e7-WESTIN-HOTEL-PHOTO_6.png?rlkey=fep658hw1ytd0snl4m9e6xvem&dl=0',
      'https://www.dropbox.com/scl/fi/z4mi0uzstbiavfclsxk5v/6077a83081100-WESTIN-HOTEL-PHOTO_5.png?rlkey=7wtbelyzunsjlrrcxpfa77wod&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'w-retreat-&-spa',
    title: 'W Retreat & Spa',
    location: 'Maldives',
    scope: 'Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/galnt83k21uttvj5ez74n/5d0a5c3a300eb-2.png?rlkey=be079ekmtrf7v68iu3eqydiwe&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description/h2>
        <p> The play of ' Light & Shadows" and different colored temperature of white light add depth and further accentuate the colonial building envelope. Festivities are elaborated with the use of different use of RGB lighting fixtures that transform the whole facade into a play of colours.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/s6f0i701hrhz0m9drk3hj/6077e86dd6a70-MALDIVES.png?rlkey=urm6p73e573et0mjm4pqiyi1g&dl=0',
      'https://www.dropbox.com/scl/fi/galnt83k21uttvj5ez74n/5d0a5c3a300eb-2.png?rlkey=be079ekmtrf7v68iu3eqydiwe&dl=0',
    ],
    featured: false
  },
  {
    code: '28',
    slug: 'ancient-hue-resort',
    title: 'Ancient Hue Resort',
    location: 'Hue, Vietnam',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/msfq4go3kkimydrkhay99/6182451835fd8-Ancient-Hue.png?rlkey=xtgix7ld0fumxmools8v9r9y8&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> An experience to step back in time, reimagine the era of 1800s with Hue traditional architecture and lighting surrounding. The grand villa is a 2-storey wooden house with its woodcraft and layout that resembles the royal court of the Nguyen Dynasty. Lighting has been carefully placed to bring out the beauty of the craftsmanship and subtly highlight the wall artefacts.
Lighting has been added in a form of poetic telling in order to complement the impressive landscape. The villa's exterior is surrounded by a lush garden and lake with an arch bridge whereby the light sources are mostly concealed or blended with architecture elements.
</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/ysqyqcjtdm4n2nqerzwi1/5d2c0bbca3c89-photo_2019-07-14_17-02-32-3.png?rlkey=qaltuvdecj1l526o0zsh2auxo&dl=0',
      'https://www.dropbox.com/scl/fi/3h70ppirt667f09g51p08/5d2c0bbd0cc46-photo_2019-07-14_17-02-32.png?rlkey=07y2pvyy6s7ik2mt1e5yrloe6&dl=0',
      'https://www.dropbox.com/scl/fi/bffjr2n0d1tp55teufqah/5d2c0bbe64c06-photo_2019-07-14_17-02-31-3.png?rlkey=5tcvheh6xpkqigfoxm62w1qj5&dl=0',
      'https://www.dropbox.com/scl/fi/zskwrygnclj364r3v0sdo/5d2c0bbf799ea-photo_2019-07-14_17-02-31-2.png?rlkey=ycykb74ufq5s4b6adta2akbgm&dl=0',
      'https://www.dropbox.com/scl/fi/c542r7899rmc9jgqhuiw8/5d2c0bc0c2ea4-photo_2019-07-14_17-02-31.png?rlkey=0c3xy4kb7a77ejif1yasjk2cr&dl=0',
      'https://www.dropbox.com/scl/fi/g4nx8l4e0pbzt2ykis24u/5d2c0bc13fe57-photo_2019-07-14_17-02-32-2.png?rlkey=ak45tc7eclwvuhgpiqprxqltg&dl=0',
      'https://www.dropbox.com/scl/fi/pi5dvx70505secbsaduow/618245108b429-Ancient-Hue-1.png?rlkey=2e2rbe4e9zpo9mjz2qsdbtxr2&dl=0',
      'https://www.dropbox.com/scl/fi/vtegwnvmzfvxlfqs9vdg1/6182451088b23-Ancient-Hue-2.png?rlkey=gla9qp0xfuj1zkvqnad721eu6&dl=0',
      'https://www.dropbox.com/scl/fi/msfq4go3kkimydrkhay99/6182451835fd8-Ancient-Hue.png?rlkey=xtgix7ld0fumxmools8v9r9y8&dl=0',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'sofitel-damamsara-nizza-bar',
    title: 'Sofitel Damamsara Nizza Bar',
    location: 'Malaysia',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://images.alphacoders.com/133/1339872.png',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> When it comes to lighting landscape, safety, security and circulations comes to mind first as that serves as the main purpose of lighting in the area. The aesthetic of the lighting for landscape comes as secondary. Proportionately lit landscape can help visitors embrace distinctive experience.
For example, the same street you walk pass on a daily basis gives you a different vibe when it is over day time and night time. That is simply because during the day you are able to see the full visual of the street and all the elements around. Where as when night falls, the street is lit at the interval of light poles and visuality is not as clear therefore creating a totally different experience.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/ojsy3r07151v6o2fnj301/5d2c0e9e25ec5-photo_2019-07-14_17-08-15-4.png?rlkey=8kmamoortcth4rnpmhb6g723s&dl=0',
      'https://www.dropbox.com/scl/fi/qmbfehoyk5zyepz7mibhs/5d2c0e95cba67-photo_2019-07-14_17-08-15.png?rlkey=4bv0x4rq02gsc1lmd0isn3vod&dl=0',
      'https://www.dropbox.com/scl/fi/wuvudlufb0cpe88ssethn/5d2c0e95eb9fb-photo_2019-07-14_17-08-16-2.png?rlkey=kbe7xb8fsjxctx4x3pi30a3me&dl=0',
      'https://www.dropbox.com/scl/fi/l5ovky5kugc3gtvb1qood/5d2c0e972edf4-photo_2019-07-14_17-08-15-3.png?rlkey=86x2uk77xhy7augj9mn7tuh4t&dl=0',
      'https://www.dropbox.com/scl/fi/do1pi5slhtyqftdchxgzo/5d2c0e9834a63-photo_2019-07-14_17-08-15-2.png?rlkey=dctd19y8ni9i3jcdhqpnwmgd8&dl=0',
      'https://www.dropbox.com/scl/fi/6mzns463czrxnvi3wg2m0/5d2c0e967373c-photo_2019-07-14_17-08-16.png?rlkey=6efyboxv1g8jk4knwxi08w083&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'banyan-tree-hotel',
    title: 'Banyan Tree Hotel',
    location: 'Malaysia',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/gicf4v8v3652xkzx4l1i4/5d2c9326e348b-Crown.png?rlkey=v31ydubs4zb9wk43z5q3eycjy&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Architecture with a minimal touch of form reflects the elegant branding proposition of Banyan Tree. The sleek line of light connecting the podium design to the top crown accentuates Kuala Lumpur city's skyline.
Contemporary Design interiors complemented by modern lighting concept of accentuated clean light line for general ambience within the space.
</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/dtt5yed6wnn0f19qgpsda/5d2c9318f12db-photo_2019-04-12_15-07-23.png?rlkey=7u4yxoooxzs67gmdmyc8n0ses&dl=0',
      'https://www.dropbox.com/scl/fi/gicf4v8v3652xkzx4l1i4/5d2c9326e348b-Crown.png?rlkey=v31ydubs4zb9wk43z5q3eycjy&dl=0',
      'https://www.dropbox.com/scl/fi/lfrax4f2gxgeb1x4ivcr0/5d2c93184b5fa-photo_2019-04-12_15-07-06.png?rlkey=anb8clu8q59yt63tjnwtrt6x9&dl=0',
      'https://www.dropbox.com/scl/fi/v649j5dm2zinw3dham9at/5d2c93186c21b-photo_2019-04-12_15-07-03.png?rlkey=zelomh3chvdalpaa4vrpttoat&dl=0',
      'https://www.dropbox.com/scl/fi/2jx1yqn9cq5au12061p3d/5d2c931993a88-b3c1_ro_04_p_2048x1536.png?rlkey=1qt568ii33td0mwiuqp0bl75o&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'novotel-pullman-aerocity',
    title: 'Novotel Pullman Aerocity',
    location: 'New Delhi, India',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/0jm1p4bm0t3wwcvttr8bu/5d342ce43db52-Novotel-Aerocity-New-Delhi1.png?rlkey=tthxkqrtptms767mj3ugeb6nb&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The play of ' Light & Shadows" and different colored temperature of white light add depth and further accentuate the colonial building envelope. Festivities are elaborated with the use of different use of RGB lighting fixtures that transform the whole facade into a play of colours.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/ci2lnjtvy699gscis66e1/5d342cd0b772a-guest-room-Pullman.png?rlkey=dz8dir8pxy6hyhwxyvpr9ns3r&dl=0',
      'https://www.dropbox.com/scl/fi/kp05ko52vtqw6blqcbixn/5d342cd88c9a2-Novotel_Lobby.png?rlkey=8oe53emompv7qdz8h85nbyl8q&dl=0',
      'https://www.dropbox.com/scl/fi/4tiahqdn3pplisldji5rb/5d342cdde7cc5-Lobby_Pullman-New-Delhi-Aerocity-India.png?rlkey=p3cllsq0xzg1g8r3yikfcmfzf&dl=0',
      'https://www.dropbox.com/scl/fi/wxb1h17s9jzv9a5n51rmf/5d342ce7ab450-novotel-pullman-exterior.png?rlkey=9mm0h29f449m4lq3z6fse25gp&dl=0',
      'https://www.dropbox.com/scl/fi/0jm1p4bm0t3wwcvttr8bu/5d342ce43db52-Novotel-Aerocity-New-Delhi1.png?rlkey=tthxkqrtptms767mj3ugeb6nb&dl=0',
      'https://www.dropbox.com/scl/fi/i9mbphqm1ntu1hstp3dkb/5d342ceec620b-Pullman-Lobby.png?rlkey=bu6ksq12wvehygc6knyj3fkxb&dl=0',
      'https://www.dropbox.com/scl/fi/frki2nwu6rxq6ks41v7su/5d342cf28fd3a-Pullman-1.png?rlkey=yq9elqkymgbzaejzd3b9qg23x&dl=0',
      'https://www.dropbox.com/scl/fi/zhhxjej7q2p6lklnu0tar/5d342cff7a2a3-Ballroom.png?rlkey=sylb6iiae4jdb0iemglb9l5v8&dl=0',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'ibis-style,-midpoint-place',
    title: 'Ibis Syle, Midpoint Place',
    location: 'Jakarta, Indonesia',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/s4m05mlzj1hdidu7w9y9f/5d342f6f81051-ADD-2.png?rlkey=55g2j1msjmtun9iymnc24z3o4&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The new Ibis style Tanah Abang is just a stone  throw away from the largest textile market of South East Asia. It boasts a funky interior design and unique decorations to attract visitors of the textile market.
The lighting at the lobby has been designed to comfort business travelers where by it provides sufficient illumination all around. Visitors can expect to experience a warm and friendly welcome from the warm lighting of the space and sense of security as the place is brightly lit up.
A harmonious transition the the themed guest rooms. Where it offers a welcoming vibe and offers an element of surprise. Guests will be able to feel comfortable as room lighting was engineered for business and leisure.
Given a harmonious, fancy and unique interior design paired with curated lighting, surely it will pave a way in helping guests create an exceptional experience and memory while staying in.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/8dmzvcip8amcm1gg9ie7z/5d342f5a5e77a-Lift.png?rlkey=ab2nq8l7yainmonb91ftefyzz&dl=0',
      'https://www.dropbox.com/scl/fi/ofdbry2a4uy31uckpjizw/5d342f5a8bb3b-Ballroom-2.png?rlkey=nw3i2ay6mur1gfgd8ujzjf5wq&dl=0',
      'https://www.dropbox.com/scl/fi/gvej07lkhz99q19tcmwwp/5d342f5b2dc94-Lift2.png?rlkey=mpumtpfmi576xdxipkde9wbrx&dl=0',
      'https://www.dropbox.com/scl/fi/4dl9sqk2r58nyc7q82d44/5d342f5bf4101-Pref.png?rlkey=9x5xqe5grb4qr0p1afbs6zaf3&dl=0',
      'https://www.dropbox.com/scl/fi/sn0jcw5gcsdhtdeo396uj/5d342f5d06edc-Toilet.png?rlkey=542lhfiai86fd7bduw2fambys&dl=0',
      'https://www.dropbox.com/scl/fi/rtn4qjpyuguc9vdyyrkip/5d342f5e8dd7b-WC.png?rlkey=3hrdz3541qbuaj838zxriq2t8&dl=0',
      'https://www.dropbox.com/scl/fi/s4m05mlzj1hdidu7w9y9f/5d342f6f81051-ADD-2.png?rlkey=55g2j1msjmtun9iymnc24z3o4&dl=0',
      'https://www.dropbox.com/scl/fi/5eccspox2s6whgwhyrjk3/5d342f5979a45-Add3.png?rlkey=mzjao9cem9sma8eusmx03we5o&dl=0',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'amara-hotel',
    title: 'Amara Hotel',
    location: 'Shanghai, China',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/u2l3nmi0z2pahgftp1jaa/5d342ff58d8e5-amara-signature-shanghai-3_6680.png?rlkey=g1qt5b0rxim4jyy7d5jxwszil&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Amara </p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/u2l3nmi0z2pahgftp1jaa/5d342ff58d8e5-amara-signature-shanghai-3_6680.png?rlkey=g1qt5b0rxim4jyy7d5jxwszil&dl=0',
      'https://www.dropbox.com/scl/fi/t7kw3d0u156uxb8deqgh5/5d3430556c581-HI506169657.png?rlkey=q0ix4kfupstv4isvo0u1a0vdy&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'oupost-and-village-hotel',
    title: 'Outpost and Village Hotel',
    location: 'Sentosa, Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/8pjcpeelf8bw06oc0sx5t/5ddcb5d8d1a8b-photo6260039641830893848.png?rlkey=c1v501gbmfh5fy6zc7kdh87x6&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Outpost and Village hotel simutated in the heart of Sentosa area Resort World Singapore. The unique facade design with play of colored lighting surround the elaborated landscape creates impressive environment when you are at the landscape deck. Integrated design of building, landscape and light are perceived.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/67lsfgvcayrc6ucj8eesq/5ddcb5c5e4c29-photo6262296415216708698.png?rlkey=rdo75d26kvdcsm7c1sgbfuv8j&dl=0',
      'https://www.dropbox.com/scl/fi/qggna507ef0a35eaa21st/5d3ffaa5d3b4d-outport.png?rlkey=f4bdt7f7gn5lzaylfun1tfv6e&dl=0',
      'https://www.dropbox.com/scl/fi/vx6ohey0ga8vt14h3hukx/5ddcb5cd36379-photo6260039641830893850.png?rlkey=93kwisw7e1xb5z4lqlpp4tj2p&dl=0',
      'https://www.dropbox.com/scl/fi/8pjcpeelf8bw06oc0sx5t/5ddcb5d8d1a8b-photo6260039641830893848.png?rlkey=c1v501gbmfh5fy6zc7kdh87x6&dl=0',
      'https://www.dropbox.com/scl/fi/mqihgn6yysn15rw5i7rvp/5ddcb5d8ee049-photo6260039641830893847.png?rlkey=zo95ig911ebeczrjjtl9e72x9&dl=0',

    ],
    featured: false
  },
  {
    code: '18',
    slug: 'TRX-Landscape',
    title: 'TRX Landscape',
    location: 'Petaling, Malaysia',
    scope: 'Facade/Landscape',
    year: '2018',
    image: 'https://www.dropbox.com/scl/fi/h5xb9t90u1vrfukjd126f/5d4002fc2f520-3-min.png?rlkey=rcdheu9oxfunqifuoyn1yjvgq&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> In our opinion, landscape lighting is the most tricky lighting to work with. The perception of dark and bright are critically depending on the users. It is visually not possible to measure the number of illuminance when the reflection of light on surrounding objects influences the matter.
Direct illumination such as pole light and bollard light designs to give safety and circulation aspect. Whilst the less obvious illumination method such as uplight or spotlights are the means for supplement functionality with main purpose to articulate the form of landscape environment.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/ewwoguvdj28jz60th1kdv/5d4003023e95c-1-min.png?rlkey=9oiz27hk32b732qo8dqc0yh5q&dl=0',
      'https://www.dropbox.com/scl/fi/h5xb9t90u1vrfukjd126f/5d4002fc2f520-3-min.png?rlkey=rcdheu9oxfunqifuoyn1yjvgq&dl=0',
    ],
    featured: true
  },
  {
    code: '18',
    slug: 'holiday-inn-express',
    title: 'Holiday Inn Express',
    location: 'Hongdae, Seoul, South Korea',
    scope: 'Interior',
    year: '2018',
    image: 'https://www.dropbox.com/scl/fi/sl64d2dtgmh37shg3pgxg/5eb4f41dbd3ad-photo6168189678851828400.png?rlkey=skwj3h6nv243p1vpoj623bqxl&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Luxury 4 star hotel in Hongdae Seoul.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/sl64d2dtgmh37shg3pgxg/5eb4f41dbd3ad-photo6168189678851828400.png?rlkey=skwj3h6nv243p1vpoj623bqxl&dl=0',
      'https://www.dropbox.com/scl/fi/0woudijbmnceva7l4xv7e/5eb5fc2ab6ecb-photo_2020-05-09_06-06-27.png?rlkey=nim6ndh0eakd4scdfp43553gv&dl=0',
      'https://www.dropbox.com/scl/fi/xoj2cih5yqeqrfcsere93/5eb5fc282af1c-photo_2020-05-09_06-05-50.png?rlkey=11us2om2buw89ffsvsyo8smte&dl=0',
      'https://www.dropbox.com/scl/fi/7rsaz1d4ndcmjag75ptiz/5eb5fc2760e23-photo_2020-05-09_06-06-33.png?rlkey=0zot5xj3garheco93gkfyro7z&dl=0',
      'https://www.dropbox.com/scl/fi/btrcoty6fylfd7y75dlav/5eb5fc297097f-photo_2020-05-09_06-06-19.png?rlkey=9zptqvj7gyn9jd8f8hts5ewv4&dl=0',

    ],
    featured: true
  },
  {
    code: '14',
    slug: 'Curio-hotel',
    title: 'Curio Hotel',
    location: 'Crossroad, Maldives',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/n6pugaiimybsg7ahi0rrb/5f2cbb7f61eb7-5.jpeg?rlkey=zt5svml9hxj84wk97rs20iuaq&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Curio Hotel, Crossroads Project, Maldives</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/lotmqpkw7m9ya2blxtcq1/5d4002fc2f520-3-min.png?rlkey=ps9qm7tkdigzdp7cjpbjuf1kn&dl=0',
      'https://www.dropbox.com/scl/fi/4jebhuyx0gfwmkg3vy9a5/5f2cbb7edd7a9-1.jpeg?rlkey=5iws1lyosd6n3qp3ctidq01xj&dl=0',
      'https://www.dropbox.com/scl/fi/y69by8g2b1gfxtwmfq9pt/5f2cbb7edee05-2.jpeg?rlkey=1xk5dwjlbn9c90bozouru5z69&dl=0',
      'https://www.dropbox.com/scl/fi/4gfzd7zk82kbft6y9qy4z/5f2cbb7f2d4cc-4.jpeg?rlkey=syo6q1yy6i7bhjsfp2li7bhsl&dl=0',
      'https://www.dropbox.com/scl/fi/o891jaal7mygf1s9e1vfn/5f2cbb7f7fbd0-6.jpeg?rlkey=5p0j08tgm4vrlq8tj7fefuvhe&dl=0',
      'https://www.dropbox.com/scl/fi/n6pugaiimybsg7ahi0rrb/5f2cbb7f61eb7-5.jpeg?rlkey=zt5svml9hxj84wk97rs20iuaq&dl=0',
      'https://www.dropbox.com/scl/fi/k9fk0db27n7goac2us144/5f2cbb7f290dc-3.jpeg?rlkey=1my0t9rhesc4ltgwfqigrddi7&dl=0',
      'https://www.dropbox.com/scl/fi/ohy6p64umsggqhaexhjot/5f2cbb7fb5c5b-7.jpeg?rlkey=yzhj9a7s1ipvafblili0dernv&dl=0',
      'https://www.dropbox.com/scl/fi/oqbw1t7mprnknfcx33q4w/5f2cbb99777b3-curio-main.jpeg?rlkey=63rlb81ipxytk4cs73bcbrkb5&dl=0g',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'Saii-lagoon',
    title: 'Saii Lagoon',
    location: 'Malidives',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/fgztrhq0u5h2scucgtuqh/61823b06a08fe-SAII-LAGOON-1.png?rlkey=n51ns7mdyl2jg1c7lcq5rfwuo&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Saii Lagoon</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/5j8xd0ame75eqyghyvgv5/61823af6a5eac-SAII-LAGOON-2.png?rlkey=p3m5ci49dcem4c0y40b5bgy4x&dl=0',
      'https://www.dropbox.com/scl/fi/ckysl8r4jcmy005i1f80e/61823af6bb8c8-SAII-LAGOON-5.png?rlkey=r2z60g32yfi4nlsg0nqtg78gr&dl=0',
      'https://www.dropbox.com/scl/fi/0au21zy8tn39o0k8dwn2j/61823af6bc47f-SAII-LAGOON-3.png?rlkey=iiaki80nuxn6nesaa7vwwgw2w&dl=0',
      'https://www.dropbox.com/scl/fi/j7sqjuw6p2fwbyu8f7b06/61823af6c7d14-SAII-LAGOON-4.png?rlkey=7ddx0u9opqvy6tddbmfopwrq2&dl=0',
      'https://www.dropbox.com/scl/fi/fgztrhq0u5h2scucgtuqh/61823b06a08fe-SAII-LAGOON-1.png?rlkey=n51ns7mdyl2jg1c7lcq5rfwuo&dl=0',
    ],
    featured: false
  },
  {
    code: '20',
    slug: 'farifield-by-marriot',
    title: 'Farifield by Marriot',
    location: 'Binh Duong, Vietnam',
    scope: 'Facade/Interior/Landscape',
    year: '2020',
    image: 'https://www.dropbox.com/scl/fi/gsxzxqfa5aw2n0b7ilu2d/6188de03e48a7-photo_2021-11-08_15-17-01.png?rlkey=ovjt9mcrpmegob2jbx6h4sluj&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Originally, the area is now known as Binh Duong was heavily forested, and was dominated by people of the Xtiêng, Cho Ro, Mnong, and Khmer Krom ethnic groups.
The building was formed the architectural monuments, bases on detail local housing with pagoda, temple and ancient town from 1989 until now. City was developing with traditional villages such as incense making, clay piggy bank crafting, wood sculpture, lacquer bamboo, and rattan weaving craft, pottery.</p>
      </div>

    `,
    image_collections: [
      'hhttps://www.dropbox.com/scl/fi/tb4l4qratjlstwrcrspqo/6188e050c009b-photo_2021-11-08_15-16-59-2.png?rlkey=706t2989g6wfpjzwqbteqojme&dl=0',
      'https://www.dropbox.com/scl/fi/63wm7tipar09pah29hql2/6188ddea27846-photo_2021-11-08_15-17-02.png?rlkey=g2jotwyoawudpr4au4u3psi60&dl=0',
      'https://www.dropbox.com/scl/fi/ysdqea2ohe444n1auwehe/6188ddea29738-photo_2021-11-08_15-17-34.png?rlkey=6mnbc5idpvticscrsbw6xoqvk&dl=0',
      'https://www.dropbox.com/scl/fi/kw07maa3yy9q1wq0jgt7f/6188de0da8ccd-photo_2021-11-08_15-17-00.png?rlkey=evmxnndmdy2yevoi5a129qs36&dl=0',
      'https://www.dropbox.com/scl/fi/gsxzxqfa5aw2n0b7ilu2d/6188de03e48a7-photo_2021-11-08_15-17-01.png?rlkey=ovjt9mcrpmegob2jbx6h4sluj&dl=0',
      'https://www.dropbox.com/scl/fi/7jomgt6irna7pec68mld6/6188de19c67fd-photo_2021-11-08_15-20-39.png?rlkey=5849z78vzxmnv84ijufb3t10a&dl=0',
      'https://www.dropbox.com/scl/fi/a50v1m4ngemhvdyib4ihq/6188de136e484-photo_2021-11-08_15-16-59.png?rlkey=a5bwmul9c18ehovm0cbt1ot7y&dl=0',
      'https://www.dropbox.com/scl/fi/bfsdut7a28q0jkwfo3ued/6188de1726213-photo_2021-11-05_15-19-04.png?rlkey=8do5ih7dbazomgrovosvdlxao&dl=0',

    ],
    featured: true
  },
  {
    code: '14',
    slug: 'kgpa-resort-hotel',
    title: 'KGPA Resort Hotel',
    location: 'Kuala Lumpur, Malaysia',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/n6m5y2gz667u4gmct64wb/6333b5ca8e57a-IMG-20220818-WA0038.png?rlkey=0agjpjlmxwzzibdhnldpota9e&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> KGPA Resort Hotel, Malaysia</p>
      </div>

    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/gw63gxlkc8uzmo7rmzvgp/6333b5ce08f6c-IMG-20220818-WA0039.png?rlkey=z2qvqfs3v221tn14fsmp2xx5s&dl=0',
      'https://www.dropbox.com/scl/fi/wh68qei1jinyj4vp12gcr/6333b5d1e7f8f-IMG-20220818-WA0040.png?rlkey=fb0kiwfetn6oqtpebhgywu0uf&dl=0',
      'https://www.dropbox.com/scl/fi/gz9x37yhb48e28kntslpi/6333b5d5bba60-IMG-20220818-WA0042.png?rlkey=21js1w152jrauom9zscd8hh3v&dl=0',
      'https://www.dropbox.com/scl/fi/brfol8i889o4n0oz18kw2/6333b5d94ccef-IMG-20220818-WA0043.png?rlkey=w06buzht4lczn1z5tl3n230b8&dl=0',
      'https://www.dropbox.com/scl/fi/7iha1jwqbrcuj3tpu2s5o/6333b5e03ad5c-IMG-20220818-WA0045.png?rlkey=s38wsnqj1tt1ra4veukuhxm9q&dl=0',
      'https://www.dropbox.com/scl/fi/h79kaan4aqbuwacxjuxf0/6333b68ce15ad-IMG-20220818-WA0048.png?rlkey=ow4d3p5e55abcvuox9symmdnh&dl=0',
      'https://www.dropbox.com/scl/fi/7d9w8ay5cdzjzlcw7286q/6333b6056480e-IMG-20220818-WA0037.png?rlkey=4v7saj705w009pj2fwi4humvd&dl=0',
      'https://www.dropbox.com/scl/fi/l3gse6gob2bc9ex2jlxxe/6333bb0183fb4-IMG-20220818-WA0047.png?rlkey=lmxu1rno8em0en4y2py4xene3&dl=0',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'avani-hotel',
    title: 'Avani Hotel',
    location: 'Samui, Thailand',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/dhywk39h3etbj98nvhp1k/63c89a6cae582-photo_6199339899828679440_y.png?rlkey=8f4x0306pvc903wmzwvib0jqo&dl=0',
    category: 'HOSPITALITY',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The play of ' Light & Shadows" and different colored temperature of white light add depth and further accentuate the colonial building envelope. Festivities are elaborated with the use of different use of RGB lighting fixtures that transform the whole facade into a play of colours.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/jobntt1xsgpkv3lnq9rns/63c89a6f917e5-photo_6199339899828679447_y.png?rlkey=8of3j93znj36sn1t3tdassb09&dl=0',
      'https://www.dropbox.com/scl/fi/4o0568u65diaoa8n5pns1/63c899cee4b4a-photo_6199339899828679441_y.png?rlkey=ky94d9qgqm61ps2zwadehgagp&dl=0',
      'https://www.dropbox.com/scl/fi/3dthvz20mxlqll4j8mqqd/63c899cee4b4a-photo_6199339899828679446_y.png?rlkey=c4akx5yep795rqdu5jsnplseo&dl=0',
      'https://www.dropbox.com/scl/fi/bjr0s3uccuy41m7lhopk5/63c899cf1ee3b-photo_6199339899828679443_y.png?rlkey=ulmxdsciuqxt2sy3pam5ibno6&dl=0',
      'https://www.dropbox.com/scl/fi/o5b9v0i2xrh7wyzso87cv/63c899cf232ee-photo_6199339899828679444_y.png?rlkey=g41y79j5bv6qjp398wfst51dr&dl=0',
      'https://www.dropbox.com/scl/fi/foc2lk8gyaz8fg7d0ggsf/63c899cf45407-photo_6199339899828679442_y.png?rlkey=ridm75s2dy94uudzw9feazqek&dl=0',
      'https://www.dropbox.com/scl/fi/wvn0f803u1ixjn2w93bzg/63c899dc5d480-photo_6199339899828679439_y.png?rlkey=3d4og37fitlnr8ybu6h0a0udw&dl=0',
      'https://www.dropbox.com/scl/fi/35su097w9ywfisyr0y8ei/63c899dc61098-photo_6199339899828679445_y.png?rlkey=rprcmedsrj4ya6awg111716pj&dl=0',
      'https://www.dropbox.com/scl/fi/cqqm21h48b9o7smlippwq/64816dc154495-avani-1.png?rlkey=d077wy73lkh90zfa9ruo7088n&dl=0',

    ],
    featured: false
  },
  {
    code: '14',
    slug: 'ALTITUDE-BAR-BANYAN-TREE',
    title: 'Altitude Bar, Banyan Tree',
    location: 'Kuala Lumpur, Malaysia',
    scope: 'facede/interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/aa97lbz2xbvcs6518loej/5d3415fdb68ab-Altitude.png?rlkey=2wt2311iup4g6pyte3llnz9mm&dl=0',
    category: 'RETAIL & RESTAURANT',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Luxury dining in the heart of Kuala Lumpur created with the simplicity of warm lighting. The bar lighting is simplified to a minimal design approach on the internal light ambiance hence creating tender environment for people to enjoy the skyline outside at night.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/aa97lbz2xbvcs6518loej/5d3415fdb68ab-Altitude.png?rlkey=2wt2311iup4g6pyte3llnz9mm&dl=0',
      'https://www.dropbox.com/scl/fi/lw3vai1vkd8od5mqmjvbn/5d3415fe938dc-IMG_2074-1.png?rlkey=i4tq7r411wkt3diam2fhgiuif&dl=0',
      'https://www.dropbox.com/scl/fi/0sobhzgvqtax56gjntnj8/5d3415ff93797-43474037072_c0c1151f75_c.png?rlkey=p6ns8th4rllednu3psyjrrypl&dl=0',
      'https://www.dropbox.com/scl/fi/n92ocjuqii9mhdk0xg7ac/5d3415ffc3810-photo_2019-04-12_15-06-57.png?rlkey=atgcistk16imegfnp39m0drs2&dl=0',
      'https://www.dropbox.com/scl/fi/wexjzskirimfxpbdelk1p/5d341605c9acf-41502343440_45bb7c7983_c.png?rlkey=nzwq2kk5abu6gja9h7g44dve5&dl=0',
    ],
    featured: false
  },
  {
    code: '04',
    slug: 'seen-samui-beach-club',
    title: 'Seen Samui Beach Club',
    location: 'Thailand',
    scope: 'facade/interior/Landscape',
    year: '2004',
    image: 'https://www.dropbox.com/scl/fi/z1u4c4s9v5lw1k0vcj59x/63c896b05ee7e-IMG_1168.png?rlkey=4ncihm4m5cisoo9kpgiorjnte&dl=0',
    category: 'RETAIL & RESTAURANT',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> N/A.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/kx990v91ee2mmk187949v/5e06c69a0cf6d-1.png?rlkey=baoxf0rbxggx3e1bn022n749f&dl=0',
      'https://www.dropbox.com/scl/fi/s8ymvbjkgm6ohd5hm6w78/63c896f26fae0-IMG_1169.png?rlkey=5v7pndhaxim01gpc5v8vlbtki&dl=0',
      'https://www.dropbox.com/scl/fi/csxt2q8vmwqp1rcjghjj0/5e06c69aac289-3.png?rlkey=5hovr7h96k56a1ghyypvry43s&dl=0',
      'https://www.dropbox.com/scl/fi/6u73qnya9ii85dlm7wkcb/5e06c69b9d526-4.png?rlkey=l8aa814j4fpolsdkzp2u7xn1z&dl=0',
      'https://www.dropbox.com/scl/fi/9vgxwhdu3ksa2shalh473/63c89af6105d1-photo_6199339899828679437_y.png?rlkey=9udyaaw2m0pizsx3uwrv1ublo&dl=0',
      'https://www.dropbox.com/scl/fi/d1692ybfe1lpqp85v7n0f/5e06c6990b217-6.png?rlkey=nyf4zamb2w2qqj258joe8lh36&dl=0',
      'https://www.dropbox.com/scl/fi/z1u4c4s9v5lw1k0vcj59x/63c896b05ee7e-IMG_1168.png?rlkey=4ncihm4m5cisoo9kpgiorjnte&dl=0',
    ],
    featured: true
  },
  {
    code: '14',
    slug: 'saffron',
    title: 'Saffron',
    location: 'Bangkok, Thailand',
    scope: 'Facade/Intrior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/1tov1wx44rcm8fnyhq06b/5d2c944b4a71f-photo_2019-06-07_09-37-07.png?rlkey=nk9h482q3zvo1f35g0lhh9l2s&dl=0',
    category: 'RETAIL & RESTAURANT',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Saffron, Banyan Tree Hotel.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/nxwfv8s9a2ezp8a7fr05p/5d2c946010cc2-photo_2019-06-07_09-36-01.png?rlkey=6k9yhvpxnqh40qbgrr8au8cl6&dl=0',
      'https://www.dropbox.com/scl/fi/1tov1wx44rcm8fnyhq06b/5d2c944b4a71f-photo_2019-06-07_09-37-07.png?rlkey=nk9h482q3zvo1f35g0lhh9l2s&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'atlantic-dining',
    title: 'Atlantic Dining',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/izfyf6y92iimbgj2zxpyb/1.png?rlkey=4zm4g1cuvsmmxy4hxvdw6vqhn&dl=0',
    category: 'RESTAURANT & RETAIL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The luxury dining cuisine in one of the tallest building in Singapore is designed with a classical contemporary touch. Golden wall and the crystal right lighting elements magnified the special character to the interior space</p>
      </div>
    `,
    image_collections: [
      'hhttps://www.dropbox.com/scl/fi/ojbg0869camwbpqanf6vq/2.png?rlkey=mbm5hwrfo2wfchp0hf74rfjxk&dl=0',
      'https://www.dropbox.com/scl/fi/izfyf6y92iimbgj2zxpyb/1.png?rlkey=4zm4g1cuvsmmxy4hxvdw6vqhn&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'barnone',
    title: 'Barnone',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/f3uy9d82nlvfvmzeepzx2/4.png?rlkey=e1728hlhkmredc3du73nyznrf&dl=0',
    category: 'RESTAURANT & RETAIL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Loft like concept integrated with concealed lighting created a subdue lighting environment. Colour changing iceberg at the bar accentuates this little loft space into the bar mood at different hours.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/mqz9z8d6jde968xnzivr0/1.png?rlkey=medhmewugob5kzyx9ent4sncx&dl=0',
      'https://www.dropbox.com/scl/fi/7zmycke4tk7zf6yy3opmw/2.png?rlkey=vickc7x9q2f8fpnz1p0x73stz&dl=0',
      'https://www.dropbox.com/scl/fi/tlgzworeft4c28ve0p13s/3.png?rlkey=oj11huy3hdd69bxpxksgf56dt&dl=0'
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'kinokuniya,-dubai',
    title: 'Kinokuniya, Dubai',
    location: 'Dubai, Middle East',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/ajjp491ev54v0a22vgzrs/1.png?rlkey=oz507vm244d8a411fw9upguz4&dl=0',
    category: 'RESTAURANT & RETAIL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Kinokuniya is known to be the largest Japanese franchise bookstore around the world. For the outlet in Dubai, Lightbox proposed ceiling panels that are inspired by Islamic architectural patterns featured in the main zones of the store. The patterns are hence simplified into a modern and minimalist shape to suite the concept of the brand. Linear light lines are used to highlight the outer form of the pattern and acts as a circulation guide in the space. While Overall illumination of the interior spaces are created by seamless downlights embedded into the black panels that imitates the look of a starry night sky.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/kcor8u5dh1rppix1x2ru5/2.png?rlkey=jufexzbl9tbe0kfkaw0t2poqq&dl=0',
      'https://www.dropbox.com/scl/fi/ajjp491ev54v0a22vgzrs/1.png?rlkey=oz507vm244d8a411fw9upguz4&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'kinokuniya, singapore',
    title: 'Kinokuniya, Singapore',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/7u75ptwz2dqtxx62naysc/1.png?rlkey=sl3bz1d547gii11j861q4kmy3&dl=0',
    category: 'RESTAURANT & RETAIL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> N/A.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/7u75ptwz2dqtxx62naysc/1.png?rlkey=sl3bz1d547gii11j861q4kmy3&dl=0',
      'https://www.dropbox.com/scl/fi/i6u50qewte8pao6k5n1ta/2.png?rlkey=fzfdbwjqdj2zoepai62k6omwo&dl=0',
      'https://www.dropbox.com/scl/fi/bg5ydf75dwz0lwt97gqgm/3.png?rlkey=p4fx8palj8glq7grlvkqe122b&dl=0',
      'https://www.dropbox.com/scl/fi/kqj31ygitart3s2gi26ry/4.png?rlkey=3nuerxr7nd1lqlf5sert7z7dz&dl=0',
      'https://www.dropbox.com/scl/fi/5x0649usldhy1zqts5deo/5.png?rlkey=5yxmbwgiblm85c9a8akf5x9pc&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'kinokuniya,-thailand',
    title: 'Kinokuniya, Bangkok',
    location: 'Bangkok, Thailand',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/im3ksnsrwmha35kshafm3/2.png?rlkey=y586l56236tfcmycubcw6estd&dl=0',
    category: 'RESTAURANT & RETAIL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> N/A</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/jnbsblp3xxkzox9w42r6f/1.png?rlkey=75p2tel55h53dllwtfquuwm3s&dl=0',
      'https://www.dropbox.com/scl/fi/im3ksnsrwmha35kshafm3/2.png?rlkey=y586l56236tfcmycubcw6estd&dl=0',
      'https://www.dropbox.com/scl/fi/dcaza72diykasg0laf78o/3.png?rlkey=y2bi4kacoya76ei3k5vg9cw9v&dl=0'
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'the-jewel-box',
    title: 'The Jewel Box',
    location: 'Singapore',
    scope: 'Facade',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/77bqe9qfawua9mlwz4pvo/1.png?rlkey=lm8auo06jfvestmcl7qf93cr4&dl=0',
    category: 'RESTAURANT & RETAIL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Designed with light, this building resembles a giant trinket box at night. The building is lit with different colour combinations representing spring, summer, autumn and winter. Embedded in the facade are numerous twinkling LEDs to create attraction, "The Jewel Box".</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/77bqe9qfawua9mlwz4pvo/1.png?rlkey=lm8auo06jfvestmcl7qf93cr4&dl=0',
      'https://www.dropbox.com/scl/fi/umsik7z5igzesjmvd7vvw/2.png?rlkey=rdy6h8d4i9l70nr9jjnz1k3mx&dl=0',
      'https://images.alphacoders.com/133/1339872.png'
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'banyan-tree-signatures',
    title: 'Banyan Tree Signatures',
    location: 'Malaysia',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/ovwq706eijrm0r2biz8u5/1.png?rlkey=0t5lm6r8ly9auiso8mac8vlvh&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> A Signature project by Bayan Tree brand is the luxury high-end residential development in downtown KL. The sleek design of architecture and lighting play an important role to give the outstanding glow at the top of the building, further highlighting the building's unique shape as a sky scraper. The composition of the top crown and the podium are connected by the simple gesture of the building shafts. This alone resulted in a very well integrated element for the KL skyline.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/ovwq706eijrm0r2biz8u5/1.png?rlkey=0t5lm6r8ly9auiso8mac8vlvh&dl=0',
      'https://www.dropbox.com/scl/fi/e3l9l4y9dfkll2ien1nfu/2.png?rlkey=iy4nu2cu3bohqge4t54bm03w7&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'boutiq-condo',
    title: 'Boutiq Condo',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/bfpprhp6zo4iop2o29ujd/1.png?rlkey=1ic1scl1xwbppf0bmddt1s1bb&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> For the Boutiq Condominium, light was expressed through the medium of acrylic. The in facing of fiber optics light shine through the crafted piece one by one to create the perfect mood and effect symbolizing abundance of youth and vibrancy for young executive urban living.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/bfpprhp6zo4iop2o29ujd/1.png?rlkey=1ic1scl1xwbppf0bmddt1s1bb&dl=0',
      'https://www.dropbox.com/scl/fi/xup5zvg2jfqfd6vmdadmg/2.png?rlkey=6qt68a8lhmsoiwxxwonc1e7qj&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'cliveden-at-grange-residence',
    title: 'Cliveden at Grange Residence',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/mgc659zy3fos9d4tyi48c/3.png?rlkey=8jhef7y31rzt4voztefn0jt4d&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> This prominent landmark apartment features clean linear architecture. The lighting language translates the feelings of wellness and an atmosphere of balance and welcoming.The facades were enhanced by dot-looking lighting for every level to draw the eye all the way to the luxurious penthouse located at the top of each building. The main entrance vertical elements are expressed with light to enhance the language. Lighting is used to create interaction between rock, water, and the plants elements, reflecting a harmony in landscape design.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/5pf3xy8av4tuocu059sxr/1.png?rlkey=7t1pwfy40vsrdcw580myouufk&dl=0',
      'https://www.dropbox.com/scl/fi/zsvxznpf5abmo4egh1y2l/2.png?rlkey=ghab1ujk838autm3xqiusyeud&dl=0',
      'https://www.dropbox.com/scl/fi/mgc659zy3fos9d4tyi48c/3.png?rlkey=8jhef7y31rzt4voztefn0jt4d&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'corals-condominium',
    title: 'Corals Condominium',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/cdcltzm0300qrqozoatdo/6.png?rlkey=7h14fhwroafz458wmzvxeqnlp&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Stimulated by the project's stunning architecture and the site, the facade lighting scheme that Lightbox proposed had conceptual origins in the swaying movement of dandelions. The LED strips lights located at balcony edges are programmed to mirror the motion of the wind on any given night. Starring from the bottom, the LED lights are switched on all the way to the crown. The rhythm can be gradual or immediate, taking cues from the pattern of the wind at any given time. Fiber optics were installed at the crown to create a visual scattering of precise light points that recall the dispersion of dandelion seeds. This visual spectacle creates a pleasing and relaxing facade for both building users and strollers along the bay.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/zwzv86o5eu147hql8363q/1.png?rlkey=0l56li0eqstlfh54smr81iryn&dl=0',
      'https://www.dropbox.com/scl/fi/ulq3n0tyudbgxdu8nus1q/2.png?rlkey=umt6raknggl6ubz50blq89j6d&dl=0',
      'https://www.dropbox.com/scl/fi/6gl0lex7hro4ws7gl7b2l/3.png?rlkey=cxckbjtg69hdzxqpg9ayj267s&dl=0',
      'https://www.dropbox.com/scl/fi/zf6l5feoyh5ywfqo0rtla/5.png?rlkey=pycviccaua4emskj0nd3w2rsk&dl=0',
      'https://www.dropbox.com/scl/fi/mbobo0xuxdixpk5yxrwid/4.png?rlkey=6ny0j2m205fj5yo9p26pk1p0x&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'holland-collection',
    title: 'Holland Collection',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/8lpu0kqfsvgqm5wv1iu4m/3.png?rlkey=4y7cacgvgedc5mxxotm9uy97r&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Located along the Holland Road, The Holland Collection is a modern low-rise condominium. The lighting design is aimed towards the vibrant and dynamic ambience. The swimming pool which is located at the center of the condominium is considered as the main feature of the building. During daytime, the round-shaped frosted glass at the base of the swimming pool will allow the natural light to penetrate to the basement carpark. At night, the programable LED strip at the basement carpark pendant lights evoke vigorous ambience on both basement car park and ground level area.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/046twhj7vptgzl7l11s2l/1.png?rlkey=7chzj25m11udfxy63wy9khviz&dl=0',
      'https://www.dropbox.com/scl/fi/yqc220xz0uc0483c0r9ku/2.png?rlkey=fx6vsammg6v82tikpfzzxx8iv&dl=0',
      'https://www.dropbox.com/scl/fi/8lpu0kqfsvgqm5wv1iu4m/3.png?rlkey=4y7cacgvgedc5mxxotm9uy97r&dl=0',
      'https://www.dropbox.com/scl/fi/po60ef72kohncecqr6k5h/4.png?rlkey=cxbsd538xwtt28co7k820fokx&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'rodyk-residence',
    title: 'Rodyk Residence',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/zpzgx2g4mf6cnimb9jpyj/1.png?rlkey=f4lpjrc0p1ysktleuhrfsmgho&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Rodyk is the new apartment situated on the side of the well-known beautiful Singapore River. The facade lighting suggests the visual of the water droplet to further integrate the apartment seamlessly to its surrounding. Modern contemporary language has been blended so well with its tiny "old" architecture in front of it and create such an innovative dwelling space for the residents.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/h82fvjukdyod0uz93a5bd/2.png?rlkey=zih3lsga2a03cj9rr8j1lae0h&dl=0',
      'https://www.dropbox.com/scl/fi/25w51l24tukun4h47uu35/3.png?rlkey=r0ufbwc9i56n30ekxge5x4yr4&dl=0',
      'https://www.dropbox.com/scl/fi/iu58pl1fvizmu3ezz05xu/4.png?rlkey=42azzl56irpfsnihjxncda0d3&dl=0',
      'https://www.dropbox.com/scl/fi/zpzgx2g4mf6cnimb9jpyj/1.png?rlkey=f4lpjrc0p1ysktleuhrfsmgho&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'saumata',
    title: 'Saumata',
    location: 'Indonesia',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/0vprwkwihmprhzsu7jm36/1.jpeg?rlkey=d20nu2fhwntu0cavrynsdjuo5&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Saumata means '' The One and Only '' in traditional Javanese language. Lighting Design enhances the identity of Saumata as the one and only luxury condominium development in its area Alam Sutera, Indonesia. Bringing out the essence of earthy tone and golden touches of every detail in the Saumata development; to further enhance the user experience, creating the essence of home and where gold glitters even brighter.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/3rzm7u5jmr8834ovt6y7t/3.png?rlkey=s6fl9btsdj042660r843nrqds&dl=0',
      'https://www.dropbox.com/scl/fi/vk1vqdtq3uoxu3l677f4z/2.png?rlkey=hy9r5j7n2d89z4nz90i4vkt42&dl=0',
      'https://www.dropbox.com/scl/fi/0vprwkwihmprhzsu7jm36/1.jpeg?rlkey=d20nu2fhwntu0cavrynsdjuo5&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'seascape-residence',
    title: 'Seascape Residence',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/0cjww4n9b4kbxz6grc8iy/3.png?rlkey=xqwtxkfowj7b7xgwim9sqbmw4&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Seascape is a high end residential condominium developed in Sentosa Cove, Singapore. The landscape and architectural has directed artistic sea-life sculptures within their design language which has created the strong landscape theme.The lighting design has created an enhancement to its many sea life sculptures as night approaches, and keeping the other landscape subtle and functional. At night, the development is in harmony with the surrounding with artistic atmosphere.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/re4d0y7ehamxqzbwcl5ep/2.png?rlkey=8wgzf90795p1xgj9vki04ooeu&dl=0',
      'https://www.dropbox.com/scl/fi/2hmszxyk4a6a0ymgtqn7z/1.png?rlkey=lqvwqv5cbdhvz1ys9dsq6tv46&dl=0',
      'https://www.dropbox.com/scl/fi/0cjww4n9b4kbxz6grc8iy/3.png?rlkey=xqwtxkfowj7b7xgwim9sqbmw4&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'the-empire-place',
    title: 'The Empire Place',
    location: 'Bangkok, Thailand',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/1mmy8e3lt4a4n5spb0gk3/2.png?rlkey=5tu7xbbqpe7u34hdlxnpcy282&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Luxurious condominium at the heart of Bangkok, the architectural and interior character is inspired by Art-Deco Chicago style. Lighting is designed to bring out this character with elaborate details. The lighting design in warm colour tone enhances the overall interior ambience.The luxurious atmosphere is achieved by harmonizing the lighting elements with interior materials.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/us4i1nr76m6rwvtaqszyg/1.png?rlkey=vztr00hhrqh0age87gv2cu76z&dl=0',
      'https://www.dropbox.com/scl/fi/1mmy8e3lt4a4n5spb0gk3/2.png?rlkey=5tu7xbbqpe7u34hdlxnpcy282&dl=0',
      'https://www.dropbox.com/scl/fi/eacqplt091hucgnpro1pk/3.png?rlkey=8fv692m0vfo2q76yy2rpkzpkn&dl=0',
      'https://www.dropbox.com/scl/fi/tj4eprq497bxuk1vmlg9g/4.png?rlkey=3f6blnryurrmnuu24u4wb99n9&dl=0',
      'https://www.dropbox.com/scl/fi/obp1uldxk42bmce9uieko/5.png?rlkey=lqrxsv5u14zig4ipsecpbncs9&dl=0',
      'https://www.dropbox.com/scl/fi/e446o5jqdnl5pm35lw7x8/6.png?rlkey=w310ag7g64nbrqwxzgd4yao3c&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'tong-fu-xi-residence',
    title: 'Tong Fu Xi Residence',
    location: 'Guangzhou, China',
    scope: 'Facade/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/7srxcdvct3clb2lhfm7p0/3.png?rlkey=2rnnf63uf9makwat55jxa84ez&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The Ultra modern architecture of residential development is located along the River Delta in Guangzhou. The context of architecture is the true interpretation of the future for the fast urban developing city like Guangzhou. The river that served as the main means of transportation in the past has been transformed into a larger than life feature backdrop of the entire development. This is due to the evolution of today's world whereby the transportation of the urban city is now flooded with vehicles human traffic. Lighting for the facade hence utilizes such backdrop, and further emphasizes the concept of transformation by the use of light flow. The light flows represents the past and present river current and human traffic. The lighting design is controlled by human traffic and by programming, presented the traffic movement. The interior lighting on the other hand is alike to luxury for China.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/r9a4r4qv6w0c983sgcpbw/1.png?rlkey=z7fjtlsqa5d4xn95dmyd0ocpb&dl=0',
      'https://www.dropbox.com/scl/fi/9tcqtibff6w10hb8rbq5y/2.png?rlkey=ly51m6ju9crc1x38u0qyvp343&dl=0',
      'https://images.alphacoders.com/133/1339872.png',
      'https://www.dropbox.com/scl/fi/53jqzeyu9t7pxqqjpg3gx/4.png?rlkey=0akc5tqty1fuw0nk5jzbltoaf&dl=0',
      'https://www.dropbox.com/scl/fi/7srxcdvct3clb2lhfm7p0/3.png?rlkey=2rnnf63uf9makwat55jxa84ez&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'turquoise-condominium',
    title: 'Turquoise Condominium',
    location: 'Singapore',
    scope: 'Facade/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/xk906fp641ka2ju9dc63g/2.png?rlkey=7hjxyybs7ztpi1igkhdp3uzbg&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Turquoise is a prestigious property located in Sentosa cove, Singapore. The development aims to create a life-style of being an 'islander'. The lighting concept creates a cozy ambience with basic functional properties. Interaction areas are carefully designed to bring out the outdoor living room space and feel. Lighting elements are being carefully designed, to enhance its quiet existence.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/3bm2qcsucluleqywsxh4k/1.png?rlkey=8a10cca76u9z0zal7fnytqeqc&dl=0',
      'https://www.dropbox.com/scl/fi/w0luqiyr0eg5j5txhp5ds/3.png?rlkey=0220ktrkb3y7u4aymrhlyx1y3&dl=0',
      'https://www.dropbox.com/scl/fi/xk906fp641ka2ju9dc63g/2.png?rlkey=7hjxyybs7ztpi1igkhdp3uzbg&dl=0',
      'https://www.dropbox.com/scl/fi/g0mreiepr1p9hsqyiyojy/4.png?rlkey=l95crvnk7lit1rudj36nvoyzm&dl=0',
    ],
    featured: false
  },
  {
    code: '13',
    slug: 'park-avenue',
    title: 'Park Avenue',
    location: 'Kota Kinabalu, Malaysia',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/mqb8pr8xb3e3wafd4eeco/1.png?rlkey=pxgkjot1765mqgzkj5vwqqlyj&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Park Avenue a residential development which saw a brilliant blending of the architecture with the well known and beautiful nature of Kota Kinabalu. Lighting's further enhanced the aesthetic feel with the facade and landscape lighting inspired by a careful mix of nature and urban living lifestyle. The subtle & indirect lighting represents the futuristic outlook for the harmonious co-existence of urban living with our precious environment.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/etl0kgiot8rr6e1ov0fc7/2.png?rlkey=74ue66s5tt3bzyzxyf8dym2ng&dl=0',
      'https://www.dropbox.com/scl/fi/mqb8pr8xb3e3wafd4eeco/1.png?rlkey=pxgkjot1765mqgzkj5vwqqlyj&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'w-residences',
    title: 'W Residences',
    location: 'Bangkok, Thailand',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/dt80101eiie94i8s91e8l/2.png?rlkey=0jocdfnahgpgwvzktcbj5ms5x&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Luxurious condominium at the heart of Bangkok, the architectural and interior character is inspired by Art-Deco Chicago style. Lighting is designed to bring out this character with elaborate details. The lighting design in warm colour tone enhances the overall interior ambience.The luxurious atmosphere is achieved by harmonizing the lighting elements with interior materials.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/gzrm7506nvdurzpp9p7do/3.png?rlkey=ep8rr7j6ta8g2fmby616rs39f&dl=0',
      'https://www.dropbox.com/scl/fi/8d3otxufjsbpvbb2lat0b/4.png?rlkey=2b2edayxvrfic0g6bppvkar5q&dl=0',
      'https://www.dropbox.com/scl/fi/tvkxo9l2542mempkt5eug/1.png?rlkey=mrpt89ska6ejy3xq5duiv8n44&dl=0',
      'https://www.dropbox.com/scl/fi/dt80101eiie94i8s91e8l/2.png?rlkey=0jocdfnahgpgwvzktcbj5ms5x&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: '70-shenton-way',
    title: '70 Shenton Way',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/wekhm934gfqst3tjbcs1i/1.png?rlkey=gj2qy32xrvec4mlc4ndfkqnhs&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Located along Singapore's central business district area, Eon Shenton adopts a strong lighting approach to standout and highlight it's statement features among the many towering buildings. The lighting is carefully specified to be stronger on the exteriors and softer in the interiors to bring about a sense of balance in spatial functionally and awareness. The facade is a statement by its own rights to attract one's attention, while the interior plays with the balance of light and shadow, providing a tranquil ambiance for one to feel belonged.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/wekhm934gfqst3tjbcs1i/1.png?rlkey=gj2qy32xrvec4mlc4ndfkqnhs&dl=0',
      'https://www.dropbox.com/scl/fi/4mf93lxjweymi9pnfzd1v/2.png?rlkey=scxim0kpf8t41tstxbpk18rlz&dl=0',
      'https://www.dropbox.com/scl/fi/ivef6s99x62pas323bbd3/3.png?rlkey=b0ven37st8rm322ce90wonygv&dl=0',
      'https://www.dropbox.com/scl/fi/25rt3d7eg7j4xxhm5k64w/4.png?rlkey=nevk7nndy4a3bujopzkg4k3kh&dl=0',
      'https://www.dropbox.com/scl/fi/k6nf20onm167dbctzilnb/5.png?rlkey=pu4gpt1lf285ye0azih0u541k&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'fortune-way-residence',
    title: 'Fortune Way Residence',
    location: 'Guangzhou, China',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/64gkwxwu6gcnirmgek73s/1.png?rlkey=4dh3qk18w5y1soaakof3xs2mo&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Fortune Way Residence, Guangzhou, China.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/64gkwxwu6gcnirmgek73s/1.png?rlkey=4dh3qk18w5y1soaakof3xs2mo&dl=0',
      'https://www.dropbox.com/scl/fi/rv0br50maujf2vscub4tf/2.png?rlkey=8ggwt7ipnwwp2p2o0xwgascpk&dl=0',
      'https://www.dropbox.com/scl/fi/1qlv4n2cpe2y0m06t162u/3.png?rlkey=wuofumrzxvsh6r6570nzbi52l&dl=0',
      'https://www.dropbox.com/scl/fi/xygdhp4z87xwu5x5tdv3o/4.png?rlkey=c2jb0t2twydpgbvh80p7g6dxj&dl=0',
      'https://www.dropbox.com/scl/fi/hp0e3p3tcqdcjgq70ywqr/5.png?rlkey=j0tm9iq0fjaa5eg35fp2t046c&dl=0',
    ],
    featured: false
  },
  {
    code: '17',
    slug: 'Coral Residence',
    title: 'Coral Residence',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/sy32hqe0obukpicrihkmm/1.png?rlkey=0m4gwf9ohx83unhxc3zd3m2l3&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Stimulated by the project's stunning architecture and the site, the facade lighting scheme that Lightbox proposed had conceptual origins in the swaying movement of dandelions. The LED strips lights located at balcony edges are programmed to mirror the motion of the wind on any given night. Starring from the bottom, the LED lights are switched on all the way to the crown. The rhythm can be gradual or immediate, taking cues from the pattern of the wind at any given time. Fiber optics were installed at the crown to create a visual scattering of precise light points that recall the dispersion of dandelion seeds. This visual spectacle creates a pleasing and relaxing facade for both building users and strollers along the bay.</p>
      </div>

    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/sfnqbhsnjv3xyux4iozav/2.png?rlkey=ei64pmsmjmf1wixzwt0rebubw&dl=0',
      'https://www.dropbox.com/scl/fi/z3sfbixc5lprca954syio/3.png?rlkey=4xx9e2nhb4wn0q6gfjzwhi1cx&dl=0',
      'https://www.dropbox.com/scl/fi/zxlb45ghx5hylcxgkmidi/4.png?rlkey=jzdev8ve0q6a4x7xyy8rb00cx&dl=0',
      'https://www.dropbox.com/scl/fi/7wj4lmemy3vkwdmrmhb5j/5.png?rlkey=w2et6tvxkkvhw38wt1vevlya0&dl=0',
      'https://www.dropbox.com/scl/fi/9mp8znyrmiobvrvgyoxti/6.png?rlkey=nevi43jlw8imykkeo4knjw0f6&dl=0',
      'https://www.dropbox.com/scl/fi/iv6syxwu69v2a7r5sp3yg/7.png?rlkey=t8mt9wjuugnf05vxywblepknl&dl=0',
      'https://www.dropbox.com/scl/fi/76gfc5pu991gmxw3xolu4/8.png?rlkey=pdwnlbo6aag1qvvmo1bn0aejj&dl=0',
      'https://www.dropbox.com/scl/fi/kxepl3yywrotuk7bw1t5a/9.png?rlkey=2qgbv2kt36j0q8we80l4e842v&dl=0',
      'https://www.dropbox.com/scl/fi/6m0kr15hisx70j8wharxp/10.png?rlkey=8nrsa4m29l1vr1cy4kpcreahf&dl=0',
      'https://www.dropbox.com/scl/fi/8sxziv236bj08ggcftiv4/11.png?rlkey=38cwbnrk7fa3njrqb5zsv51n2&dl=0',
      'https://www.dropbox.com/scl/fi/mesooxy3t3tw4bjkq6d0j/12.png?rlkey=th0zyw4vin33ud8kzwvwi5m6q&dl=0',
      'https://www.dropbox.com/scl/fi/p2un6a7v9wys5in6p7rl1/13.png?rlkey=t15ssay8s244stqfl4p9tmpaj&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'q2-thao-dien',
    title: 'Q2 Thao Dien',
    location: 'Ho Chi Minh, Vietnam',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/hw2kzkkodeie0xl2eiccp/4.png?rlkey=76efgwytmea73cjuf0af4w7x3&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Q2 Thao Dien, Ho Chi Minh City.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/igz9dye1kcbgd05ih037u/1.png?rlkey=gfgsj5rlxn3f30bjpbctwsap9&dl=0',
      'https://www.dropbox.com/scl/fi/mb58yxmd57eov5bfd3frs/2.png?rlkey=3fiqdhwcj6wpu9b2j9upom2yl&dl=0',
      'https://www.dropbox.com/scl/fi/bnjuzm7i6koynov1wc9zb/3.png?rlkey=sygb1ocypgaq4tkhs8x5oj4ss&dl=0',
      'https://www.dropbox.com/scl/fi/hw2kzkkodeie0xl2eiccp/4.png?rlkey=76efgwytmea73cjuf0af4w7x3&dl=0',
      'https://www.dropbox.com/scl/fi/b9l8ci4ff8dzn3tnrxayl/5.png?rlkey=rospdoqkbb4504c27soy18ep0&dl=0',
    ],
    featured: false
  },
  {
    code: '00',
    slug: 'Trivanada',
    title: 'Trivanada',
    location: 'Phuket, Thailand',
    scope: 'Facade/Interior/Landscape',
    year: 'none',
    image: 'https://www.dropbox.com/scl/fi/gn02cj3yt8lg9b21tuuer/3.png?rlkey=wossucs9parlczzu8vo5dcawz&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Trivanda Phuket, Thailand.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/p7ktsghumfiqgmjjqfenv/1.png?rlkey=dryisto4i418a8vngdd1v8itm&dl=0',
      'https://www.dropbox.com/scl/fi/5dtup1oxfeh5gaieejfwp/2.png?rlkey=3btjqss18d48hh060o3d2ogt0&dl=0',
      'https://www.dropbox.com/scl/fi/gn02cj3yt8lg9b21tuuer/3.png?rlkey=wossucs9parlczzu8vo5dcawz&dl=0',
      'https://www.dropbox.com/scl/fi/yeoubvgua6sbfmq2n2hbg/4.png?rlkey=dbh80zuvm2aistwvecm1e08yf&dl=0',
      'https://www.dropbox.com/scl/fi/a2j13dvjtdh29bhaxbsqz/5.png?rlkey=vjfhz0vdf4vxll8c31b9xztd7&dl=0',
      'https://www.dropbox.com/scl/fi/pl3yc7nvxkk1aa65wp94f/6.png?rlkey=ek9w6cnbuouraadu2r7xgq1fc&dl=0',
      'https://www.dropbox.com/scl/fi/ac536ahrdejrbkp42n28i/7.png?rlkey=zbkqtko26w5jiisso7ocmas0o&dl=0',
      'https://www.dropbox.com/scl/fi/pakfpxi14b4kahfu5ne6v/8.png?rlkey=qi4ooegleqnl4gzi3d0u8giab&dl=0',
      'https://www.dropbox.com/scl/fi/fkdny6uhl5rirqniplr52/9.png?rlkey=3wj8n24d3bkl5lvmdavc6pqk5&dl=0',
      'https://www.dropbox.com/scl/fi/3xblatlhvg4vfbncjyiwm/10.png?rlkey=iz21hwl2c8cq2l1wiw0g09jgn&dl=0',
      'https://www.dropbox.com/scl/fi/1jyjoq1mauozj1oucbpe4/11.png?rlkey=wb54sh6xbrh11answmi9iz6vi&dl=0',
      'https://www.dropbox.com/scl/fi/n0hxq71sdzqc5f3tcudlu/12.png?rlkey=zfa3cze4vhysvhyiwdr8dpu53&dl=0',
      'https://www.dropbox.com/scl/fi/436h5d2qp9je8494e7vg4/13.png?rlkey=b1iv6a75cvl0mcgwxqy7mls99&dl=0',
      'https://www.dropbox.com/scl/fi/hvsnwr21vqrkidzsn3sii/14.png?rlkey=asnp4vxeo47a61pojvfom2xik&dl=0',

    ],
    featured: false
  },
  {
    code: '00',
    slug: 'Kalm-penthouse-ari',
    title: 'Kalm Penthouse Ari',
    location: 'Bangkok Thailand',
    scope: 'Facade/Interior',
    year: 'N/A',
    image: 'https://www.dropbox.com/scl/fi/o4phn53xd8cx5jicj7uwk/11.png?rlkey=lx1an3yrgxvaoi84nfg92cic7&dl=0',
    category: 'RESIDENTIAL',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> KALM PROJECT LOCATED IN BANGKOK, THAILAND.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/hkxzd4wu7rytuzr93udqu/1.png?rlkey=y5q4e7q1mp8expmr0z4mu27nx&dl=0',
      'https://www.dropbox.com/scl/fi/7h6pun9ptko6qjjry5g3i/2.png?rlkey=rq1yrmtmwyk2p8nrsb8l0jqlk&dl=0',
      'https://www.dropbox.com/scl/fi/m9qa1ykh5rd6rfhp9lsid/3.png?rlkey=7ozxlduc1vobxpvibl2lvjtue&dl=0',
      'https://www.dropbox.com/scl/fi/6khv7ssa8elyrkaoyqodx/4.png?rlkey=qnn2myilsjmyu2lue0jdnganq&dl=0',
      'https://www.dropbox.com/scl/fi/pyjbhy87l2mu13owvs244/5.png?rlkey=ncv0t8qa4v5jbl5ohmm8c2u0h&dl=0',
      'https://www.dropbox.com/scl/fi/squybocd8td9w9y6iugsq/6.png?rlkey=j3m8r4d7juvu5olqzjvv19ajr&dl=0',
      'https://www.dropbox.com/scl/fi/eqqcixic66yi63apgjn7m/7.png?rlkey=9flu1el3lg0p3q19o1arosn9b&dl=0',
      'https://www.dropbox.com/scl/fi/jdr4qe0e5t3et8et4dl3r/8.png?rlkey=mjr5ta680wau3a8jt4umj4rv9&dl=0',
      'https://www.dropbox.com/scl/fi/l61k1k0ma6xyftimo48a0/9.png?rlkey=3gc17pcgbdkzmex79s6r3w1wd&dl=0',
      'https://www.dropbox.com/scl/fi/u6scv88nyiytu4rgp2rr3/10.png?rlkey=e9rgw9szf5pob7p3cwdvc4vq6&dl=0',
      'https://www.dropbox.com/scl/fi/o4phn53xd8cx5jicj7uwk/11.png?rlkey=lx1an3yrgxvaoi84nfg92cic7&dl=0',
    ],
    featured: false
  },
  {
    code: '19',
    slug: 'museum-of-india-paper-money',
    title: 'Museum of India Paper Money ',
    location: 'Bangalore, India',
    scope: 'interior',
    year: '2019',
    image: 'https://www.dropbox.com/scl/fi/884ylqdt4ap2lf5s35ff7/1.png?rlkey=7ywp8jqse2jr2nlfgg3ncnqgn&dl=0',
    category: 'OTHERS',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> museum in bangalore, india</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/884ylqdt4ap2lf5s35ff7/1.png?rlkey=7ywp8jqse2jr2nlfgg3ncnqgn&dl=0',
      'https://www.dropbox.com/scl/fi/zpvtce36cil81029emep6/2.png?rlkey=cvz04k7b721uthyh7ln562zbt&dl=0',
      'https://www.dropbox.com/scl/fi/8jktoat2e55rxyzm63tuc/3.png?rlkey=3fmuizqpyaixft7nlkatjrjne&dl=0',
      'https://www.dropbox.com/scl/fi/i7pb1vsdureaf4zdfrof7/4.png?rlkey=fykyr017gnwy0qxuga7h6nym9&dl=0',
      'https://www.dropbox.com/scl/fi/1bbq4ora02qk8k89g9ljh/5.png?rlkey=h1riss2v5aavi4mh3fgw6fpn2&dl=0',
    ],
    featured: true
  },
  {
    code: '14',
    slug: '50-scotts-office',
    title: '50 Scotts Office',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/yji3g0kblns5q83rwl7fr/1.png?rlkey=qq5m5tet7omxqf81rgsy2rk4z&dl=0',
    category: 'OFFICE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> The concept of light sculpture is applied to this office building at the heart of Scotts Road. Lighting is designed to illuminate the glass at the facade like the movement of light wrapped around the building itself. The essential elements are the atrium hall, glass bridge, and staircase being sculpted by the colour of LED running, in contrast to the entire building. Lighting system of illumination has been designed to highlight the form of architecture for urbanites to enjoy.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/n6gj33uj9uxhr1uv70h3a/2.png?rlkey=kk630e47cheik9jwoxlxhac54&dl=0',
      'https://www.dropbox.com/scl/fi/6yztp124l8h3fta7e2hbs/3.png?rlkey=ujipbrkrw61cqfo9sa0f85yrv&dl=0',
      'https://www.dropbox.com/scl/fi/n1iv9kf59sgp8sutg50wt/4.png?rlkey=mgy655ar0zv8hjz2by5bc3fbt&dl=0',
      'https://images.alphacoders.com/133/1339872.png',
      'https://images.alphacoders.com/133/1339872.png',
      'https://www.dropbox.com/scl/fi/yji3g0kblns5q83rwl7fr/1.png?rlkey=qq5m5tet7omxqf81rgsy2rk4z&dl=0',
      'https://www.dropbox.com/scl/fi/lv9pdhpqzrih77cl3u3gf/5.png?rlkey=vgfn0dv02oq23jowtxzosmquc&dl=0',
      'https://www.dropbox.com/scl/fi/clxocxjutwyd75ya3f9c1/6.png?rlkey=lhfqki52sexw567wmmf8n5fyu&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'google',
    title: 'Google',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/xpooov7mqwgaqimx2ct8u/5.png?rlkey=3xv6tx77ibu68ya6fgs2ko2o3&dl=0',
    category: 'OFFICE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> Office of the world most innovative corporation is located in the heart of Singapore new CBD Area. The concept of interior designs the place to cultivating talents around the region and the world. The multicultural atmosphere is one office emerged. Lighting then is designed to enhanced this idea. The importance of lighting to meet certain spaces in the office is the challenge. The video conference room and facility are always the key point for such a global company. Lighting to suit this facility is essential to the lighting design in each of the meeting rooms. The relaxing areas are as well need to be well design to support and encourage creativity. Colorful and lively lighting helps to generate the sense of vibrancy with never ending creativity.</p>
      </div>
    `,
    image_collections: [
      'hhttps://www.dropbox.com/scl/fi/3lf11sau3vnbte25wfgha/1.png?rlkey=u0qamehm1l0i6q1ymzl9i0wmu&dl=0',
      'https://www.dropbox.com/scl/fi/lkvine2g3uu2errihzj04/2.png?rlkey=t1mm2a5zi1avx6nj5gn1y9wgz&dl=0',
      'https://www.dropbox.com/scl/fi/kokz7gza29p9ehgwv7l8k/3.png?rlkey=s3g3xlywrbl3z18qt28nhgl70&dl=0',
      'https://www.dropbox.com/scl/fi/xpooov7mqwgaqimx2ct8u/5.png?rlkey=3xv6tx77ibu68ya6fgs2ko2o3&dl=0',
    ],
    featured: false
  },
  {
    code: '19',
    slug: 'green-office-park-9',
    title: 'Green Office Park 9',
    location: 'Indonesia',
    scope: 'Facade/Interior',
    year: '2019',
    image: 'https://www.dropbox.com/scl/fi/qyp5p23ve8kdym22oopa1/2.png?rlkey=spixdbgees7qcom3u8atc2ff6&dl=0',
    category: 'OFFICE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> BSD Green Office Park is one of Indonesia's first green offices in a district encompassing a total area of 25 hectares.Representing a volcano (auditorium) located in the center of the compound, the lighting is designed to enhance the surface of the volcano to give it the look of an eruption- ready state. As a symbol of presence and strength, the mountainous form protruding out from an otherwise calming landscape. The flowing volcano lava is perfectly stimulated by the fading linear LED.
The lines were further brought to both of the main office towers, steadily stand by the side of the volcano. It is representing both the philosophical concept of the volcano and the sophistication of the Green Office Park 9.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/3lrp7jq4t005ftyznc0ae/1.png?rlkey=3c4ff6ua07boqqa1ty0oqibbr&dl=0',
      'https://www.dropbox.com/scl/fi/ba9v4d5v9dey64q0yn3y3/3.png?rlkey=ip2g49ba0frs0te9k1mxb8aco&dl=0',
      'https://www.dropbox.com/scl/fi/dp15299me0nqasz5306cs/4.png?rlkey=nf68dfvvf7rw9aivqu554qu19&dl=0',
      'https://www.dropbox.com/scl/fi/nlb8ee53jxf4quilzjhmq/5.png?rlkey=tjal2c1nebwf7x6a2xqpocpha&dl=0',
      'https://www.dropbox.com/scl/fi/kq57zf53c4zielsp0d3nq/6.png?rlkey=yhm53hcowicjab5fww561hktt&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'SAP-office',
    title: 'SAP Office',
    location: 'Singapore',
    scope: 'Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/kvtr0zr83jivlhorm5u8i/1.png?rlkey=scbb3lcfrlaymuxuzp1hl0gfa&dl=0',
    category: 'OFFICE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> SAP is one of the leading IT companies worldwide. Emphasizing on the strong corporate identity, the designer came up with the concept of "reveal" and also mixing the organic line of the light fittings with the rigid form of the space. The idea of this office is raw and naked. As one strolls down the corridor to the main lobby, one could see the transparent floor with its glowing bones and all the cables inside. To strengthen this idea, lighting elements were designed and selected to express the "reveal" nature, such as exposed tube, peek of light, see-through light fittings, etc.</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/p6n1o1fs6eyqoxguhnsfg/2.png?rlkey=ekw7frymbeklj9omg1uzkfwhc&dl=0',
      'https://www.dropbox.com/scl/fi/kvtr0zr83jivlhorm5u8i/1.png?rlkey=scbb3lcfrlaymuxuzp1hl0gfa&dl=0',
      'https://www.dropbox.com/scl/fi/abmucslmey5y72mm7i9du/3.png?rlkey=jwkq1kjeuqseeb0mtgmce779n&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'SBF-tower',
    title: 'SBF Tower',
    location: 'Singapore',
    scope: 'Facade/Interior/Landscape',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/4huzujpujep7206o6e6nc/1.png?rlkey=763pzw4d8wzvbmdglgv5ks6sk&dl=0',
    category: 'OFFICE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> museum in bangalore, india</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/4huzujpujep7206o6e6nc/1.png?rlkey=763pzw4d8wzvbmdglgv5ks6sk&dl=0',
      'https://www.dropbox.com/scl/fi/7syscn3xdv8frqosc1vh8/2.png?rlkey=skwp3tqz40n9k99c7znbnnmhx&dl=0',
    ],
    featured: false
  },
  {
    code: '14',
    slug: 'fraser-office-tower',
    title: 'Fraser Office Tower',
    location: 'Singapore',
    scope: 'Facade/Interior',
    year: '2014',
    image: 'https://www.dropbox.com/scl/fi/h4tq71woccqda1l4hki81/1.jpeg?rlkey=acywtstwcdfm8uv4d3nhj6o9z&dl=0',
    category: 'OFFICE',
    text: `
      <div className="">
        <h2>Project Description</h2>
        <p> museum in bangalore, india</p>
      </div>
    `,
    image_collections: [
      'https://www.dropbox.com/scl/fi/h4tq71woccqda1l4hki81/1.jpeg?rlkey=acywtstwcdfm8uv4d3nhj6o9z&dl=0',
      'https://www.dropbox.com/scl/fi/yt4uodfk2ugdzr5wvitvj/2.jpeg?rlkey=cqoinaowqmw0if5bhj7xai3ca&dl=0',
      'https://www.dropbox.com/scl/fi/bs90rcrtlfdvpdlkwv7ej/3.jpeg?rlkey=0d2u8e6r814l9ubjix75tbu1t&dl=0',
    ],
    featured: false
  },


]

module.exports = data