let data = {
    singapore: {
        email: 'SINGAPORE@LIGHTBOXLIGHTING.COM',
        phone: '(+65) 6294 6803',
        address: '19 Jalan Kilang Barat 03-05 Acetech Center Building 159361'
    },
    indonesia: {
        email: 'INDONESIA@LIGHTBOXLIGHTING.COM',
        phone: '(+62) 21-758 111 31',
        address: 'Jalan Cipete IX No. 23A Jakarta, Indonesia 12410'
    },
    philippines: {
        email: 'PHILIPPINES@LIGHTBOXLIGHTING.COM',
        phone: null,
        address: 'U718 Swire Elan Suites 49 Annapolis Greenhills San Juan City 15000'
    },
    thailand: {
        email: 'PLUSBANGKOK@LIGHTBOXLIGHTING.COM',
        phone: '(+66) 9697 4547 9',
        address: '5th Floor, Ploymitr Building, No. 81 Sukhumvit 2, Klongtoey, Klongtoey, Bangkok 10110'
    },
    vietnam: {
        email: 'VIETNAM@LIGHTBOXLIGHTING.COM',
        phone: '(+84) 968 195 486',
        address: 'Vinhomes Central Park 720A Dien Bien Phu Binh Thanh District, HCMC, Vietnam'
    },
}

module.exports = data