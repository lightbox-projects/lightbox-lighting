import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import Header from '../components/Header'
// import { StaticImage } from 'gatsby-plugin-image';

import ServiceLanding from '../components/Service/ServiceLanding'
import ServiceDesignRetail from '../components/Service/ServiceDesignRetail'
import ServiceDesignProcess from '../components/Service/ServiceDesignProcess'
import ServiceFixtureDesign from '../components/Service/ServiceFixtureDesign'
import ServiceMasterplan from '../components/Service/ServiceMasterplan'
import ServiceProductDesign from '../components/Service/ServiceProductDesign'

import { isMobile } from 'react-device-detect';

const LandingPage = ({ data, location }) => {
  const siteTitle = 'Services'

  const [step, setStep] = React.useState(0)
  const stepColour = {
    0: '#C0BCAD',
    1: '#B5AF9D',
    2: '#A9A28D',
    3: '#9C957D',
    4: '#A69C7A',
    5: '#90886D',
  }

  return (
    <Layout location={location} title={siteTitle}>
      <div style={{ overflowX: 'hidden', height: isMobile ? 'unset' : '100vh', background: stepColour[step] }} className={`${isMobile ? 'p-4' : 'px-5 pt-5'} position-relative d-flex flex-column`}>
        <Header title={'Services'} />

        <div className="my-auto py-2">
          {step === 0 && <ServiceLanding step={step} setStep={setStep} />}
          {step === 1 && <ServiceDesignProcess step={step} setStep={setStep} />}
          {step === 2 && <ServiceMasterplan step={step} setStep={setStep} />}
          {step === 3 && <ServiceDesignRetail step={step} setStep={setStep} />}
          {step === 4 && <ServiceProductDesign step={step} setStep={setStep} />}
          {step === 5 && <ServiceFixtureDesign step={step} setStep={setStep} />}
        </div>

        {/* <StaticImage src="../../static/assets/lightbox lighting logo 1.png" alt="" className='mt-3' style={{ maxWidth: '130px' }} /> */}
      </div>
    </Layout>
  )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Services" />