import React from 'react'
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import { withoutCredentials } from '../../../static/baseURL'

const Element = ({ location }) => {
    const [siteTitle, setSiteTitle] = React.useState('Tender System')
    const [data, setData] = React.useState(null)

    React.useEffect(() => {
        withoutCredentials().post('api/domain-alias/get-if-available', {
            domain: 'https://lightboxlighting.com',
            unique: location.pathname.split("/")[2]
        }, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        }).then(res => {
            setData(res.data)
            setSiteTitle(res.data.slink_title)
        })
    }, [location.pathname])

    return (
        <Layout location={location} title={siteTitle}>
            {
                data &&
                <div style={{ overflowX: 'hidden' }} className='d-flex flex-column vh-100'>
                    <iframe src={data.slink_data} title={siteTitle} className='flex-fill w-100' style={{ overflow: 'hidden' }}></iframe>
                </div>
            }
        </Layout>
    )
}

export default Element

export const Head = () => <Seo />