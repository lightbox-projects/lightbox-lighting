import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { navigate } from "gatsby"

const LandingPage = ({ data, location }) => {
  const siteTitle = 'Landing'

  React.useEffect(() => {
    navigate('/projects')
  }, [])

  return (
    <Layout location={location} title={siteTitle} noFooter absoluteNav>

      <main style={{}} className="bg-white d-flex flex-column vh-100 justify-content-center align-items-center">
        {/* <div className="position-relative vh-100 vw-100">
          <div className="jumbo position-absolute vh-100 vw-100"></div>
        </div> */}

        <h1 className="vh-100 vw-100 position-absolute d-flex align-items-center justify-content-center fs-5 fw-bold">
          <div className="shimmer fw-bold" style={{ letterSpacing: '20px', fontSize: '120px' }}>
            LIGHTBOX
          </div>
        </h1>
      </main>

    </Layout>
  )
}

export default LandingPage

export const Head = () => <Seo title="Landing Page" />
