import * as React from "react"

import Layout from "../../components/layout"
import Seo from "../../components/seo"

import Header from '../../components/Header'
import { withoutCredentials } from '../../../static/baseURL'
import { StaticImage } from 'gatsby-plugin-image';

// import dawta from "../../static/content/projects"
import { composeArrays, filterByThreshold, decideColumnLayout } from '../../components/Projects/project-helper'

import { isMobile } from 'react-device-detect';

import ScrollContainer from 'react-indiana-drag-scroll'

const hasVideoExtension = (filename) => {
  const videoExtensions = [
    '.mp4', '.avi', '.mov', '.mkv', '.flv', '.wmv', 
    '.webm', '.m4v', '.mpg', '.mpeg', '.3gp', '.ogg'
  ];

  const lowercasedFilename = filename.toLowerCase();

  return videoExtensions.some(extension => lowercasedFilename.endsWith(extension));
};

const mediaItem = (item) => {
  if (hasVideoExtension(item.parsedUrl)) {
    return (
      <video className="mb-0" style={{ width: 'fit-content', maxHeight: '300px', objectFit: 'contain' }} controls>
        <source src={item.parsedUrl} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    );
  } else {
    return <img src={item.parsedUrl} className="mb-0" alt="...." style={{ width: 'fit-content', maxHeight: '300px', objectFit: 'contain' }} />;
  }
};

const DetailPage = ({ data, location, pageContext }) => {
  const [current, setCurrent] = React.useState(null)
  // const [slides, setSlides] = React.useState([])
  const [dividedSlides, setDividedSlides] = React.useState([[], []])
  const parentElm = React.useRef(null)

  const goBack = () => {
    window.history.back();
  }

  // const moveRight = () => {
  //   parentElm.current.scrollLeft += 120
  // }

  React.useEffect(() => {
    withoutCredentials().get('api/masterdata/gallery/lightbox-lighting', {
      params: {
        slug: location.pathname.split("/")[2]
      },
    }).then(res => {
      setCurrent(res.data)

      let newArray = res.data.slides
      const promises = newArray.map(item => {
        // if(hasVideoExtension(item.parsedUrl))
          return item  
        // else
        // return new Promise((resolve) => {
        //   const img = new Image();
        //   img.src = item.parsedUrl;

        //   img.onload = () => {
        //     const imageAspectRatio = img.width / img.height;
        //     item.columnThreshold = decideColumnLayout(imageAspectRatio);
        //     resolve(item);
        //   };
        // });
      });

      Promise.all(promises).then(newArray => {
        // const onesArray = filterByThreshold(newArray, 1);
        // const twosArray = filterByThreshold(newArray, 2);

        // let resultArray = composeArrays(onesArray, twosArray)
        // const flattenedArray = [].concat(...resultArray);
        // setSlides(flattenedArray);

        let dd = [[], []]
        let i = 0
        newArray.forEach((item) => {
          if (i === 2) i = 0
          dd[i].push(item)
          i++
        })
        dd = dd.filter(item => (item.length > 0))
        setDividedSlides(dd)

      });

    })
  }, [location.pathname])

  return (
    <Layout location={location} title={current ? current.gallery_title : 'Projects'} absoluteNav>

      {
        current && <div style={{ overflowX: 'hidden', height: isMobile ? '' : '100vh', background: '#B1B1B1' }} className={`${isMobile ? 'p-4' : 'px-5 pt-5'} position-relative d-flex flex-column`}>
          <Header title={current.gallery_lbx_category} />
          <button onClick={() => goBack()} className={`${isMobile ? 'h4' : 'h3'} clickable-button text-start`} style={{ fontWeight: '200' }}>Back to Projects</button>

          <div className="row mt-2 flex-fill" style={{ gap: '10px 0' }}>
            <div className="col-lg my-auto">
              <div className="d-flex flex-column px-4">
                <div className="my-auto">
                  <div className="h2 mb-3" style={{ fontWeight: 400, letterSpacing: '1px' }}>
                    {current.gallery_title}
                  </div>
                  <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px' }} dangerouslySetInnerHTML={{ __html: current.gallery_lbx_content }} />
                </div>
              </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg-8 my-auto">

              {/* <div className="scroll-no-scrollbar" style={{ overflowX: 'scroll', cursor: 'grab' }}> */}
              <ScrollContainer ref={parentElm} className="scroll-container" >
                <div className="d-flex flex-column mx-4" style={{ gap: '20px', }} title="Drag to Scroll">
                  {
                    dividedSlides.map((col, index) => (
                      <div key={index + 'ASDSADSD'} className={`d-flex bg align-items-${index === 0 ? 'end' : 'start'}`} style={{ gap: '20px', }}>
                        {
                          col.map((item, i) => (
                            <div className="flex- mb-0" key={i + 'ASDASDDFSDF'} >
                              {mediaItem(item)}
                              {/* <img src={item.parsedUrl} className="mb-0" alt="...." style={{ width: 'fit-content', maxHeight: '300px', objectFit: 'contain' }} /> */}
                            </div>
                          ))
                        }
                      </div>
                    ))
                  }
                </div>
                {/* </div> */}
              </ScrollContainer>
            </div>

            {/* <div className="col-lg d-flex">
              <div className="row m-auto" style={{ gap: '20px 0' }}>
                {
                  slides.map((item, i) => ( 
                    <div className={`col-lg-${item.columnThreshold == 1 ? '3' : '6'}`} key={item + i}>
                      <button className="position-relative project-card clickable-button w-100" style={{ cursor: 'unset' }}>
                        <img src={item.parsedUrl} className="w-100" style={{ objectFit: 'cover', minHeight: '200px', maxHeight: '400px' }} alt="...." />
                      </button>
                    </div>
                  ))
                }
              </div>
            </div> */}

          </div>

          <StaticImage src="../../../static/assets/lightbox lighting logo 1.png" alt="" className='mt-3' style={{ maxWidth: '130px' }} />

        </div>
      }

    </Layout>
  )
}

export default DetailPage

export const Head = () => <Seo title="Project Detail" />

