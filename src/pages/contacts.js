import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import { StaticImage } from 'gatsby-plugin-image';
import Header from '../components/Header'
import Navigation from '../components/Contacts/Navigation'

import Studios from '../components/Contacts/Studios'
import Clients from '../components/Contacts/Clients'
import Publications from '../components/Contacts/Publications'
import Awards from '../components/Contacts/Awards'
import Events from '../components/Contacts/Events'

import { isMobile } from 'react-device-detect';

const LandingPage = ({ data, location }) => {
  const siteTitle = 'Contacts'

  const stepColour = {
    0: '#B9B8B1',
    1: '#B9B8B1',
    2: '#B9B8B1',
    3: '#B9B8B1',
    4: '#B9B8B1',
    // 1: '#ACAAA2',
    // 2: '#9E9D93',
    // 3: '#9E9D93',
    // 4: '#908E83',
  }

  const [step, setStep] = React.useState(0)

  return (
    <Layout location={location} title={siteTitle}>
      <div style={{ overflowX: 'hidden', minHeight: '100vh', background: stepColour[step] }} className={`${isMobile ? 'p-4' : 'p-5'} position-relative d-flex flex-column`}>
        <Header title={'Our Contacts'} />

        <div className={`row mt-5 ${isMobile ? '' : 'flex-fill'}`} style={{ gap: '10px 0' }}>
          <div className="col-lg-3 d-flex flex-column" style={{ maxWidth: '250px' }}>
            <Navigation setStep={setStep} step={step} />
            {!isMobile && <StaticImage src="../../static/assets/lightbox lighting logo 1.png" alt="" placeholder='none' className='mt-auto' style={{ maxWidth: '130px' }} />}
          </div>
          {!isMobile && <div className='col-lg-auto' style={{ padding: '0 4em' }}><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>}
          <div className={`col-lg ${isMobile ? 'pt-3' : 'pt-3'}`}>
            {step === 0 && <Studios step={step} setStep={setStep} />}
            {step === 1 && <Clients step={step} setStep={setStep} />}
            {step === 2 && <Publications step={step} setStep={setStep} />}
            {step === 3 && <Awards step={step} setStep={setStep} />}
            {step === 4 && <Events step={step} setStep={setStep} />}
          </div>

        </div>

      </div>
    </Layout>
  )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Contacts" />
