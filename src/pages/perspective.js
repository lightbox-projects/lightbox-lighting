import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import AboutUs from '../components/Perspective/AboutUs'
import CompanyJourney from '../components/Perspective/CompanyJourney'

import { isMobile } from 'react-device-detect';

const Element = ({ data, location }) => {
  const siteTitle = 'Perspective'

  return (
    <Layout location={location} title={siteTitle}>
      <AboutUs />
      {!isMobile && <CompanyJourney />}
    </Layout>
  )
}

export default Element

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Perspective" />