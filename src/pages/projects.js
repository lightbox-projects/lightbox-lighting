import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Drawer from 'react-modern-drawer'

import ProjectLanding from '../components/Projects/ProjectLanding'
import ProjectTimeline from '../components/Projects/ProjectTimeline'
import {  decideColumnLayout } from '../components/Projects/project-helper'
import { withoutCredentials } from '../../static/baseURL'

const LandingPage = ({ data, location }) => {
  const siteTitle = 'Projects'
  const [isOpen, setIsOpen] = React.useState(false)

  const [rawProjects, setRawProjects] = React.useState([])
  const [projects, setProjects] = React.useState([])
  // const [projectsBeforeConcat, setProjectBeforeConcat] = React.useState([])
  const [category, setCategory] = React.useState(null)
  const [search, setSearch] = React.useState(null)

  const toggleDrawer = () => {
    setIsOpen((prevState) => !prevState)
  }

  function onClickCategory(category, search = null) {
    setCategory(category)
    setSearch(search)

    if (!category && !search) {
      callLandingProjects()
    } else {
      let newArray = rawProjects
      if (category && !search)
        newArray = newArray.filter(item => (item.gallery_lbx_category?.toLowerCase() === category.toLowerCase()))

      
      if (search) newArray = newArray.filter(item => (item.gallery_title?.toLowerCase()?.includes(search?.toLowerCase())))
      
      newArray = newArray.sort((a, b) => parseInt(b.gallery_lbx_year) - parseInt(a.gallery_lbx_year));
      
      const promises = newArray.map(item => {
        return new Promise((resolve) => {
          // const img = new Image();
          // img.src = item.thumbnail;
          
          // img.onload = () => {
          //   const imageAspectRatio = img.width / img.height;
          //   item.columnThreshold = decideColumnLayout(imageAspectRatio);
          //   resolve(item);
          // };

          const imageAspectRatio = item.thumbnail_d.width / item.thumbnail_d.height;
          item.columnThreshold = decideColumnLayout(imageAspectRatio);
          resolve(item);
        });
      });
      
      Promise.all(promises).then(newArray => {
        // const onesArray = filterByThreshold(newArray, 1);
        // const twosArray = filterByThreshold(newArray, 2);
        
        // let resultArray = composeArrays(onesArray, twosArray)
        
        let flattenedArray = []
        if (!category) {
          flattenedArray = [].concat(...newArray.slice(0, 2));
        } else {
          flattenedArray = [].concat(...newArray);
        }
        
        setProjects(flattenedArray);
      });
    }

  }

  const callLandingProjects = () => {
    withoutCredentials().get('api/masterdata/gallery/lightbox-lighting').then(res => {
      const promises = res.data.map(item => {
        return new Promise((resolve) => {
          // const img = new Image();
          // img.src = item.thumbnail;

          // img.onload = () => {
          //   const imageAspectRatio = img.width / img.height;
          //   item.columnThreshold = decideColumnLayout(imageAspectRatio);
          //   resolve(item);
          // };
          const imageAspectRatio = item.thumbnail_d.width / item.thumbnail_d.height;
          item.columnThreshold = decideColumnLayout(imageAspectRatio);
          resolve(item);
        });
      });

      Promise.all(promises).then(newArray => {
        // const onesArray = filterByThreshold(newArray, 1);
        // const twosArray = filterByThreshold(newArray, 2);

        // let resultArray = composeArrays(onesArray, twosArray)
        // setProjectBeforeConcat(resultArray)
        const flattenedArray = [].concat(...newArray);

        setRawProjects(res.data)
        setProjects(flattenedArray);
      });

    });
  } 

  React.useEffect(() => {
    callLandingProjects()
  }, []);

  return (
    <Layout location={location} title={siteTitle}>

      <div >
        <ProjectLanding search={search} onClickCategory={onClickCategory} isOpen={location.state?.justOpened} title={category}
          projects={projects} toggleDrawer={toggleDrawer} style={isOpen ? { overflowY: 'hidden', height: '100vh' } : {}} />
      </div>

      <Drawer
        open={isOpen}
        onClose={toggleDrawer}
        direction='bottom'
        className='vh-100'
        style={{ background: '#DFDFDF', overflowY: 'auto', overflowX: 'hidden' }}
      >
        {isOpen && <ProjectTimeline onClickCategory={onClickCategory} projects={rawProjects} toggleDrawer={toggleDrawer} />}
      </Drawer>

    </Layout>
  )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Projects" />
