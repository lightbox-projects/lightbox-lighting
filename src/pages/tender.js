import React from 'react'
import Layout from "../components/layout"
import Seo from "../components/seo"

const Element = ({ location }) => {
    const siteTitle = 'Tender System'

    let params = new URLSearchParams(location.search);

    return (
        <Layout location={location} title={siteTitle}>
            <div style={{ overflowX: 'hidden' }} className='d-flex flex-column vh-100'>
                <iframe src={params.get("url")} title="Tender System" className='flex-fill w-100' style={{ overflow: 'hidden' }}></iframe>
            </div>
        </Layout>
    )
}

export default Element

export const Head = () => <Seo title={'Tender System'} />