import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { navigate } from 'gatsby';

import logo from '../../static/assets/Comp_1_4.gif'

const LandingPage = ({ data, location }) => {
    const siteTitle = null

    React.useEffect(() => { 
        setTimeout(() => { 
            navigate('/landing')
        }, 5000)
    }, [])

    return (
        <Layout location={location} title={siteTitle}>

            <div className={`position-absolute vh-100 w-100 d-flex`} id="backdrop-loading" style={{ top: '0', left: '0', opacity: 1, zIndex: '999999999', background: '#FBFBFB' }}>
                <img className="m-auto" src={logo} alt="" placeholder='none' style={{ maxWidth: '700px' }} />
            </div>

        </Layout>
    )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo />
