import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
 
import Profile from '../components/AboutUs/Profile'
import CoreValue from '../components/AboutUs/CoreValue'
import Clients from '../components/AboutUs/Clients'

const LandingPage = ({ data, location }) => {
  const siteTitle = 'About Us'

  return (
    <Layout location={location} title={siteTitle}>

        <Profile />
        <CoreValue />
        <Clients />

    </Layout>
  )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="About Us" />

 
