import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import { isMobile } from 'react-device-detect';

import HeaderDrawer from '../components/HeaderDrawer'


const LandingPage = ({ data, location }) => {
  const siteTitle = null

  return (
    <Layout location={location} title={siteTitle}>

      <div style={{ background: '#E4E3E3', overflowY: 'auto', zIndex: '9999', height:  isMobile ? '100vh' : '100vh', justifyContent: isMobile ? 'center' : '' }} className="d-flex flex-column">
        <HeaderDrawer isOpen={true} />
      </div>

      {/* <div style={styles.container}>
        <div style={styles.gear}>⚙️</div>
        <h1 style={styles.title}>We're Under Maintenance</h1>
        <p style={styles.description}>
          Our website is currently undergoing scheduled maintenance.
          <br />
          We should be back shortly. Thank you for your patience!
        </p>
      </div> */}
    </Layout>
  )
}

const styles = {
  container: {
    background: '#E4E3E3',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    textAlign: 'center',
    flexDirection: 'column',
    color: '#333',
  },
  gear: {
    fontSize: '60px',
    animation: 'spin 4s infinite linear',
  },
  title: {
    fontSize: '48px',
    marginBottom: '10px',
  },
  description: {
    fontSize: '18px',
    marginBottom: '20px',
  },
  '@keyframes spin': {
    '0%': { transform: 'rotate(0deg)' },
    '100%': { transform: 'rotate(360deg)' },
  },
}

export default LandingPage

export const Head = () => <Seo />
