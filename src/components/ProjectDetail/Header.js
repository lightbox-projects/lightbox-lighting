import React from "react";
import { Parallax } from "react-scroll-parallax"

const Element = ({ projectData }) => {
    return (
        <div className="bg-tertiary position-relative" style={{ zIndex: 8 }}>
            <Parallax speed={-20} style={{ marginTop: '', paddingTop: '' }}>
                <img src={projectData.thumbnail} className="w-100" style={{ height: '700px', objectFit: 'cover', marginTop: '-3em' }} alt="..." />
            </Parallax>
            <div className="h-100 position-absolute d-flex flex-column" style={{ top: 0, left: 0 }}>
                <div className="mt-auto mb-5">
                    <Parallax speed={-0}>
                        <div className="px-5">
                            <p className="h5 fw-bolder text-white mb-0">{projectData.gallery_lbx_scope}</p>
                            <h5 className="display-4 fw-bolder text-white mb-0">{projectData.gallery_title}</h5>
                            <div className="bg-white w-100 my-1" style={{ height: '4px' }}></div>
                            <p className="h2 fw-bolder text-white mb-0">{projectData.gallery_lbx_location} | {projectData.gallery_lbx_year}</p>
                        </div>
                    </Parallax>
                </div>
            </div>
        </div>
    )
}

export default Element