import React from "react";

// import RViewerJS from 'viewerjs-react'
// import 'viewerjs-react/dist/index.css'

const Element = ({ projectData }) => {

    const [current, setCurrent] = React.useState(null)



    return (
        // <RViewerJS>
        <div className="h-100">
            {
                !current ?
                    <div className="row px-3" style={{ gap: '0', }}>
                        {
                            projectData.slides.map((item, i) => (
                                <button key={item + i} className="col-lg-4 p-0 project-card clickable-button" style={{ cursor: 'pointer' }} onClick={() => setCurrent(item)}>
                                    <img
                                        src={item.parsedUrl}
                                        style={{ width: '100%', aspectRatio: '1/1', objectFit: 'cover' }} alt="..." />
                                </button>
                            ))
                        }
                    </div>
                    :
                    <div className="position-relative h-100 w-100">
                        <img src={current.parsedUrl} className="w-100 h-100" style={{ objectFit: 'contain' }} alt="..." />
                        <div className="position-absolute d-flex h-100 w-100" style={{ top: 0, left: 0 }}>
                            <button className="mx-auto mt-4 mb-auto btn text-white fw-bolder" onClick={() => setCurrent(null)} style={{ width: '150px', border: '5px solid white' }}>
                                VIEW MORE
                            </button>
                        </div>
                    </div>
            }
        </div>
        // </RViewerJS>
    )
}

export default Element