import React from 'react'
import RViewerJS from 'viewerjs-react'
import { isMobile } from 'react-device-detect';

import { withoutCredentials } from '../../../static/baseURL'

// import event_img_1 from '../../../static/assets/events/image 517.png'
// import event_img_1_2 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/1.png'
// import event_img_1_3 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/2.png'
// import event_img_1_4 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/3.png'
// import event_img_1_5 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/4.png'
// import event_img_1_6 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/5.png'
// import event_img_1_7 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/6.png'
// import event_img_1_8 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/7.png'
// import event_img_1_9 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/8.png'
// import event_img_1_10 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/9.png'
// import event_img_1_11 from '../../../static/assets/events/Lightbox 15th Charity Trip to Pway Nyet San/10.png' 

const Element = () => {

    const [content, setContent] = React.useState([]) 

    React.useEffect(() => {
        withoutCredentials().get('api/masterdata/events/lightbox-lighting').then(res => {
            let temp = res.data 

            setContent(temp)
        });
      }, []);

    // const [content, setContent] = React.useState([
    //     {
    //         image: event_img_1, title: 'Lightbox 15th Charity Trip to Pway Nyet San',
    //         desc: "We are thrilled to share the heartwarming moments and impactful experiences from Lightbox's 15th Charity Trip to Pway Nyet San. This event, which took place on [Insert Date], brought together a dedicated group of volunteers determined to make a difference in the lives of the residents of Pway Nyet San.",
    //         video: 'https://www.youtube.com/embed/38Len8VyKb4',
    //         galleries: [event_img_1_2, event_img_1_3, event_img_1_4, event_img_1_5, event_img_1_6, event_img_1_7, event_img_1_8, event_img_1_9, event_img_1_10, event_img_1_11]
    //     }, 
    // ]);

    return (
        <div>

            {!isMobile && <div className='h3 fw-bold text-primary mb-4'>
                Events
            </div>}

            <div className='d-flex flex-column' style={{ gap: '6em' }}>
                {
                    content.map(item => (
                        <div className=''>
                            <div className="row mb-4" style={{ gap: ' 10px 0' }}>
                                <div className='col-lg-5'>
                                    {!item.event_video && <img src={item.thumbnail} style={{ background: '#B7B7B7', border: '2px solid var(--primary)' }} className='w-100' alt="...." />}
                                    {item.event_video &&
                                        <div className='h-100'>
                                            <iframe className='w-100 h-100' style={{minHeight: '400px'}} src={item.event_video} title={item.event_title}
                                                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                        </div>
                                    }
                                </div>
                                <div className='col-lg-7 my-auto'>
                                    <div className="text-start">
                                        <div className={`${isMobile ? 'h2 mt-3' : 'h1'} fw-bold mb-0`} style={{ fontWeight: 400, letterSpacing: '1px' }}>{item.event_title}</div>
                                        <hr />
                                        <div className="mb-0 mt-3" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>{item.event_desc}</div>
                                    </div>
                                </div>
                            </div>
                            <button className='clickable-button'>
                                <RViewerJS className='position-relative'>
                                    <div className='d-flex' style={{ gap: '10px', overflowX: 'scroll' }}>
                                        {
                                            item.gallery.map(o => (
                                                <img src={o.parsedUrl} className='m-0' alt='...' style={{ height: '200px', border: '2px solid var(--primary)' }} />
                                            ))
                                        }
                                    </div>
                                </RViewerJS>
                            </button>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Element