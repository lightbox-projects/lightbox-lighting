// import { StaticImage } from 'gatsby-plugin-image'
import React from 'react'
// import RViewerJS from 'viewerjs-react'
import { isMobile } from 'react-device-detect';

import { withoutCredentials } from '../../../static/baseURL'

const Element = () => { 

    const [content, setContent] = React.useState([[], [], []]) 

    React.useEffect(() => {
        withoutCredentials().post('api/masterdata/awards/dt').then(res => {
            let temp = res.data?.data
            let temp_arr = [[], [], []]

            let i = 0
            temp.forEach(item => {
                temp_arr[i].push(item)
                i++
                if(i === 3) i = 0
            })

            setContent(temp_arr)
        });
      }, []);

    return (
        <div>

            {!isMobile && <div className='h3 fw-bold text-primary mb-4'>
                Awards
            </div>}

            {isMobile && <div className='scroll-no-scrollbar' style={{ maxHeight: '650px', overflowY: 'scroll' }}>
                <div className="column-feed" style={{ width: `${50 - 1}%`, }}>
                    {
                        content[0].map(item => (
                            <div className="image-card pe-1">
                                {/* <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS> */}
                                <div className="text-start">
                                    <div className="h4 fw-normal mb-0" style={{ textDecoration: 'underline' }}>{item.mawd_title}</div>
                                    <div className="h4 fw-bold mb-0">{item.mawd_desc}</div>
                                    <div className="h4 fw-normal mb-0">{item.mawd_about}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="column-feed" style={{ width: `${50 - 1}%`, }}>
                    {
                        content[1].map(item => (
                            <div className="image-card ps-1">
                                {/* <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS> */}
                                <div className="text-start">
                                    <div className="h4 fw-normal mb-0" style={{ textDecoration: 'underline' }}>{item.mawd_title}</div>
                                    <div className="h4 fw-bold mb-0">{item.mawd_desc}</div>
                                    <div className="h4 fw-normal mb-0">{item.mawd_about}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
            }

            {!isMobile && <div className='scroll-no-scrollbar' style={{ maxHeight: '650px', overflowY: 'scroll' }}>
                <div className="column-feed" style={{ width: `${33 - 1}%` }}>
                    {
                        content[0].map(item => (
                            <div className="image-card py-3">
                                {/* <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS> */}
                                <div className="text-start">
                                    <div className="h4 fw-normal mb-0" style={{ textDecoration: 'underline' }}>{item.mawd_title}</div>
                                    <div className="h4 fw-bold mb-0">{item.mawd_desc}</div>
                                    <div className="h4 fw-normal mb-0">{item.mawd_about}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="column-feed" style={{ width: `${33 - 1}%` }}>
                    {
                        content[1].map(item => (
                            <div className="image-card p-3">
                                {/* <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS> */}
                                <div className="text-start">
                                    <div className="h4 fw-normal mb-0" style={{ textDecoration: 'underline' }}>{item.mawd_title}</div>
                                    <div className="h4 fw-bold mb-0">{item.mawd_desc}</div>
                                    <div className="h4 fw-normal mb-0">{item.mawd_about}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="column-feed" style={{ width: `${33 - 1}%` }}>
                    {
                        content[2].map(item => (
                            <div className="image-card py-3">
                                {/* <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS> */}
                                <div className="text-start">
                                    <div className="h4 fw-normal mb-0" style={{ textDecoration: 'underline' }}>{item.mawd_title}</div>
                                    <div className="h4 fw-bold mb-0">{item.mawd_desc}</div>
                                    <div className="h4 fw-normal mb-0">{item.mawd_about}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>}
        </div>
    )
}

export default Element