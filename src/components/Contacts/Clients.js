import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'
import { isMobile } from 'react-device-detect';

const Element = () => {
    return (
        <div>

            {!isMobile && <div className='h3 fw-bold text-primary'>
                Clients
            </div>}

            <div className={`ql-editor p-0 h4 mb-5 ${isMobile ? '' : 'mt-5'}`} style={{ letterSpacing: '1.5px', lineHeight: '20px', maxWidth: '700px', textAlign: 'justify' }}>
                Lightbox is a lighting design company that uses light to improve life, enhance space and experience another dimension of architecture. We are always evolving and adopting new lighting design to integrate with architecture.
            </div>

            <div className='row pt-3 scroll-no-scrollbar' style={{ gap: '30px 0', height: '500px', overflowY: 'scroll' }}>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/14.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/image-removebg-preview 2.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/image-removebg-preview (3) 1.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/17.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/9.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/13.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/16.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/18.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/image-removebg-preview (2) 1.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/16.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/11.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>

                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/20.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/21.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/22.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/23.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/24.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/25.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/26.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/27.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/28.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/29.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/31.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/32.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/33.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/34.png" alt="" className='m-auto' style={{ maxWidth: '100%', height: '200px', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>
                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/add/35.png" alt="" className='m-auto' style={{ maxWidth: '100%', mixBlendMode: 'color-burn' }} />
                    </div>
                </div>

                <div className='col-lg-3 col-6'>
                    <div className='h-100 d-flex'>
                        <StaticImage src="../../../static/assets/clients/12.png" alt="" className='m-auto' style={{ maxWidth: '100%' }} />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Element