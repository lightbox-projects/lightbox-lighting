import React from 'react'

import { isMobile } from 'react-device-detect';

import companiesData from "../../../static/content/companies_data"
// import { StaticImage } from 'gatsby-plugin-image'

const Element = () => {

    const elmRef = React.useRef(null)
    // const [isTop, setTop] = React.useState(true)

    const companies = {
        'singapore': companiesData['singapore'],
        'thailand': companiesData['thailand'],
        'indonesia': companiesData['indonesia'],
        'vietnam': companiesData['vietnam'],
        'philippines': companiesData['philippines'],
    }

    // const toggleScroll = () => {
    //     if (elmRef.current.scrollTop != 0) {
    //         elmRef.current.scrollTop = 0
    //         setTop(true)
    //     } else {
    //         elmRef.current.scrollTop = 700
    //         setTop(false)
    //     }
    // }

    return (
        <div className=''>

            {/* <div className="position-absolute d-flex flex-column" style={{ bottom: 0, left: '48%' }}>
                <button onClick={() => toggleScroll()} className="clickable-button mx-auto my-auto mb-2" style={{ fontSize: '80px' }}>
                    {
                        isTop ?
                            <StaticImage src="../../../static/assets/image 522.png" alt="" className='my-5' style={{ maxWidth: '40px' }} />
                            :
                            <StaticImage src="../../../static/assets/image 521.png" alt="" className='my-5' style={{ maxWidth: '40px' }} />
                    }
                </button>
            </div> */}

            <div className='scroll-no-scrollbar' ref={elmRef} style={{ maxHeight: '700px', overflowY: 'scroll' }}>
                <div className={`row justify-content-between align-items-center`} style={{ gap: '60px 0', }}>
                    {
                        Object.entries(companies).map((item, i) => (
                            <div className={`col-lg-6`} style={{ gap: '10px 0' }}>
                                <div className=' d-flex flex-column ' style={{ gap: '20px 0' }}>

                                    <div className='text-center text-primary h3 fw-bolder' style={{ textTransform: 'capitalize' }}>{item[0]}</div>

                                    <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                        <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                                            <div>
                                                <div className="rounded-circle d-flex bg-dark" style={{ width: isMobile ? '25px' : '35px', height: isMobile ? '25px' : '35px' }}>
                                                    <i className="ti ti-mail-filled m-auto" style={{ fontSize: isMobile ? '15px' : '23px', color: '#B9B8B1' }}></i>
                                                </div>
                                            </div>
                                            <div className={`mb-0 ${isMobile ? 'ms-1 h5' : 'ms-4 h4'}`} style={{ textTransform: 'uppercase' }}>{item[1].email}</div>
                                        </div>
                                    </div>
                                    {item[1].phone && <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                        <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                                            <div>
                                                <div className="rounded-circle d-flex bg-dark" style={{ width: isMobile ? '25px' : '35px', height: isMobile ? '25px' : '35px' }}>
                                                    <i className="ti ti-phone-filled m-auto" style={{ fontSize: isMobile ? '15px' : '23px', color: '#B9B8B1' }}></i>
                                                </div>
                                            </div>
                                            <div className={`mb-0 ${isMobile ? 'ms-1 h5' : 'ms-4 h4'}`} style={{ textTransform: 'uppercase' }}>{item[1].phone}</div>
                                        </div>
                                    </div>}
                                    <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                        <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                                            <div>
                                                <div className="rounded-circle d-flex bg-dark" style={{ width: isMobile ? '25px' : '35px', height: isMobile ? '25px' : '35px' }}>
                                                    <i className="ti ti-map-pin-filled m-auto" style={{ fontSize: isMobile ? '15px' : '23px', color: '#B9B8B1' }}></i>
                                                </div>
                                            </div>
                                            <div className={`mb-0 ${isMobile ? 'ms-1 h5' : 'ms-4 h4'}`} style={{ textTransform: 'uppercase' }}>{item[1].address}</div>
                                        </div>
                                    </div>
                                </div>

                                {/* <div className='col-lg-auto'>
                                {item[0] === 'singapore' && <StaticImage className='h-100' alt='...' src='../../../static/assets/image 503.png' />}
                                {item[0] === 'thailand' && <StaticImage className='h-100' alt='...' src='../../../static/assets/image 528.png' />}
                                {item[0] === 'indonesia' && <StaticImage className='h-100' alt='...' src='../../../static/assets/image 532.png' />}
                                {item[0] === 'vietnam' && <StaticImage className='h-100' alt='...' src='../../../static/assets/image 533.png' />}
                                {item[0] === 'philippines' && <StaticImage className='h-100' alt='...' src='../../../static/assets/image 503.png' />}
                            </div> */}

                            </div>
                        ))
                    }
                </div>
            </div>

        </div>
    )
}

export default Element