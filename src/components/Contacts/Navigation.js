import React from 'react'

const Element = ({ step, setStep }) => {
    return (
        <div>
            <div className="d-flex flex-column service-nav">
                <div className={`${step === 0 ? 'active' : ''}`}>
                    <button className="h3 clickable-button text-start" onClick={() => setStep(0)}>
                        Studios
                    </button>
                    <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                </div>
                <div className={`${step === 1 ? 'active' : ''}`}>
                    <button className="h3 clickable-button text-start" onClick={() => setStep(1)}>
                        Clients
                    </button>
                    <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                </div>
                <div className={`${step === 2 ? 'active' : ''}`}>
                    <button className="h3 clickable-button text-start" onClick={() => setStep(2)}>
                        Publications
                    </button>
                    <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                </div>
                <div className={`${step === 3 ? 'active' : ''}`}>
                    <button className="h3 clickable-button text-start" onClick={() => setStep(3)}>
                        Awards
                    </button>
                    <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                </div>
                <div className={`${step === 4 ? 'active' : ''}`}>
                    <button className="h3 clickable-button text-start" onClick={() => setStep(4)}>
                        Events
                    </button>
                    <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                </div>
            </div>
        </div>
    )
}

export default Element