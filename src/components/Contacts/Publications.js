import React from 'react'

import RViewerJS from 'viewerjs-react'
import { isMobile } from 'react-device-detect';
import { withoutCredentials } from '../../../static/baseURL'

// import publication_img_1 from '../../../static/assets/publications/63e06a373d038-photo_6260029879370234398_y 6.png'
// import publication_img_2 from '../../../static/assets/publications/photo_2024-02-19 14.24-2.png'
// import publication_img_3 from '../../../static/assets/publications/photo_2024-02-19 14.24-3.png'

// import publication_img_4 from '../../../static/assets/publications/63e06a373d038-photo_6260029879370234398_y 7.png'
// import publication_img_5 from '../../../static/assets/publications/63e06a373d038-photo_6260029879370234398_y 10.png'
// import publication_img_6 from '../../../static/assets/publications/photo_2024-02-19 14.24-1.png'

// import publication_img_7 from '../../../static/assets/publications/photo_2024-02-19 14.24.png'
// import publication_img_8 from '../../../static/assets/publications/photo_2024-02-19 14.24(1).png'

const Element = () => {

    const [content, setContent] = React.useState([[], [], [], []])

    // const content = [
    //     [
    //         { image: publication_img_1, title: 'THE LIGHTBOX PTE. LTD.', desc: 'MONDO ARC 50TH ISSUE CELEBRATES 50 LIGHTING DESIGNERS' },
    //         { image: publication_img_2, title: 'Jewel Box', desc: 'test' },
    //         { image: publication_img_3, title: 'Jewel Box', desc: 'tes' },
    //     ],

    //     [
    //         { image: publication_img_4, title: 'PARAGON', desc: 'A LIGHT TOUCH IN ORCHARD AND BUGIS (THE STRAITS TIME)' },
    //         { image: publication_img_5, title: 'THE JEWEL BOX', desc: 'CROWN JEWEL' },
    //         { image: publication_img_6, title: 'Jewel Box', desc: 'tes' },
    //     ],

    //     [
    //         { image: publication_img_7, title: 'THE JEWEL BOX', desc: 'CROWN JEWEL' },
    //         { image: publication_img_8, title: 'Jewel Box', desc: 'tes' },
    //     ],

    //     [
    //         { image: publication_img_7, title: 'THE JEWEL BOX', desc: 'CROWN JEWEL' },
    //         { image: publication_img_8, title: 'Jewel Box', desc: 'tes' },
    //     ]
    // ];

    React.useEffect(() => {
        withoutCredentials().post('api/masterdata/publication/dt').then(res => {
            let temp = res.data?.data
            let temp_arr = [[], [], [], []]

            let i = 0
            temp.forEach(item => {
                temp_arr[i].push(item)
                i++
                if (i === 4) i = 0
            })

            setContent(temp_arr)
        });
    }, []);

    return (
        <div>

            {!isMobile && <div className='h3 fw-bold text-primary mb-4'>
                Publications
            </div>}

            {isMobile &&
                <div className='scroll-no-scrollbar' style={{ maxHeight: '650px', overflowY: 'scroll' }}>
                    <div className="column-feed" style={{ width: `${50 - 1}%`, }}>
                        {
                            content[0].map(item => (
                                <div className="image-card pe-1">
                                    <RViewerJS>
                                        <img src={item.image_raw}
                                            style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                    </RViewerJS>
                                    <div className="text-start mt-4">
                                        <div className="h4 fw-bold mb-1">{item.mpub_title}</div>
                                        <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                    </div>
                                </div>
                            ))
                        }
                        {
                            content[2].map(item => (
                                <div className="image-card pe-1">
                                    <RViewerJS>
                                        <img src={item.image_raw}
                                            style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                    </RViewerJS>
                                    <div className="text-start mt-4">
                                        <div className="h4 fw-bold mb-1">{item.mpub_title}</div>
                                        <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                    <div className="column-feed" style={{ width: `${50 - 1}%`, }}>
                        {
                            content[1].map(item => (
                                <div className="image-card ps-1">
                                    <RViewerJS>
                                        <img src={item.image_raw}
                                            style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                    </RViewerJS>
                                    <div className="text-start mt-4">
                                        <div className="h4 fw-bold mb-1">{item.mpub_title}</div>
                                        <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                    </div>
                                </div>
                            ))
                        }
                        {
                            content[3].map(item => (
                                <div className="image-card ps-1">
                                    <RViewerJS>
                                        <img src={item.image_raw}
                                            style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                    </RViewerJS>
                                    <div className="text-start mt-4">
                                        <div className="h4 fw-bold mb-1">{item.mpub_title}</div>
                                        <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            }

            {!isMobile && <div className='scroll-no-scrollbar' style={{ maxHeight: '650px', overflowY: 'scroll' }}>
                <div className="column-feed" style={{ width: `${25 - 1}%`, }}>
                    {
                        content[0].map(item => (
                            <div className="image-card py-3">
                                <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS>
                                <div className="text-start mt-4">
                                    <div className="h4 fw-bold mb-1" dangerouslySetInnerHTML={{ __html: item.mpub_title }}></div>
                                    <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="column-feed" style={{ width: `${25 - 1}%` }}>
                    {
                        content[1].map(item => (
                            <div className="image-card p-3">
                                <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS>
                                <div className="text-start mt-4">
                                    <div className="h4 fw-bold mb-1" dangerouslySetInnerHTML={{ __html: item.mpub_title }}></div>
                                    <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="column-feed" style={{ width: `${25 - 1}%` }}>
                    {
                        content[2].map(item => (
                            <div className="image-card p-3">
                                <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS>
                                <div className="text-start mt-4">
                                    <div className="h4 fw-bold mb-1" dangerouslySetInnerHTML={{ __html: item.mpub_title }}></div>
                                    <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="column-feed" style={{ width: `${25 - 1}%` }}>
                    {
                        content[3].map(item => (
                            <div className="image-card py-3">
                                <RViewerJS>
                                    <img src={item.image_raw}
                                        style={{ background: '#B7B7B7', }} className='w-100' alt="...." />
                                </RViewerJS>
                                <div className="text-start mt-4">
                                    <div className="h4 fw-bold mb-1" dangerouslySetInnerHTML={{ __html: item.mpub_title }}></div>
                                    <div className="h5 fw-normal mb-0">{item.mpub_desc}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>}

        </div>
    )
}

export default Element