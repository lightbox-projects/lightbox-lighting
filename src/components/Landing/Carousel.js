import * as React from "react"
import { useRef } from 'react';
import {withoutCredentials} from '../../../static/baseURL'

import { navigate } from "gatsby";

import { Swiper, SwiperSlide } from 'swiper/react';
import { A11y, Navigation } from 'swiper/modules';//
import 'swiper/css';

const Element = () => {

    const swiperRef = useRef();

    const [currentSlide, setCurrentSlide] = React.useState(0)
    const [data, setData] = React.useState([])
    // let data = dawta.filter(item => (item.featured))

    const onClickSlide = e => {
        setCurrentSlide(e.target.dataset['bsSlideTo'])
        swiperRef.current.slideTo(e.target.dataset['bsSlideTo'])
    }

    const seeDetail = (data) => {
        navigate('/projects/'+data.gallery_lbx_slug)
    }

    React.useEffect(() => {
        withoutCredentials().get('api/masterdata/gallery/lightbox-lighting', {
            params: {
                featured: 'y'
            }
        }).then(res => {
            setData(res.data)
        })
    }, [])

    return (
        <div className="bg-tertiary">
            <div id="carouselExampleDark" className="carousel slide" data-bs-touch="false" data-bs-interval="false" >
                <div className="position-absolute w-100 d-flex justify-content-center" style={{ zIndex: 9, bottom: '2em' }}>
                    <Swiper className="w-100"
                        modules={[A11y, Navigation]}
                        spaceBetween={50}
                        slidesPerView={'auto'}
                        centeredSlides
                        onBeforeInit={(swiper) => {
                            swiperRef.current = swiper;
                        }}
                    >
                        {
                            data.map((item, i) => (
                                <SwiperSlide style={{ width: '70px' }} className="d-flex justify-content-center" data-slide-to={i} onClick={onClickSlide} key={item.gallery_lbx_code}>
                                    <button className="text-white fw-bolder h3 mb-0"
                                        data-bs-slide-to={i} data-bs-target="#carouselExampleDark"
                                        style={{ background: 'unset', border: 'unset' }}>
                                        '{item.gallery_lbx_code}
                                    </button>
                                </SwiperSlide>
                            ))
                        }
                    </Swiper>
                </div>
                <div className="carousel-inner">
                    {
                        data.map((item, i) => (
                            <div className={`carousel-item ${currentSlide === i ? 'active' : ''}`} key={item.gallery_lbx_code}>
                                <img src={item.thumbnail} className="d-block w-100 vh-100" alt="..." />
                                <div className="vw-100 vh-100 d-flex flex-column position-absolute" style={{ top: 0, left: 0 }}>
                                    <div className="mx-auto mt-auto text-center">
                                        <h5 className="display-4 fw-bolder text-white">{item.gallery_title}</h5>
                                        <p className="h2 fw-bolder text-white mb-0">{item.gallery_lbx_location}</p>
                                    </div>
                                    <button className="mb-auto mx-auto mt-4 btn text-white fw-bolder" onClick={() => seeDetail(item)} style={{ width: '150px', border: '5px solid white' }}>
                                        SEE DETAIL
                                    </button>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export default Element