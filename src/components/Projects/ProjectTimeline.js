import React from 'react'
// import { Swiper, SwiperSlide } from 'swiper/react';
import { StaticImage } from 'gatsby-plugin-image';
// import { navigate } from 'gatsby'

import ProjectCard from './ProjectCard'
import { composeArrays, filterByThreshold, decideColumnLayout } from './project-helper'
import Header from '../Header'

import { isMobile } from 'react-device-detect';

const Element = ({ projects: rawProjects, toggleDrawer, style, onClickCategory }) => {
    // const categories = [
    //     'HOSPITALITY', 'COMMERCIAL', 'RESIDENTIAL', 'HEALTHCARE', 'INSTITUTION', 'Restaurant & Retail', 'Mix Development', 'OFFICE', 'OTHER',
    // ]

    const [projects, setProjects] = React.useState([])
    const [years, setYears] = React.useState([])
    const [currentYear, setCurrentYear] = React.useState(null)

    React.useEffect(() => {
        let ye = rawProjects.map(o => o['gallery_lbx_year']).filter(function (item, pos, self) {
            return self.indexOf(item) === pos;
        }).sort()

        var index = ye.indexOf(null);
        if (index !== -1) {
            ye.splice(index, 1);
        }

        ye = ye.filter(item => (item.length === 4))

        setYears(ye)
        setCurrentYear(ye[ye.length - 1])
    }, [rawProjects]);

    React.useEffect(() => {
        const promises = rawProjects.filter(item => (item.gallery_lbx_year === currentYear)).map(item => {
            return new Promise((resolve) => {
                const img = new Image();
                img.src = item.thumbnail;

                img.onload = () => {
                    const imageAspectRatio = img.width / img.height;
                    item.columnThreshold = decideColumnLayout(imageAspectRatio);
                    resolve(item);
                };
            });
        });

        Promise.all(promises).then(newArray => {
            const onesArray = filterByThreshold(newArray, 1);
            const twosArray = filterByThreshold(newArray, 2);

            const resultArray = composeArrays(onesArray, twosArray).slice(0, 1)
            const flattenedArray = [].concat(...resultArray);

            setProjects(flattenedArray);
        });
    }, [currentYear, rawProjects])

    return (
        <div style={{ ...style }} className={`${isMobile ? 'p-4' : 'p-5'} position-relative d-flex flex-column`}>
            <Header title={'Our Projects'} customCallback={toggleDrawer} />
            <div className={`${isMobile ? 'h3' : 'h2'} mb-5`} style={{ fontWeight: '200' }}>Timeline</div>

            {/* <div className='mb-2'>
                <Swiper
                    navigation={isMobile ? {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    } : null}
                    spaceBetween={0}
                    slidesPerView={isMobile ? 2 : 5} 
                >
                    {
                        categories.map(item => (
                            <SwiperSlide key={item} className='text-center'>
                                <button onClick={() => onClickCategory(item)}
                                    className={`${isMobile ? '' : 'h3'} mb-0 text-nowrap text-center clickable-button timeline-title-hover`}
                                    style={{ textTransform: 'capitalize', fontWeight: '300' }}>
                                    {item.toLocaleLowerCase()}
                                </button>
                            </SwiperSlide>
                        ))
                    }
                    {isMobile && <div className="swiper-button-next"></div>}
                    {isMobile && <div className="swiper-button-prev"></div>}
                </Swiper>
            </div> */}


            {/* <div className='position-relative' style={{ marginTop: '-1.2em' }}>
                <div className='position-absolute w-100 px-3' style={{ top: '50%' }}>
                    <div style={{ height: '3px', background: 'black', }}></div>
                </div>
                <div className="horizontal-timeline timeline-container">
                    <div className="timeline-event d-flex flex-column align-items-center" style={{ marginTop: '.2em' }}>
                        <div className='rounded-circle bg-dark' style={{ aspectRatio: '1/1', height: '10px' }}></div>
                    </div>
                    <div className="timeline-event d-flex flex-column align-items-center" style={{ marginTop: '.2em' }}>
                        <div className='rounded-circle bg-dark' style={{ aspectRatio: '1/1', height: '10px' }}></div>
                    </div>
                </div>
            </div> */}


            <div className="row mt-4 align-items-center" style={{ gap: isMobile ? '20px' : '80px 0px', height: isMobile ? 'auto' : '500px' }}>
                {(parseInt(currentYear) === 2012 || parseInt(currentYear) === 2016 || parseInt(currentYear) === 2019 || parseInt(currentYear) === 2024) ?
                    <div className='col-lg'>
                        <div className='row align-items-center' style={{ gap: isMobile ? '20px' : '80px 0px', height: isMobile ? 'auto' : '500px' }}>
                            {projects.map(item => (
                                <ProjectCard isTimeline item={item} key={item.gallery_id + '-GALLERY-TIMELINE'} className={'h-100'} />
                            ))}
                        </div>
                    </div>
                    :
                    projects.map(item => (
                        <ProjectCard item={item} key={item.gallery_id + '-GALLERY-TIMELINE'} className={'h-100'} />
                    ))
                }
                {currentYear === 2004 && <div className='col-lg-4'>
                    <div>
                        <div className='text-primary h2-half fw-bolder'>LIGHTBOX LIGHTING</div>
                        <div className='h3 text-primary mt-2 fw-normal'>
                            ESTABLISHED ON 2004 <br />
                            Our Journey Starts Here
                        </div>
                    </div>
                </div>}
                {parseInt(currentYear) === 2012 && <div className='col-lg-4 mb-auto'>
                    <div>
                        <div className='text-primary h2-half fw-bolder' style={{ lineHeight: '1.2em' }}>LIGHTBOX <br /> Anniversary</div>
                        <div className='h3 text-primary mt-2 fw-normal'>
                            Company Trip to <br />
                            Bali, Indonesia
                        </div>
                    </div>
                </div>}
                {parseInt(currentYear) === 2016 && <div className='col-lg-4 mb-auto'>
                    <div>
                        <div className='text-primary h2-half fw-bolder' style={{ lineHeight: '1.2em' }}>LIGHTBOX <br />10th Anniversary</div>
                        <div className='h3 text-primary mt-2 fw-normal'>
                            Charity Trip to <br />
                            Sri Lanka
                        </div>
                    </div>
                </div>}
                {parseInt(currentYear) === 2019 && <div className='col-lg-4 mb-auto'>
                    <div>
                        <div className='text-primary h2-half fw-bolder' style={{ lineHeight: '1.2em' }}>LIGHTBOX <br />15th Anniversary</div>
                        <div className='h3 text-primary mt-2 fw-normal'>
                            Charity Trip to <br />
                            Pay Nyet San <br />
                            Myanmar
                        </div>
                    </div>
                </div>}
                {parseInt(currentYear) === 2024 && <div className='col-lg-4 mb-auto'>
                    <div>
                        <div className='text-primary h2-half fw-bolder' style={{ lineHeight: '1.2em' }}>LIGHTBOX <br />20th Anniversary</div>
                        <div className='h3 text-primary mt-2 fw-normal'>
                            Relaunch Website
                        </div>
                    </div>
                </div>}
            </div>

            <div className='position-relative'>
                <div className='position-absolute w-100 px-4' style={{ top: '50%' }}>
                    <div style={{ height: '3px', background: 'black', }}></div>
                </div>
                <div className="horizontal-timeline timeline-container align-items-center">
                    <button onClick={() => setCurrentYear(2004)}
                        className={`clickable-button timeline-event d-flex flex-column align-items-center active`} style={{ marginTop: '2.5em' }} key={2004}>
                        <div className={`rounded-circle bg-${currentYear === 2004 ? 'tertiary' : 'dark'}`} style={{ aspectRatio: '1/1', height: currentYear === 2004 ? '30px' : '15px' }}></div>
                        <div className={`mt-2 text-${currentYear === 2004 ? 'tertiary' : 'dark'}`}>
                            <div className="event-date">2004</div>
                        </div>
                    </button>
                    {years.map((year, index) => (
                        <button onClick={() => setCurrentYear(year)}
                            className={`clickable-button timeline-event d-flex flex-column align-items-center active`} style={{ marginTop: '2.5em' }} key={index}>
                            <div className={`rounded-circle bg-${currentYear === year ? 'tertiary' : 'dark'}`} style={{ aspectRatio: '1/1', height: currentYear === year ? '30px' : '15px' }}></div>
                            <div className={`mt-2 text-${currentYear === year ? 'tertiary' : 'dark'}`}>
                                <div className="event-date">{year}</div>
                            </div>
                        </button>
                    ))}
                </div>
            </div>


            <div className="position-absolute d-flex flex-column" style={{ bottom: 0, left: isMobile ? '' : '48%', right: isMobile ? '30px' : '' }}>
                <button onClick={() => toggleDrawer()} className="clickable-button mx-auto mt-auto mb-2" style={{ fontSize: '80px' }}>
                    {/* <i className='ti ti-chevron-compact-up'></i> */}
                    {!isMobile && <StaticImage src="../../../static/assets/image 522.png" alt="" className='my-5' style={{ maxWidth: '60px' }} />}
                </button>
            </div>

            <StaticImage src="../../../static/assets/lightbox lighting logo 1.png" alt="" className='mt-5' style={{ maxWidth: '130px' }} />

        </div>
    )
}

export default Element