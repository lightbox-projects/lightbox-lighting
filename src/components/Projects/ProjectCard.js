import React from 'react'
import { navigate } from "gatsby";

const Element = ({ item, className, style, isTimeline }) => {

    const seeDetail = (data) => {
        navigate('/projects/' + data.gallery_lbx_slug)
    }

    return (
        <div className={`${isTimeline ? 'col-lg' : `col-lg-${item.columnThreshold === 1 ? '2' : '4'}`} ${className}`} key={item.gallery_lbx_slug} style={{ ...style, maxWidth: isTimeline ? '500px' : 'unset' }}>
            <button className="position-relative project-card clickable-button w-100" onClick={() => seeDetail(item)} style={{ cursor: 'pointer' }}>
                <img src={item.thumbnail} className="w-100" style={{ objectFit: 'cover', minHeight: '200px', maxHeight: '400px' }} alt="...." />
                <div className="text-start">
                    <div className="h3 fw-normal mb-0">{item.gallery_title}</div>
                    <div className="h5 fw-bolder mb-0">{item.gallery_lbx_location} | {item.gallery_lbx_year}</div>
                </div>
            </button>
        </div>
    )
}

export default Element