const filterByThreshold = (dataArray, threshold) => dataArray.filter(obj => obj.columnThreshold === threshold);

const composeArrays = (array1, array2) => {
    const composedArray = [];
    let i = 0;
    let j = 0;

    while (i < array1.length || j < array2.length) {
        let sum = 0;
        const currentSet = [];

        if (i < array1.length) {
            sum += array1[i].columnThreshold;
            currentSet.push(array1[i]);
            i++;
        }

        if (j < array2.length) {
            sum += array2[j].columnThreshold;
            currentSet.push(array2[j]);
            j++;
        }

        while (i < array1.length && sum + array1[i].columnThreshold <= 6) {
            sum += array1[i].columnThreshold;
            currentSet.push(array1[i]);
            i++;
        }

        while (j < array2.length && sum + array2[j].columnThreshold <= 6) {
            sum += array2[j].columnThreshold;
            currentSet.push(array2[j]);
            j++;
        }

        composedArray.push(currentSet);
    }

    return composedArray;
};

const decideColumnLayout = (aspectRatio) => {
    const aspectRatioThreshold = 1.5;
    return aspectRatio > aspectRatioThreshold ? 2 : 1;
};

module.exports = {
    composeArrays, filterByThreshold, decideColumnLayout
}