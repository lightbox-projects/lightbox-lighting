import React from 'react'
import ProjectCard from './ProjectCard'
import { StaticImage } from 'gatsby-plugin-image';
import Header from '../Header'

import { isMobile } from 'react-device-detect';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper/modules';


const Element = ({ projects, toggleDrawer, style, search, title, isOpen, onClickCategory }) => {

    let count = [1, 2, 3, 4, 5, 6, 7, 8]

    // const categories = [
    //     ['Hospitality', 'Commercial', 'Residential'],
    //     ['Healthcare', 'Institution', 'Restaurant & Retail'],
    //     ['Mix Development', 'Office', 'Other']
    // ]

    const categories = [
        'HOSPITALITY', 'COMMERCIAL', 'RESIDENTIAL', 'HEALTHCARE', 'INSTITUTION', 'Retail & Restaurant', 'Mixed Development', 'OFFICE', 'OTHER',
    ]

    const [state, setState] = React.useState({
        search: ''
    });

    // const [isCategories, setCategories] = React.useState(null)

    // const toggleCategories = (cat) => {
    //     if (cat) setCategories(cat)
    //     else {
    //         if (isCategories) {
    //             setCategories(null)
    //             onClickCategory('')
    //         }
    //         else setCategories('y')
    //     }
    // }

    const resetCategories = () => {
        onClickCategory(null, null)
    }

    const handleChange = (event, id) => {
        setState({ ...state, [id]: event.target.value });
        onClickCategory(null, event.target.value)
    };

    return (
        <div style={{ overflowX: 'hidden', minHeight: '100vh', ...style }} className={`${isMobile ? 'p-4' : 'p-5'} bg-quartenary position-relative d-flex flex-column`}>
            <Header isBackButton={title} resetCategories={resetCategories} title={title ? title : 'Our Projects'} isOpenDefault={isOpen}>
                {!isMobile && <div className={`ms-5 d-flex flex-column ${!(!isMobile) ? 'me-auto' : ''}`}>
                    {/* <button className={`${isCategories ? 'active' : ''} clickable-button text-on-hover-scale text-start`} onClick={() => toggleCategories()}>CATEGORIES</button> */}
                </div>}
                {(!isMobile) && <div className='ms-auto w-100 me-5 d-flex flex-column justify-content-end' style={{ maxWidth: '400px' }}>
                    <input className='search-project' value={!search ? '' : search} placeholder='Search Project' onChange={event => handleChange(event, 'search')} />
                    <hr className='w-100 my-0' />
                    <div className='d-flex flex-wrap justify-content-between' style={{ gap: '10px' }}>
                        <a className="text-on-hover-scale text-end mt-2 text-primary" rel='noreferrer' style={{ textDecoration: 'unset' }} target='_blank'
                            href='https://drive.google.com/drive/folders/1qkRYT2Rgg4aIwGluGh16EJxAcjlN2NLv?usp=sharing' download>Download Our Catalogs</a>
                        <button className="clickable-button text-on-hover-scale text-end mt-2 text-primary" onClick={() => toggleDrawer()}>Timeline</button>
                    </div>
                </div>}
            </Header>


            <div className='mt-5' title='Drag to Scroll'>
                <Swiper
                    modules={[Navigation]}
                    navigation
                    spaceBetween={50}
                    style={{ cursor: 'grab' }}
                    slidesPerView={'auto'}
                >
                    {
                        categories.map(item => (
                            <SwiperSlide key={item} className='text-center mx-auto px-5' style={{ width: 'fit-content' }}>
                                <button onClick={() => onClickCategory(item)}
                                    className={`${isMobile ? '' : 'h3'} ${title ? (title.toLowerCase() === item.toLowerCase() ? 'text-primary fw-bolder' : '') : ''} mb-0 text-nowrap text-center clickable-button timeline-title-hover`}
                                    style={{ textTransform: 'capitalize', fontWeight: '300', width: 'fit-content' }}>
                                    {item.toLowerCase()}
                                </button>
                            </SwiperSlide>
                        ))
                    }
                    {/* <div className="swiper-button-next"></div>
                    <div className="swiper-button-prev"></div> */}
                </Swiper>
            </div>

            <hr />

            {
                (projects.length !== 0 && (title || search)) &&
                <div className="row my-5 align-items-center" style={{ gap: isMobile ? '20px' : '60px 0px', }}>
                    {
                        [].concat(...projects).map(item => (
                            <ProjectCard item={item} key={item.gallery_id + '-GALLERY-ITEM'} />
                        ))
                    }
                </div>
            }
            {
                (projects.length !== 0 && (!title && !search)) &&
                <div className="row my-5 align-items-center" style={{ gap: isMobile ? '20px' : '60px 0px', }}>
                    {
                        [].concat(...projects.filter(item => (item.gallery_lbx_featured === 'y'))).map(item => (
                            <ProjectCard item={item} key={item.gallery_id + '-GALLERY-ITEM'} />
                        ))
                    }
                </div>
            }

            {
                projects.length === 0 && <div className="row my-5 align-items-center" style={{ gap: isMobile ? '20px' : '20px 0px', }}>
                    {count.map(item => (
                        <div className="col-lg-3" key={item}>
                            <div className="skeleton">
                                <div className="skeleton-left">
                                    <div className="square"></div>
                                    <div className="line h17 w40 m10 mt-3"></div>
                                    <div className="line  w75"></div>
                                </div>
                            </div>
                        </div>
                    ))
                    }
                </div>
            }

            <div className="position-absolute d-flex flex-column" style={{ bottom: 0, left: isMobile ? '' : '48%', right: isMobile ? '30px' : '' }}>
                <button onClick={() => toggleDrawer()} className="clickable-button mx-auto my-auto mb-2" style={{ fontSize: '80px' }}>
                    {/* <i className='ti ti-chevron-compact-down'></i> */}
                    {isMobile &&
                        <div className='d-flex align-items-center' style={{ gap: '10px' }}>
                            <div className='mb-0 h3 fw-bolder'>More</div>
                            <StaticImage src="../../../static/assets/image 523.png" alt="" className='my-5' style={{ maxWidth: '30px' }} />
                        </div>
                    }
                    {/* {!isMobile && <StaticImage src="../../../static/assets/image 522.png" alt="" className='my-5' style={{ maxWidth: '70px' }} />} */}
                </button>
            </div>

            <StaticImage src="../../../static/assets/lightbox lighting logo 1.png" alt="" className='mt-auto' style={{ maxWidth: '130px' }} />

        </div>
    )
}

export default Element