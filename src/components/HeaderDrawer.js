import React from "react";
import { isMobile } from 'react-device-detect';
import logo from '../../static/assets/Logo_Big_Banner.png'
import { StaticImage } from 'gatsby-plugin-image';
import { navigate } from 'gatsby'

const Element = ({ isOpen, toggleDrawer }) => {

    const clickNav = (params) => {
        if (toggleDrawer)
            toggleDrawer()
        navigate(params)
    }

    return (
        <div>
            {
                isMobile &&
                <div className='p-4' style={{ display: isOpen ? 'block' : 'none' }}>
                    <div className='d-flex justify-content-between align-items-center'>
                        <img src={logo} className='m-auto' style={{ maxWidth: '200px' }} alt="..." />

                        {toggleDrawer && <button className='clickable-button' onClick={() => toggleDrawer()}>
                            <StaticImage src="../../static/assets/hamburger.png" alt="..." style={{ maxWidth: isMobile ? '50px' : '130px', opacity: '.5' }} />
                        </button>}
                    </div>
                    <hr className='my-4' />
                    <button onClick={() => clickNav('/perspective')} className='clickable-button d-flex align-items-center ' style={{ gap: '20px' }}>
                        <StaticImage src='../../static/assets/image 521.png' alt="..." style={{ objectFit: 'contain', maxWidth: '30px' }} />
                        <div className=' h3 fw-normal mb-0'>Our Perspective</div>
                    </button>
                    <button onClick={() => clickNav('/contacts')} className='clickable-button d-flex align-items-center mt-5' style={{ gap: '20px' }}>
                        <StaticImage src='../../static/assets/image 524.png' alt="..." style={{ objectFit: 'contain', maxWidth: '20px' }} />
                        <div className=' h3 fw-normal mb-0 ms-2'>Our Contacts</div>
                    </button>
                    <button onClick={() => clickNav('/projects')} className='clickable-button d-flex align-items-center mt-5' style={{ gap: '20px' }}>
                        <StaticImage src='../../static/assets/image 523.png' alt="..." style={{ objectFit: 'contain', width: '20px' }} />
                        <div className=' h3 fw-normal mb-0 ms-2'>Our Projects</div>
                    </button>
                    <button className='clickable-button d-flex align-items-center mt-5' onClick={() => clickNav('/services')} style={{ gap: '20px' }}>
                        <StaticImage src='../../static/assets/image 522.png' alt="..." style={{ objectFit: 'contain', maxWidth: '30px' }} />
                        <div className='h3 fw-normal mb-0'>Our Services</div>
                    </button>
                </div>
            }

            {!isMobile &&
                <div className='position-relative vh-100 vw-100' style={{ display: isOpen ? 'block' : 'none' }}>
                    <div className='position-absolute w-100 h-100 d-flex' style={{ top: 0, left: 0 }}>
                        <img src={logo} className='m-auto' placeholder='none' style={{ maxWidth: '700px' }} alt="..." />
                    </div>
                    <div className='position-absolute w-100 d-flex flex-column align-items-center' style={{ top: '40px', }}>
                        <button onClick={() => clickNav('/perspective')} className='clickable-button'>
                            <StaticImage src='../../static/assets/image 521.png' alt="..." placeholder='none' style={{ objectFit: 'contain', maxWidth: '60px' }} />
                            {/* <i className='ti ti-chevron-up' style={{ fontSize: '80px' }}></i> */}
                            <div className=' h2 fw-normal mb-0 mt-3'>Our Perspective</div>
                        </button>
                    </div>
                    <div className='position-absolute h-100 d-flex align-items-center' style={{ left: '40px', }}>
                        <button onClick={() => clickNav('/contacts')} className='clickable-button d-flex align-items-center'>
                            <StaticImage src='../../static/assets/image 524.png' alt="..." placeholder='none' style={{ objectFit: 'contain', maxWidth: '40px' }} />
                            {/* <i className='ti ti-chevron-left' style={{ fontSize: '80px' }}></i> */}
                            <div className=' h2 fw-normal mb-0 ms-3'>Our Contacts</div>
                        </button>
                    </div>
                    <div className='position-absolute h-100 d-flex align-items-center' style={{ right: '40px', }}>
                        <button onClick={() => clickNav('/projects')} className='clickable-button d-flex align-items-center'>
                            <div className=' h2 fw-normal mb-0 me-3'>Our Projects</div>
                            <StaticImage src='../../static/assets/image 523.png' alt="..." placeholder='none' style={{ objectFit: 'contain', maxWidth: '40px' }} />
                            {/* <i className='ti ti-chevron-right' style={{ fontSize: '80px' }}></i> */}
                        </button>
                    </div>
                    <div className='position-absolute w-100 d-flex flex-column align-items-center' style={{ bottom: '40px', }}>
                        <button className='clickable-button' onClick={() => clickNav('/services')}>
                            <div className='h2 fw-normal mb-3'>Our Services</div>
                            <StaticImage src='../../static/assets/image 522.png' alt="..." placeholder='none' style={{ objectFit: 'contain', maxWidth: '60px' }} />
                            {/* <i className='ti ti-chevron-down' style={{ fontSize: '80px' }}></i> */}
                        </button>
                    </div>
                    <div className="position-absolute text-end" style={{ right: '20px', bottom: '20px' }}>
                        <div className="fw-bold h5">Contact Us Now!</div>
                        <div className="d-flex align-items-center justify-content-end" style={{gap:'5px'}}>
                            <a style={{textDecoration: 'unset'}} href="https://www.instagram.com/lightbox.lighting?igsh=MTZsNGNndXZ6NTNlZg%3D%3D" target="_blank"><i style={{fontSize: '35px'}} className="ti ti-brand-instagram text-dark"></i></a>
                            <a style={{textDecoration: 'unset'}} href="https://wa.me/6597411599" target="_blank"><i style={{fontSize: '33px'}} className="ti ti-brand-whatsapp text-dark"></i></a>
                            {/* <a style={{textDecoration: 'unset'}} href="https://t.me/Warinya" target="_blank"><i style={{fontSize: '35px'}} className="ti ti-brand-telegram text-dark"></i></a> */}
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default Element