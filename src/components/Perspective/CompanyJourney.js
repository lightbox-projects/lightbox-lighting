import React from 'react'
// import Header from '../Header'
import { StaticImage } from 'gatsby-plugin-image';
import Timeline from './Timeline';

const Element = () => {
    return (
        <div style={{ overflowX: 'hidden', minHeight: '100vh', background: '#949494' }} className="py-5 position-relative d-flex flex-column">
            <div className='px-5'>
                {/* <Header title={'Company Journey'} /> */}
                <div className="h2" style={{ fontWeight: '200' }}>Company Journey</div>
            </div>


            <div className="my-auto">
                <Timeline />
            </div>

            <div className='px-5'>
                <div className='mt-3 d-flex justify-content-between flex-wrap align-items-center'>
                    <StaticImage src="../../../static/assets/lightbox lighting logo 1.png" alt="" style={{ maxWidth: '130px' }} />
                    <div className="h1 text-primary fw-bolder mb-0" style={{ textTransform: 'capitalize' }}>
                        Our Perspective
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Element