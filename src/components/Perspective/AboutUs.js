import React from 'react'
import Header from '../Header'

import { isMobile } from 'react-device-detect';

const Element = () => {
    return (
        <div style={{ overflowX: 'hidden', minHeight: '100vh', background: '#B7B7B7' }} className={`${isMobile ? 'p-4' : 'p-5'} position-relative d-flex flex-column`}>
            <Header title={'Perspective'} />

            <div className={`${isMobile ? 'my-5' : 'my-auto'}`}>
                <div className="row" style={{ gap: '10px 0' }}>

                    <div className="col-lg my-auto d-flex">
                        <div style={{ maxWidth: '400px' }} className="m-auto">
                            <div className="h2 mb-5 text-tertiary" style={{ fontWeight: 800, letterSpacing: '1px' }}>
                                ABOUT US
                            </div>
                            <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                                Lightbox is a lighting design company that uses light to improve life, enhance space and experience another dimension of architecture. We are always evolving and adopting new lighting design to integrate with architecture. We already do expand to few country to upscale our market reach.
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

                    <div className="col-lg my-auto d-flex">
                        <div style={{ maxWidth: '400px' }} className="m-auto">
                            <div className="h2 mb-5 text-tertiary" style={{ fontWeight: 800, letterSpacing: '1px' }}>
                                VISION
                            </div>
                            <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                                Conveying lighting designs and strategies to create positive impact for the people and environment. We are a trusted lighting design company who always delivers what we imagine into reality.
                            </div>

                            <div className="h2 mb-5 text-tertiary" style={{ fontWeight: 800, letterSpacing: '1px', marginTop: '2em' }}>
                                MISSION
                            </div>
                            <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                                Our Mission is to enhance your project value aesthetically and functionally, with our ideas through lighting in:
                                <br /> <br />
                                1. Creative lighting design thinking <br />
                                2. Optimise lighting applications <br />
                                3. Minimised time and cost <br />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

                    <div className="col-lg my-auto d-flex">
                        <div style={{ maxWidth: '' }} className="m-auto">
                            <div className="h2 mb-5 text-tertiary" style={{ fontWeight: 800, letterSpacing: '1px' }}>
                                VALUES
                            </div>

                            <div className="d-flex flex-column" style={{ gap: '20px 0' }}>
                                <div className={`d-flex ${isMobile ? 'flex-wrap' : ''}`} style={{ gap: '10px' }}>
                                    <div><div style={{ width: '180px' }} className="fw-bolder">Ambiance</div></div>
                                    <div>Creating ambience for all purposes </div>
                                </div>
                                <div className={`d-flex ${isMobile ? 'flex-wrap' : ''}`} style={{ gap: '10px' }}>
                                    <div><div style={{ width: '180px' }} className="fw-bolder">Technology</div></div>
                                    <div>Advancing Lighting Technology </div>
                                </div>
                                <div className={`d-flex ${isMobile ? 'flex-wrap' : ''}`} style={{ gap: '10px' }}>
                                    <div><div style={{ width: '180px' }} className="fw-bolder">Energy</div></div>
                                    <div>Effective Resources and Power </div>
                                </div>
                                <div className={`d-flex ${isMobile ? 'flex-wrap' : ''}`} style={{ gap: '10px' }}>
                                    <div><div style={{ width: '180px' }} className="fw-bolder">Solutions Details</div></div>
                                    <div>Provide Detail Services Approach </div>
                                </div>
                                <div className={`d-flex ${isMobile ? 'flex-wrap' : ''}`} style={{ gap: '10px' }}>
                                    <div><div style={{ width: '180px' }} className="fw-bolder">Efficiency</div></div>
                                    <div>Manage Environment Friendly Energy </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    )
}

export default Element