import React from 'react'
import companiesJourney from "../../../static/content/company_journey"

const Element = () => {

    const [active, setActiveIdx] = React.useState(0)
    const parentElm = React.useRef(null)
    const [parentWidth, setParentWidth] = React.useState(0)

    const isOdd = (number) => {
        if (number % 2 === 0) {
            return false;
        } else {
            return true;
        }
    }

    const setActive = (idx) => {
        setActiveIdx(prev => idx)
        if (idx === 0) {
            parentElm.current.scrollLeft = 0
        } else {
            if (active > idx)
                parentElm.current.scrollLeft -= 120 * Math.abs(active - idx)
            else
                parentElm.current.scrollLeft += 120 * Math.abs(active - idx)
        }
    }

    React.useEffect(() => {
        setParentWidth(parentElm?.current.scrollWidth)

        // let idx = 0
        // const interval = setInterval(() => {
        //     if (idx === companiesJourney.length - 1) {
        //         setActive(0)
        //         idx = 0
        //     }
        //     else {
        //         idx += 1
        //         setActive(idx)
        //     }
        // }, 5000); // 5 seconds

        // return () => clearInterval(interval);

    }, [parentElm])

    return (
        <div ref={parentElm} className='position-relative d-flex ps-5  flex-nowrap timeline-company-scrollbar align-items-center' style={{ gap: '10px 0em', overflowX: 'scroll' }}>
            {/* <div className='position-absolute' style={{ top: 0 }}>{active}</div> */}
            <div className='position-absolute h-100 d-flex flex-column' style={{ width: `${parentWidth - 100}px` }}>
                <div className='w-100 my-auto bg-dark' style={{ height: '2px', marginLeft: '7em' }}></div>
            </div>
            {
                companiesJourney.map((item, i) => (
                    <div className='timeline-company-item'>
                        <div className='d-flex justify-content-center pb-5 text-center' style={{ height: '250px', width: '300px' }}>
                            <div style={{ fontWeight: '300' }} className={`mt-auto ${!isOdd(i) ? '' : 'd-none'} ${active === i ? 'h3 fw-bolder' : 'h4'}`}>
                                <div dangerouslySetInnerHTML={{ __html: item.text }}></div>
                            </div>
                        </div>
                        <button onClick={() => setActive(i)} className='clickable-button d-flex' style={{ width: '300px' }}>
                            <div className={`m-auto bg-${active === i ? 'tertiary' : 'dark'} rounded-circle d-flex position-relative`}
                                style={{ width: active === i ? '120px' : '80px', aspectRatio: '1/1', zIndex: '9' }}>
                                <div className={`m-auto text-white fw-bolder ${active === i ? 'h3' : 'h4'}`}>{item.year}</div>
                            </div>
                        </button>
                        <div style={{ height: '250px', width: '300px' }} className='pt-5 text-center'>
                            <div style={{ fontWeight: '300' }} className={`${isOdd(i) ? '' : 'd-none'} ${active === i ? 'h3 fw-bolder' : 'h4'}`}>
                                <div dangerouslySetInnerHTML={{ __html: item.text }}></div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

export default Element