import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';
import Drawer from 'react-modern-drawer'

import { isMobile } from 'react-device-detect';

import HeaderDrawer from './HeaderDrawer'


const Element = ({ title, customCallback, isOpenDefault, children, resetCategories, isBackButton }) => {
    const [isOpen, setIsOpen] = React.useState(false)

    const toggleDrawer = () => {
        setIsOpen((prevState) => !prevState)
    }

    React.useEffect(() => {
        if (isOpenDefault) {
            toggleDrawer()
        }
    }, [isOpenDefault])

    return (
        <div>
            <div className='d-flex align-items-center'>
                <div className='d-flex flex-column'>
                    {title && <button onClick={() => toggleDrawer()} className={`clickable-button ${isMobile ? 'h3' : 'h1'} ${!children ? 'me-auto' : ''} text-primary fw-bolder mb-0`} style={{ textTransform: 'capitalize' }}>{title.toLowerCase()}</button>}
                    {isBackButton && <button onClick={() => resetCategories()} className={`clickable-button ${isMobile ? 'h4' : 'h3'}`} style={{ fontWeight: '200', width: 'fit-content' }}>
                        Back to Projects
                    </button>}
                </div>
                {children}
                <button className={`clickable-button ${!children ? 'ms-auto' : ''}`} onClick={() => customCallback ? customCallback() : toggleDrawer()}>
                    <StaticImage src="../../static/assets/hamburger.png" alt="" placeholder='none' style={{ maxWidth: isMobile ? '50px' : '130px', opacity: '.5' }} />
                </button>
            </div>
            <Drawer
                open={isOpen}
                onClose={toggleDrawer}
                direction='bottom'
                className={`${isOpen ? isMobile ? '' : "vw-100 vh-100" : ''}`}
                style={{ background: '#E4E3E3', overflowY: 'auto', zIndex: '9999', height: isOpen ? isMobile ? '' : '100vh' : '0' }}
            >
                <HeaderDrawer isOpen={isOpen} toggleDrawer={toggleDrawer} />
            </Drawer>
        </div>
    )
}

export default Element