import React from "react";
import { Link } from 'gatsby'
import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';

const Navbar = ({ isAbsolute, activeMenu }) => {

    let navcolor_based_url = {
        '' : '',
        'projects' : 'tertiary',
        'services' : 'secondary',
        'about-us' : 'secondary',
        'contacts' : 'secondary'
    }
    
    let textcolor_based_url = {
        '' : 'white',
        'projects' : 'white',
        'services' : 'dark',
        'about-us' : 'dark',
        'contacts' : 'dark'
    }

    let rawcolor_based_url = {
        '' : 'white',
        'projects' : 'white',
        'services' : 'black',
        'about-us' : 'black',
        'contacts' : 'black'
    }

    return (
        <nav className={`navbar navbar-expand-lg py-0 ${isMobile ? '' : 'px-5'} ${isAbsolute ? 'w-100 position-absolute' : `bg-${navcolor_based_url[activeMenu]} position-fixed w-100`}`}
            style={{ borderBottom: isAbsolute ? '' : '5px solid black', top: 0, zIndex: 99, minHeight: '70px' }}>
            <div className="container-fluid">
                {!isMobile ? null :
                    <Link className="navbar-brand" to={'/'}>
                        {isMobile ?
                            <StaticImage src="../../static/assets/logo-horizontal.png" alt="" style={{ maxWidth: '130px' }} />
                            :
                            <StaticImage src="../../static/assets/logo.png" alt="" style={{ maxWidth: '70px' }} />
                        }
                    </Link>
                }
                <button className="navbar-toggler" type="button"
                    data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation"
                    style={{ border: 'unset' }}
                >
                    <i className={`ti ti-menu-2`}></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className={`navbar-nav ${isMobile ? 'justify-content-center' : ''}`} style={{ gap: isMobile ? '10px 20px' : '10px 40px', flexDirection: 'unset' }}>
                        <li className="nav-item">
                            <Link className={`nav-link text-${textcolor_based_url[activeMenu]} h5 mb-0`}
                                style={{ borderBottom: activeMenu === '' ? `2px solid ${rawcolor_based_url[activeMenu]}` : '2px solid transparent' }}
                                aria-current="page" to={'/'}>HOME</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={`nav-link text-${textcolor_based_url[activeMenu]} h5 mb-0`}
                                style={{ borderBottom: activeMenu === 'projects' ? `2px solid ${rawcolor_based_url[activeMenu]}` : '2px solid transparent' }}
                                aria-current="page" to={'/projects'}>PROJECTS</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={`nav-link text-${textcolor_based_url[activeMenu]} h5 mb-0`}
                                style={{ borderBottom: activeMenu === 'services' ? `2px solid ${rawcolor_based_url[activeMenu]}` : '2px solid transparent' }}
                                aria-current="page" to={'/services'}>SERVICES</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={`nav-link text-${textcolor_based_url[activeMenu]} h5 mb-0`}
                                style={{ borderBottom: activeMenu === 'about-us' ? `2px solid ${rawcolor_based_url[activeMenu]}` : '2px solid transparent' }}
                                aria-current="page" to={'/about-us'}>COMPANY</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={`nav-link text-${textcolor_based_url[activeMenu]} h5 mb-0`}
                                style={{ borderBottom: activeMenu === 'contacts' ? `2px solid ${rawcolor_based_url[activeMenu]}` : '2px solid transparent' }}
                                aria-current="page" to={'/contacts'}>CONTACTS</Link>
                        </li>
                    </ul>
                </div>
                {isMobile ? null :
                    <Link className="navbar-brand" to={'/'}>
                        <StaticImage className="mt-2" src="../../static/assets/lightbox.png" alt="" style={{ maxWidth: '130px', filter: rawcolor_based_url[activeMenu] === 'white' ? 'contrast(0%) brightness(100)' : '' }} />
                    </Link>
                }
            </div>
        </nav>
    );
}

export default Navbar;