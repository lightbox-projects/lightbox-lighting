import React from "react";
// import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';

import companiesData from "../../static/content/companies_data"

const Footer = (props) => {

    const [state, setState] = React.useState({
        name: '',
        email: '',
        body: '',
        subject: '',
        activeCompany: 'singapore'
    });

    const handleChange = (event, id) => {
        setState({ ...state, [id]: event.target.value });
    };

    const handleChangeCompany = (data) => {
        setState({ ...state, activeCompany: data })
    }

    function handleContact() {
        window.open(`mailto:${companiesData[state.activeCompany].email}?subject=${state.subject}&body=name:${state.name}, email:${state.email} | ${state.body}`)
    }

    return (
        <div className="">
            <div className="bg-white py-5">
                <div className="container-lg py-3">

                    <div className={`d-flex align-items-center mb-5 ${isMobile ? 'justify-content-center' : ''}`} style={{ gap: '10px' }}>
                        <h1 className="fw-bolder display-4 mb-0">Contacts</h1>
                    </div>
                    <div className="row pt-4" style={{ gap: '10px 0' }}>
                        <div className="col-lg-3">
                            <div className="h-100 d-flex flex-column justify-content-between" style={{ gap: '10px' }}>
                                <button className={`fw-${state.activeCompany === 'singapore' ? 'bolder' : 'normal'} h2 mb-0`}
                                    onClick={() => handleChangeCompany('singapore')}
                                    style={{ border: 'unset', cursor: 'pointer', background: 'unset', textAlign: 'left' }}>
                                    <div style={{ border: 'unset', borderBottom: state.activeCompany === 'singapore' ? '2px solid black' : 'unset', width: 'fit-content' }}>
                                        SINGAPORE
                                    </div>
                                </button>
                                <button className={`fw-${state.activeCompany === 'vietnam' ? 'bolder' : 'normal'} h2 mb-0`}
                                    onClick={() => handleChangeCompany('vietnam')}
                                    style={{ cursor: 'pointer', background: 'unset', border: 'unset', textAlign: 'left' }}>
                                    <div style={{ border: 'unset', borderBottom: state.activeCompany === 'vietnam' ? '2px solid black' : 'unset', width: 'fit-content' }}>
                                        VIETNAM
                                    </div>
                                </button>
                                <button className={`fw-${state.activeCompany === 'thailand' ? 'bolder' : 'normal'} h2 mb-0`}
                                    onClick={() => handleChangeCompany('thailand')}
                                    style={{ cursor: 'pointer', background: 'unset', border: 'unset', textAlign: 'left' }}>
                                    <div style={{ border: 'unset', borderBottom: state.activeCompany === 'thailand' ? '2px solid black' : 'unset', width: 'fit-content' }}>
                                        THAILAND
                                    </div>
                                </button>
                                <button className={`fw-${state.activeCompany === 'philippines' ? 'bolder' : 'normal'} h2 mb-0`}
                                    onClick={() => handleChangeCompany('philippines')}
                                    style={{ cursor: 'pointer', background: 'unset', border: 'unset', textAlign: 'left' }}>
                                    <div style={{ border: 'unset', borderBottom: state.activeCompany === 'philippines' ? '2px solid black' : 'unset', width: 'fit-content' }}>
                                        PHILIPPINES
                                    </div>
                                </button>
                                <button className={`fw-${state.activeCompany === 'indonesia' ? 'bolder' : 'normal'} h2 mb-0`}
                                    onClick={() => handleChangeCompany('indonesia')}
                                    style={{ cursor: 'pointer', background: 'unset', border: 'unset', textAlign: 'left' }}>
                                    <div style={{ border: 'unset', borderBottom: state.activeCompany === 'indonesia' ? '2px solid black' : 'unset', width: 'fit-content' }}>
                                        INDONESIA
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div className="col-lg my-auto">
                            <div className="d-flex flex-column" style={{ gap: '30px' }}>

                                <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                    <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                                        <div className="rounded-circle d-flex bg-dark" style={{ width: '40px', height: '40px' }}>
                                            <i className="ti ti-mail-filled m-auto text-white" style={{ fontSize: '26px' }}></i>
                                        </div>
                                        <div className="fw-bolder mb-0 h4">Email</div>
                                    </div>
                                    <h1 className="h5 fw-normal mb-0">{companiesData[state.activeCompany].email}</h1>
                                </div>

                                <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                    <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                                        <div className="rounded-circle d-flex bg-dark" style={{ width: '40px', height: '40px' }}>
                                            <i className="ti ti-phone-filled m-auto text-white" style={{ fontSize: '26px' }}></i>
                                        </div>
                                        <div className="fw-bolder mb-0 h4">Phone Number</div>
                                    </div>
                                    <h1 className="h5 fw-normal mb-0">{companiesData[state.activeCompany].phone}</h1>
                                </div>

                                <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                    <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                                        <div className="rounded-circle d-flex bg-dark" style={{ width: '40px', height: '40px' }}>
                                            <i className="ti ti-map-pin-filled m-auto text-white" style={{ fontSize: '26px' }}></i>
                                        </div>
                                        <div className="fw-bolder mb-0 h4">Office Address</div>
                                    </div>
                                    <h1 className="h5 fw-normal mb-0">{companiesData[state.activeCompany].address}</h1>
                                </div>

                            </div>
                        </div>
                        <div className={`col-lg-4 ${isMobile ? '' : 'px-5'}`}>
                            <h2 className="fw-normal mb-0 text-center">SEND US A MESSAGE!</h2>

                            <form id="form-footer" className="mt-4">
                                <div className="row" style={{ gap: '15px 0' }}>
                                    <div className="col-lg-12">
                                        <label className="mb-2 h6 fw-normal" htmlFor="nameInput">NAME</label>
                                        <input className="form-control bg-secondary fw-bolder" value={state.name} onChange={event => handleChange(event, 'name')} id="nameInput" placeholder="Name" required="" />
                                    </div>
                                    <div className="col-lg-12">
                                        <label className="mb-2 h6 fw-normal" htmlFor="emailInput">EMAIL ADDRESS</label>
                                        <input className="form-control bg-secondary fw-bolder" value={state.email} onChange={event => handleChange(event, 'email')} id="emailInput" placeholder="Email Address" required="" />
                                    </div>
                                    <div className="col-lg-12">
                                        <label className="mb-2 h6 fw-normal" htmlFor="subjectInput">SUBJECT</label>
                                        <input className="form-control bg-secondary fw-bolder" value={state.subject} onChange={event => handleChange(event, 'subject')} id="subjectInput" placeholder="Subject" required="" />
                                    </div>
                                    <div className="col-lg-12">
                                        <label className="mb-2 h6 fw-normal" htmlFor="messageInput">MESSAGE</label>
                                        <textarea className="form-control bg-secondary fw-bolder" value={state.body} onChange={event => handleChange(event, 'body')}
                                            id="messageInput" required=""
                                            placeholder="Ask your question and send it right away to our inbox" rows="3"></textarea>
                                    </div>
                                </div>
                            </form>
                            <div className="d-flex mt-3">
                                <button style={{ gap: '10px' }} onClick={() => handleContact()} className="btn btn-primary px-5 d-flex align-items-center mt-1 ms-auto waves-effect waves-float waves-light">
                                    Send <i className="ti ti-send"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    {/* <div className="d-flex justify-content-center mt-5">
                        <StaticImage src="../../static/assets/lightbox.png" style={{ width: '200px' }} alt="convertible type" />
                    </div> */}

                    <div className="d-flex mt-5 px-4">
                        <div className="ms-auto">
                            <div className="fw-bolder">
                                FIND US ON :
                            </div>
                            <div className="d-flex mt-1" style={{ gap: '10px' }}>
                                <i className="ti ti-brand-instagram" style={{ fontSize: '40px' }}></i>
                                <i className="ti ti-brand-facebook" style={{ fontSize: '40px' }}></i>
                                <i className="ti ti-brand-linkedin" style={{ fontSize: '40px' }}></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            {/* <div className="w-100 d-flex align-items-center flex-wrap justify-content-between container-fluid py-3" style={{ gap: '10px' }}>
                <div className="h6 mb-0" style={{ fontSize: '14px' }}>
                    Copyright {new Date().getFullYear()} by Lightbox Lighting. All Rights Reserved.
                </div>
                <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                    <div className="d-flex bg-primary rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-facebook" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-google" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-linkedin" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-twitter" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-youtube" style={{ fontSize: '18px' }}></i>
                    </div>
                </div>
            </div> */}
        </div>
    );
}

export default Footer