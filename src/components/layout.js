import * as React from "react"
// import { Link } from "gatsby"

// import Navbar from "./Navbar"
// import Footer from "./Footer"


const Layout = ({ location, title, children, noFooter, absoluteNav }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  let isRootPath = location.pathname === rootPath

  // let splited_pathname = location.pathname.split('/')
  // if(['id', 'th'].includes() && splited_pathname[2] === ""){
  //   isRootPath = true
  // }

  return (
    <div className={`global-wrapper`} data-is-root-path={isRootPath}>
      {/* <header className="global-header" id="navbar">
        <Navbar isAbsolute={absoluteNav} activeMenu={splited_pathname[1]} />
      </header> */}
      {/* <div className={`${absoluteNav ? '' : 'pt-5'}`}> */}
        <main>{children}</main>
      {/* </div> */}
      {/* {
        !noFooter &&
        <footer>
          <Footer />
        </footer>
      } */}
    </div>
  )
}

export default Layout
