import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';
import ScrollContainer from 'react-indiana-drag-scroll'

const Element = ({ step, setStep }) => {
    return (
        <div className="row" style={{ gap: '10px 0' }}>
            <div className="col-lg-5 my-auto" style={{ maxWidth: '650px' }}>
                <div className="d-flex flex-column">
                    <div className="my-auto">
                        <div className="h2 mb-3" style={{ fontWeight: 400, letterSpacing: '1px' }}>
                            Lighting Master Plan
                        </div>
                        <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                            Lighting master plan is the testament of our desire to create relevant identity of environment that encourage human interaction at night. Lighting Master Plan is the lighting planning for people to community to urban environment of which can be fitted four various societies, cultures, and generations. Because human always evolve, so our perception does so the same.
                        </div>
                        <div className='mt-3 d-flex justify-content-between'>
                            <button onClick={() => setStep(step - 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                <i className='ti ti-chevron-left'></i>
                                Previous
                            </button>
                            <button onClick={() => setStep(step + 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                Next
                                <i className='ti ti-chevron-right'></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg">
                <ScrollContainer style={{ height: '80vh' }}>
                    <div className='row mx-3' style={{ gap: '20px 0' }} title='Drag on Scroll'>
                        <div className='col-lg-7 mt-auto'>
                            <div>
                                <StaticImage src='../../../static/assets/services/masterplan/1.png'
                                    className='flex-fill' alt='....' style={{ maxHeight: '350px' }} />
                            </div>
                        </div>
                        <div className='col-lg-5'>
                            <div>
                                <StaticImage src='../../../static/assets/services/masterplan/3.png' style={{}} className='' alt='....' />
                            </div>
                        </div>
                        <div className='col-lg-7'>
                            <div>
                                <StaticImage src='../../../static/assets/services/masterplan/Screen Shot 2024-04-16 at 11.04.26.png'
                                    className='flex-fill' alt='....' style={{ maxHeight: '350px' }} />
                            </div>
                        </div>
                        <div className='col-lg-5'>
                            <div>
                                <StaticImage src='../../../static/assets/services/masterplan/2.png' style={{ maxHeight: '400px' }} className='' alt='....' />
                            </div>
                        </div>
                    </div>
                </ScrollContainer>
            </div>
        </div>
    )
}

export default Element