import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';
import ScrollContainer from 'react-indiana-drag-scroll'

const Element = ({ setStep, step }) => {
    return (
        <div className="row" style={{ gap: '10px 0' }}>
            <div className="col-lg-5 pt-5" style={{ maxWidth: '650px' }}>
                <div className="d-flex flex-column service-nav">
                    <div>
                        <button className="h2 clickable-button text-start" onClick={() => setStep(1)}>
                            Lighting Design Process
                        </button>
                        <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                    </div>
                    <div>
                        <button className="h2 clickable-button text-start" onClick={() => setStep(2)}>
                            Lighting Master Plan
                        </button>
                        <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                    </div>
                    <div>
                        <button className="h2 clickable-button text-start" onClick={() => setStep(3)}>
                            Lighting Strategy Design for Retail
                        </button>
                        <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                    </div>
                    <div>
                        <button className="h2 clickable-button text-start" onClick={() => setStep(4)}>
                            Light Fixture Product Design
                        </button>
                        <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                    </div>
                    <div>
                        <button className="h2 clickable-button text-start" onClick={() => setStep(5)}>
                            Decorative Lighting Fixture Design
                        </button>
                        <div className='bg-dark' style={{ height: '2px', width: '100px', }}></div>
                    </div>
                </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg">
                <ScrollContainer style={{ height: '80vh' }}>
                    <div className='row mx-3' style={{ gap: '25px 0', cursor: 'grab' }} title='Drag to Scroll'>
                        <div className='col-lg-4'>
                            <StaticImage src='../../../static/assets/services/1 112.png' alt='....' className='w-100 h-100' style={{ }} />
                        </div>
                        <div className='col-lg-4'>
                            <StaticImage src='../../../static/assets/services/1 (2).png' alt='....' className='w-100 h-100' style={{ }} />
                        </div>
                        <div className='col-lg-4'>
                            <StaticImage src='../../../static/assets/services/photo_6120479459845978759_y.jpg' className='w-100 h-100' alt='....' style={{  }} />
                        </div>
                        <div className='col-lg-5'>
                            <StaticImage src='../../../static/assets/services/WhatsApp Image 2024-04-16 at 4.47.17 PM.jpeg' alt='....' className='w-100 h-100' style={{  }} />
                        </div>
                        <div className='col-lg-7'>
                            <StaticImage src='../../../static/assets/services/land-1.jpeg' style={{ maxHeight: '450px' }} className='w-100 h-100' alt='....' />
                        </div>
                    </div>
                </ScrollContainer>
            </div>
        </div>
    )
}

export default Element