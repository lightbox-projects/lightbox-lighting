import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';
import ScrollContainer from 'react-indiana-drag-scroll'

const Element = ({ step, setStep }) => {
    return (
        <div className="row" style={{ gap: '10px 0' }}>
            <div className="col-lg-5 my-auto" style={{ maxWidth: '650px' }}>
                <div className="d-flex flex-column">
                    <div className="my-auto">
                        <div className="h2 mb-3" style={{ fontWeight: 400, letterSpacing: '1px' }}>
                            Light Fixture Product Design
                        </div>
                        <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                            Products design is the combination of function, innovation, form and technology in one single object to serve human needs. Our lighting product design service derives from years of collective experience in lighting industry. We are not only design the product but ensure the needs of those products in the market. Good products shall perform well in both technology and economical aspects whether for commercial, residential or industrial realm.
                        </div>
                        <div className='mt-3 d-flex justify-content-between'>
                            <button onClick={() => setStep(step - 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                <i className='ti ti-chevron-left'></i>
                                Previous
                            </button>
                            <button onClick={() => setStep(step + 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                Next
                                <i className='ti ti-chevron-right'></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg">
                <ScrollContainer style={{ height: '80vh' }}>
                    <div className='row' style={{ gap: '10px 0' }} title='Drag on Scroll'>
                        {/* <div className='col-lg-12'>
                            <div>
                                <StaticImage src='../../../static/assets/services/Pt_1_page-0001_copy_4.jpg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                            </div>
                        </div> */}
                        <div className='col-lg-12'>
                            <StaticImage src='../../../static/assets/services/1.png' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                        </div>
                    </div>
                </ScrollContainer>
            </div>
        </div>
    )
}

export default Element