import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';

const Element = ({ step, setStep }) => {
    return (
        <div className="row" style={{ gap: '10px 0' }}>
            <div className="col-lg-5 my-auto" style={{ maxWidth: '650px' }}>
                <div className="d-flex flex-column">
                    <div className="my-auto">
                        <div className="h2 mb-3" style={{ fontWeight: 400, letterSpacing: '1px' }}>
                            Lighting Strategy Design for Retail
                        </div>
                        <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                            Retail lighting is the business of visual attraction and experience. By design of retail lighting is the key to differentiate brands. Visual focal points designed to not only accentuate merchandise but also to create attractive atmosphere. Our lighting design to strategically plan priority of space besides strategy of visualization merchandise. Our design approach in energy efficiency solution using smart retail control to minimise cost and optimised profits to each store.
                        </div>
                        <div className='mt-3 d-flex justify-content-between'>
                            <button onClick={() => setStep(step - 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                <i className='ti ti-chevron-left'></i>
                                Previous
                            </button>
                            <button onClick={() => setStep(step + 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                Next
                                <i className='ti ti-chevron-right'></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg">
                <div className='row mx-3' style={{ gap: '25px 0' }}>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/Retail_Lighting/1.jpeg' className='w-100' alt='....' style={{ height: '450px' }} />
                    </div>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/Retail_Lighting/2.jpeg' className='w-100' alt='....' style={{ height: '450px' }} />
                    </div>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/Retail_Lighting/3.jpeg' alt='....' className='w-100' style={{ height: '450px' }} />
                    </div>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/Retail_Lighting/4.jpeg' className='w-100' alt='....' />
                    </div>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/Retail_Lighting/5.png' alt='....' className='w-100' />
                    </div>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/Retail_Lighting/6.jpeg' alt='....' className='w-100' />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Element