import React from 'react'

import anime from 'animejs'

const Element = ({ data }) => {

    const [state, setState] = React.useState(false)
    const compRef = React.useRef()

    const toggleContent = () => {
        setState(!state)

        if (!state)
            anime({
                targets: compRef.current,
                keyframes: [
                    { height: '0px', opacity: '0', top: '-350px', marginTop: '0' },
                    { height: '100%', opacity: '1', top: '0', marginTop: '7em' },
                ],
                easing: 'easeInOutSine',
                duration: 1000
            });
        else
            anime({
                targets: compRef.current,
                keyframes: [
                    { height: '100%', opacity: '1', top: '0', marginTop: '7em' },
                    { height: '0px', opacity: '0', top: '-350px', marginTop: '0' },
                ],
                easing: 'easeInOutSine',
                duration: 1000
            });
    }

    return (
        <div className='py-5'>
            <div className='row position-relative' style={{ gap: '10px 30px', zIndex: 9 }}>
                <div className='col-lg'>
                    <div className='d-flex flex-column h-100 py-5'>
                        <div className="h1 fw-normal mb-5" style={{ textTransform: 'uppercase', }}>
                            {data.title}
                        </div>

                        <div className="h5 fw-normal" style={{ lineHeight: '1.5em', textTransform: 'uppercase', textAlign: 'justify', maxWidth: '600px' }}>
                            {data.text}
                        </div>

                        <div className='mt-auto ms-auto' >
                            <button className='clickable-button' onClick={() => toggleContent()}>
                                {!state &&
                                    <i className='ti ti-plus' style={{ fontSize: '80px' }}></i>
                                }
                                {state &&
                                    <i className='ti ti-minus' style={{ fontSize: '80px' }}></i>
                                }
                            </button>
                        </div>
                    </div>
                </div>
                <div className='col-lg-5'>
                    <div className="" style={{ boxShadow: '-15px 15px 0 rgba(180, 169, 145, .3)' }}>
                        <img
                            src={data.image}
                            style={{ width: '100%', aspectRatio: '1/1', objectFit: 'cover', maxHeight: '400px' }} alt="..." />
                    </div>
                </div>
            </div>

            <div className='position-relative' style={{ marginTop: '0em', height: '0', opacity: '0', textTransform: 'uppercase', }} ref={compRef}>
                <div className="h1 text-center fw-normal mb-5">
                    {data.title}
                </div>

                <div className="h4 fw-normal" style={{ lineHeight: '1.5em', textTransform: 'uppercase', textAlign: 'justify' }}>
                    {data.detail_text}
                </div>
            </div>
        </div>
    )
}

export default Element