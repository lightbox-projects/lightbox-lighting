import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';
import ScrollContainer from 'react-indiana-drag-scroll'

const Element = ({ step, setStep }) => {
    return (
        <div className="row" style={{ gap: '10px 0' }}>
            <div className="col-lg-5 my-auto" style={{ maxWidth: '650px' }}>
                <div className="d-flex flex-column">
                    <div className="my-auto">
                        <div className="h2 mb-3" style={{ fontWeight: 400, letterSpacing: '1px' }}>
                            Lighting Design Process
                        </div>
                        <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                            Lighting Design starts with ideas to conceptualize the lighting and architecture which will be applied into the building form and space.
                            Lighting Rendering is the way how lighting to be visualised on the building or within spaces. Lighting calculation to simulate the lighting level according to the function of space Lighting Detailing indicate the placement of light fitting for optimal outcome with the minimum interference to architecture, Product Specification is the process where the right product being apply  to the space to achieve those design intent.achieved Mockup as for making sure everything is installed correctly and the desired outcome are achieve in the smaller area before apply to the main spaces. and Site Commisioning is the last process where lighting  adjusted, aimed and ensure the full ambience & functionality are finally achieved.
                        </div>
                        <div className='mt-3 d-flex justify-content-between'>
                            <button onClick={() => setStep(step - 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                <i className='ti ti-chevron-left'></i>
                                Menu
                            </button>
                            <button onClick={() => setStep(step + 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                Next
                                <i className='ti ti-chevron-right'></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg">
                <ScrollContainer style={{ height: '80vh' }}>
                    <div className='d-flex flex-column' style={{ gap: '30px', }} title='Drag to Scroll'>
                        <div>
                            <div className='fw-bolder h3'>1. Ideation Sketches</div>
                            <StaticImage src='../../../static/assets/services/design-process/Ideation & Skecthes.png' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                        </div>
                        <div>
                            <div className='fw-bolder h3'>2. Concept Rendering</div>
                            <div className='d-flex' style={{ gap: '10px' }}>
                                <StaticImage src='../../../static/assets/services/design-process/2. render/image5.jpeg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/2. render/image41.jpeg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/ren-1.jpg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/ren-2.jpg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                            </div>
                        </div>
                        <div>
                            <div className='fw-bolder h3'>3. Lighting Calculation</div>
                            <div className='d-flex flex-wrap' style={{ gap: '10px' }}>
                                <StaticImage src='../../../static/assets/services/design-process/3. calculation/1.jpg' style={{ height: 'fit-content', maxWidth: '450px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/3. calculation/2.png' style={{ height: 'fit-content', maxWidth: '550px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/3. calculation/3.jpeg' style={{ height: 'fit-content', maxWidth: '450px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/3. calculation/4.jpeg' style={{ height: 'fit-content', maxWidth: '450px' }} alt='....' />
                            </div>
                        </div>
                        <div>
                            <div className='fw-bolder h3'>4. Lighting Detail</div>
                            <div className='d-flex flex-wrap' style={{ gap: '10px' }}>
                                <StaticImage src='../../../static/assets/services/design-process/4. lighting detail/0.png' class='mt-auto' style={{ height: 'fit-content', maxWidth: '300px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/4. lighting detail/1.png' class='mt-auto' style={{ height: '250px', width: '350px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/4. lighting detail/2.png' class='mt-auto' style={{ height: 'fit-content', maxWidth: '300px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/4. lighting detail/3.png' style={{ height: 'fit-content', maxWidth: '300px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/4. lighting detail/4.png' style={{ height: 'fit-content', maxWidth: '300px' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/4. lighting detail/5.png' style={{ height: 'fit-content', maxWidth: '300px' }} alt='....' />
                            </div>
                        </div>
                        <div>
                            <div className='fw-bolder h3'>5. Product Specification</div>
                            {/* <StaticImage src='../../../static/assets/services/design-process/Product Specifcation.png' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' /> */}
                            <div className='d-flex flex-wrap' style={{ gap: '10px' }}>
                                <StaticImage src='../../../static/assets/services/design-process/product-specification/1.jpeg' class='mt-auto' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/product-specification/2.jpeg' class='mt-auto' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/product-specification/3.jpeg' class='mt-auto' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/product-specification/5.png' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/product-specification/6.png' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/product-specification/7.png' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                            </div>
                        </div>
                        <div>
                            <div className='fw-bolder h3'>6. Mockup and Site commissioning</div>
                            <div className='d-flex' style={{ gap: '10px' }}>
                                <StaticImage src='../../../static/assets/services/design-process/6. mockup/image65.jpeg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/6. mockup/image67.jpeg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/6. mockup/image68.jpeg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                                <StaticImage src='../../../static/assets/services/design-process/6. mockup/image69.jpeg' style={{ height: 'fit-content', width: 'fit-content' }} alt='....' />
                            </div>
                        </div>
                    </div>
                </ScrollContainer>
            </div>
        </div >
    )
}

export default Element