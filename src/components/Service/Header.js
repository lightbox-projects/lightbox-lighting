import React from 'react'


const Element = ({ services }) => {
    return (
        <div className='bg-secondary py-5'>
            <div className='container'>
                <div className='fw-bolder h1'>LIGHTING DESIGN SERVICES</div>
                <div className='ms-4 h5 mb-0 fw-normal'>WE BUILD OUR REPUTATION BY WORKING WITH SEVERAL COMPANY AROUND COUNTRY</div>

                <div className='mt-5'>
                    <div className="d-flex py-3 " style={{ gap: '10px 1em', overflowX: 'auto' }}>
                        {
                            services.map((item) => (
                                <a href={`/services/#${item.slug}`} className="position-relative" style={{ cursor: 'pointer' }} key={item.slug}>
                                    <div style={{ width: '230px', aspectRatio: '1/1', background: '#B4A991', marginTop: '15px', marginRight: '15px', opacity: .3 }}></div>
                                    <div className="position-absolute" style={{ top: 0, right: 0 }}>
                                        <img
                                            src={item.image}
                                            style={{ width: '230px', aspectRatio: '1/1', objectFit: 'cover' }} alt="..." />
                                    </div>
                                    <div className="p-2 mb-0 fw-bolder position-absolute text-white w-100" style={{ background: '#747474', bottom: 30, fontSize: '14px', left: 0, textTransform: 'uppercase' }}>
                                        {item.title}
                                    </div>
                                </a>
                            ))
                        }
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Element