import React from 'react'
import { StaticImage } from 'gatsby-plugin-image';

const Element = ({ step, setStep }) => {
    return (
        <div className="row" style={{ gap: '10px 0' }}>
            <div className="col-lg-5 my-auto" style={{ maxWidth: '650px' }}>
                <div className="d-flex flex-column">
                    <div className="my-auto">
                        <div className="h2 mb-3" style={{ fontWeight: 400, letterSpacing: '1px' }}>
                            Decorative Lighting Fixture Design
                        </div>
                        <div className="ql-editor p-0 h4" style={{ letterSpacing: '1.5px', lineHeight: '20px', textAlign: 'justify' }}>
                            Decorative light fixture is the form if expression through exceptional craftmanships and technical skill of utilizing materials. Beyond the visual feast that decorative lighting can offer to architectural spaces, it express the engineering methods that could become expression of space it dwell.
                        </div>
                        <div className='mt-3 d-flex justify-content-between'>
                            <button onClick={() => setStep(step - 1)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                <i className='ti ti-chevron-left'></i>
                                Previous
                            </button>
                            <button onClick={() => setStep(0)} className='clickable-button d-flex align-items-center fw-bolder  h4 mb-0' style={{ gap: '5px' }}>
                                Menu
                                <i className='ti ti-chevron-right'></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='col-lg-auto'><div className='bg-dark h-100' style={{ width: '2px' }}></div></div>

            <div className="col-lg">
                <div className='row mx-3' style={{ gap: '25px 0' }}>
                    <div className='col-lg-8'>
                        <div className='row' style={{ gap: '25px 0' }}>
                            <div className='col-lg-6'>
                                <div style={{ height: '350px' }}>
                                    <StaticImage src='../../../static/assets/services/decorative/1.png' className='w-100 h-100' alt='....' />
                                </div>
                            </div>
                            <div className='col-lg-6'>
                                <div style={{ height: '350px' }}>
                                    <StaticImage src='../../../static/assets/services/decorative/2.png' className='w-100 h-100' alt='....' />
                                </div>
                            </div>
                            <div className='col-lg-6'>
                                <div style={{ height: '350px' }}>
                                    <StaticImage src='../../../static/assets/services/decorative/3.png' className='w-100 h-100' alt='....' />
                                </div>
                            </div>
                            <div className='col-lg-6'>
                                <div style={{ height: '350px' }}>
                                    <StaticImage src='../../../static/assets/services/decorative/4.png' className='w-100 h-100' alt='....' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-4'>
                        <StaticImage src='../../../static/assets/services/decorative/5.png' className='w-100 h-100' alt='....' />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Element