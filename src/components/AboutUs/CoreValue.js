import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"
import { useRef } from 'react';

import { Swiper, SwiperSlide } from 'swiper/react';
import { A11y, Navigation } from 'swiper/modules';//
// Import Swiper styles
import 'swiper/css';

import companiesJourney from "../../../static/content/company_journey"

const Element = () => {
    const swiperRef = useRef();

    return (
        <div className="" style={{ background: '#E1E1E1', padding: '5em 0' }}>

            <div className="container">
                <div className=" display-3 fw-bolder mb-5 pb-5">CORE VALUE</div>

                <div className="d-flex flex-wrap justify-content-between pb-5" style={{ gap: '10px' }}>
                    <div className="text-center">
                        <div className=" fw-bolder h3">AMBIENCE</div>
                        <StaticImage alt="image" src="../../../static/assets/2.png" className="my-3" style={{ width: '150px', }} />
                    </div>
                    <div className="text-center">
                        <div className=" fw-bolder h3">EFFICIENCY</div>
                        <StaticImage alt="image" src="../../../static/assets/3.png" className="my-3" style={{ width: '150px', }} />
                    </div>
                    <div className="text-center">
                        <div className=" fw-bolder h3">TECHNOLOGY</div>
                        <StaticImage alt="image" src="../../../static/assets/4.png" className="my-3" style={{ width: '150px', }} />
                    </div>
                    <div className="text-center">
                        <div className=" fw-bolder h3">ENERGY</div>
                        <StaticImage alt="image" src="../../../static/assets/5.png" className="my-3" style={{ width: '150px', }} />
                    </div>
                    <div className="text-center">
                        <div className=" fw-bolder h3">SOLUTION DETAILS</div>
                        <StaticImage alt="image" src="../../../static/assets/6.png" className="my-3" style={{ width: '150px', }} />
                    </div>
                </div>

                <div className="display-3 fw-bolder mb-5 mt-5">COMPANY JOURNEY</div>
            </div>

            <div className="pt-5">
                <Swiper
                    modules={[A11y, Navigation]}
                    spaceBetween={50}
                    slidesPerView={'auto'}
                    onBeforeInit={(swiper) => {
                        swiperRef.current = swiper;
                    }}
                >
                    {companiesJourney.map(item => (
                        <SwiperSlide style={{ width: '250px' }} className="px-3" key={item.year}>
                            <div className="text-center" style={{ width: '250px' }}>
                                <h5 className=" fw-bolder h1">{item.year}</h5>
                                <div className="">
                                    {item.text}
                                </div>
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>
                <div className="d-flex justify-content-between px-3">
                    <button onClick={() => swiperRef.current?.slidePrev()}
                        style={{ background: 'unset', border: 'unset', }}
                    >
                        <StaticImage alt="image" src="../../../static/assets/arrow.png" className="my-3" style={{ width: '150px', transform: 'rotate(180deg)', filter: 'contrast(100%) brightness(0)' }} />
                    </button>
                    <button onClick={() => swiperRef.current?.slideNext()}
                        style={{ background: 'unset', border: 'unset', }}
                    >
                        <StaticImage alt="image" src="../../../static/assets/arrow.png" className="my-3" style={{ width: '150px', filter: 'contrast(100%) brightness(0)' }} />
                    </button>
                </div>
            </div>

        </div>
    )
}

export default Element