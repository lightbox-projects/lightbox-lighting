import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

const Element = () => {
    return (
        <div>
            <div className="bg-secondary" style={{overflowX: 'hidden'}}>
                <div className="row align-items-center" style={{ gap: '10px 0' }}>
                    <div className="col-lg">
                        <div className="container py-5">
                            <div className="d-flex">
                                <div className="m-auto" style={{ maxWidth: '700px' }}>
                                    <div className="text-white display-3 fw-bolder mb-5">
                                        ABOUT US
                                    </div>

                                    <div className=" h4 fw-normal" style={{ lineHeight: '1.5em' }}>
                                        LIGHTBOX IS A LIGHTING DESIGN COMPANY THAT USES LIGHT TO IMPROVE LIFE, ENHANCE SPACE AND EXPERIENCE ANOTHER DIMENSION OF ARCHITECTURE. WE ARE ALWAYS EVOLVING AND ADOPTING NEW LIGHTING DESIGN TO INTEGRATE WITH ARCHITECTURE.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <StaticImage src="../../../static/assets/image 462.jpg" className="w-100 h-100" alt="convertible type" />
                    </div>
                </div>
            </div>
            <div className="bg-secondary">
                <div className="container py-5">
                    <div className="row" style={{ gap: '30px 0' }}>
                        <div className="col-lg">
                            <div className=" h1 fw-bolder mb-4">MISSION</div>

                            <div className=" fw-normal" style={{ lineHeight: '1.5em' }}>
                                OUR MISSION IS TO ENHANCE YOUR PROJECT VALUE AESTHETICALLY AND FUNCTIONALLY, WITH OUR IDEAS THROUGH LIGHTING IN:
                                <br /> <br />
                                1. CREATIVE LIGHTING DESIGN THINKING <br />
                                2. OPTIMISE LIGHTING APPLICATIONS <br />
                                3. MINIMISED TIME AND COST
                            </div>
                        </div>
                        <div className="col-lg">
                            <div className=" h1 fw-bolder mb-4">MISSION</div>

                            <div className=" fw-normal" style={{ lineHeight: '1.5em' }}>
                                CONVEYING LIGHTING DESIGNS AND STRATEGIES TO CREATE POSITIVE IMPACT FOR THE PEOPLE AND ENVIRONMENT. WE ARE A TRUSTED LIGHTING DESIGN COMPANY WHO ALWAYS DELIVERS WHAT WE IMAGINE INTO REALITY.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Element