import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import clients from "../../../static/content/clients"

const Element = () => {
    return (
        <div>
            <div className="bg-white" style={{ overflowX: 'hidden' }}>
                <div className="row " style={{ gap: '10px 0' }}>
                    <div className="col-lg my-auto">
                        <div className="container py-5">
                            <div className="d-flex">
                                <div className="m-auto" style={{ maxWidth: '700px' }}>
                                    <div className="display-3 fw-bolder mb-5">
                                        CLIENTS
                                    </div>

                                    <div className="h4 fw-normal mb-5" style={{ lineHeight: '1.5em' }}>
                                        WE BUILD OUR REPUTATION BY WORKING WITH SERVERAL COMPANY AROUND COUNTRY
                                    </div>

                                    <div className="d-flex flex-wrap justify-content-center" style={{ gap: '10px', flexFlow: '4' }}>
                                        {clients.map(item => (
                                            <img key={item.image} src={item.image} style={{ width: '100%', maxWidth: '130px' }} className="flex-fill" alt="convertible type" />
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 ">
                        <StaticImage src="../../../static/assets/image 462.jpg" className="w-100 h-100" alt="convertible type" />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Element