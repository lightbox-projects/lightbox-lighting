import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import Header from "../components/ProjectDetail/Header"
import Slides from "../components/ProjectDetail/Slides"

import dawta from "../../static/content/projects"

import { navigate } from "gatsby";

const DetailPage = ({ data, location, pageContext }) => {
  const siteTitle = 'Project'

  let current = dawta.filter(item => (item.slug === pageContext.id))
  if (current.length > 0) {
    current = current[0]
  } else {
    navigate('/404')
  }

  return (
    <Layout location={location} title={siteTitle} absoluteNav>

      <div className="position-relative">
        <Header projectData={current} />

        <div className=" w-100 bg-tertiary position-relative text-white" style={{ zIndex: '9', overflowX: 'hidden' }}>
          <div className="">
            <div className="py-2 bg-dark"></div>
            <div className="row" style={{ gap: '10px 0' }}>
              <div className="col-lg">
                <div className="p-5" dangerouslySetInnerHTML={{ __html: current.text }}></div>
              </div>
              <div className="col-lg bg-dark px-0">
                <Slides projectData={current} />
              </div>
            </div>


          </div>
        </div>
      </div>

    </Layout>
  )
}

export default DetailPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Project Detail" />

 
